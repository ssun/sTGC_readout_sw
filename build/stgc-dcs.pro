#
# project file for stgc-dcs
#
# ssun@cern.ch
# Jan 2018
#

#####################################################
# directory and machine specific items here
#####################################################
linebreak="---------------------------------------------------------------"

# for now, the user specifies where Boost is located
boostinclude=/opt/boost_1_60_0/include/
boostlib=/opt/boost_1_60_0/lib/

# do not touch these
sourcepath=../src
includepath=../include
imagepath=../images

message($$linebreak)
message("qmake sourcepath:      $$sourcepath")
message("qmake includepath:     $$includepath")
message("qmake boostinclude:    $$boostinclude")
message("qmake boostlib:        $$boostlib")
message("qmake imagepath:       $$imagepath")
message("ROOTSYS:               $(ROOTSYS)")
message($$linebreak)

#####################################################

QT      += core gui
QT      += network
QT      += widgets
QT      += xml
CONFIG  += console
CONFIG  +=declarative_debug
CONFIG  +=c++11

TARGET   = stgc-dcs
TEMPLATE = app

# make sure the boost version we point to is this one
DEFINES += BOOST_ALL_NO_LIB

linux {
	QMAKE_RPATHDIR += $$boostlib
	QMAKE_RPATHDIR += ./objects
}

INCLUDEPATH += $(ROOTSYS)/include
win32:LIBS += -L$(ROOTSYS)/lib -llibCint -llibRIO -llibNet \
       -llibHist -llibGraf -llibGraf3d -llibGpad -llibTree \
       -llibRint -llibPostscript -llibMatrix -llibPhysics \
       -llibGui -llibRGL -llibMathCore
else:LIBS += -L$(ROOTSYS)/lib -lCore -lCint -lRIO -lNet \
       -lHist -lGraf -lGraf3d -lGpad -lTree \
       -lRint -lPostscript -lMatrix -lPhysics \
       -lGui -lMathCore #-lRGL -lMathCore

LIBS +=  -L$$boostlib -lboost_thread-mt -lboost_filesystem-mt  -lboost_system-mt -lboost_chrono-mt -lboost_atomic-mt

LIBS += -L./objects

INCLUDEPATH += $$includepath
DEPENDPATH  += $$includepath
INCLUDEPATH += $$includepath/configuration/
DEPENDPATH  += $$includepath/configuration/
INCLUDEPATH += $$boostinclude
DEPENDPATH  += $$boostinclude

OBJECTS_DIR += ./objects/
MOC_DIR     += ./moc/
RCC_DIR     += ./rcc/
UI_DIR      += ./ui/

SOURCES += $$sourcepath/main.cpp\
           $$sourcepath/mainwindow.cpp\
           ## config handler
           $$sourcepath/config_handler_vmm.cpp\
           $$sourcepath/config_handler_tds.cpp\
           $$sourcepath/config_handler_roc_fpga.cpp\
           $$sourcepath/config_handler_roc_asic.cpp\
           $$sourcepath/config_handler_board.cpp\
           $$sourcepath/config_handler_miniDAQ.cpp\
           ## configuration
           $$sourcepath/configuration/comm_info.cpp\
           $$sourcepath/configuration/vmm_global_setting.cpp\
           $$sourcepath/configuration/vmm_calib_setting.cpp\
           $$sourcepath/configuration/vmm_channel.cpp\
           $$sourcepath/configuration/tds_global_setting.cpp\
           $$sourcepath/configuration/tds_channel.cpp\
           $$sourcepath/configuration/tds_lut.cpp\
           $$sourcepath/configuration/roc_fpga_setting.cpp\
           $$sourcepath/configuration/roc_asic_setting.cpp\
           $$sourcepath/configuration/roc_asic_analog_epll.cpp\
           $$sourcepath/configuration/board_setting.cpp\
           $$sourcepath/configuration/miniDAQ_setting.cpp\
           ## modules
           $$sourcepath/config_module_base.cpp\
           $$sourcepath/config_module_vmm.cpp\
           $$sourcepath/config_module_tds.cpp\
           $$sourcepath/config_module_board.cpp\
           $$sourcepath/config_module_roc_fpga.cpp\
           $$sourcepath/config_module_roc_asic.cpp\
           $$sourcepath/calib_module_vmm.cpp\
           ## sockets
           $$sourcepath/socket_handler.cpp\
           $$sourcepath/stgcsocket.cpp\
           ## other tools
           $$sourcepath/message_handler.cpp\
           $$sourcepath/daq_server.cpp\
           $$sourcepath/data_handler.cpp\
           ## daq buffer
           $$sourcepath/daq_buffer.cpp\
           ## data decoder
           $$sourcepath/vmm_decoder.cpp

HEADERS  += $$includepath/mainwindow.h\
            ## configuration
            $$includepath/configuration/comm_info.h\
            $$includepath/configuration/vmm_global_setting.h\
            $$includepath/configuration/vmm_calib_setting.h\
            $$includepath/configuration/vmm_channel.h\
            $$includepath/configuration/tds_global_setting.h\
            $$includepath/configuration/tds_channel.h\
            $$includepath/configuration/tds_lut.h\
            $$includepath/configuration/roc_fpga_setting.h\
            $$includepath/configuration/roc_asic_setting.h\
            $$includepath/configuration/roc_asic_analog_epll.h\
            $$includepath/configuration/board_setting.h\
            $$includepath/configuration/miniDAQ_setting.h\
            ## config handlers
            $$includepath/config_handler_vmm.h\
            $$includepath/config_handler_tds.h\
            $$includepath/config_handler_roc_fpga.h\
            $$includepath/config_handler_roc_asic.h\
            $$includepath/config_handler_board.h\
            $$includepath/config_handler_miniDAQ.h\
            ## modules
            $$includepath/config_module_base.h\
            $$includepath/config_module_vmm.h\
            $$includepath/config_module_tds.h\
            $$includepath/config_module_roc_fpga.h\
            $$includepath/config_module_roc_asic.h\
            $$includepath/config_module_board.h\
            $$includepath/calib_module_vmm.h\
            ## sockets
            $$includepath/socket_handler.h\
            $$includepath/stgcsocket.h\
            ## other tools
            $$includepath/message_handler.h\
            $$includepath/data_handler.h\
            $$includepath/daq_server.h\
            ## daq buffer
            $$includepath/readerwriterqueue.h\
            $$includepath/atomicops.h\
            $$includepath/daq_buffer.h\
            $$includepath/data_array_types.h\
            # data decoder
            $$includepath/vmm_decoder.h

FORMS    += $$sourcepath/mainwindow.ui

RESOURCES += \
    $$imagepath/icons.qrc
