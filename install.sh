#!/usr/bin/env bash

is_mac_=${1}

workdir=${PWD}

cd include
#git clone -b master https://github.com/dantrim/boost_includes.git boost

#rootcint -f aDict.cxx -c LinkDef.h


#mkdir ../build/objects

libdir=${workdir}/build/objects
mkdir -p ${libdir}
echo "Creating directory: ${libdir}"
echo "Executing: mkdir -p ${libdir}"
#g++ -o ${libdir}/libMylib.so aDict.cxx `root-config --cflags --libs` -shared -fPIC
#g++ -o ../build/objects/libMylib.so aDict.cxx `root-config --cflags --libs` -shared -fPIC
#g++ -o ${libdir}/libMylib.so aDict.cxx `root-config --cflags --libs` -shared -fPIC 

cd ../build
#ln -s objects/libMylib.so .

if [ "${is_mac_}" == "--mac" ]
then
    qmake -spec macx-g++ -o Makefile stgc-dcs.pro
    ln -s ${workdir}/build/stgc-dcs.app/Contents/MacOS/stgc-dcs stgc-dcs
else
    qmake -o Makefile stgc-dcs.pro
fi
