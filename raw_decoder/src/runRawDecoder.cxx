#include "TChain.h"
#include "TTree.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TPostScript.h"
#include "TH1F.h"
#include "TInterpreter.h"

#include "include/monitoring.h"


//#include "boost/algorithm/string.hpp"

#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

using namespace std;

void help() {
  std::cout << "--inRaw: OUTDATED input raw datafile ( .txt file of the recorded data ) " << std::endl;
  std::cout << "--inBinary: input binary datafile ( .bin file of the recorded data ) " << std::endl;
  std::cout << "--outDir: output directory" << std::endl;
  std::cout << "--run: run number" << std::endl;
  return;

}

int main( int argc, char **argv) {

  std::string inputRaw = "";
  std::string inputBinary = "";
  std::string inputRoot = "";
  std::string outputDir = "./decoded";
  int         runNumber;
  int         nEvent=0;
  bool 	      verbose = true;


  stringstream m_sx;

  for ( int i=1; i < argc; i++ ) {
    if      ( strcmp(argv[i], "--inRaw" )  == 0 ) inputRaw  =  argv[++i];
    else if      ( strcmp(argv[i], "--inBinary" )  == 0 ) inputBinary  =  argv[++i];
    else if ( strcmp(argv[i], "--inRoot" ) == 0 ) inputRoot =  argv[++i];
    //    else if ( strcmp(argv[i], "--run" )    == 0 ) runNumber = (argv[++i]).atoi();
    else if ( strcmp(argv[i], "--outDir" ) == 0 ) outputDir =  argv[++i];
    else if ( strcmp(argv[i], "--nEvt"   ) == 0 ) nEvent = atoi(argv[++i]);
    else {
      help();
      return 0;
    }
  }

  std::cout << "running decoder" << std::endl;

  monitoring * m_monitor = new monitoring();

  m_sx.str("");
  m_sx << outputDir << "/outDecoded.root";
  if(inputRaw != ""){
    m_sx.str("");
    m_sx << inputRaw.substr(0, inputRaw.size()-8) << "_decoded.root";
  }
  if(inputBinary != ""){
    m_sx.str("");
    m_sx << inputBinary.substr(0, inputBinary.size()-8);
    if ( nEvent > 0 ) m_sx << "_nEvt_" << nEvent << "_decoded.root";
    else              m_sx << "_decoded.root";
  }
  m_monitor->setFilenameOut(m_sx.str());

  m_monitor->setNMaxEvents( nEvent );
  m_monitor->setInputRoot(   inputRoot);
  m_monitor->setInputBinary( inputBinary );
  m_monitor->setInputRaw(    inputRaw);

  m_sx.str("");
  m_sx << outputDir << "/outHist.eps";
  m_monitor->setOutPs(m_sx.str());
  
  m_monitor->InitOutput();
  m_monitor->InitHists();

  if ( inputRaw != "" ) m_monitor->RunInputRaw();
  if ( inputBinary != "" ) m_monitor->RunInputBinary();

  m_monitor->Draw_and_Save();
  m_monitor->End();

  return 0;

}
