#include "hit.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

hit::hit() :
  EVID(-1),
  ok(false)
{
  clear();
}

void hit::clear()
{
  EVID = -1;
  udp_id  =0;
  bcid    =0;
  bcid_rel=0;
  elink_id=0;
  vmm_id  =0;
  chan_id =0;
  tdo     =0;
  pdo     =0;
}
