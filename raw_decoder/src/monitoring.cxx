
#include "include/reply_type.h"
#include "include/monitoring.h"

#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

#include <stdio.h>
#include <iomanip>

using namespace std;

monitoring::monitoring() :
  filename_out(""),
  input_file_root(""),
  input_file_raw(""),
  nMaxEvents(0),
  out_ps("")
{
}

monitoring::~monitoring() {
  delete outfile;
  delete c1;
  delete ps;
  delete decoded_tree;
  delete decoded_event_tree;

  delete decoder;
}

void monitoring::InitOutput() {

  outfile = new TFile(filename_out.c_str(),"RECREATE");

  c1 = new TCanvas("c1","c1");
  ps = new TPostScript(out_ps.c_str(),112);

}

void monitoring::InitHists() {

  decoder = new VMMDecoder();

  for ( int i=0; i<8; i++ ){
    std::vector<std::vector<TH1F*>> h_vmm_pdo_tmp;
    //std::vector<std::vector<TH1F*>> h_vmm_tdo_tmp;
    std::vector<std::vector<TH1F*>> h_vmm_l1id_tmp;
    std::vector<std::vector<TH1F*>> h_vmm_diff_bcid_tmp;

    std::vector<TH1F*> h_hits_tmp;
    for ( int j=0; j<8; j++ ) {
      std::vector<TH1F*> h_chan_pdo_tmp;
      //std::vector<TH1F*> h_chan_tdo_tmp;
      std::vector<TH1F*> h_chan_l1id_tmp;
      std::vector<TH1F*> h_chan_diff_bcid_tmp;

      stringstream m_sx;
      m_sx << "elink_" << i << "_vmm_" << j << "_channel_hits";
      h_hits_tmp.push_back( new TH1F(m_sx.str().c_str(), m_sx.str().c_str(), 64,-0.5,63.5) );

      for ( int k=0; k<64; k++ ) {
	stringstream m_sx;
	m_sx.str("");
	m_sx << "elink_" << i << "_vmm_" << j << "_chan_" << k << "_pdo";
	
	h_chan_pdo_tmp.push_back( new TH1F(m_sx.str().c_str(), m_sx.str().c_str(),
					   1030, -0.5, 1029.5) );
	
	m_sx.str("");
	m_sx << "elink_" << i << "_vmm_" << j << "_chan_" << k << "_tdo";
	//h_chan_tdo_tmp.push_back( new TH1F(m_sx.str().c_str(), m_sx.str().c_str(),
	//					   260,-0.5,259.5) );

        m_sx.str("");
        m_sx << "elink_" << i << "_vmm_" << j << "_chan_" << k << "_l1id";
        h_chan_l1id_tmp.push_back( new TH1F(m_sx.str().c_str(), m_sx.str().c_str(),
					    400,-0,60000) );

        m_sx.str("");
        m_sx << "elink_" << i << "_vmm_" << j << "_chan_" << k << "_diff_bcid";
        h_chan_diff_bcid_tmp.push_back( new TH1F(m_sx.str().c_str(), m_sx.str().c_str(),
						 8193,-4096.5,4096.5) );
      }
      h_vmm_pdo_tmp.push_back(h_chan_pdo_tmp);
      //h_vmm_tdo_tmp.push_back(h_chan_tdo_tmp);
      h_vmm_l1id_tmp.push_back(h_chan_l1id_tmp);
      h_vmm_diff_bcid_tmp.push_back(h_chan_diff_bcid_tmp);

    }
    channel_pdo.push_back(h_vmm_pdo_tmp);
    //channel_tdo.push_back(h_vmm_tdo_tmp);
    channel_l1id.push_back(h_vmm_l1id_tmp);
    channel_diff_bcid.push_back(h_vmm_diff_bcid_tmp);
    channel_hits.push_back( h_hits_tmp );
  }

  decoded_tree = new TTree("decoded_tree","decoded_tree");
  setupOutputTree(decoded_tree);

  decoded_event_tree = new TTree("decoded_event_tree","decoded_event_tree");
  setupOutputEventTree(decoded_event_tree);


}

//--------------------------------------------------------------------//

void monitoring::RunInputRoot() {

  TChain* chain = new TChain(input_file_root.c_str());
  Long64_t nEntries = chain->GetEntries();
  chain->ls();
  
  gROOT->ProcessLine("#include <vector>");
  
  output_tree *tree = new output_tree(chain);
  
  tree->Loop();
  //  tree->WriteHistos();

}

//-------------------------------------------------------------------//

void monitoring::RunInputRaw() {

  string line;
  
  ievent = 0;

  ifstream myfile(input_file_raw.c_str());
  
  if ( myfile.is_open() ) {
    
    vector<uint32_t> datagram;
    
    while ( getline ( myfile,line ) ) {

      if ( line == "" ) return;
      
      std::string data_str =  line; //line.substr( 16 );
      
      bool verbose = true;

      if ( verbose ) std::cout << data_str << std::endl;
      
      datagram.resize(0);
      
      for ( int i=0; i<data_str.size(); i=i+8){
	std::string data_word_str = data_str.substr( i, 8 );
	
	TString str_tmp_hex (data_word_str);
	TString str_tmp_dec = TString::BaseConvert( str_tmp_hex, 16, 10);
	
	//	  int data_word_signed = std::atoi( data_word_str.c_str() );
	Long64_t data_word_signed = str_tmp_dec.Atoll();
	
	uint32_t data_word = (uint32_t) data_word_signed ;
	//	  if ( data_word_signed < 0 ) data_word += 0x80000000;
	
	//std::cout << data_word_str << " " << data_word << std::endl;
	
	datagram.push_back(data_word);
      }
      
      Decode_and_Fill(datagram);

    }

    // fill last event
    if ( m_event.nVMM_hits > 0 ) {
      fillEvent();
    }


  }

}

//-----------------------------------------------------------------------------//

void monitoring::RunInputBinary() {

  stringstream m_sx;
  m_sx << std::internal << std::setfill('0');

  string line;

  uint32_t buffer_uint;

  vector<uint32_t> datagram;
  datagram.resize(0);

  ievent = 0;

  FILE *ptr_myfile=fopen(input_file_binary.c_str(),"rb");
  if (!ptr_myfile) {
    printf("Unable to open file!");
    return;
  }

  while ( fread(&buffer_uint,sizeof(buffer_uint),1,ptr_myfile) && ( nMaxEvents == 0 || ievent < nMaxEvents) )  {
    //m_sx.str("");
    //m_sx << std::hex << std::setw(8) << buffer_uint ;
    //std::cout << m_sx.str() << std::endl;

    datagram.push_back( buffer_uint );
    if ( buffer_uint == 0xffffffff ) {
      Decode_and_Fill(datagram);      
      datagram.resize(0);             
    }

    //ievent++;      

  }
  fclose(ptr_myfile);

}

//---------------------------------------------------------------------------//

void monitoring::Decode_and_Fill( std::vector<uint32_t> datagram ) {
  
  uint32_t udp_id;
  int message_type;
  std::string out_str;
  bool output_decoded = false;
  

  bool verbose = true;

  bool reply_normal = decoder->decode_reply(datagram, udp_id, message_type,
					    &out_str, output_decoded);
  
  if ( message_type == raw_data ) {
    decoder->decode_vmm_raw_data(datagram);

    if ( verbose ) std::cout << "UPD_ID " << udp_id ;
    if ( verbose ) std::cout << " EVID " << decoder->raw_l1id() ;
    if ( verbose ) std::cout << " elink ID " << decoder->raw_elink();
    if ( verbose ) std::cout << " vmm ID " << decoder->raw_vmm_ID() ;
    if ( verbose ) std::cout << " chan ID " << decoder->raw_chan_ID() ;
    if ( verbose ) std::cout << " pdo " << decoder->raw_pdo() ;
    if ( verbose ) std::cout << " tdo " << decoder->raw_tdo() ;
    if ( verbose ) std::cout << " bcid " << decoder->raw_bcid() ;
    if ( verbose ) std::cout << " rel_bcid " << decoder->raw_rel_bcid() << std::endl;

    m_ip = "";
    
    m_bcid = decoder->raw_bcid();
    m_l1_id = decoder->raw_l1id();
    
    m_bcid_rel = decoder->raw_rel_bcid();
    m_vmm_id   = decoder->raw_vmm_ID();
    m_chan     = decoder->raw_chan_ID();
    m_pdo      = decoder->raw_pdo();
    m_tdo      = decoder->raw_tdo();
    
    m_elink_id = decoder->raw_elink();
    
    m_flag     = 0;

    //-----------------------------------------------------------//
    //                 Build Event
    //-----------------------------------------------------------//

    m_hit.udp_id   = udp_id;
    m_hit.bcid     = m_bcid;
    m_hit.bcid_rel = m_bcid_rel;
    m_hit.elink_id = m_elink_id;
    m_hit.vmm_id   = m_vmm_id;
    m_hit.chan_id  = m_chan;
    m_hit.tdo      = m_tdo;
    m_hit.pdo      = m_pdo;

    if ( m_l1_id != m_event.EVID ) {
      if ( m_event.nVMM_hits > 0 ) {
        fillEvent();
      }
      m_event.clear();
      m_event.EVID = m_l1_id;
    }

    m_event.addHit( m_hit );

    //----------------------------------------------------------//

    //	  std::cout << m_l1_id << std::endl;
    
    decoded_tree->Fill();
    channel_hits.at(m_elink_id).at(m_vmm_id)->Fill( m_chan );
    channel_pdo.at(m_elink_id).at(m_vmm_id).at(m_chan)->Fill( m_pdo );
    //channel_tdo.at(m_elink_id).at(m_vmm_id).at(m_chan)->Fill( m_tdo );	
    channel_l1id.at(m_elink_id).at(m_vmm_id).at(m_chan)->Fill( m_l1_id );
    
    //--------------------------------------------------//
    
    bool new_active_chan   = true;
    
    for ( int i=0; i<active_chans.size(); i++ ){
      
      vector<int> active_chan = active_chans.at(i);
      
      if ( active_chan.at(0) == m_elink_id &&
	   active_chan.at(1) == m_vmm_id   &&
	   active_chan.at(2) == m_chan  ) {
	new_active_chan = false;
      }
      
    }
    
    if ( new_active_chan ) {
      vector<int> tmp = {m_elink_id, m_vmm_id, m_chan};
      active_chans.push_back( tmp );
    }
    
    //---------------------------------------------------//
    
    bool new_active_vmm   = true;
    
    for ( int i=0; i<active_vmms.size(); i++ ){
      
      vector<int> active_vmm = active_vmms.at(i);
      
      if ( active_vmm.at(0) == m_elink_id &&
	   active_vmm.at(1) == m_vmm_id   ) {
	new_active_vmm = false;
      }
      
    }
    
    if ( new_active_vmm ) {
      vector<int> tmp = {m_elink_id, m_vmm_id};
      active_vmms.push_back( tmp );
    }
    
    //---------------------------------------------------//
 
  }
  
}

void monitoring::Draw_and_Save() {

  for ( int ilink = 0; ilink < 8; ilink++ ) {
    for ( int ivmm = 0; ivmm < 8; ivmm++ ) {
      for ( int i=0; i<active_vmms.size(); i++ ){

	if ( ilink == active_vmms.at(i).at(0) &&
	     ivmm  == active_vmms.at(i).at(1) ) {
	  channel_hits
	    .at( active_vmms.at(i).at(0) )
	    .at( active_vmms.at(i).at(1) )->Draw();
	  c1->Update(); ps->NewPage();
	  
	  channel_hits
	    .at( active_vmms.at(i).at(0) )
	    .at( active_vmms.at(i).at(1) )->Write();
	}
      }
    }
  }

  for ( int ilink = 0; ilink < 8; ilink++ ) {
    for ( int ivmm = 0; ivmm < 8; ivmm++ ) {
      for ( int ichan = 0; ichan < 64; ichan++ ) {
	for ( int i=0; i<active_chans.size(); i++ ){
	  
	  if ( ilink == active_chans.at(i).at(0) &&
	       ivmm  == active_chans.at(i).at(1) &&
	       ichan == active_chans.at(i).at(2) ) {
	    channel_pdo
	      .at( active_chans.at(i).at(0) )
	      .at( active_chans.at(i).at(1) )
	      .at( active_chans.at(i).at(2) )->Draw();
	    c1->Update(); ps->NewPage();
	    
	    /*
	    channel_tdo
	      .at( active_chans.at(i).at(0) )
	      .at( active_chans.at(i).at(1) )
	      .at( active_chans.at(i).at(2) )->Draw();
	    c1->Update(); ps->NewPage();
	    */

	    channel_l1id
	      .at( active_chans.at(i).at(0) )
	      .at( active_chans.at(i).at(1) )
	      .at( active_chans.at(i).at(2) )->Draw();
	    c1->Update(); ps->NewPage();
	  
	    channel_diff_bcid
              .at( active_chans.at(i).at(0) )
              .at( active_chans.at(i).at(1) )
              .at( active_chans.at(i).at(2) )->Draw();
            c1->Update(); ps->NewPage();

	    channel_pdo
	      .at( active_chans.at(i).at(0) )
	      .at( active_chans.at(i).at(1) )
	      .at( active_chans.at(i).at(2) )->Write();
	    
	    /*
	    channel_tdo
	      .at( active_chans.at(i).at(0) )
	      .at( active_chans.at(i).at(1) )
	      .at( active_chans.at(i).at(2) )->Write();
	    */

	    channel_l1id
	      .at( active_chans.at(i).at(0) )
	      .at( active_chans.at(i).at(1) )
	      .at( active_chans.at(i).at(2) )->Write();

            channel_diff_bcid
              .at( active_chans.at(i).at(0) )
              .at( active_chans.at(i).at(1) )
              .at( active_chans.at(i).at(2) )->Write();

	  }
	}
      }
    }
  }
}

void monitoring::End() {

  ps->Close();
  decoded_event_tree->Write();
  decoded_tree->Write();
  outfile->Close();

}

void monitoring::fillEvent() {

  ievent++;

  //  std::cout << "filling event" << m_event.tdo->size() << std::endl;

  m_vec_tdo      = m_event.tdo;
  m_vec_pdo      = m_event.pdo;
  m_vec_chan     = m_event.chan_id;
  m_vec_vmm_id   = m_event.vmm_id;
  m_vec_bcid_rel = m_event.bcid_rel;
  m_vec_bcid     = m_event.bcid;
  m_vec_elink_id = m_event.elink_id;

  m_evid    = m_event.EVID;
  m_nhits   = m_event.nVMM_hits;
  
  decoded_event_tree->Fill();

  //------------------------------------------------------------------------------//
  //                            Find Trigger BCID
  //------------------------------------------------------------------------------//

  uint32_t trigger_hit_pdo = 0;
  int trigger_bcid = 9999;

  for ( int i=0; i<m_vec_bcid->size(); i++ ) {
    
    //    if ( ( m_vec_elink_id->at(i) == 0 && m_vec_vmm_id->at(i) == 1 && m_vec_chan->at(i) == 32 ) || 
    if ( ( m_vec_elink_id->at(i) == 0 && m_vec_vmm_id->at(i) == 2 && m_vec_chan->at(i) == 36 ) ) {    
      if ( m_vec_pdo->at(i) > trigger_hit_pdo ) {
	trigger_hit_pdo = m_vec_pdo->at(i);
	trigger_bcid = m_vec_bcid->at(i);
      }
    }

  }

  //std::cout << "trigger elink 0 vmm 1 chan 32: bcid " << trigger_bcid << " pdo " << trigger_hit_pdo << std::endl;

  for ( int i=0; i<m_vec_bcid->size(); i++ ) {
    if ( trigger_bcid < 4096 ) {
      int hit_bcid = m_vec_bcid->at(i);

      int hit_diff_bcid = hit_bcid - trigger_bcid;
      if ( hit_diff_bcid < 0 ) hit_diff_bcid = hit_diff_bcid + 4095;

      channel_diff_bcid.at(m_vec_elink_id->at(i))
	.at(m_vec_vmm_id->at(i))
	.at(m_vec_chan->at(i))->Fill( hit_diff_bcid );

      if ( m_vec_elink_id->at(i) == 0 &&
	   m_vec_vmm_id->at(i)   == 2 &&
	   m_vec_chan->at(i)     == 16 ) {
	//std::cout << "trigger difference for elink 0 vmm 2 chan 16 " << hit_diff_bcid << std::endl;
      }

    }
  }

}

void monitoring::setupOutputTree( TTree* tree ) {

  m_ip       = "";
  m_tdo      = 0;
  m_pdo      = 0;
  m_chan     = 0;
  m_vmm_id   = 0;
  m_bcid_rel = 0;
  m_flag     = 0;
  m_board_id = 0;
  m_bcid     = 0;
  m_l1_id    = 0;
  m_elink_id = 0;

  
  tree->Branch("m_ip", &m_ip);
  tree->Branch("m_tdo", &m_tdo);
  tree->Branch("m_pdo", &m_pdo);
  tree->Branch("m_chan", &m_chan);
  tree->Branch("m_vmm_id", &m_vmm_id);
  tree->Branch("m_board_id", &m_board_id);
  tree->Branch("m_elink_id", &m_elink_id);
  tree->Branch("m_bcid_rel", &m_bcid_rel);
  tree->Branch("m_flag", &m_flag);
  tree->Branch("m_bcid", &m_bcid);
  tree->Branch("m_l1_id", &m_l1_id);
 
}

void monitoring::setupOutputEventTree( TTree* tree ) {

  m_vec_tdo      = 0;
  m_vec_pdo      = 0;
  m_vec_chan     = 0;
  m_vec_vmm_id   = 0;
  m_vec_bcid_rel = 0;
  m_vec_bcid     = 0;
  m_vec_elink_id = 0;

  m_evid    = 0;
  m_nhits   = 0;

  tree->Branch("m_tdo",      &m_vec_tdo);
  tree->Branch("m_pdo",      &m_vec_pdo);
  tree->Branch("m_chan",     &m_vec_chan);
  tree->Branch("m_vmm_id",   &m_vec_vmm_id);
  tree->Branch("m_elink_id", &m_vec_elink_id);
  tree->Branch("m_bcid_rel", &m_vec_bcid_rel);
  tree->Branch("m_bcid",     &m_vec_bcid);

  tree->Branch("m_evid",     &m_evid);
  tree->Branch("m_nhits",    &m_nhits);

}

