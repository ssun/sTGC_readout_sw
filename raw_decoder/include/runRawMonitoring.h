


TString         m_ip;
int             m_tdo;
int             m_pdo;
int             m_chan;
int             m_vmm_id;
int             m_bcid_rel;
int             m_flag;
int             m_board_id;
int             m_bcid;
int             m_l1_id;
int             m_elink_id;

TBranch        *b_m_ip;   //!               
TBranch        *b_m_tdo;   //!              
TBranch        *b_m_pdo;   //!              
TBranch        *b_m_chan;   //!             
TBranch        *b_m_vmm_id;   //!           
TBranch        *b_m_elink_id;
TBranch        *b_m_bcid_rel;   //!         
TBranch        *b_m_flag;   //!             
TBranch        *b_m_board_id;   //!         
TBranch        *b_m_bcid;   //!             
TBranch        *b_m_l1_id;   //!   

void setupOutputTree( TTree* tree );
