#ifndef VMM_DECODER_H
#define VMM_DECODER_H

//////////////////////////////////////////////////////////
//
// vmm_decoder
//
// tool to decode the packets from the VMM for sTGC
//
// ssun@cern.ch
// July 2017
//
//////////////////////////////////////////////////////////

// std/stl
#include <vector>
#include <string>
#include <cstdint>

#include <stdint.h>
#include <TMath.h>

class VMMDecoder
{
    public :
        VMMDecoder();
        virtual ~VMMDecoder(){};

        void set_debug(bool doit) { m_dbg = doit; }
        void clear();

	bool decode_reply(std::vector<uint32_t> datagram, uint32_t &udp_id,
			  int &message_type, std::string *out_str, bool output_decoded=false);

	bool decode_query_elink     ( uint32_t d0 );
        bool decode_query_EVID      ( uint32_t d0 );
        bool decode_query_TTC_status( uint32_t d0 );
	void decode_current_TTC_setting(std::vector<uint32_t> datagram );
	

	bool decode_vmm_event_data( uint32_t d0 );
	bool decode_vmm_raw_data(std::vector<uint32_t> datagram);

	void outputDatagram(std::vector<uint32_t> datagram);
        void outputDatagram(uint32_t datagram);

        ///////////////////////////////////////////////
        // grab VMM event data
        ///////////////////////////////////////////////
        std::vector<uint32_t> pdo()      { return m_pdo; }
        std::vector<uint32_t> tdo()      { return m_tdo; }
	std::vector<uint32_t> chan()     { return m_chan; }
	std::vector<uint32_t> vmm_id()   { return m_vmm_id; }
	std::vector<uint32_t> bcid_rel() { return m_bcid_rel; }
        std::vector<uint32_t> flag()     { return m_flag; }

	std::vector<uint32_t> board_id() { return m_board_id; }
        std::vector<uint32_t> bcid()     { return m_bcid; }
	std::vector<uint32_t> l1_id()    { return m_l1_id; }

	std::vector<bool>  locked_elink() { return m_locked_elink; }
	uint32_t          current_EVID() { return m_current_EVID; }
	uint32_t          current_TTC_status() { return m_current_TTC_status; }

	uint32_t raw_bcid() { return m_raw_bcid; }
	uint32_t raw_l1id() { return m_raw_l1id; }

	uint32_t raw_rel_bcid() { return m_raw_rel_bcid; }
	uint32_t raw_vmm_ID()   { return m_raw_vmm_ID; }
	uint32_t raw_chan_ID()  { return m_raw_chan_ID; }
	uint32_t raw_pdo()      { return m_raw_pdo; }
	uint32_t raw_tdo()      { return m_raw_tdo; }

	uint32_t raw_elink()    { return m_raw_elink; }

	std::vector<uint32_t> current_TTC_reg_address() { 
	  //	  if ( m_TTC_reg_address.size() != 0 ) {
	    return m_TTC_reg_address; 
	    ///	  }
	    //	  else {
	    ///	    std::vector<uint32_t> blank = {0};
	    //	    blank.resize(0);
	    //	    return blank;
	    //	  }
	}
	std::vector<uint32_t> current_TTC_reg_value()   { 
	  //	  if ( m_TTC_reg_value.size() != 0 ) {
	    return m_TTC_reg_value; 
	    //	  }
	    //	  else {
	    //	    std::vector<uint32_t> blank = {0};
	    //	    blank.resize(0);
	    //	    return blank;
	    //	  }
	}

	uint32_t reverse_bits( uint32_t orig, int size);
	//uint32_t grayToBinary(uint32_t gray_num);
	unsigned int grayToBinary(unsigned int num);

    private :
        bool m_dbg;


        //-------------------------------------------------------------------//     
        //  /header (32 bits)/Unique_ID (16 bits)/Command ( 16 bits )/ Data  //     
        //-------------------------------------------------------------------//     
        static const uint32_t default_header = 0xdecafbad; // 32 bits                

        // pre-defined commands on firmware (16 bits )                              
        static const uint32_t cmd_SCA_init    = 0x0100;
        static const uint32_t cmd_ASIC_config = 0x0200;
        static const uint32_t cmd_ASIC_reset  = 0x0300;
        static const uint32_t cmd_TTC_config  = 0x0400;

        //-------------------------------------------------------------------//     
        //          hexdecimal of different headers+IDs for replies                 
        //-------------------------------------------------------------------//     
        //   /header (16 bits)/UDP_ID (16 bits)/Module ID (8 bits)                  
        //           /Message Type (8 bits)/0x0000 (16 bit place holder)            
        //           /0x0 (1 bit place holder) + Data (31 bits)  
	//                (perhaps multiple 32 bit place holder+data packets)                                                   
	//           /0xFFFFFFFF (32 bit trailer)                                   
	//-------------------------------------------------------------------//     
        static const uint32_t rep_header = 0xface; // 16 bits                        
	
	// module ID that sent reply message ( 8 bits )                             
	static const uint32_t rep_modID_ethernet_rec   = 0x01; // ethernet receiver
	static const uint32_t rep_modID_ethernet_trans = 0x02; // ethernet transmitter
	static const uint32_t rep_modID_SCA            = 0x03; // SCA
	static const uint32_t rep_modID_TTC            = 0x04; // TTC
	static const uint32_t rep_modID_VMMout         = 0x05; // VMM readout
	
	// Message type of reply message ( 8 bits )                                 
	static const uint32_t rep_type_echo            = 0xEC;
	static const uint32_t rep_type_err             = 0xEE;
	static const uint32_t rep_type_status          = 0x01;
	static const uint32_t rep_type_data            = 0x02;
	
	static const uint32_t Elink_status_hex  = 0xd0;
	static const uint32_t EVID_status_hex   = 0xde;
	static const uint32_t TTC_status_hex    = 0xdc;

        ///////////////////////////////////////////////
        // VMM event data
        ///////////////////////////////////////////////

	std::vector<uint32_t> m_TTC_reg_address;
	std::vector<uint32_t> m_TTC_reg_value;

	std::vector<uint32_t> m_tdo;
	std::vector<uint32_t> m_pdo;
	std::vector<uint32_t> m_chan;
	std::vector<uint32_t> m_vmm_id;
	std::vector<uint32_t> m_bcid_rel;
	std::vector<uint32_t> m_flag;

	std::vector<uint32_t> m_board_id;
	std::vector<uint32_t> m_bcid;
	std::vector<uint32_t> m_l1_id;

	std::vector<bool>     m_locked_elink;
	uint32_t m_current_EVID;
	uint32_t m_current_TTC_status;

	uint32_t m_raw_bcid;
	uint32_t m_raw_l1id;

	uint32_t m_raw_rel_bcid;
	uint32_t m_raw_vmm_ID;
	uint32_t m_raw_chan_ID;
	uint32_t m_raw_pdo;
	uint32_t m_raw_tdo;

	uint32_t m_raw_elink;

}; // class VMMDecoder

#endif
