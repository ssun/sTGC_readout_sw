#ifndef CONFIG_MODULE_ROC_FPGA_H
#define CONFIG_MODULE_ROC_FPGA_H

/////////////////////////////////////////
//
// configuration_module
//
// Tool for building and sending the
// configuration packets to the front-end/
// VMMs
//
//  - sends the global SPI and configuration
//    of all the VMM channels
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include "config_module_base.h"
#include "config_handler_roc_fpga.h"
#include "socket_handler.h"
#include "message_handler.h"

// Qt
#include <QString>


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_TDS
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////

class Config_RocFPGA : public Config_Base
{

    public :
        explicit Config_RocFPGA(Config_Base *parent = 0);
        virtual ~Config_RocFPGA(){};

        Config_RocFPGA& LoadConfig(ConfigHandlerRocFPGA& config);

        bool SendRocFPGAConfig(int send_to_port, const QHostAddress & target_ip);

	void fillRocFPGAGlobalRegisters(std::vector<QString>& globalRegisters);

        ConfigHandlerRocFPGA& config() { return *m_configHandler; }

 private :

	ConfigHandlerRocFPGA *m_configHandler;
	
 signals :

	public slots :
       
}; // class Config_RocFPGA



#endif
