#ifndef CONFIG_HANDLER_MINIDAQ_H
#define CONFIG_HANDLER_MINIDAQ_H

/////////////////////////////////////////
//
// config_handler
//
// containers for configuration of KC705 miniDAQ
//
// Siyuan.Sun@cern.ch
// August 2017
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

// vmm
#include "message_handler.h"

#include "comm_info.h"
#include "miniDAQ_setting.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerMiniDAQ : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerMiniDAQ(QObject *parent = 0);
        virtual ~ConfigHandlerMiniDAQ(){};

        ConfigHandlerMiniDAQ& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

	bool All_TTC_Locked() {     
	  std::cout << m_miniDAQ.elink_locked_mask << " " << ~( m_miniDAQ.TTC_locked_mask ) << std::endl;
	  return ( (  m_miniDAQ.elink_locked_mask     & 0b11111111 ) &
		   ~( m_miniDAQ.TTC_locked_mask       & 0b11111111 ) ) == 0; }
	bool All_Boards_Configed() { return ( (  m_miniDAQ.elink_locked_mask     & 0b11111111 ) &
					      ~( m_miniDAQ.configured_board_mask & 0b11111111 ) ) == 0; }
	
	int nElinks();

	bool LoadMiniDAQConfig_from_File(const QString &filename);
	void WriteMiniDAQConfig_to_File(QString filename);

        CommInfo LoadCommInfo(const boost::property_tree::ptree& p);
        void LoadCommInfo(const CommInfo& info);

	MiniDAQ_Setting LoadMiniDAQSettings(const boost::property_tree::ptree& p);

        // methods for GUI interaction
	void LoadMiniDAQConfiguration( MiniDAQ_Setting &miniDAQ );

        // retrieve the objects
        CommInfo& commSettings()                { return m_commSettings; }
	MiniDAQ_Setting& miniDAQ_Settings()     { return m_miniDAQ; }
	
    private :
        bool m_dbg;

        CommInfo                m_commSettings;
	MiniDAQ_Setting         m_miniDAQ;

        MessageHandler* m_msg;

}; // class ConfigHandlerMiniDAQ



#endif
