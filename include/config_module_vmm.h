#ifndef CONFIG_MODULE_VMM_H
#define CONFIG_MODULE_VMM_H

/////////////////////////////////////////
//
// configuration_module
//
// Tool for building and sending the
// configuration packets to the front-end/
// VMMs
//
//  - sends the global SPI and configuration
//    of all the VMM channels
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include "config_module_base.h"
#include "config_handler_vmm.h"
#include "socket_handler.h"
#include "message_handler.h"

// Qt
#include <QString>


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Configuration
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////

class Config_VMM : public Config_Base
{

    public :
        explicit Config_VMM(Config_Base *parent = 0);
        virtual ~Config_VMM(){};

        Config_VMM& LoadConfig(ConfigHandlerVMM& config);

        bool SendVMMConfig(int send_to_port, const QHostAddress & target_ip);

        void fillVMMGlobalRegisters(std::vector<QString>& globalRegisters);
        void fillVMMChannelRegisters(std::vector<QString>& channelRegisters);

        ConfigHandlerVMM& config() { return *m_configHandler; }

 private :

	ConfigHandlerVMM *m_configHandler;
	
 signals :

	public slots :
       
}; // class Config_VMM



#endif
