#ifndef CONFIG_HANDLER_VMM_H
#define CONFIG_HANDLER_VMM_H

/////////////////////////////////////////
//
// config_handler
//
// containers for global configuration
// (global, VMM channels, IP/socket information)
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

// vmm
#include "message_handler.h"

#include "vmm_global_setting.h"
#include "vmm_calib_setting.h"
#include "vmm_channel.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerVMM : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerVMM(QObject *parent = 0);
        virtual ~ConfigHandlerVMM(){};

        ConfigHandlerVMM& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

        bool LoadVMMConfig_from_File(const QString &filename);
        void WriteVMMConfig_to_File(QString filename);

        VMMGlobalSetting LoadVMMGlobalSettings(const boost::property_tree::ptree& p);
	
        std::vector<VMM_Channel> LoadVMMChannelConfig(const boost::property_tree::ptree& p);

        // methods for GUI interaction
        void LoadVMMChipConfiguration(VMMGlobalSetting& global,    std::vector<VMM_Channel>& channels);

        bool EnabledOrDisabled(int val, std::string& out, std::string reg_name, bool invert_logic);
        bool isEnabledOrDisabled(std::string reg, int& idx, std::string reg_name, bool invert_logic);
        int isOn(std::string onOrOff = "", std::string where ="");
        std::string isOnOrOff(int onOrOf, std::string node);
        std::string isEnaOrDis(int enaOrDis, std::string node);

        // retrieve the objects
        VMMGlobalSetting& VMMGlobalSettings()      { return m_vmm_globalSettings; }
	VMMCalibSettings& VMMCalibSettings()       { return m_vmm_calibSettings; }
        VMM_Channel& VMM_ChannelSettings(int i)    { return m_vmm_channels[i]; }

	QString VMMMask();

    private :
        bool m_dbg;

        VMMGlobalSetting         m_vmm_globalSettings;
	VMMCalibSettting         m_vmm_calibSettings;
        std::vector<VMM_Channel> m_vmm_channels;

        MessageHandler* m_msg;

}; // class ConfigHandler



#endif
