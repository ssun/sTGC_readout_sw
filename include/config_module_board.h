#ifndef CONFIG_MODULE_BOARD_H
#define CONFIG_MODULE_BOARD_H

/////////////////////////////////////////
//
// configuration_module
//
// Tool for building and sending the
// configuration packets to the front-end/
// VMMs
//
//  - sends the global SPI and configuration
//    of all the VMM channels
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include "config_module_base.h"
#include "config_handler_board.h"
#include "socket_handler.h"
#include "message_handler.h"

// Qt
#include <QString>


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_Board
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////

class Config_Board : public Config_Base
{
    public :
        explicit Config_Board(Config_Base *parent = 0);
        virtual ~Config_Board(){};

        Config_Board& LoadConfig(ConfigHandlerBoard& config);

	bool config_SCA(int send_to_port, const QHostAddress &target_ip, int board_type, int boardID);

	bool setELinkDelay0to3( int send_to_port, const QHostAddress &target_ip, std::vector<int> delay);
	bool setELinkDelay4to7( int send_to_port, const QHostAddress &target_ip, std::vector<int> delay);
	bool SetFinalELinkDelay(int send_to_port, const QHostAddress &target_ip, std::vector<int> ELink_BestLockLoc);

	std::vector<bool> QueryELink(int send_to_port, const QHostAddress &target_ip, int nLinks, bool also_decode = true);

	std::vector<uint32_t> current_TTC_reg_address();
	std::vector<uint32_t> current_TTC_reg_value();
	uint32_t TTC_firmware_version();

	uint32_t current_SCA_ID() { return m_current_SCA_ID; }

	int Query_EVID(int send_to_port, const QHostAddress &target_ip);
	int Query_TTC_Lock(int send_to_port, const QHostAddress &target_ip);

	bool     Set_SCA_GPIO( int send_to_port, const QHostAddress &target_ip, 
			       uint32_t boardType, uint32_t boardID, 
			       uint32_t dir_mask,  uint32_t dout_mask);
	std::vector<uint32_t> Query_SCA_ADC(int send_to_port, const QHostAddress &target_ip,
					    uint32_t boardType, uint32_t boardID, 
					    uint32_t chan_mux, uint32_t current);
	std::vector<uint32_t> Query_SCA_ADC_VMM( int send_to_port, const QHostAddress &target_ip,
						 uint32_t boardType, uint32_t boardID,
						 uint32_t ivmm,  uint32_t current);
	std::vector<uint32_t> Query_SCA_ADC_temp( int send_to_port, const QHostAddress &target_ip,
						  uint32_t boardType, uint32_t boardID, uint32_t current);

	bool eFIFO_disable(int send_to_port, const QHostAddress &target_ip, uint32_t elink_mask);

	bool GlobalCtr_KC705Reset(int send_to_port, const QHostAddress &target_ip);
	bool GlobalCtr_TrigMode(int send_to_port, const QHostAddress &target_ip, bool internal_trig);
	bool GlobalCtr_DAQ_Enabled(int send_to_port, const QHostAddress &target_ip, bool enabled);
	bool GlobalCtr_Readout_Enabled(int send_to_port, const QHostAddress &target_ip, bool enabled);
	bool GlobalCtr_TTC_StartBit(int send_to_port, const QHostAddress &target_ip, int bit);

	bool TTC_Ctr_Training(int send_to_port, const QHostAddress &target_ip, int elink_mask);
	bool FEB_Soft_Reset_GPIO_Low(int send_to_port, const QHostAddress &target_ip, int boardType, int boardID);
        bool FEB_Soft_Reset_GPIO_High(int send_to_port, const QHostAddress &target_ip, int boardType, int boardID);
	bool TTC_Ctr_Gate_nBC(int send_to_port, const QHostAddress &target_ip, int nBC);
	bool TTC_Ctr_TrigLatency(int send_to_port, const QHostAddress &target_ip, int latency);
	bool TTC_Ctr_SCA_Reset(int send_to_port, const QHostAddress &target_ip);
	bool TTC_Ctr_Evt_Reset(int send_to_port, const QHostAddress &target_ip);
	bool TTC_Ctr_BC_Reset(int send_to_port, const QHostAddress &target_ip);
	bool TTC_Ctr_Sync_Reset(int send_to_port, const QHostAddress &target_ip);
	bool TTC_Ctr_SCA_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);
        bool TTC_Ctr_Evt_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);
        bool TTC_Ctr_BC_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);

	bool TTC_Ctr_L0_Evt_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);
	bool TTC_Ctr_L0_Accept_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);
	bool TTC_Ctr_L1_Accept_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);
        bool TTC_Ctr_VMM_Soft_Reset(int send_to_port, const QHostAddress &target_ip, bool reset);
	bool TTC_Ctr_VMM_TP(int send_to_port, const QHostAddress &target_ip, bool reset);
	bool TTC_Ctr_TTC_pulse_lv(int send_to_port, const QHostAddress &target_ip, uint32_t val);

	bool TTC_Ctr_L0_Evt_Reset(int send_to_port, const QHostAddress &target_ip);
	bool TTC_Ctr_L0_Accept_Reset(int send_to_port, const QHostAddress &target_ip);
        bool TTC_Ctr_L1_Accept_Reset(int send_to_port, const QHostAddress &target_ip);
        bool TTC_Ctr_VMM_Soft_Reset(int send_to_port, const QHostAddress &target_ip);
        bool TTC_Ctr_VMM_TP(int send_to_port, const QHostAddress &target_ip);

	bool TTC_F1(int send_to_port, const QHostAddress &target_ip, bool onoff);
	bool TTC_F1(int send_to_port, const QHostAddress &target_ip);
	bool TTC_F2(int send_to_port, const QHostAddress &target_ip, bool onoff);
	bool TTC_F2(int send_to_port, const QHostAddress &target_ip);

	bool VMM_TestPulse_CKTK_nMax(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_Freq(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_CKTP_nMax(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_CKTP_Skew(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_CKTP_Period(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_CKTP_Width(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_CKBC_nMax(int send_to_port, const QHostAddress &target_ip, int val);
	bool VMM_TestPulse_CKTP_Spacing(int send_to_port, const QHostAddress &target_ip, int val);

        ConfigHandlerBoard& config() { return *m_configHandler; }

	QString reg_addr_Query_ELink     ; // ="000000D0";
	QString reg_addr_set_elink_0to3  ; // ="000000D1";
        QString reg_addr_set_elink_4to7  ; // ="000000D2";
	QString reg_addr_Query_TTC_Lock  ; // ="000000DC";
        QString reg_addr_Query_EVID      ; // ="000000DE";

	QString reg_addr_Global_Reset    ; // ="000000AF";
	QString reg_addr_Global_TrigMode ; // ="000000AB";
	QString reg_addr_Global_DaqEn    ; // ="0000000F";
	QString reg_addr_TTC_startBit    ; // ="000000E0";

        QString reg_addr_TTC_Training    ; // ="000000E1";
	QString reg_addr_Gate_nBC        ; // ="000000E2";
	QString reg_addr_TTC_TrigLatency ; // ="000000E3";

	QString reg_addr_SCA_reset       ; // ="000000E4";
        QString reg_addr_TTC_Evt_Reset   ; // ="000000E5";
        QString reg_addr_TTC_BC_Reset    ; // ="000000E6";
        QString reg_addr_TTC_Sync_Reset  ; // ="000000E7";   
        QString reg_addr_TP_CKTK_nMax    ; // ="000000C1";
        QString reg_addr_TP_Freq         ; // ="000000C2";
	QString reg_addr_TP_CKTP_nMax    ; // ="000000C3";
	QString reg_addr_TP_CKTP_Skew    ; // ="000000C4";
	QString reg_addr_TP_CKTP_Period  ; // ="000000C5";
	QString reg_addr_TP_CKTP_Width   ; // ="000000C6";
	QString reg_addr_TP_CKBC_nMax    ; // ="000000C7";
	QString reg_addr_TP_CKTP_Spacing ; // ="000000C8";

        QString reg_addr_Global_ReadEn   ; // ="000000CD";

 private :

	ConfigHandlerBoard *m_configHandler;
	uint32_t m_current_SCA_ID;

	//------------------------------------------------------//

	const static uint32_t m_ADC_ch_pFEB_v21_vmm0 = 4;
	const static uint32_t m_ADC_ch_pFEB_v21_vmm1 = 3;
	const static uint32_t m_ADC_ch_pFEB_v21_vmm2 = 5;

	const static uint32_t m_ADC_ch_pFEB_v21_FEAST_Temp0 = 2;
        const static uint32_t m_ADC_ch_pFEB_v21_FEAST_Temp1 = 9;
        const static uint32_t m_ADC_ch_pFEB_v21_FEAST_Temp2 = 6;

        const static uint32_t m_ADC_ch_pFEB_v21_Board_Temp0 = 24;
        const static uint32_t m_ADC_ch_pFEB_v21_SCA_Temp    = 31;

	//------------------------------------------------------//

        const static uint32_t m_ADC_ch_sFEB_v21_vmm0 = 0;
	const static uint32_t m_ADC_ch_sFEB_v21_vmm1 = 1;
	const static uint32_t m_ADC_ch_sFEB_v21_vmm2 = 2;
        const static uint32_t m_ADC_ch_sFEB_v21_vmm3 = 3;
	const static uint32_t m_ADC_ch_sFEB_v21_vmm4 = 4;
	const static uint32_t m_ADC_ch_sFEB_v21_vmm5 = 5;
        const static uint32_t m_ADC_ch_sFEB_v21_vmm6 = 6;
	const static uint32_t m_ADC_ch_sFEB_v21_vmm7 = 7;

	const static uint32_t m_ADC_ch_sFEB_v21_NTC_Temp1 = 10;
	const static uint32_t m_ADC_ch_sFEB_v21_NTC_Temp2 = 11;

	const static uint32_t m_ADC_ch_sFEB_v21_SCA_Temp  = 31;

	//------------------------------------------------------//

	const static uint32_t m_ADC_ch_pFEB_v22_vmm0 = 0;
	const static uint32_t m_ADC_ch_pFEB_v22_vmm1 = 1;
	const static uint32_t m_ADC_ch_pFEB_v22_vmm2 = 2;

        const static uint32_t m_ADC_ch_pFEB_v22_A1V2_FEAST_Temp  = 26;
        const static uint32_t m_ADC_ch_pFEB_v22_D1V2_FEAST_Temp1 = 27;
        const static uint32_t m_ADC_ch_pFEB_v22_D1V5_FEAST_Temp2 = 28;

        const static uint32_t m_ADC_ch_pFEB_v22_NTC_Temp    = 10;
        const static uint32_t m_ADC_ch_pFEB_v22_SCA_Temp    = 31;

	//------------------------------------------------------//

        const static uint32_t m_ADC_ch_sFEB_v22_vmm0 = 0;
	const static uint32_t m_ADC_ch_sFEB_v22_vmm1 = 1;
        const static uint32_t m_ADC_ch_sFEB_v22_vmm2 = 2;
        const static uint32_t m_ADC_ch_sFEB_v22_vmm3 = 3;
	const static uint32_t m_ADC_ch_sFEB_v22_vmm4 = 4;
        const static uint32_t m_ADC_ch_sFEB_v22_vmm5 = 5;
        const static uint32_t m_ADC_ch_sFEB_v22_vmm6 = 6;
	const static uint32_t m_ADC_ch_sFEB_v22_vmm7 = 7;

	const static uint32_t m_ADC_ch_sFEB_v22_NTC_Temp1 = 10;
        const static uint32_t m_ADC_ch_sFEB_v22_NTC_Temp2 = 11;

        const static uint32_t m_ADC_ch_sFEB_v22_SCA_Temp = 31;

	//------------------------------------------------------//

 signals :

	public slots :
       
}; // class Config_Board



#endif
