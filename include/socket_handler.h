#ifndef SOCKET_HANDLER_H
#define SOCKET_HANDLER_H

/////////////////////////////////////////
//
// socket_handler
//
// Tool for holding any of the sockets that
// communicate with the front-end. To ensure
// that UDP sockets do not conflict
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// Qt
#include <QObject>
#include <QUdpSocket>
#include <QStringList>

// stgc
#include "stgcsocket.h"
#include "message_handler.h"

// std/stl
#include <string>


/////////////////////////////////////////////////////////////////////////////
// ----------------------------------------------------------------------- //
//  SocketHandler
// ----------------------------------------------------------------------- //
/////////////////////////////////////////////////////////////////////////////

class SocketHandler : public QObject
{
    Q_OBJECT;

    public :
        explicit SocketHandler(QObject *parent = 0);
        virtual ~SocketHandler(){};
        SocketHandler& setDebug(bool dbg) { m_dbg = dbg; return *this; }
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

        // add sockets
        void addSocket(const QHostAddress &IPaddress,
		       std::string name = "",
		       quint16 bindingPort = 0,
		       QAbstractSocket::BindMode mode = QAbstractSocket::DefaultForPlatform);

        // send data
        bool SendDatagram(const QByteArray& datagram, const QHostAddress& ip,
			  const quint16& destinationPort,
			  const QString& whichSocket = "");

	//        virtual bool waitForReadyRead(std::string socketName="", int msec=200, quint32 cmd_delay=0);
	virtual bool waitForReadyRead(std::string socketName="", int msec=200);
        QByteArray processReply(std::string name, QHostAddress *target_ip,
				quint16 *target_port);
        void closeAndDisconnect(std::string name="", std::string callingFn="");

        // retrieve sockets
	bool stgcSocketOK();
	sTGCSocket& stgcSocket()   { return *m_sTGCSocket; }
	bool daqSocketOK();
	sTGCSocket& daqSocket() { return *m_daqSocket; }
	bool SocketOK( const QString& whichSocket );
	bool SocketOK( std::string whichSocket );

        QByteArray buffer(std::string name="");

        // Print
        void Print();

    private :
        bool m_dbg;
        MessageHandler *m_msg;
	sTGCSocket *m_sTGCSocket;
	bool m_stgcSetup;
	sTGCSocket * m_daqSocket;
	bool m_daqSetup;

        // retrieve socket by name
        sTGCSocket& getSocket(std::string whichSocket="");

    signals :

    public slots :

}; // class SocketHandler


#endif
