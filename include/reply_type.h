enum reply_message_type { ethernet_rec_echo, ethernet_rec_err_unknown_cmd, ethernet_rec_err_FIFO_full, 
			  ethernet_rec_err_target_timeout, ethernet_rec_err_undef,
			  ethernet_trans_err_send_FIFO_full, ethernet_trans_err_length_FIFO_full,
			  ethernet_trans_err_both_FIFO_full, ethernet_trans_err_undef,
			  SCA_normal, SCA_error, SCA_undef, 
			  TTC_setting, TTC_query_elink, TTC_query_EVID, TTC_query_TTC, TTC_undef,
			  no_reply, data, raw_data, error_formatting, error_undefined };

enum board_type_list { board_type_pFEB, board_type_sFEB, board_type_ROC_int, board_type_pFEB_v22, board_type_sFEB_v22 };
