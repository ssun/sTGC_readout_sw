#ifndef CONFIG_BASE_H
#define CONFIG_BASE_H

/////////////////////////////////////////
//
// configuration_module
//
// Tool for building and sending the
// configuration packets to the front-end/
// VMMs
//
//  - sends the global SPI and configuration
//    of all the VMM channels
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include "socket_handler.h"
#include "message_handler.h"
#include "vmm_decoder.h"
//#include "reply_type.h"

// for clocks and waiting
#include <boost/thread.hpp>

// Qt
#include <QString>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Configuration
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////

class Config_Base : public QObject
{
    Q_OBJECT

    public :
        explicit Config_Base(QObject *parent = 0);
        virtual ~Config_Base(){};
        Config_Base& setDebug(bool dbg) { m_dbg = dbg; return *this; }
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

        Config_Base& LoadSocket(SocketHandler& socket);

	QByteArray bitsToBytes(QBitArray bits);
	quint32 reverse32(QString binary_in);
        QString reverseString(QString string);
	std::vector<uint32_t> QString_to_UInt32_t(QString str);
	std::vector<uint32_t> QString_to_UInt32_t(std::string str);

	QString Build_Mask(int boardType, int BoardID, int ASIC_Type, int ASIC_ID);

        bool SendConfig(int send_to_port, const QHostAddress & target_ip, QString bit_stream, 
			bool listen_for_reply=true, bool silent=false, int search_for_message_type = -1, int wait_time = 200);

	bool SendConfigN(int send_to_port, const QHostAddress & target_ip, QString bit_stream, int ntries,
			 bool listen_for_reply=true, bool silent=false, int search_for_message_type = -1, int wait_time = 200 );

        SocketHandler& socket() { return *m_socketHandler; }
	VMMDecoder* decoder() { return m_decode; }

	void set_output_buffer( QString str );
	void set_output_buffer( std::string str);
	void set_output_buffer( char *str );

	void clear_output_buffer() { m_buffer = ""; }
	QString output_buffer() { return m_buffer; }

	void set_is_running( bool running ) { m_running = running; }

	//-------------------------------------------------------------------//
	//   hexdecimals of different headers+commands for sending packages  //
	//-------------------------------------------------------------------//
	//  /header (32 bits)/Unique_ID (16 bits)/Command ( 16 bits )/ Data  //
	//-------------------------------------------------------------------//
	const QString default_header = "decafbad"; // 32 bits

	const uint32_t Board_Type_pFEB     = 0b00;
	const uint32_t Board_Type_sFEB     = 0b01;
	const uint32_t Board_Type_ROCint   = 0b10;
        const uint32_t Board_Type_pFEB_v22 = 3;
        const uint32_t Board_Type_sFEB_v22 = 4;

	const uint32_t ASIC_Type_VMM = 0b00;
	const uint32_t ASIC_Type_TDS = 0b01;
	const uint32_t ASIC_Type_ROC = 0b10;
	const uint32_t ASIC_Type_SCA = 0b11;

	// pre-defined commands on firmware (16 bits )
	const QString cmd_SCA_init                = "0100";
	const QString cmd_SCA_GPIO                = "0101";
	const QString cmd_SCA_ADC                 = "0102";
	const QString cmd_ASIC_config             = "0200";
	const QString cmd_ASIC_reset              = "0300";
	const QString cmd_ASIC_reset_GPIO_enable  = "0301";
	const QString cmd_TTC_config              = "0400";

	//-------------------------------------------------------------------//
	//          hexdecimal of different headers+IDs for replies
	//-------------------------------------------------------------------//
	//   /header (16 bits)/UDP_ID (16 bits)/Module ID (8 bits)
	//           /Message Type (8 bits)/0x0000 (16 bit place holder)
	//           /0x0 (1 bit place holder) + Data (31 bits)  (perhaps multiple 32 bit place holder+data packets)
	//           /0xFFFFFFFF (32 bit trailer)
	//-------------------------------------------------------------------//
	const QString rep_header = "face"; // 16 bits

	// module ID that sent reply message ( 8 bits )
	const QString rep_modID_ethernet_rec   = "01";
	const QString rep_modID_ethernet_trans = "02";
	const QString rep_modID_SCA            = "03";
	const QString rep_modID_TTC            = "04";
	const QString rep_modID_VMMout         = "05";
	
	// Message type of reply message ( 8 bits )
	const QString rep_type_echo            = "EC";
	const QString rep_type_err             = "EE";
	const QString rep_type_status          = "01";
	const QString rep_type_data            = "02";

 private :

	bool m_running;

        // any replies will be saved to output buffer
        QString m_buffer;

	//  Interfaces to socket+gui
	bool m_dbg;

	SocketHandler *m_socketHandler;
	MessageHandler* m_msg;

	VMMDecoder *m_decode;
	
 signals :
	public slots :
       
}; // class Config_Base



#endif
