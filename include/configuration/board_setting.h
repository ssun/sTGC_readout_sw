#ifndef BOARD_SETTING_H
#define BOARD_SETTING_H

#include <QStringList>
#include <QString>
#include <QList>

class BoardSetting {

    public :
        BoardSetting();
        virtual ~BoardSetting(){};

	int boardType;
	int boardID;
	QString boardUN;
	QString ROC_UniqueS;

	int nVMM;
	int nTDS;
        
	std::vector<int> VMMChip_ID;
	QStringList VMM_UniqueS_Numbers;
	std::vector<int> TDSChip_ID;
	QStringList TDS_UniqueS_Numbers;

	std::vector<bool> GPIO_dir_enable;
	std::vector<bool> GPIO_dout_enable;

        void validate();
        void print();
        bool ok; // loading went ok

        //acceptable values
        
};

#endif
