#ifndef VMM_GLOBAL_SETTING_H
#define VMM_GLOBAL_SETTING_H

#include <QStringList>
#include <QList>

class VMMGlobalSetting {

    public :
        VMMGlobalSetting();
        virtual ~VMMGlobalSetting(){};

	std::string header;
	int board_Type;
	int boardID;
	int ASIC_Type;
	int vmmmask;
	std::string command;

        ///////////////////////////////////////////////////
        // VMM3 registers
        ///////////////////////////////////////////////////
        int sp;     // input charge polarity ([0] negative, [1] positive)
        int sdp;    // disable-at-peak
        int sbmx;   // routes analog monitor to PDO output
        int sbft;   // analog output buffer (TOD) ([1] enable)
        int sbfp;   // analog output buffer (PDO) ([1] enable)
        int sbfm;   // analog output buffer (MO) ([1] enable)
        int slg;    // leakage current disable ([0] leakage current enabled)
        int sm5;    // monitor multiplexing
        int scmx;   // monitor multiplexing mode ([0] common, [1] channel)
        int sfa;    // ART enable ([1] enable)
        int sfam;   // ART mode ([0] timing at threshold, [1] timing at peak)
        int st;     // peak time (200, 100, 50, 25 ns)
        int sfm;    // enables dynamic discharge for AC coupling ([1] enable)
        int sg;     // gain (0.5, 1, 3, 4.5, 6, 9, 12, 16 mV/fC)
        int sng;    // neighbor triggering enable ([1] enable)
        // timing outputs: [stpp, stot] -> [00,01,10,11] == [TtP,ToT,PtP,PtT]
        int stot;   // timing outputs control 1
        int stpp;   // timing outputs control 2
        int sttt;   // enables direct-output-logic ([1] enable)
        int ssh;    // enables sub-hysteresis ([1] enable)
        int stc;    // TAC slope adjustment (60, 100, 350, 650 ns)
        int sdt_dac;    // coarse threshold DAC
        int sdp_dac;    // test pulse DAC
        int sc10b;  // 10-bit ADC conversion time
        int sc8b;   // 8-bit ADC conversion time
        int sc6b;   // 6-bit ADC conversion time
        int s8b;    // 8-bit ADC conversion mode
        int s6b;    // enables 6-bit ADC (requries sttt enabled)
        int s10b;   // enables high-resoluation ADCs (10/8bit) ([1] enable)
        int sdcks;  // dual clock edge serialized data ([1] enable)
        int sdcka;  // dual clock edge serialized ART ([1] enable)
        int sdck6b; // dual clock edge serialized 6-bit ([1] enable)
        int sdrv;   // tri-states analog outputs with token, used in analog mode
        int slvs;   // enables direct output IOs ([1] enable)
        int stcr;   // enables auto-reset (at the end of the ramp, if no stop occurs)
        int ssart;  // enables ART flag synchronization (trail to next trail)
        int s32;    // skips channels 16-47 and makes 15 and 48 neighbors
        int stlc;   // enables mild tail cancellation ([1] enable)
        int srec;   // enables fast recovery from high charge ([1] enable)
        int sbip;   // enables bipolar shape ([1] enable)
        int srat;   // enables timing ramp at threshold ([1] enable)
        int sfrst;  // enables fast reset at 6-b completion ([1] enable)
        int slvsbc; // enables slvs 100-ohm termination on ckbc ([1] enable)
        int slvstp; // enables slvs 100-ohm termination on cktp ([1] enable)
        int slvstk; // enables slvs 100-ohm termination on cktk ([1] enable)
        int slvsdt; // enables slvs 100-ohm termination on ckdt ([1] enable)
        int slvsart;    // enables slvs 100-ohm termination on ckart ([1] enable)
        int slvstki;    // enables slvs 100-ohm termination on cktki ([1] enable)
        int slvsena;    // enables slvs 100-ohm termination on ckena ([1] enable)
        int slvs6b; // enables slvs 100-ohm termination on ck6b ([1] enable)
        int sL0enaV;    // disable mixed signal functions when L0 enabled ([1] enable)
        int reset;  // Hard reset
        int sL0ena; // enable L0 core / reset core and gate clock if 0 ([1] enable)
        int l0offset;   // L0 BC offset
        int offset; // Channel tagging BC offset
        int rollover;   // channel tagginb BC rollover
        int window; // size of trigger window
        int truncate;   // max hits per L0
        int nskip;  // number of L0 triggers to skip on overflow
        int sL0cktest;  // enable clocks when L0 core disabled (test)
        int sL0ckinv;   // invert BCCLK ([1] enable)
        int sL0dckinv;  // invert DCK ([1] enable)
        int nskipm; // BCID skip

	std::vector<uint32_t> chan_baselines;
	std::vector<std::vector<uint32_t>> chan_baseline_vs_trim;
	std::vector<int> opt_trim_bits;

        ///////////////////////////////////////////////////
        // VMM2 registers
        ///////////////////////////////////////////////////
        /*
        int polarity;               //SP
        int leakage_current;
        int analog_tristates;
        int double_leakage;
        int gain;
        int peak_time;
        int neighbor_trigger;
        int tac_slope;
        int disable_at_peak;
        int art;
        int art_mode;
        int dual_clock_art;
        int out_buffer_mo;          //sbfm
        int out_buffer_pdo;         //sbfp
        int out_buffer_tdo;         //sbft
        int channel_monitor;
        int monitoring_control;     //scmx
        int monitor_pdo_out;        //sbmx
        int adcs;
        int sub_hysteresis;
        int direct_time;
        int direct_time_mode;
        int direct_time_mode0;
        int direct_time_mode1;
        int conv_mode_8bit;
        int enable_6bit;
        int adc_10bit;
        int adc_8bit;
        int adc_6bit;
        int dual_clock_data;
        int dual_clock_6bit;
        int threshold_dac;
        int test_pulse_dac;
        */
        
        void validate();
        void print();
        bool ok; // loading went ok

        //acceptable values
        static const QStringList all_gains;
        static const QList<int> all_peakTimes;
        static const QList<int> all_TACslopes;
        static const QStringList all_polarities;
        static const QStringList all_ARTmodes;
        static const QStringList all_directTimeModes;
        static const QStringList all_ADC10bits;
        static const QStringList all_ADC8bits;
        static const QStringList all_ADC6bits;
        
};

#endif
