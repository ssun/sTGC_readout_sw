#ifndef ROC_ASIC_SETTING_H
#define ROC_ASIC_SETTING_H

#include <QStringList>
#include <QList>

class ROC_ASIC_Setting {

    public :
        ROC_ASIC_Setting();
        virtual ~ROC_ASIC_Setting(){};

	std::string header;
	int boardID;
	int board_Type;
	int ASIC_Type;
        int chipID;
	std::string command;
	std::string UniqueS;

	//-----------------------------------------------------//
	//                Digital Configurations
	//-----------------------------------------------------//

        uint32_t L1_first;
	uint32_t even_parity;
	uint32_t ROC_ID;

	uint32_t elink_speed_sROC0;
        uint32_t elink_speed_sROC1;
        uint32_t elink_speed_sROC2;
        uint32_t elink_speed_sROC3;

	uint32_t vmm_mask_sROC0;
	uint32_t vmm_mask_sROC1;
	uint32_t vmm_mask_sROC2;
	uint32_t vmm_mask_sROC3;

	uint32_t EOP_enable_sROC_mask;
	uint32_t NullEvt_enable_sROC_mask;

	bool bypass;
	bool timeout_ena;
	
	uint32_t TTC_start_bits;
	
	uint32_t enable_sROC_mask;

	uint32_t vmm_enable_mask;
	uint32_t timeout;
	
	uint32_t TX_current;
	
	uint32_t BC_offset;
	uint32_t BC_rollover;

	uint32_t eport_sROC0;
	uint32_t eport_sROC1;
	uint32_t eport_sROC2;
	uint32_t eport_sROC3;

	uint32_t fake_vmm_failure_mask;
	uint32_t TDS_enable_sROC_mask;
	uint32_t BUSY_enable_sROC_mask;
	
	uint32_t BUSY_On_Limit;
	uint32_t BUSY_Off_Limit;

	uint32_t Max_L1_Events_no_comma;

	//-----------------------------------------------//
	//              Analog Configurations
	//-----------------------------------------------//

	uint32_t tp_bypass_global;
	uint32_t tp_phase_global;
	
	uint32_t LockOutInv;
	uint32_t testOutEn;
	uint32_t testOutMux;

	uint32_t VMM_BCR_INV;
	uint32_t VMM_ENA_INV;
	uint32_t VMM_L0_INV;
	uint32_t VMM_TP_INV;

	uint32_t TDS_BCR_INV;

        void validate();
        void print();
        bool ok; // loading went ok

};

#endif
