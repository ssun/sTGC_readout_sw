#ifndef TDS_GLOBAL_SETTING_H
#define TDS_GLOBAL_SETTING_H

#include <QStringList>
#include <QList>

class TDSGlobalSetting {

    public :
        TDSGlobalSetting();
        virtual ~TDSGlobalSetting(){};

	std::string header;
	int board_Type;
	int boardID;
	int ASIC_Type;
        int chipID;
	std::string command;

        ///////////////////////////////////////////////////
        // VMM3 registers
        ///////////////////////////////////////////////////
	int bcid_offset;
	int bcid_rollover;
	int bcid_clockphase;
	int match_window;
	int vmm0_clockphase;
	int vmm1_clockphase;
	int SER_PLL_current;
	int SER_PLL_resistor;
	int soft_resets;
	int reject_window;
	int bypass_prompt;
	int prompt_circuit;
	int bypass_trigger;
	int bypass_scrambler;
	int test_frame2Router_enable;
	int stripTDS_globaltest;
	int PRBS_en;
	//	int timer;

        void validate();
        void print();
        bool ok; // loading went ok

        //acceptable values
	/*
        static const QStringList all_gains;
        static const QList<int> all_peakTimes;
        static const QList<int> all_TACslopes;
        static const QStringList all_polarities;
        static const QStringList all_ARTmodes;
        static const QStringList all_directTimeModes;
        static const QStringList all_ADC10bits;
        static const QStringList all_ADC8bits;
        static const QStringList all_ADC6bits;
        */
};

#endif
