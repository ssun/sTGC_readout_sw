#ifndef MINIDAQ_SETTING_H
#define MINIDAQ_SETTING_H

#include <QStringList>
#include <QList>

class MiniDAQ_Setting {

    public :
        MiniDAQ_Setting();
        virtual ~MiniDAQ_Setting(){};

	int elink_locked_mask;

	std::vector<std::vector< bool >> elink_matrix;

	int TTC_locked_mask;
	int configured_board_mask;

	std::vector<int> board_UniqueIDs;
	
        void validate();
        void print();
        bool ok; // loading went ok

};

#endif
