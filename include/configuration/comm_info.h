#ifndef COMM_INFO_H
#define COMM_INFO_H

#include <QString>

class CommInfo {

    public :
        CommInfo();
        virtual ~CommInfo(){};

        // UDP info
        int fec_port;
	int comm_info;
        int daq_port;
        int vmmasic_port;
        int vmmapp_port;
        int clocks_port;
	int stgc_port;

	int host_port;
	int target_port;

        // general info
        QString config_filename;
        QString vmm_id_list;
        QString ip_list;
        QString comment;

        void print();
        bool ok;
};



#endif
