#ifndef TDS_CHANNEL_H
#define TDS_CHANNEL_H

class TDS_Channel {

    public :
        TDS_Channel();
        virtual ~TDS_Channel(){};

        int number;
	bool enabled;
	int delay;

        void print();
        bool ok; // loading went ok
};

#endif
