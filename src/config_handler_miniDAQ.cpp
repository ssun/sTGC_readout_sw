// vmm
#include "config_handler_miniDAQ.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>



//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandlerMiniDAQ -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerMiniDAQ::ConfigHandlerMiniDAQ(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    m_msg(0)
{
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
//// ------------------------------------------------------------------------ //
bool ConfigHandlerMiniDAQ::LoadMiniDAQConfig_from_File(const QString &filename)
{

  m_commSettings.config_filename = filename;

  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree pt;
  read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);

  m_miniDAQ = LoadMiniDAQSettings(pt);
  bool board_ok = m_miniDAQ.ok;
  if(!board_ok) {
    msg()("Problem loading GlobalSettings", "ConfigHandlerMiniDAQ::LoadConfig");
  }

  if(!( board_ok ) ) { 
    msg()("Configuration loaded unsucessfully","ConfigHandlerMiniDAQ::LoadConfig");
    if(!board_ok)      cout << "board_ok" << endl;
  }

  return (board_ok); 
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::WriteMiniDAQConfig_to_File(QString filename)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;

  ptree out_root;

  ptree out_miniDAQ;
  out_miniDAQ.put("elink_locked_mask", miniDAQ_Settings().elink_locked_mask);
  out_miniDAQ.put("TTC_locked_mask",   miniDAQ_Settings().TTC_locked_mask);
  out_miniDAQ.put("configured_board_mask", miniDAQ_Settings().configured_board_mask);

  stringstream elink_i_str;

  for ( uint i=0; i<miniDAQ_Settings().elink_matrix.size(); i++ ) {
    ptree out_link;

    ss.str("");
    ss << "elink_" << format("%01i") % i;

    elink_i_str.str("");
    elink_i_str << "elink_phase_mask";

    //--------------------------------------------------------------------//

    uint phase_mask = 0;
    for ( uint j=0; j<miniDAQ_Settings().elink_matrix.at(i).size(); j++ ) {
      uint tmp_phase = miniDAQ_Settings().elink_matrix.at(i).at(j);
      tmp_phase = tmp_phase << j; // bitwise operation: push back by j bits
      phase_mask += tmp_phase;
    }

    //-------------------------------------------------------------------//

    out_link.put(elink_i_str.str(), phase_mask);

    out_miniDAQ.add_child(ss.str(), out_link);

  }

  //-------------------------------------------------------------//

  for ( uint i=0; i< miniDAQ_Settings().board_UniqueIDs.size(); i++ ) {

    ptree out_board;

    ss.str("");
    ss << "board_" << format("%01i") % i;

    elink_i_str.str("");
    elink_i_str << "board_uniqueID";

    out_board.put(elink_i_str.str(), miniDAQ_Settings().board_UniqueIDs.at(i));

    out_miniDAQ.add_child(ss.str(), out_board);
  }

  out_root.add_child("miniDAQ_settings", out_miniDAQ);
  outpt.add_child("configuration", out_root);

  stringstream sx;
  try {
        #if BOOST_VERSION >= 105800
    write_xml(filename.toStdString(), outpt, std::locale(),
              boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
    write_xml(filename.toStdString(), outpt, std::locale(),
              boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

    sx.str("");
    sx << "Configuration written successfully to file: \n";
    sx << " > " << filename.toStdString();
    msg()(sx,"ConfigHandlerMiniDAQ::WriteConfig");
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Unable to write output configuration XML file: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::WriteConfig");
    return;
  }


}
//// ------------------------------------------------------------------------ //
CommInfo ConfigHandlerMiniDAQ::LoadCommInfo(const boost::property_tree::ptree& pt)
{
    using boost::property_tree::ptree;

    CommInfo comm;

    try
    {
        for(const auto& conf : pt.get_child("configuration")) {
            if(!(conf.first == "udp_setup"))
                continue;

            if(conf.first=="udp_setup") {
                // FEC port
	        //   comm.fec_port = conf.second.get<int>("fec_port");
		// stgc port
		comm.host_port = conf.second.get<int>("host_port");
                // DAQ port
                comm.target_port = conf.second.get<int>("target_port");
                // VMMASIC port
		//   comm.vmmasic_port = conf.second.get<int>("vmmasic_port");
                // VMMAPP port
		//   comm.vmmapp_port = conf.second.get<int>("vmmapp_port");
                // clocks port
                //   comm.clocks_port = conf.second.get<int>("clocks_port");

                comm.ok = true;
            } // udp_setup
        }
    }
    catch(std::exception &e)
    {
        stringstream sx;
        sx << "!! -------------------------------------------- !! \n"
           << "  ERROR CONF: " << e.what() << "\n"
           << "!! -------------------------------------------- !!"; 
        msg()(sx,"ConfigHandlerMiniDAQ::LoadCommInfo"); 
        comm.ok = false;
    }

    return comm;
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::LoadCommInfo(const CommInfo& info)
{
    m_commSettings = info;
    if(dbg())
        m_commSettings.print();
}
// -------------------------------------------------------------------------- //
MiniDAQ_Setting ConfigHandlerMiniDAQ::LoadMiniDAQSettings( const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;
  using boost::format;

  stringstream sx;

  MiniDAQ_Setting g;

  bool outok = true;

  try{
    for(const auto& conf : pt.get_child("configuration")) {
      if(!(conf.first == "miniDAQ_settings")) continue;

      g.elink_locked_mask     = conf.second.get<int>("elink_locked_mask");
      g.TTC_locked_mask       = conf.second.get<int>("TTC_locked_mask");
      g.configured_board_mask = conf.second.get<int>("configured_board_mask");

    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading miniDAQ settings: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQ_Settings");
    outok = false;
  } 

  stringstream ss;

  std::vector <bool> empty;
  empty.resize(0);

  try {
    g.elink_matrix.resize(0);
    for(int i = 0; i < 8; i++) {
      ss.str("");
      ss << "elink_" << format("%01i") % i;
      g.elink_matrix.push_back( empty );

      for(const auto& conf : pt.get_child("configuration.miniDAQ_settings")) {
        size_t find = conf.first.find(ss.str());
        if(find==string::npos) continue;

	uint32_t trim;
        trim = conf.second.get<uint32_t>("elink_phase_mask");

	for ( int i=0; i<32; i++ ) {
	  g.elink_matrix.at(i).push_back( ((trim & ( 0b1 << i )) >> i ) ); 
	}
	
      }
    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading miniDAQ settings: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQSettings");
    outok = false;
  } 

  try {

    g.board_UniqueIDs.resize(0);
    for(int i = 0; i < 8; i++) {
      ss.str("");
      ss << "board_" << format("%01i") % i;
      for(const auto& conf : pt.get_child("configuration.miniDAQ_settings")) {
        size_t find = conf.first.find(ss.str());
        if(find==string::npos) continue;

        uint32_t trim;
        trim = conf.second.get<int>("board_uniqueID");

	g.board_UniqueIDs.push_back(trim);

      }
    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading miniDAQ settings: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQSettings");
    outok = false;
  }


    
  g.ok = outok;

  return g;

}
//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::LoadMiniDAQConfiguration(MiniDAQ_Setting& board)
{
  m_miniDAQ = board;
}
//-------------------------//
int ConfigHandlerMiniDAQ::nElinks() {

  int nlinks=0;

  for ( int i=0; i<8; i++ ) {
    if ( miniDAQ_Settings().elink_locked_mask & ( 0b1 << i ) ) nlinks++;
  }

  return nlinks;
}
