
#include "daq_server.h"

#include <QString>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>
#include <QByteArray>
#include <QBitArray>

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

#include <boost/filesystem.hpp>
#include "TROOT.h"
#include "TFile.h"

//#include "map_handler.h"
//#include "OnlineMonTool.h"
#include "message_handler.h"
#include "reply_type.h"

// converts network MSB/LSB convention to host MSB/LSB convention
#include <netinet/in.h>

#include "daq_buffer.h"

boost::mutex global_stream_lock;
//boost::mutex output_stream_lock;

namespace ip = boost::asio::ip;


DaqServer::DaqServer(QObject* parent) :
  QObject(parent),
  m_continue_gathering(new bool())
  //  m_ofile(NULL)
{
  //  out_decoded_tree = new TTree("output_tree_decoded", "output_tree_decoded");

  m_thread_count=2;
  evt_count=0;

  output_raw = false;
  output_decoded = false;
  filename_raw = "";
  filename_decoded = "";
  m_socket = NULL;
  m_io_service = NULL;
  m_idle_work = NULL;
  m_buffer = NULL;
  m_decode = NULL;
}

DaqServer::~DaqServer(){
  delete outFile;
  delete out_decoded_tree;
}

void DaqServer::LoadMessageHandler(MessageHandler& msg)
{
  m_msg = &msg;
}


void DaqServer::setDebug(bool dbg)
{
  m_dbg = dbg;
}

void SPSCThread(boost::shared_ptr< boost::asio::io_service> io_service)
{
  
  global_stream_lock.lock();
  std::cout << "begin io thread" << std::endl;
  global_stream_lock.unlock();      

  io_service->run();

  global_stream_lock.lock();  
  std::cout << "io thread ended" << std::endl;
  global_stream_lock.unlock();

}

void DaqServer::initialize() {

  m_buffer = boost::shared_ptr<DaqBuffer>(new DaqBuffer());
  //  m_ofile = boost::shared_ptr<std::ofstream>(new std::ofstream());

  m_decode = boost::shared_ptr<VMMDecoder>(new VMMDecoder());

  output_raw     = true;
  output_decoded = true;

  write_tex = false;
  write_bin = true;

  filename_raw     = "default_raw.txt";
  filename_decoded = "default_decoded.root";
  
  m_run_max_events = 0;
  m_current_locked_elink.resize(0);
  m_current_EVID = 0;
  m_current_TTC_status = 0;

}

bool DaqServer::initializeRun(){

  //--------------------------------------------//
  //            setup IO service
  //--------------------------------------------//

  if ( !m_io_service ) m_io_service = boost::shared_ptr<boost::asio::io_service>(new boost::asio::io_service());
  m_io_service->reset();

  int port_to_bind_to = 6008;
#ifndef __linux__
  m_idle_work = boost::shared_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(*m_io_service));
#else
  m_idle_work = boost::shared_ptr<boost::asio::io_service::work>(new boost::asio::io_service::work(*m_io_service));
#endif


  //--------------------------------------------//
  //               open socket
  //--------------------------------------------//

  boost::system::error_code ec;

  if ( m_socket ) {
    if ( m_socket->is_open()) {
      m_socket->close();
    }
  }
  m_socket = boost::shared_ptr<boost::asio::ip::udp::socket>(new ip::udp::socket(*m_io_service, ip::udp::endpoint(ip::udp::v4(), port_to_bind_to)));
  if (!m_socket->is_open()) {
    m_sx.str("");
    m_sx << "ERROR DAQ socket not setup at port: " << port_to_bind_to;
    msg()(m_sx,"DaqServer::initializeRun"); m_sx.str("");
    return false;
  }
  m_buffer->initialize(1000);


  //-----------------------------------------------//
  //             initialize output                 //
  //-----------------------------------------------//

  m_tdo.resize(0);
  m_pdo.resize(0);
  m_chan.resize(0);
  m_vmm_id.resize(0);
  m_bcid_rel.resize(0);
  m_flag.resize(0);

  m_board_id.resize(0);
  m_bcid.resize(0);
  m_l1_id.resize(0);

  //------------------------------------------------//
  //             open all output files
  //------------------------------------------------//

  m_ofile_meta.open(filename_meta.c_str());

  if ( output_decoded ) outFile = new TFile( filename_decoded.c_str(), "RECREATE" );

  if ( output_raw     ) InitRawTTree();
  if ( output_decoded ) InitDecodedTTree();

  *m_continue_gathering = true;

  return true;

}

void DaqServer::setRunStartTime(std::string starttime){
  if ( output_raw ) m_ofile_meta << "Run Start: " << starttime << std::endl;
}

void DaqServer::setRunEndTime(std::string endtime){
  if ( output_raw ) m_ofile_meta << "Run End: " << endtime << std::endl;
}


void DaqServer::listen()
{

  m_sx.str("");
  m_sx << "listening for data in [" << boost::this_thread::get_id() << "] thread \n";
  msg()(m_sx, "DaqServer::listen"); m_sx.str("");

  *m_continue_gathering = true;

  //----------------------------------------------------------------//
  //     opens two independent threads for io_service
  //
  //     one thread can read in data from port to buffer 
  //          while the other thread reads out buffer to write to disk
  //----------------------------------------------------------------//

  //  std::cout << "number of threads " << m_thread_group.size() << std::endl;

  for ( int i=0;i< m_thread_count;i++ ) {
    m_thread_group.create_thread(boost::bind(SPSCThread, m_io_service));
  }

  //-----------------------------------------------------------//
  //   posts receiving and waiting for data to m_io_service
  //
  //   boost::bind resolves compatibility issues between input for post and the
  //   user defined reading methods
  //------------------------------------------------------------//

  m_io_service->post(boost::bind(&DaqServer::handle_data,this));
  m_io_service->post(boost::bind(&DaqServer::read_data,  this));
				

}

bool DaqServer::is_stopped()
{
  return m_io_service->stopped();
}

//---------------------------------------------------------//
//    Receive data using socket.  Save to m_data_buffer
//---------------------------------------------------------//
void DaqServer::handle_data()
{
  m_socket->async_receive_from(boost::asio::buffer(m_data_buffer), m_remote_endpoint,
			       boost::bind(&DaqServer::gather_data, this,
					   boost::asio::placeholders::error,
					   boost::asio::placeholders::bytes_transferred)
			       );
}

//----------------------------------------------------------//
//       Push data from m_data_buffer to m_buffer
//       call handle_data() again as soon as done to be 
//       ready to receive anothe packet
//----------------------------------------------------------//
void DaqServer::gather_data(const boost::system::error_code error, std::size_t size_)
{

  //--------------------------------------------------------//
  //       break out of data gathering as soon as
  //       command to stop is given
  //--------------------------------------------------------//

  if(!(*m_continue_gathering)) return;

  //--------------------------------------------------------//
  //       Also save IP of packet
  //--------------------------------------------------------//

  std::string ip_ = m_remote_endpoint.address().to_string();

  //global_stream_lock.lock();
  //std::cout << "gathering from ip" << ip_ << std::endl;
  //global_stream_lock.unlock();

  //-------------------------------------------------------//
  //       Take data from m_data buffer and save it to
  //       m_buffer which is later read and decoded
  //-------------------------------------------------------//

  std::vector<uint32_t> incoming_;
  for ( auto &x : m_data_buffer) {
    incoming_.push_back(ntohl(x));
    if ( x==0xffffffff) break;  // not sure if this is safe...

    //global_stream_lock.lock();
    //std::cout << "pushing_data" << std::endl;
    //std::cout  << std::hex<<x<< "\n " ;
    //std::cout  << std::dec<<x << "\n " ;
    //global_stream_lock.unlock();
  }

  IPDataArray incoming_data = std::make_pair(ip_, incoming_);
  
  if ( !m_buffer->add_to_queue(incoming_data)) {
    global_stream_lock.lock();
    std::cout << "failed to add to queue!" << std::endl;
    global_stream_lock.unlock();
  }

  //-------------------------------------------------------------------//
  //
  //      Ready to receive next packet when finished
  //
  //      Since two io_service threads are running
  //      one thread is always ready to receive data while the other
  //      is running gather_data()
  //
  //      Unless data rate is so high that more than 2 packets arrive
  //      in the time to run gather_data()
  //
  //-------------------------------------------------------------------//

  if((*m_continue_gathering))  handle_data();

}
  
void DaqServer::read_data() {

  //------------------------------------------------------------------------------//
  //
  //      Take data from m_buffer and save it to an output
  //
  //      Either just save the raw data packet as a hexa number to an text file
  //      Or decode using VMM_Decoder and save the decoded data to a TTree.
  //      
  //      If the data rate is higher than the time to run read_data()
  //      then m_buffer is receiving packet faster than read_data() can output()
  //      
  //      In which case it is advised to output just the raw data to m_ofile and
  //      decode later using a separate program running VMM_Decoder
  //
  //------------------------------------------------------------------------------//


  m_sx.str("");
  m_sx << "reading data \n ";
  msg()(m_sx, "DaqServer::read_data"); m_sx.str("");

  IPDataArray incoming_element;
  //  std::vector<uint32_t> datagram;

  //-------------------------------------------------------------------------------------//
  //      Continuously run until stop signal is given (m_continue_gathering == false)
  //-------------------------------------------------------------------------------------//

  while(true) {

    if (  *m_continue_gathering && m_buffer->has_data() ) {
      
      if ( m_buffer->dequeue(incoming_element) ) {

	// if max event set break out of run once event count reaches max
	evt_count++;
	if ( m_run_max_events > 0 && evt_count >= m_run_max_events) {
	  *m_continue_gathering = false;
	}

	//---------------------------------------------------------------//
	//           Message must be at least 64 bits long
	//---------------------------------------------------------------//

	if ( incoming_element.second.size() < 2 ) {
	  global_stream_lock.lock();
	  std::cout << "Message shorter than 64 bits!" << std::endl;
          global_stream_lock.unlock();
	}

	//--------------------------------------------------------------//
	//          decoding must happen during stream lock
	//--------------------------------------------------------------//
        global_stream_lock.lock();

	//--------------------------------------------------------------//
	//          Decode header and determine message type
	//--------------------------------------------------------------//

	uint32_t udp_id; 
	int message_type;
	std::string out_str;

	bool reply_normal = m_decode->decode_reply(incoming_element.second, udp_id, message_type,
						   &out_str, output_decoded);

	//-----------------------------------------------------//
	//         if there are errors, output it
	//-----------------------------------------------------//

	if ( !reply_normal ) {

          if ( output_raw ) {
            for ( uint i=0; i < incoming_element.second.size()-1; i++ ) {
              if ( write_tex ) {
                m_tex_ofile << std::hex << std::setw(8) << incoming_element.second.at(i) ;
              }
	      if ( write_bin ) {
		uint32_t temp = incoming_element.second.at(i);
		fwrite(&temp, sizeof(temp), 1, m_ofile);
	      }
            }
	    if ( write_tex ) {
	      m_tex_ofile << std::hex << std::setw(8) << incoming_element.second.back() << std::endl;
	    }
	    if ( write_bin ) {
	      uint32_t temp = incoming_element.second.back();
	      fwrite(&temp, sizeof(temp), 1, m_ofile);
	    }
          }

	  m_sx.str("");
	  m_sx << "warning packet failed decoding: " << out_str.c_str();
	  msg()(m_sx,"DaqServer::read_data");
	  
	  outputDatagram(incoming_element.second);
	  global_stream_lock.unlock();

	  continue;
	}

	//---------------------------------------------------------------------------//
	//          if its a query or echo reply,    don't output it
	//---------------------------------------------------------------------------//

	if ( message_type == TTC_query_EVID ) {
	  m_current_EVID = m_decode->current_EVID();
	}

	if ( message_type == TTC_query_elink ) {
	  m_current_locked_elink = m_decode->locked_elink();
	}

	if ( message_type == TTC_query_TTC ) {
	  m_current_TTC_status = m_decode->current_TTC_status();
	}
	if ( message_type == SCA_normal ) {
	  std::cout << "SCA status normal" << std::endl;
	}
	if ( message_type == TTC_setting ) {
	  std::cout << "TTC setting normal" << std::endl;
	}

	//---------------------------------------------------------------------------//
	//                          if its data, output it
	//---------------------------------------------------------------------------//

	if ( message_type == data ) {

	  //-------------------------------------------------------------------//
	  //           Save decoded events to a TTree
	  //           This option is slow.  Only do so if data rate is low
	  //           Otherwise decode offline
	  //-------------------------------------------------------------------//
	  
	  if ( output_decoded ) {

	    m_tdo = m_decode->tdo();
	    m_pdo = m_decode->pdo();
	    m_chan = m_decode->chan();
	    m_vmm_id = m_decode->vmm_id();
	    m_bcid_rel = m_decode->bcid_rel();
	    m_flag = m_decode->flag();
	    
	    m_board_id = m_decode->board_id();
	    m_bcid = m_decode->bcid();
	    m_l1_id = m_decode->l1_id();
	    
	    out_decoded_tree->Fill();  
	    
	  }
	  
	  //-----------------------------------------------------------------//
	  //              Write raw output to a ttree
	  //-----------------------------------------------------------------//
	  
	  if ( output_raw ) {
	    //	    m_tex_ofile << incoming_element.first.c_str() << "     ";
	    for ( uint i=0; i < incoming_element.second.size()-1; i++ ) {
	      if ( write_tex ) {
		m_tex_ofile << std::hex << std::setw(8) << incoming_element.second.at(i) ;
	      }
	      if ( write_bin ) {
		uint32_t temp = incoming_element.second.at(i);
		fwrite(&temp, sizeof(temp), 1, m_ofile);
	      }
	    }
	    if ( write_tex ) {
	      m_tex_ofile << std::hex << std::setw(8) << incoming_element.second.back() << std::endl;
	    }
	    if ( write_bin ) {
	      uint32_t temp = incoming_element.second.back();
	      fwrite(&temp, sizeof(temp), 1, m_ofile);
	    }
	  }
	}
	else if ( message_type == raw_data ) {

          if ( output_raw ) {
	    //            m_ofile << incoming_element.first.c_str() << "     ";
            for ( uint i=0; i < incoming_element.second.size()-1; i++ ) {
	      if ( write_tex ) {
		m_tex_ofile << std::hex << std::setw(8) << incoming_element.second.at(i) ;
	      }
	      if ( write_bin ) {
		uint32_t temp = incoming_element.second.at(i);
		fwrite(&temp, sizeof(temp), 1, m_ofile);
	      }
            }
	    if ( write_tex ) {
	      m_tex_ofile << std::hex << std::setw(8) << incoming_element.second.back() << std::endl;
	    }
	    if ( write_bin ) {
	      uint32_t temp = incoming_element.second.back();
	      fwrite(&temp, sizeof(temp), 1, m_ofile);
	    }
          }

          //m_sx.str("");
          //m_sx << "received data packet: " << out_str.c_str();
          //msg()(m_sx,"DaqServer::read_data");
          //outputDatagram(incoming_element.second);

	}

	/*
	else if ( message_type == TTC_query_elink ) {

	  m_currrent_locked_elink = m_decode->locked_elink();

	  int nlinks = 0;
	  for ( int i=0; i< m_currrent_locked_elink.size(); i++ ) {
	    if ( m_currrent_locked_elink.at(i) ) nlinks++;
	  }
	  
	  if ( nlinks != n_start_elink ) {
	    m_sx.str("");
	    m_sx << "Error: number of locked elinks incorrect \n" ;
            msg()(m_sx, "DaqServer::read_data");
	    m_sx << "locked links: ";
	    for ( int i=0; i<m_currrent_locked_elink.size(); i++ ) {
	      m_sx << m_currrent_locked_elink.at(i) << " ";
	    }
	    m_sx << "\n";
	    msg()(m_sx, "DaqServer");
	  }

	}
	*/

	//------------------------------------------------------------------------//
	
	global_stream_lock.unlock();

	//----------------------------------------------------------------//

      }
    }
    else if(!(*m_continue_gathering)) {
      break;
    }

  }
}

//-----------------------------------------------------------------//
void DaqServer::outputDatagram(std::vector<uint32_t> datagram)
{

  stringstream m_sx;

  m_sx << std::internal << std::setfill('0');

  for ( uint i=0; i < datagram.size(); i++ ) {
    m_sx << std::hex << std::setw(8) << datagram.at(i) ;
  }

  m_sx << "\n";

  msg()(m_sx, "DaqServer");

}

//-----------------------------------------------------------------//
void DaqServer::stop_listening()
{
  //------------------------------------//
  //        stop gathering data
  //------------------------------------//

  *m_continue_gathering = false;

  //   wait a bit for gather_data() and read_data() to stop
  boost::this_thread::sleep(boost::posix_time::milliseconds(100));

  global_stream_lock.lock();
  m_sx.str("");
  m_sx << "ending run \n ";
  msg()(m_sx, "DaqServer::endRun"); m_sx.str("");
  global_stream_lock.unlock();

  //------------------------------------//
  //        shutdown sockets
  //------------------------------------//

  if(m_socket->is_open()) {
    m_socket->close();
  }
  boost::system::error_code ec;
  m_socket->shutdown(ip::udp::socket::shutdown_both, ec);

  if ( dbg() ) {
    global_stream_lock.lock();
    m_sx.str("");
    m_sx << "shutdown data socket \n ";
    msg()(m_sx, "DaqServer::endRun"); m_sx.str("");
    global_stream_lock.unlock();
  }

}

void DaqServer::stop_server() 
{

  //-------------------------------//
  //       stop io service
  //-------------------------------//

  m_io_service->stop();

  if ( dbg() ) {
    global_stream_lock.lock();
    m_sx.str("");
    m_sx << "stop io service \n ";
    msg()(m_sx, "DaqServer::endRun"); m_sx.str("");
    global_stream_lock.unlock();
  }

  boost::this_thread::sleep(boost::posix_time::milliseconds(200));

  //------------------------------//
  //        join all threads
  //------------------------------//

  m_thread_group.join_all();

  if (dbg()) {
    global_stream_lock.lock();
    m_sx.str("");
    m_sx << "stopped IO thread \n ";
    msg()(m_sx, "DaqServer::endRun"); m_sx.str("");
    global_stream_lock.unlock();
  }

}

void DaqServer::write_output() {

  m_sx.str("");
  m_sx << "collected " << evt_count << " events in run, writing output \n ";
  msg()(m_sx, "DaqServer::write_output"); m_sx.str("");

  m_ofile_meta.close();

  if ( output_raw && write_tex ) m_tex_ofile.close();
  if ( output_raw && write_tex ) m_tex_ofile.flush();

  if ( output_raw && write_bin ) fclose( m_ofile );

  if ( output_decoded )  out_decoded_tree->Write("", TObject::kOverwrite);
  if ( output_decoded )  outFile->Write();
  if ( output_decoded )  outFile->Close();

}

void DaqServer::InitRawTTree()
{

  //---------------------------------------------//
  //      open txt file
  //---------------------------------------------//

  if ( write_tex ) m_tex_ofile.open((filename_raw+".txt").c_str(), ios::out );
  if ( write_tex ) m_tex_ofile << std::internal << std::setfill('0');

  if ( write_bin ) m_ofile = fopen((filename_raw+".bin").c_str(), "wb");

}

void DaqServer::InitDecodedTTree()
{

  out_decoded_tree = new TTree("output_tree_decoded", "output_tree_decoded");

  out_decoded_tree->Branch("m_ip",       &m_ip);

  out_decoded_tree->Branch("m_tdo",      &m_tdo);
  out_decoded_tree->Branch("m_pdo",      &m_pdo);
  out_decoded_tree->Branch("m_chan",     &m_chan);
  out_decoded_tree->Branch("m_vmm_id",   &m_vmm_id);
  out_decoded_tree->Branch("m_bcid_rel", &m_bcid_rel);
  out_decoded_tree->Branch("m_flag",     &m_flag);

  out_decoded_tree->Branch("m_board_id", &m_board_id);
  out_decoded_tree->Branch("m_bcid",     &m_bcid);
  out_decoded_tree->Branch("m_l1_id",    &m_l1_id);

}
