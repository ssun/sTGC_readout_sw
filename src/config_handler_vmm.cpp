// vmm
#include "config_handler_vmm.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>



//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandlerVMM -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerVMM::ConfigHandlerVMM(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    //    m_board_selection(-1),
    m_msg(0)
{
  //    m_channelmap.clear();
    m_vmm_channels.clear();
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerVMM::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}

void ConfigHandlerVMM::setResetHardBit( bool reset ) {
  m_vmm_globalSettings.reset = reset;
}

void ConfigHandlerVMM::setResetAutoBit( bool reset ) {
  m_vmm_globalSettings.stcr = reset;
}
// --------------------------------------------------------------------------- //
void ConfigHandlerVMM::Set_CalibrateVMM_ThDAC( int ithreshold ) {

  m_vmm_globalSettings.sbfp  = true;
  m_vmm_globalSettings.sbmx  = true;

  // monitored object must be global threhold DAC
  m_vmm_globalSettings.scmx  = false;
  // monitor 2nd channel (Th DAC)
  m_vmm_globalSettings.sm5   = 2;

  // all adc to zero
  m_vmm_globalSettings.sdt_dac = ithreshold;

}

// -------------------------------------------------------------------------- //
void ConfigHandlerVMM::Set_CalibrateVMM_Channel( int ichan, bool trimming, int trimmingbit ) {

  // turn on PDO analog output and route to PDO analog output
  m_vmm_globalSettings.sbfp  = true;
  m_vmm_globalSettings.sbmx  = true;

  // monitored object must be channel
  m_vmm_globalSettings.scmx  = true;
  // monitor ith channel
  m_vmm_globalSettings.sm5   = ichan;

  // all adc to zero
  VMM_ChannelSettings(ichan).sz10b=0;
  VMM_ChannelSettings(ichan).sz8b=0;
  VMM_ChannelSettings(ichan).sz6b=0;

  if ( trimming ) {

    VMM_ChannelSettings(ichan).sc  = false;
    VMM_ChannelSettings(ichan).sl  = false;

    // turn test pulse capacitors off
    VMM_ChannelSettings(ichan).sth = false;
    VMM_ChannelSettings(ichan).st  = false;

    // always unmask channel that is being scanned
    VMM_ChannelSettings(ichan).sm = false;

    // turn channel monitor mode to trimmed threshold
    VMM_ChannelSettings(ichan).smx = true;

    // set trimming bit
    VMM_ChannelSettings(ichan).sd  = trimmingbit;
  }
  else { //baseline measurment mode
    // not large sensor capacitance
    // leakage current enabled
    VMM_ChannelSettings(ichan).sc  = false;
    VMM_ChannelSettings(ichan).sl  = false;

    // turn test pulse capacitors off
    VMM_ChannelSettings(ichan).sth = false;
    VMM_ChannelSettings(ichan).st  = false;

    // always unmask channel that is being scanned
    VMM_ChannelSettings(ichan).sm = false;

    // turn channel monitor mode to analog output
    VMM_ChannelSettings(ichan).smx = false;

    // set trimming bit but it doesn't matter
    VMM_ChannelSettings(ichan).sd  = trimmingbit;
  }

}
void ConfigHandlerVMM::SetVMM_OptTrimBits( std::vector<uint32_t> & opt_trim_bits ) {

  m_vmm_calibSettings.opt_trim_bits = opt_trim_bits;

  //----------------------------------------------------//
  //   set each channel as optimal trimming bit
  //----------------------------------------------------//

  for ( uint i=0; i<opt_trim_bits.size(); i++ ) {
    VMM_ChannelSettings(i).sd = opt_trim_bits.at(i);
  }

}
//// ------------------------------------------------------------------------ //
bool ConfigHandlerVMM::LoadVMMConfig_from_File(const QString &filename)
{

    using boost::property_tree::ptree;
    using namespace boost::property_tree::xml_parser;
    ptree pt;
    read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);

    // Load the global configuration
    m_vmm_globalSettings = LoadVMMGlobalSettings(pt);
    bool global_ok = m_vmm_globalSettings.ok;
    if(!global_ok) {
        msg()("Problem loading GlobalSettings", "ConfigHandlerVMM::LoadConfig_from_file");
    }

    // Load the VMM channel configuration
    m_vmm_channels = LoadVMMChannelConfig(pt);
    bool vmmchan_ok = (m_vmm_channels.size() ? true : false);
    for(int ichan = 0; ichan < (int)m_vmm_channels.size(); ichan++) {
        bool chan_ok = m_vmm_channels.at(ichan).ok;
        if(!chan_ok) {
            stringstream sx;
            sx << "Problem loading channel " << ichan;
            msg()(sx, "ConfigHandlerVMM::LoadConfig");
            vmmchan_ok = false;
            break;
        }
    }

    // load the calibration configuration
    m_vmm_calibSettings = LoadVMMCalibConfig( pt );
    bool calib_ok = true;//VMMCalibSettings().ok;
    if ( !calib_ok) {
      msg()("Problem loading CalibSettings", "ConfigHandlerVMM::LoadConfig_from_file");
    }

    if(!( global_ok && vmmchan_ok && calib_ok) ) { //daq_ok && m_vmmMap.ok && vmmchan_ok && clocks_ok)) {
        msg()("Configuration loaded unsucessfully","ConfigHandlerVMM::LoadConfig_from_file");
        if(!global_ok)      cout << "global_ok" << endl;
	if(!calib_ok)       cout << "calib_ok" << endl;
        if(!vmmchan_ok)     cout << "vmmchan_ok" << endl;
    }
    else {
      msg()("VMM config loaded correctly");
    }
    return (global_ok && vmmchan_ok ); //daq_ok && m_vmmMap.ok && vmmchan_ok && clocks_ok);

}

//// ------------------------------------------------------------------------ //
//                 Write VMM configuration as an XML file
// -------------------------------------------------------------------------- //

void ConfigHandlerVMM::WriteVMMConfig_to_File(QString filename)
{
    using boost::format;
    using boost::property_tree::ptree;
    using namespace boost::property_tree::xml_parser;
    ptree outpt;
    stringstream ss;   

    ptree out_root;

    // ----------------------------------------------- //
    //  global settings
    // ----------------------------------------------- //
    bool global_ok = true;
    
    ptree out_global;

    out_global.put("header",        VMMGlobalSettings().header);
    out_global.put("board_Type",    VMMGlobalSettings().board_Type);
    out_global.put("boardID",       VMMGlobalSettings().boardID);
    out_global.put("ASIC_Type",     VMMGlobalSettings().ASIC_Type);
    out_global.put("vmmmask",       VMMGlobalSettings().vmmmask);
    out_global.put("command",       VMMGlobalSettings().command);

    out_global.put("sdp_dac", VMMGlobalSettings().sdp_dac);
    out_global.put("sdt_dac", VMMGlobalSettings().sdt_dac);
    out_global.put("st",
		   VMMGlobalSetting::all_peakTimes[VMMGlobalSettings().st]);
    out_global.put("sm5", VMMGlobalSettings().sm5);
    //EnabledOrDisabled(int ena_or_dis, std::string& value, std::string from, bool invert)      
    string val = "";
    bool write_ok = EnabledOrDisabled(VMMGlobalSettings().scmx, val, "scmx", false);
    if(write_ok) {
        out_global.put("scmx", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sbmx, val, "sbmx", false);
    if(write_ok) {
        out_global.put("sbmx", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slg, val, "slg", true);
    if(write_ok) {
        out_global.put("slg", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sp, val, "sp", false);
    if(write_ok) {
        if(val=="ENABLED") { val = "POSITIVE"; }
        else if(val=="DISABLED") { val = "NEGATIVE"; }
        out_global.put("sp", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sng, val, "sng", false);
    if(write_ok) {
        out_global.put("sng", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sdrv, val, "sdrv", false);
    if(write_ok) {
        out_global.put("sdrv", val);
    } else { global_ok = false; }
    out_global.put("sg",
		   VMMGlobalSetting::all_gains[VMMGlobalSettings().sg].toStdString());
    out_global.put("stc",
		   VMMGlobalSetting::all_TACslopes[VMMGlobalSettings().stc]);
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sfm, val, "sfm", false);
    if(write_ok) {
        out_global.put("sfm", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().ssh, val, "ssh", false);
    if(write_ok) {
        out_global.put("ssh", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sdp, val, "sdp", false);
    if(write_ok) {
        out_global.put("sdp", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sbft, val, "sbft", false);
    if(write_ok) {
        out_global.put("sbft", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sbfp, val, "sbfp", false);
    if(write_ok) {
        out_global.put("sbfp", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sbfm, val, "sbfm", false);
    if(write_ok) {
        out_global.put("sbfm", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().s6b, val, "s6b", false);
    if(write_ok) {
        out_global.put("s6b", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().s8b, val, "s8b", false);
    if(write_ok) {
      out_global.put("s8b", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().s10b, val, "s10b", false);
    if(write_ok) {
        out_global.put("s10b", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sttt, val, "sttt", false);
    if(write_ok) {
        out_global.put("sttt", val);
    } else { global_ok = false; }

    //#warning check that correct direct timing mode is chosen
    int mode = 0;
    mode |= (VMMGlobalSettings().stpp << 1);
    mode |= (VMMGlobalSettings().stot);
    out_global.put("direct_timing_mode",
		   VMMGlobalSetting::all_directTimeModes[mode].toStdString());
    
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sfa, val, "sfa", false);
    if(write_ok) {
        out_global.put("sfa", val);
    } else { global_ok = false; }
    if(VMMGlobalSettings().sfam == 0) val = "THRESHOLD";
    else if(VMMGlobalSettings().sfam == 1) val = "PEAK";
    out_global.put("sfam", val);

    out_global.put("sc10b", VMMGlobalSettings().sc10b);
    out_global.put("sc08b", VMMGlobalSettings().sc8b);
    out_global.put("sc06b", VMMGlobalSettings().sc6b);
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sdcks, val, "sdcks", false);
    if(write_ok) {
        out_global.put("sdcks", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sdcka, val, "sdcka", false);
    if(write_ok) {
        out_global.put("sdcka", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sdck6b, val, "sdck6b", false);
    if(write_ok) {
        out_global.put("sdck6b", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvs, val, "slvs", false);
    if(write_ok) {
        out_global.put("slvs", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().stlc, val, "stlc", false);
    if(write_ok) {
        out_global.put("stlc", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().stcr, val, "stcr", false);
    if(write_ok) {
        out_global.put("stcr", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().ssart, val, "ssart", false);
    if(write_ok) {
        out_global.put("ssart", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().srec, val, "srec", false);
    if(write_ok) {
        out_global.put("srec", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sfrst, val, "sfrst", false);
    if(write_ok) {
        out_global.put("sfrst", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().s32, val, "s32", false);
    if(write_ok) {
        out_global.put("s32", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sbip, val, "sbip", false);
    if(write_ok) {
        out_global.put("sbip", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().srat, val, "srat", false);
    if(write_ok) {
        out_global.put("srat", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvsbc, val, "slvsbc", false);
    if(write_ok) {
        out_global.put("slvsbc", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvsart, val, "slvsart", false);
    if(write_ok) {
        out_global.put("slvsart", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvstp, val, "slvstp", false);
    if(write_ok) {
        out_global.put("slvstp", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvstki, val, "slvstki", false);
    if(write_ok) {
        out_global.put("slvstki", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvstk, val, "slvstk", false);
    if(write_ok) {
        out_global.put("slvstk", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvsena, val, "slvsena", false);
    if(write_ok) {
        out_global.put("slvsena", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvsdt, val, "slvsdt", false);
    if(write_ok) {
        out_global.put("slvsdt", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().slvs6b, val, "slvs6b", false);
    if(write_ok) {
        out_global.put("slvs6b", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sL0ena, val, "sL0ena", false);
    if(write_ok) {
        out_global.put("sL0ena", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sL0enaV, val, "sL0enaV", false);
    if(write_ok) {
        out_global.put("sL0enaV", val);
    } else { global_ok = false; }
    out_global.put("l0offset", VMMGlobalSettings().l0offset);
    out_global.put("offset", VMMGlobalSettings().offset);
    out_global.put("rollover", VMMGlobalSettings().rollover);
    out_global.put("window", VMMGlobalSettings().window);
    out_global.put("truncate", VMMGlobalSettings().truncate);
    out_global.put("nskip", VMMGlobalSettings().nskip);
    out_global.put("reset", VMMGlobalSettings().reset);
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sL0cktest, val, "sL0cktest", false);
    if(write_ok) {
        out_global.put("sL0cktest", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sL0ckinv, val, "sL0ckinv", false);
    if(write_ok) {
        out_global.put("sL0ckinv", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().sL0dckinv, val, "sL0dckinv", false);
    if(write_ok) {
        out_global.put("sL0dckinv", val);
    } else { global_ok = false; }
    write_ok = EnabledOrDisabled(VMMGlobalSettings().nskipm, val, "nskipm", false);
    if(write_ok) {
        out_global.put("nskipm", val);
    } else { global_ok = false; }

    if ( !global_ok ) {
      stringstream sx;
      sx << "ERROR: VMM Global writing xml failed ";
      msg()(sx, "ConfigHandlerVMM::WriteConfig"); sx.str("");
    }

    // ----------------------------------------------- //
    // stitch together the fields
    // ----------------------------------------------- //
    out_root.add_child("global_registers", out_global);

    // ----------------------------------------------- //
    //  individual VMM channels
    // ----------------------------------------------- //
    // get the "global" channel masks
    uint64_t channel_sc = 0;
    uint64_t channel_sl = 0;
    uint64_t channel_sth = 0;
    uint64_t channel_st = 0;
    uint64_t channel_sm = 0;
    uint64_t channel_smx = 0;

    bool channels_ok = true;

    for(int i = 0; i < 64; i++) {
        if((int)VMM_ChannelSettings(i).number != i) {
            stringstream sx;
            sx << "WARNING VMM Channel numbers out of sync! Expecting VMM channel "
               << i << " but at this index we have VMM " << VMM_ChannelSettings(i).number;
            msg()(sx, "ConfigHandlerVMM::WriteConfig"); sx.str("");
            sx << "WARNING Setting output config channel " << i << " to default values";
            msg()(sx, "ConfigHandlerVMM::WriteConfig"); sx.str("");
            channels_ok = false;
        }
        uint64_t tmp_sc = (VMM_ChannelSettings(i).sc==1 ? 1 : 0);
        uint64_t tmp_sl = (VMM_ChannelSettings(i).sl==1 ? 1 : 0);
        uint64_t tmp_sth= (VMM_ChannelSettings(i).sth==1? 1 : 0);
        uint64_t tmp_st = (VMM_ChannelSettings(i).st==1 ? 1 : 0);
        uint64_t tmp_sm = (VMM_ChannelSettings(i).sm==1 ? 1 : 0);
        uint64_t tmp_smx= (VMM_ChannelSettings(i).smx==1? 1 : 0);

        channel_sc |= (tmp_sc << i);
        channel_sl |= (tmp_sl << i);
        channel_sth |= (tmp_sth << i);
        channel_st |= (tmp_st << i);
        channel_sm |= (tmp_sm << i);
        channel_smx |= (tmp_smx << i);
    } // i

    QString hex_start = "0x";
    QString sc_str =  hex_start + QString::number(channel_sc, 16);
    QString sl_str =  hex_start + QString::number(channel_sl, 16);
    QString sth_str = hex_start + QString::number(channel_sth, 16);
    QString st_str =  hex_start + QString::number(channel_st, 16);
    QString sm_str =  hex_start + QString::number(channel_sm, 16);
    QString smx_str = hex_start + QString::number(channel_smx, 16);

    ptree out_channel_registers;

    ptree out_channel_masks;
    out_channel_masks.put("sc",  sc_str.toStdString());
    out_channel_masks.put("sl",  sl_str.toStdString());
    out_channel_masks.put("sth", sth_str.toStdString());
    out_channel_masks.put("st",  st_str.toStdString());
    out_channel_masks.put("sm",  sm_str.toStdString());
    out_channel_masks.put("smx", smx_str.toStdString());
    out_channel_registers.add_child("channel_register_mask", out_channel_masks);


    stringstream chnode;
    for(int i = 0; i < 64; i++) {
        ptree out_channel;
        
        if((int)VMM_ChannelSettings(i).number != i) {
            stringstream sx;
            sx << "WARNING VMM Channel numbers out of sync! Expecting VMM channel "
               << i << " but at this index we have VMM " << VMM_ChannelSettings(i).number;
            msg()(sx, "ConfigHandlerVMM::WriteConfig"); sx.str("");
            sx << "WARNING Setting output config channel " << i << " to default values";
            msg()(sx, "ConfigHandlerVMM::WriteConfig"); sx.str("");
            channels_ok = false;
        }

        ss.str("");
        ss << "channel_" << format("%02i") % i; 

        // trim
        //chnode << ss.str() << ".sd";        
        chnode << "sd";
        out_channel.put(chnode.str(), VMM_ChannelSettings(i).sd);
        chnode.str("");

        // sz010b
        //chnode << ss.str() << ".sz010b";
        chnode << "sz010b";
        out_channel.put(chnode.str(), VMM_ChannelSettings(i).sz10b);
        chnode.str("");

        // sz08b
        //chnode << ss.str() << ".sz08b";
        chnode << "sz08b";
        out_channel.put(chnode.str(), VMM_ChannelSettings(i).sz8b);
        chnode.str("");

        // sz06b
        //chnode << ss.str() << ".sz06b";
        chnode << "sz06b";
        out_channel.put(chnode.str(), VMM_ChannelSettings(i).sz6b);
        chnode.str("");

	chnode << "baseline_adc";
	if ( VMMCalibSettings().chan_baselines.size() > i ) {
	  out_channel.put(chnode.str(), VMMCalibSettings().chan_baselines.at(i));
	}
	else {
          out_channel.put(chnode.str(), 0);
	}
        chnode.str("");

        chnode << "opt_trim_bits";
	if ( VMMCalibSettings().opt_trim_bits.size() >i ) {
          out_channel.put(chnode.str(), VMMCalibSettings().opt_trim_bits.at(i));
	}
	else {
          out_channel.put(chnode.str(), 0);
	}
        chnode.str("");

	for ( int j=0; j < 32; j++ ){
	  
	  chnode << "trimming_bit_" << j << "_SCA_ADC";

	  if ( VMMCalibSettings().chan_baseline_vs_trim.size() > i ) {
	    if ( VMMCalibSettings().chan_baseline_vs_trim.at(i).size() > j ) {
	      out_channel.put(chnode.str(), VMMCalibSettings().chan_baseline_vs_trim.at(i).at(j));
	    }
	    else {
	      out_channel.put(chnode.str(), -1);
	    }
	    chnode.str("");
	  }	  
	  else {
	    out_channel.put(chnode.str(), -1);
	  }
	  chnode.str("");

	}
        out_channel_registers.add_child(ss.str(), out_channel);
        //out_root.add_child("channel_registers", out_channel);
    } // i

    if ( !channels_ok ) {
      stringstream sx;
      sx << "ERROR: VMM channel writing xml failed ";
      msg()(sx, "ConfigHandlerVMM::WriteConfig"); sx.str("");
    }

    out_root.add_child("channel_registers", out_channel_registers);

    //--------------------------------------------------//

    // put everything under a global node
    outpt.add_child("configuration", out_root);

    stringstream sx;
    try {
        #if BOOST_VERSION >= 105800
        write_xml(filename.toStdString(), outpt, std::locale(),
		  boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
        write_xml(filename.toStdString(), outpt, std::locale(),
		  boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

        sx.str("");
        sx << "Configuration written successfully to file: \n";
        sx << " > " << filename.toStdString();
        msg()(sx,"ConfigHandlerVMM::WriteConfig");
    }
    catch(std::exception& e) {
        sx.str("");
        sx << "ERROR Unable to write output configuration XML file: " << e.what();
        msg()(sx,"ConfigHandlerVMM::WriteConfig");
        return;
    }

}
// ------------------------------------------------------------------------ //
VMMGlobalSetting ConfigHandlerVMM::LoadVMMGlobalSettings(const boost::property_tree::ptree& pt)
{
    using boost::property_tree::ptree;

    stringstream sx;

    VMMGlobalSetting g;

    bool outok = true;

    try{
        for(const auto& conf : pt.get_child("configuration")) {
            if(!(conf.first == "global_registers")) continue;

	    g.header       = conf.second.get<string>("header");
	    g.board_Type   = conf.second.get<int>("board_Type");
	    g.boardID      = conf.second.get<int>("boardID");
	    g.ASIC_Type    = conf.second.get<int>("ASIC_Type");
	    g.vmmmask      = conf.second.get<int>("vmmmask");
	    g.command      = conf.second.get<string>("command");

            // pulser DAC
            g.sdp_dac = conf.second.get<int>("sdp_dac");

            // threshold DAC
            g.sdt_dac = conf.second.get<int>("sdt_dac");

            // integration time
            int peak_time = conf.second.get<int>("st");
            if(VMMGlobalSetting::all_peakTimes.indexOf(peak_time)>=0) {
                g.st = VMMGlobalSetting::all_peakTimes.indexOf(peak_time);
            }
            else {
                sx << "ERROR Invalid peak time [st] requested in config file: " << peak_time << "\n";
                sx << "ERROR Peak time must be one of: ";
                for(auto& i : VMMGlobalSetting::all_peakTimes) sx << " " << i << " ";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str(""); 
                outok = false;
            }

            // sm5 (channel monitor/common mode)
            int ch = conf.second.get<int>("sm5");
            if(ch>=0 && ch<64) {
                g.sm5 = ch;
            }
            else {
                sx << "ERROR Invalid channel for sm5: " << ch << "\n";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // scmx
            int idx = 0;
            bool ok = isEnabledOrDisabled(conf.second.get<string>("scmx"), idx, "scmx", false);
            if(ok) {
                g.scmx = idx;
            }
            else {
                outok = false;
            }

            // sbmx
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sbmx"), idx, "sbmx", false);
            if(ok) {
                g.sbmx = idx;
            }
            else {
                outok = false;
            }

            // slg
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slg"), idx, "slg", true);
            if(ok) {
                g.slg = idx;
            }
            else {
                outok = false;
            }

            // sp
            std::string polarity = conf.second.get<string>("sp");
            if(polarity=="POSITIVE") {
                g.sp = 1;
            }
            else if(polarity=="NEGATIVE") {
                g.sp = 0;
            }
            else {
                sx << "ERROR Invalid channel polarity [sp] provided by config: " << polarity;
                sx << "\nERROR Expect either 'POSITIVE' or 'NEGATIVE'";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // sng
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sng"), idx, "sng", false);
            if(ok) {
                g.sng = idx;
            }
            else {
                outok = false;
            }

            // sdrv
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sdrv"), idx, "sdrv", false);
            if(ok) {
                g.sdrv = idx;
            }
            else {
                outok = false;
            }

            // sg
            string gain = conf.second.get<string>("sg");
            if(VMMGlobalSetting::all_gains.indexOf(QString::fromStdString(gain))>=0) {
                g.sg = VMMGlobalSetting::all_gains.indexOf(QString::fromStdString(gain));
            }
            else {
                sx << "ERROR Invalid gain [sg] value requested in config file: " << gain << "\n";
                sx << "ERROR Gain must be one of: ";
                for(auto& i : VMMGlobalSetting::all_gains) sx << " " << i.toStdString() << " ";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // stc
            int tac = conf.second.get<int>("stc");
            if(VMMGlobalSetting::all_TACslopes.indexOf(tac)>=0) {
                g.stc = VMMGlobalSetting::all_TACslopes.indexOf(tac);
            }
            else {
                sx << "ERROR Invalid TAC slope [stc] requested in config file: " << tac << "\n";
                sx << "ERROR TAC slope must be one of: ";
                for(auto& i : VMMGlobalSetting::all_TACslopes) sx << " " << i << " ";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
           } 

            // sfm
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sfm"), idx, "sfm", false);
            if(ok) {
                g.sfm = idx;
            }
            else {
                outok = false;
            }

            // ssh
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("ssh"), idx, "ssh", false);
            if(ok) {
                g.ssh = idx;
            }
            else {
                outok = false;
            }

            // sdp
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sdp"), idx, "sdp", false);
            if(ok) {
                g.sdp = idx;
            }
            else {
                outok = false;
            }

            // sbft
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sbft"), idx, "sbft", false);
            if(ok) {
                g.sbft = idx;
            }
            else {
                outok = false;
            }

            // sbfp
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sbfp"), idx, "sbfp", false);
            if(ok) {
                g.sbfp = idx;
            }
            else {
                outok = false;
            }

            // sbfm
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sbfm"), idx, "sbfm", false);
            if(ok) {
                g.sbfm = idx;
            }
            else {
                outok = false;
            }

            // s6b
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("s6b"), idx, "s6b", false);
            if(ok) {
                g.s6b = idx;
            }
            else {
                outok = false;
            }

            // s8b
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("s8b"), idx, "s8b", false);
            if(ok) {
	      g.s8b = idx;
            }
            else {
	      outok = false;
            }


            // s10b
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("s10b"), idx, "s10b", false);
            if(ok) {
                g.s10b = idx;
            }
            else {
                outok = false;
            }

            // sttt
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sttt"), idx, "sttt", false);
            if(ok) {
                g.sttt = idx;
            }
            else {
                outok = false;
            }

            // direct_timing_mode
            string timing_mode = conf.second.get<string>("direct_timing_mode");
            if(VMMGlobalSetting::all_directTimeModes.indexOf(QString::fromStdString(timing_mode))>=0) {
                QString mode = QString::fromStdString(timing_mode);
                int time_index = VMMGlobalSetting::all_directTimeModes.indexOf(mode);
                QString tmp = QString("%1").arg(time_index, 2, 2, QChar('0'));
                g.stpp = static_cast<int>(tmp.at(0).digitValue());
                g.stot = static_cast<int>(tmp.at(1).digitValue());
            }
            else {
                sx << "ERROR Invalid direct time mode requested in config file: " << timing_mode << "\n"; 
                sx << "ERROR Direct time mode must be one of: ";
                for(auto& i : VMMGlobalSetting::all_directTimeModes) sx << " " << i.toStdString() << " ";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // sfa
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sfa"), idx, "sfa", false);
            if(ok) {
                g.sfa = idx;
            }
            else {
                outok = false;
            }

            // sfam
            string art_detect_mode = conf.second.get<string>("sfam");
            if(art_detect_mode=="THRESHOLD") {
                g.sfam = 0;
            }
            else if(art_detect_mode=="PEAK") {
                g.sfam = 1;
            }
            else {
                sx << "ERROR Invalid ART detect mode [sfam] requested in config file: " << art_detect_mode << "\n";
                sx << "ERROR ART detect mode must be either 'THRESHOLD' or 'PEAK'";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // sc10b
            int conv = conf.second.get<int>("sc10b");
            if(conv==0 || conv==1 || conv==2 || conv==3) {
                g.sc10b = conv;
            }
            else {
                sx << "ERROR Invalid sc10b value requested in config file: " << conv << "\n";
                sx << "ERROR Value can only take 0 or 1";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }
            
            // sc08b
            conv = conf.second.get<int>("sc08b");
            if(conv==0 || conv==1 || conv==2 || conv==3) {
                g.sc8b = conv;
            }
            else {
                sx << "ERROR Invalid sc08b value requested in config file: " << conv << "\n";
                sx << "ERROR Value can only take 0 or 1";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }
            
            // sc06b
            conv = conf.second.get<int>("sc06b");
            if(conv==0 || conv==1 || conv==2) {
                g.sc6b = conv;
            }
            else {
                sx << "ERROR Invalid sc06b value requested in config file: " << conv << "\n";
                sx << "ERROR Value can only take 0, 1, or 2";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // sdcks
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sdcks"), idx, "sdcks", false);
            if(ok) {
                g.sdcks = idx;
            }
            else {
                outok = false;
            }

            // sdcka
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sdcka"), idx, "sdcka", false);
            if(ok) {
                g.sdcka = idx;
            }
            else {
                outok = false;
            }

            // sdck6b
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sdck6b"), idx, "sdck6b", false);
            if(ok) {
                g.sdck6b = idx;
            }
            else {
                outok = false;
            }

            // slvs
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvs"), idx, "slvs", false);
            if(ok) {
                g.slvs = idx;
            }
            else {
                outok = false;
            }

            // stlc
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("stlc"), idx, "stlc", false);
            if(ok) {
                g.stlc = idx;
            }
            else {
                outok = false;
            }

            // stcr
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("stcr"), idx, "stcr", false);
            if(ok) {
                g.stcr = idx;
            }
            else {
                outok = false;
            }

            // ssart
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("ssart"), idx, "ssart", false);
            if(ok) {
                g.ssart = idx;
            }
            else {
                outok = false;
            }

            // srec
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("srec"), idx, "srec", false);
            if(ok) {
                g.srec = idx;
            }
            else {
                outok = false;
            }

            // sfrst
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sfrst"), idx, "sfrst", false);
            if(ok) {
                g.sfrst = idx;
            }
            else {
                outok = false;
            }

            // s32
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("s32"), idx, "s32", false);
            if(ok) {
                g.s32 = idx;
            }
            else {
                outok = false;
            }

            // sbip
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sbip"), idx, "sbip", false);
            if(ok) {
                g.sbip = idx;
            }
            else {
                outok = false;
            }

            // srat
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("srat"), idx, "srat", false);
            if(ok) {
                g.srat = idx;
            }
            else {
                outok = false;
            }

            // slvsbc
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvsbc"), idx, "slvsbc", false);
            if(ok) {
                g.slvsbc = idx;
            }
            else {
                outok = false;
            }

            // slvsart
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvsart"), idx, "slvsart", false);
            if(ok) {
                g.slvsart = idx;
            }
            else {
                outok = false;
            }

            // slvstp
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvstp"), idx, "slvstp", false);
            if(ok) {
                g.slvstp = idx;
            }
            else {
                outok = false;
            }

            // slvstki
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvstki"), idx, "slvstki", false);
            if(ok) {
                g.slvstki = idx;
            }
            else {
                outok = false;
            }

            // slvstk
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvstk"), idx, "slvstk", false);
            if(ok) {
                g.slvstk = idx;
            }
            else {
                outok = false;
            }

            // slvsena
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvsena"), idx, "slvsena", false);
            if(ok) {
                g.slvsena = idx;
            }
            else {
                outok = false;
            }

            // slvsdt
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvsdt"), idx, "slvsdt", false);
            if(ok) {
                g.slvsdt = idx;
            }
            else {
                outok = false;
            }

            // slvs6b
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("slvs6b"), idx, "slvs6b", false);
            if(ok) {
                g.slvs6b = idx;
            }
            else {
                outok = false;
            }

            // sL0ena
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sL0ena"), idx, "sL0ena", false);
            if(ok) {
                g.sL0ena = idx;
            }
            else {
                outok = false;
            }

            // sL0enaV
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sL0enaV"), idx, "sL0enaV", false);
            if(ok) {
                g.sL0enaV = idx;
            }
            else {
                outok = false;
            }

            // l0offset
            int offset = conf.second.get<int>("l0offset");
            if(offset>=0 && offset<4096) {
                g.l0offset = offset;
            }
            else {
                sx << "ERROR Invalid [l0offset] value requested in config file: " << offset << "\n";
                sx << "ERROR [l0offset] must take values [0,4095] inclusively";
                msg()(sx, "ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // offset
            offset = conf.second.get<int>("offset");
            if(offset>=0 && offset<4096) {
                g.offset = offset;
            }
            else {
                sx << "ERROR Invalid [offset] value requested in config file: " << offset << "\n";
                sx << "ERROR [offset] must take values [0,4095] inclusively";
                msg()(sx, "ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // rollover
            int rollover = conf.second.get<int>("rollover");
            if(rollover>=0 && rollover<4096) {
                g.rollover = rollover;
            }
            else {
                sx << "ERROR Invalid [rollover] value requested in config file: " << offset << "\n";
                sx << "ERROR [rollover] must take values [0,4095] inclusively";
                msg()(sx, "ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // window
            int window = conf.second.get<int>("window");
            if(window>=0 && window<8) {
                g.window = window;
            }
            else {
                sx << "ERROR Invalid trigger window size [window] requested in "
                        << "config file: " << window << "\n";
                sx << "ERROR Window size [window] must take values [0,7] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // truncate
            int truncate = conf.second.get<int>("truncate");
            if(truncate>=0 && truncate<64) {
                g.truncate = truncate;
            }
            else {
                sx << "ERROR Max hits per L0 [truncate] requested in "
		   << "config file is invalid: " << truncate << "\n";
                sx << "ERROR [truncate] must take values [0,63] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            // nskip
            int nskip = conf.second.get<int>("nskip");
            if(nskip>=0 && nskip<128) {
                g.nskip = nskip;
            }
            else {
                sx << "ERROR Number of L0 to skip on o'flow [nskip] requested in "
                    << "config file is invalid: " << nskip << "\n";
                sx << "ERROR [nskip] must take values [0,127] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings"); sx.str("");
                outok = false;
            }

            int reset = conf.second.get<int>("reset");
	    g.reset = reset;

            // sL0cktest
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sL0cktest"), idx, "sL0cktest", false);
            if(ok) {
                g.sL0cktest = idx;
            }
            else {
                outok = false;
            }

            // sL0ckinv
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sL0ckinv"), idx, "sL0ckinv", false);
            if(ok) {
                g.sL0ckinv = idx;
            }
            else {
                outok = false;
            }

            // sL0dckinv
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("sL0dckinv"), idx, "sL0dckinv", false);
            if(ok) {
                g.sL0dckinv = idx;
            }
            else {
                outok = false;
            }

            // nskipm
            idx = 0;
            ok = isEnabledOrDisabled(conf.second.get<string>("nskipm"), idx, "nskipm", false);
            if(ok) {
                g.nskipm = idx;
            }
            else {
                outok = false;
            }
            
        } // conf

    } // try
    catch(std::exception &e){
        stringstream sx;
        sx << "Exception in loading global registers: " << e.what();
        msg()(sx,"ConfigHandlerVMM::LoadGlobalSettings");
        outok = false;
    } // catch

    g.ok = outok;

    m_vmm_globalSettings = g;

    return g;
}
// -------------------------------------------------------------------------- //
void ConfigHandlerVMM::SetBoardID( int boardID ){
  m_vmm_globalSettings.boardID = boardID;
}
void ConfigHandlerVMM::SetBoardType( int boardType ){
  m_vmm_globalSettings.board_Type = boardType;
}

// -------------------------------------------------------------------------- //
QString ConfigHandlerVMM::VMMMask(){

  QString MapString = "0000000000000000";
  QString str;
  int pos = 0;

  str = QString("%1").arg(m_vmm_globalSettings.board_Type, 2, 2, QChar('0'));
  MapString.replace(1, str.size(), str);
  pos+=1+str.size();

  str = QString("%1").arg(m_vmm_globalSettings.boardID, 3, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(m_vmm_globalSettings.ASIC_Type, 2, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  /*
  if ( m_vmm_globalSettings.vmmmask == 1 ) str = "00000001";
  if ( m_vmm_globalSettings.vmmmask == 2 ) str = "00000010";
  if ( m_vmm_globalSettings.vmmmask == 3 ) str = "00000100";
  if ( m_vmm_globalSettings.vmmmask == 4 ) str = "00001000";
  if ( m_vmm_globalSettings.vmmmask == 5 ) str = "00010000";
  if ( m_vmm_globalSettings.vmmmask == 6 ) str = "00100000";
  if ( m_vmm_globalSettings.vmmmask == 7 ) str = "01000000";
  if ( m_vmm_globalSettings.vmmmask == 8 ) str = "10000000";
  */

  str = QString("%1").arg(m_vmm_globalSettings.vmmmask, 8, 2, QChar('0'));

  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  return MapString;

}
//// ------------------------------------------------------------------------ //
VMMCalibSetting ConfigHandlerVMM::LoadVMMCalibConfig( const boost::property_tree::ptree& pt )
{

  stringstream ss;

  using boost::property_tree::ptree;
  using boost::format;

  VMMCalibSetting g;

  int channel_reg = true;

  std::vector<uint32_t> baseline_adcs;
  std::vector<uint32_t> opt_trim_bits;
  std::vector<std::vector<uint32_t>> chan_baseline_vs_trim;

  baseline_adcs.resize(0);
  opt_trim_bits.resize(0);
  chan_baseline_vs_trim.resize(0);

  bool outok;

  try {
    for(int iChan = 0; iChan < 64; iChan++) {

      //      std::cout << "looking for chan " << iChan << std::endl;

      bool channel_ok = true;
      ss.str("");
      ss << "channel_" << format("%02i") % iChan;
      for(const auto& conf : pt.get_child("configuration.channel_registers")) {

	size_t find = conf.first.find(ss.str());
	if(find==string::npos) continue;

	uint32_t baseline_adc = conf.second.get<uint32_t>("baseline_adc");
	uint32_t opt_trim_bit = conf.second.get<uint32_t>("opt_trim_bits");

	std::vector<uint32_t> ichan_baseline_vs_trim;
	ichan_baseline_vs_trim.resize(0);
	
	for ( int j=0; j<32; j++ ) {

	  //std::cout << "looking for itrim" << std::endl;

	  ss.str("");
	  ss << "trimming_bit_" << j << "_SCA_ADC";
	  
	  int trimming_j_adc = conf.second.get<int>(ss.str());
	  
	  if ( trimming_j_adc >= 0 ) {
	    ichan_baseline_vs_trim.push_back( trimming_j_adc );
	  }
	  else {
	    break;
	  }

	} // i trimming

	baseline_adcs.push_back(baseline_adc);
	opt_trim_bits.push_back(opt_trim_bit);
	chan_baseline_vs_trim.push_back(ichan_baseline_vs_trim);

      } // i channel

    } // all channels

  } // end try
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading calib registers: " << e.what();
    msg()(sx,"ConfigHandlerVMM::LoadCalibSettings");
    outok = false;
  } // catch 

  g.chan_baselines =baseline_adcs;
  g.opt_trim_bits  =opt_trim_bits;
  g.chan_baseline_vs_trim = chan_baseline_vs_trim;
  g.ok = outok;

  stringstream sx;
  sx << "loaded calib registers ";
  msg()(sx,"ConfigHandlerVMM::LoadCalibSettings");

  m_vmm_calibSettings = g;

  return g;

}
std::vector<VMM_Channel> ConfigHandlerVMM::LoadVMMChannelConfig(const boost::property_tree::ptree& pt)
{
    using boost::property_tree::ptree;
    using boost::format;

    // clear the current channels
    m_vmm_channels.clear();
    std::vector<VMM_Channel> channels;
    stringstream sx;
    stringstream ss;
    stringstream where;

    uint64_t channel_sc = 0;
    uint64_t channel_sl = 0;
    uint64_t channel_sth = 0;
    uint64_t channel_st = 0;
    uint64_t channel_sm = 0;
    uint64_t channel_smx = 0;

    int channel_reg = true;

    try {
        for(const auto& conf : pt.get_child("configuration.channel_registers")) {
            if(!(conf.first == "channel_register_mask")) continue;

            // sc
            string sc_ = conf.second.get<string>("sc");
            QString sc_hex = QString::fromStdString(sc_); 
            bool ok;
            uint64_t sc = sc_hex.toULong(&ok,16);
            if(sc<=0xffffffffffffffff) {
                channel_sc = sc;
            }
            else {
                sx.str("");
                sx << "ERROR Invalid [sc] in requested configuration file: " << sc << "\n";
                sx << "ERROR [sc] takes an 8-bit value, [0,255] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                channel_reg = false;
            }

            // sl
            string sl_ = conf.second.get<string>("sl");
            QString sl_hex = QString::fromStdString(sl_);
            uint64_t sl = sl_hex.toULong(&ok,16);
            if(sl<=0xffffffffffffffff) {
                channel_sl = sl;
            }
            else {
                sx.str("");
                sx << "ERROR Invalid [sl] in requested configuration file: " << sc << "\n";
                sx << "ERROR [sl] takes an 8-bit value, [0,255] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                channel_reg = false;
            }

            // sth
            string sth_ = conf.second.get<string>("sth");
            QString sth_hex = QString::fromStdString(sth_);
            uint64_t sth = sth_hex.toULong(&ok,16);
            if(sth<=0xffffffffffffffff) {
                channel_sth = sth;
            }
            else {
                sx.str("");
                sx << "ERROR Invalid [sth] in requested configuration file: " << sc << "\n";
                sx << "ERROR [sth] takes an 8-bit value, [0,255] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                channel_reg = false;
            }

            // st
            string st_ = conf.second.get<string>("st");
            QString st_hex = QString::fromStdString(st_);
            uint64_t st = st_hex.toULong(&ok,16);
            if(st<=0xffffffffffffffff) {
                channel_st = st;
            }
            else {
                sx.str("");
                sx << "ERROR Invalid [st] in requested configuration file: " << sc << "\n";
                sx << "ERROR [st] takes an 8-bit value, [0,255] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                channel_reg = false;
            }

            // sm
            string sm_ = conf.second.get<string>("sm");
            QString sm_hex = QString::fromStdString(sm_);
            uint64_t sm = sm_hex.toULong(&ok,16);
            if(sm<=0xffffffffffffffff) {
                channel_sm = sm;
            }
            else {
                sx.str("");
                sx << "ERROR Invalid [sm] in requested configuration file: " << sc << "\n";
                sx << "ERROR [sm] takes an 8-bit value, [0,255] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                channel_reg = false;
            }

            // smx
            string smx_ = conf.second.get<string>("smx");
            QString smx_hex = QString::fromStdString(smx_);
            uint64_t smx = smx_hex.toULong(&ok,16);
            if(smx<=0xffffffffffffffff) {
                channel_smx = smx;
            }
            else {
                sx.str("");
                sx << "ERROR Invalid [smx] in requested configuration file: " << sc << "\n";
                sx << "ERROR [smx] takes an 8-bit value, [0,255] inclusively";
                msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                channel_reg = false;
            }
                
        } // conf

    } // try
    catch(std::exception& e) {
        sx.str("");
        sx << "ERROR Loading VMM registers from requested config file, exception thrown: "
            << e.what();
        msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
        for(int ichan = 0; ichan < (int)channels.size(); ichan++) {
            channels.at(ichan).ok = false;
        }
    } // catch


    try {
        for(int iChan = 0; iChan < 64; iChan++) {
            bool channel_ok = true;
            ss.str(""); 
            ss << "channel_" << format("%02i") % iChan;
            for(const auto& conf : pt.get_child("configuration.channel_registers")) {
                size_t find = conf.first.find(ss.str());
                if(find==string::npos) continue;
                where.str("");
                where << "(ch. " << format("%02i") % iChan;
                string value = "";

                VMM_Channel chan;
                chan.number = iChan;


                // sd
                int trim = conf.second.get<int>("sd");
                if(trim>=0 && trim<32) {
                    chan.sd = trim;
                }
                else {
                    sx.str("");
                    sx << "ERROR Trim [sd] value for channel " << iChan << " invalid: " << trim << "\n";
                    sx << "ERROR Trim [sd] may take values between [0,31] inclusively";
                    msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                    channel_ok = false;
                }

                // sz010b
                int sz10 = conf.second.get<int>("sz010b");
                if(sz10>=0 && sz10<32) {
                    chan.sz10b = sz10;
                }
                else {
                    sx.str("");
                    sx << "ERROR 10 bit ADC zero [sz010b] value for channel " << iChan << " invalid: " << sz10 << "\n";
                    sx << "ERROR [sz010b] may take values between [0,31] inclusively";
                    msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                    channel_ok = false;
                }

                // sz08b
                int sz8 = conf.second.get<int>("sz08b");
                if(sz8>=0 && sz8<16) {
                    chan.sz8b = sz8;
                }
                else {
                    sx.str("");
                    sx << "ERROR 8 bit ADC zero [sz08b] value for channel " << iChan << " invalid: " << sz8 << "\n";
                    sx << "ERROR [sz08b] may take values between [0,15] inclusively";
                    msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                    channel_reg = false;
                }
                
                // sz06b
                int sz6 = conf.second.get<int>("sz06b");
                if(sz6>=0 && sz6<8) {
                    chan.sz6b = sz6;
                }
                else {
                    sx.str("");
                    sx << "ERROR 8=6 bit ADC zero [sz06b] value for channel " << iChan << " invalid: " << sz6 << "\n";
                    sx << "ERROR [sz06b] may take values between [0,7] inclusively";
                    msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
                    channel_ok = false;
                }

                chan.ok = channel_ok;
                channels.push_back(chan);
            } // conf
        } // iChan

    } // try
    catch(std::exception& e) {
        sx.str("");
        sx << "ERROR Loading VMM registers from requested config file, exception thrown: "
            << e.what();
        msg()(sx,"ConfigHandlerVMM::LoadVMMChannelConfig"); sx.str("");
        for(int ichan = 0; ichan < (int)channels.size(); ichan++) {
            channels.at(ichan).ok = false;
        }
    } // catch

    if(!channel_reg || channels.size()!=64) {
        for(auto& chan : channels) chan.ok = false;
    }
    else {
        for(int i = 0; i < 64; i++) {
            for(auto& chan : channels) {
                if(!(chan.number==i)) continue;
                uint64_t tmp = 1;
                tmp = (tmp << i);

                chan.sc = (channel_sc & tmp) >> i;
                chan.sl = (channel_sl & tmp) >> i;
                chan.sth = (channel_sth & tmp) >> i;
                chan.st = (channel_st & tmp) >> i;
                chan.sm = (channel_sm & tmp) >> i;
                chan.smx = (channel_smx & tmp) >> i;

                chan.ok = true;

            } // chan
        } 
    }

    m_vmm_channels = channels;

    return channels;

}
//// ------------------------------------------------------------------------ //
void ConfigHandlerVMM::LoadVMMChipConfiguration(VMMGlobalSetting& global,
						std::vector<VMM_Channel>& channels)
{
    m_vmm_globalSettings = global;
    m_vmm_channels.clear();
    for(int i = 0; i < (int)channels.size(); i++) {
        m_vmm_channels.push_back(channels[i]);
    }
    //m_vmm_globalSettings.print();
}
//// ------------------------------------------------------------------------ //
bool ConfigHandlerVMM::EnabledOrDisabled(int ena_or_dis, std::string& value, std::string from, bool invert)
{
    stringstream sx;
    int is_enabled = 1;
    int is_disabled = 0;
    if(invert) {
        is_enabled = 0;
        is_disabled = 1;
    }
    if(ena_or_dis==is_enabled) {
        value = "ENABLED";
    }
    else if(ena_or_dis==is_disabled) {
        value = "DISABLED";
    }
    else {
        string where = "";
        if(from!="") where = " [for " + from + "]";
        sx << "ERROR Invalid input, expecting 0 or 1 but you have: " << ena_or_dis;
        msg()(sx,"ConfigHandlerVMM::EnabledOrDisabled"); sx.str("");
        value = "";
        return false;
    }
    return true;
}
//// ------------------------------------------------------------------------ //
bool ConfigHandlerVMM::isEnabledOrDisabled(std::string ena_or_dis, int& value, std::string from, bool invert)
{
    stringstream sx;
    int is_enabled = 1;
    int is_disabled = 0;
    if(invert) {
        is_enabled = 0;
        is_disabled = 1;
    }

    if(ena_or_dis=="ENABLED") {
        value = is_enabled;
    }
    else if(ena_or_dis=="DISABLED") {
        value = is_disabled;
    }
    else {
        std::string where = "";
        if(from!="") where = " [for " + from + "]";
        sx << "ERROR Expecting only 'ENABLED' or 'DISABLED' " << where << " but your config provides: "
           << ena_or_dis;
        msg()(sx,"ConfigHandlerVMM::isEnabledOrDisabled"); sx.str("");
        return false; 
    }
    return true;
}
//// ------------------------------------------------------------------------ //
int ConfigHandlerVMM::isOn(std::string onOrOff, std::string where)
{
    stringstream sx;

    if(onOrOff=="on" || onOrOff=="disabled")
    //if(onOrOff=="on" || onOrOff=="enabled")
        return 1;
    else if(onOrOff=="off" || onOrOff=="enabled")
    //else if(onOrOff=="off" || onOrOff=="disabled")
        return 0;
    else {
        std::string from = "";
        if(where!="") from = " [for " + where + "]";
        sx << "Expecting either 'on'/'enabled' or 'off'/'disabled'" << from << ".\n"
           << ">>> Returning 'off'/'disabled'.";
        msg()(sx,"ConfigHandlerVMM::isOn");
        return 0;
    } 
}
//// ------------------------------------------------------------------------ //
std::string ConfigHandlerVMM::isOnOrOff(int onOrOff, std::string node)
{
    stringstream sx;

    if(onOrOff==1)
        return "on";
    else if(onOrOff==0)
        return "off";
    else {
        sx << "WARNING Expect only '0' or '1' as input. You have provided: " << onOrOff;
        msg()(sx,"ConfigHandlerVMM::isOnOrOff"); sx.str("");
        sx << "WARNING Setting node \"" << node << "\" to \"off\"";
        msg()(sx,"ConfigHandlerVMM::isOnOrOff"); sx.str("");
        string out = "off";
        return out;
    }
}
//// ------------------------------------------------------------------------ //
std::string ConfigHandlerVMM::isEnaOrDis(int enaOrDis, std::string node)
{
    stringstream sx;
    if(enaOrDis==0)
    //if(enaOrDis==1)
        return "enabled";
    else if(enaOrDis==1)
    //else if(enaOrDis==0)
        return "disabled";
    else {
        sx << "WARNING Expect only '0' or '1' as input. You have provided: " << enaOrDis;
        msg()(sx,"ConfigHandlerVMM::isEnaOrDis"); sx.str("");
        sx << "WARNING Setting node \"" << node << "\" to \"disabled\"";
        msg()(sx, "ConfigHandlerVMM::isEnaOrDis"); sx.str("");
        string out = "disabled";
        return out;
    }
}
