// vmm
#include "config_handler_tds.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>



//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandlerTDS -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerTDS::ConfigHandlerTDS(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    //    m_board_selection(-1),
    m_msg(0)
{
    m_tds_channels.clear();
}
//// ------------------------------------------------------------------------ //
/*
void ConfigHandlerTDS::setSelectedBoards(int board_selection)
{
    m_board_selection = board_selection;
}
*/
//// ------------------------------------------------------------------------ //
void ConfigHandlerTDS::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerTDS::SetBoardID( int boardID ){
  m_tds_globalSettings.boardID = boardID;
}
void ConfigHandlerTDS::SetBoardType( int boardType ){
  m_tds_globalSettings.board_Type = boardType;
}

bool ConfigHandlerTDS::LoadTDSConfig_from_File(const QString &filename)
{

  //  m_commSettings.config_filename = filename;

  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree pt;
  read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);

  // load the communication settings (UDP ports, board IPs, etc...)
  /*
    m_commSettings = LoadCommInfo(pt);
    bool comm_ok = m_commSettings.ok;
    if(!comm_ok) {
        msg()("Problem loading CommInfo", "ConfigHandlerTDS::LoadConfig");
    }
  */

  // Load the global configuration
  m_tds_globalSettings = LoadTDSGlobalSettings(pt);
  bool global_ok = m_tds_globalSettings.ok;
  if(!global_ok) {
    msg()("Problem loading GlobalSettings", "ConfigHandlerTDS::LoadConfig");
  }

  // Load the VMM channel configuration
  m_tds_channels = LoadTDSChannelConfig(pt);
  bool tdschan_ok = (m_tds_channels.size() ? true : false);
  for(int ichan = 0; ichan < (int)m_tds_channels.size(); ichan++) {
    bool chan_ok = m_tds_channels.at(ichan).ok;
    if(!chan_ok) {
      stringstream sx;
      sx << "Problem loading channel " << ichan;
      msg()(sx, "ConfigHandlerTDS::LoadConfig");
      tdschan_ok = false;
      break;
    }
  }

  m_tds_luts = LoadTDSLutConfig(pt);
  bool tdslut_ok = (m_tds_luts.size() ? true : false);
  for(int ilut = 0; ilut < (int)m_tds_luts.size(); ilut++) {
    bool lut_ok = m_tds_luts.at(ilut).ok;
    if(!lut_ok) {
      stringstream sx;
      sx << "Problem loading lut " << ilut;
      msg()(sx, "ConfigHandlerTDS::LoadConfig");
      tdslut_ok = false;
      break;
    }
  }

  if(!( global_ok && tdschan_ok && tdslut_ok) ) { //daq_ok && m_vmmMap.ok && vmmchan_ok && clocks_ok)) {
    msg()("Configuration loaded unsucessfully","ConfigHandlerTDS::LoadConfig");
    //if(!comm_ok)        cout << "comm_ok" << endl;
    if(!global_ok)      cout << "global_ok" << endl;
    //if(!daq_ok)         cout << "daq_ok" << endl;
    //if(!m_vmmMap.ok)    cout << "m_vmmMap" << endl;
    if(!tdschan_ok)     cout << "tdschan_ok" << endl;
    //if(!clocks_ok)      cout << "clocks_ok" << endl;
    if(!tdslut_ok)  cout << "tdslut_ok" << endl;
  }

  return (global_ok && tdschan_ok && tdslut_ok ); //daq_ok && m_vmmMap.ok && vmmchan_ok && clocks_ok);
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerTDS::WriteTDSConfig_to_File(QString filename)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;   

  ptree out_root;

  // ----------------------------------------------- //
  //  global settings
  // ----------------------------------------------- //
  //  bool global_ok = true;

  ptree out_global;
  out_global.put("header",          TDSGlobalSettings().header);
  out_global.put("board_Type",      TDSGlobalSettings().board_Type);
  out_global.put("boardID",         TDSGlobalSettings().boardID);
  out_global.put("ASIC_Type",       TDSGlobalSettings().ASIC_Type);
  out_global.put("chipID",          TDSGlobalSettings().chipID);
  out_global.put("command",         TDSGlobalSettings().command);

  out_global.put("bcid_offset",     TDSGlobalSettings().bcid_offset);
  out_global.put("bcid_rollover",   TDSGlobalSettings().bcid_rollover);
  out_global.put("bcid_clockphase", TDSGlobalSettings().bcid_clockphase);
  out_global.put("match_window",    TDSGlobalSettings().match_window);
  out_global.put("vmm0_clockphase", TDSGlobalSettings().vmm0_clockphase);
  out_global.put("vmm1_clockphase", TDSGlobalSettings().vmm1_clockphase);
  out_global.put("SER_PLL_current", TDSGlobalSettings().SER_PLL_current);
  out_global.put("SER_PLL_resistor",TDSGlobalSettings().SER_PLL_resistor);
  out_global.put("soft_resets",     TDSGlobalSettings().soft_resets);
  out_global.put("reject_window",   TDSGlobalSettings().reject_window);
  out_global.put("bypass_prompt",   TDSGlobalSettings().bypass_prompt);
  out_global.put("prompt_circuit",  TDSGlobalSettings().prompt_circuit);
  out_global.put("bypass_trigger",  TDSGlobalSettings().bypass_trigger);
  out_global.put("bypass_scrambler",TDSGlobalSettings().bypass_scrambler);
  out_global.put("test_frame2Router_enable",TDSGlobalSettings().test_frame2Router_enable);
  out_global.put("stripTDS_globaltest",TDSGlobalSettings().stripTDS_globaltest);
  out_global.put("PRBS_en",         TDSGlobalSettings().PRBS_en);
  
  // ----------------------------------------------- //
  //  Clocks settings
  // ----------------------------------------------- //

  // ----------------------------------------------- //
  // stitch together the fields
  // ----------------------------------------------- //
  out_root.add_child("global_registers", out_global);


  // ----------------------------------------------- //
  //  individual VMM channels
  // ----------------------------------------------- //
  // get the "global" channel masks
  ptree out_channel_registers;

  bool channels_ok = true;

  stringstream chnode;
  for(int i = 0; i < 128; i++) {
    ptree out_channel;
        
    if((int)TDS_ChannelSettings(i).number != i) {
      stringstream sx;
      sx << "WARNING TDS Channel numbers out of sync! Expecting TDS channel "
	 << i << " but at this index we have TDS " << TDS_ChannelSettings(i).number;
      msg()(sx, "ConfigHandlerTDS::WriteConfig"); sx.str("");
      sx << "WARNING Setting output config channel " << i << " to default values";
      msg()(sx, "ConfigHandlerTDS::WriteConfig"); sx.str("");
      channels_ok = false;
    }

    ss.str("");
    ss << "channel_" << format("%03i") % i; 

    // trim
    //chnode << ss.str() << ".sd";        
    chnode << "enabled";
    out_channel.put(chnode.str(), TDS_ChannelSettings(i).enabled);
    chnode.str("");

    // sz010b
    //chnode << ss.str() << ".sz010b";
    chnode << "delay";
    out_channel.put(chnode.str(), TDS_ChannelSettings(i).delay);
    chnode.str("");

    out_channel_registers.add_child(ss.str(), out_channel);

  } // i

  if ( !channels_ok ) {
    stringstream sx;
    sx << "ERROR: TDS Channel writing xml failed ";
    msg()(sx, "ConfigHandlerTDS::WriteConfig"); sx.str("");
  }


  out_root.add_child("channel_registers", out_channel_registers);

  //-----------------------------------------------------------------------//

  bool tds_lut_ok = true;

  ptree out_lut_registers;

  for(int i = 0; i < 16; i++) {
    ptree out_lut;

    if((int)TDS_LUTSettings(i).number != i) {
      stringstream sx;
      sx << "WARNING TDS LUT numbers out of sync! Expecting TDS LUT "
         << i << " but at this index we have TDS " << TDS_LUTSettings(i).number;
      msg()(sx, "ConfigHandlerTDS::WriteConfig"); sx.str("");
      sx << "WARNING Setting output config channel " << i << " to default values";
      msg()(sx, "ConfigHandlerTDS::WriteConfig"); sx.str("");
      tds_lut_ok = false;
    }

    ss.str("");
    ss << "lut_" << format("%02i") % i;

    // trim                                                                                   
    //chnode << ss.str() << ".sd";                                                            
    chnode << "BandID";
    out_lut.put(chnode.str(), TDS_LUTSettings(i).BandID);
    chnode.str("");

    // sz010b                                                                                 
    //chnode << ss.str() << ".sz010b";                                                        
    chnode << "LeadStrip";
    out_lut.put(chnode.str(), TDS_LUTSettings(i).LeadStrip);
    chnode.str("");

    out_lut_registers.add_child(ss.str(), out_lut);

  } // i                                                                                      

  if ( !tds_lut_ok ) {
    stringstream sx;
    sx << "ERROR: TDS LUT writing xml failed ";
    msg()(sx, "ConfigHandlerTDS::WriteConfig"); sx.str("");
  }

  out_root.add_child("lut_registers", out_lut_registers);

  //------------------------------------------------------------------------//

  // add the rest of the nodes
  //    out_root.add_child("udp_setup", out_udpsetup);
  //    out_root.add_child("trigger_daq", out_tdaq);
  //    out_root.add_child("fpga_clocks", out_clocks);
  //    out_root.add_child("vmm_map", out_vmm);

  // put everything under a global node
  outpt.add_child("configuration", out_root);


  stringstream sx;
  try {
        #if BOOST_VERSION >= 105800
    write_xml(filename.toStdString(), outpt, std::locale(),
	      boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
    write_xml(filename.toStdString(), outpt, std::locale(),
	      boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

    sx.str("");
    sx << "Configuration written successfully to file: \n";
    sx << " > " << filename.toStdString();
    msg()(sx,"ConfigHandlerTDS::WriteConfig");
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Unable to write output configuration XML file: " << e.what();
    msg()(sx,"ConfigHandlerTDS::WriteConfig");
    return;
  }

}
//// ------------------------------------------------------------------------ //
TDSGlobalSetting ConfigHandlerTDS::LoadTDSGlobalSettings(const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;

  stringstream sx;

  TDSGlobalSetting g;

  bool outok = true;

  try{
    for(const auto& conf : pt.get_child("configuration")) {
      if(!(conf.first == "global_registers")) continue;

      g.header          = conf.second.get<string>("header");
      g.board_Type      = conf.second.get<int>("board_Type");
      g.boardID         = conf.second.get<int>("boardID");
      g.ASIC_Type       = conf.second.get<int>("ASIC_Type");
      g.chipID          = conf.second.get<int>("chipID");
      g.command         = conf.second.get<string>("command");

      g.bcid_offset     = conf.second.get<int>("bcid_offset");
      g.bcid_rollover   = conf.second.get<int>("bcid_rollover");
      g.bcid_clockphase = conf.second.get<int>("bcid_clockphase");
      g.match_window = conf.second.get<int>("match_window");
      g.vmm0_clockphase = conf.second.get<int>("vmm0_clockphase");
      g.vmm1_clockphase = conf.second.get<int>("vmm1_clockphase");
      g.SER_PLL_current = conf.second.get<int>("SER_PLL_current");
      g.SER_PLL_resistor = conf.second.get<int>("SER_PLL_resistor");
      g.soft_resets = conf.second.get<int>("soft_resets");
      g.reject_window = conf.second.get<int>("reject_window");
      g.bypass_prompt = conf.second.get<int>("bypass_prompt");
      g.prompt_circuit = conf.second.get<int>("prompt_circuit");
      g.bypass_trigger = conf.second.get<int>("bypass_trigger");
      g.bypass_scrambler = conf.second.get<int>("bypass_scrambler");
      g.test_frame2Router_enable = conf.second.get<int>("test_frame2Router_enable");
      g.stripTDS_globaltest = conf.second.get<int>("stripTDS_globaltest");
      g.PRBS_en = conf.second.get<int>("PRBS_en");

    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading tds global registers: " << e.what();
    msg()(sx,"ConfigHandlerTDS::LoadTDSGlobalSettings");
    outok = false;
  } // catch                                                                                       

  g.ok = outok;

  m_tds_globalSettings = g;

  return g;

}
//// ------------------------------------------------------------------------ //                     
std::vector<TDS_Channel> ConfigHandlerTDS::LoadTDSChannelConfig(const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;
  using boost::format;

  // clear the current channels                                                                    
  m_tds_channels.clear();
  std::vector<TDS_Channel> channels;
  stringstream sx;
  stringstream ss;
  stringstream where;

  try {
    for(int iChan = 0; iChan < 128; iChan++) {
      bool channel_ok = true;
      ss.str("");
      ss << "channel_" << format("%03i") % iChan;
      for(const auto& conf : pt.get_child("configuration.channel_registers")) {
	size_t find = conf.first.find(ss.str());
	if(find==string::npos) continue;
	where.str("");
	where << "(ch. " << format("%03i") % iChan;
	string value = "";

	TDS_Channel chan;
	chan.number = iChan;

	bool trim;
	trim = conf.second.get<bool>("enabled");
	chan.enabled = trim;

	int val;
	val = conf.second.get<int>("delay");
        chan.delay = val;

	chan.ok = channel_ok;
	channels.push_back(chan);

	//sx.str("");
	//sx << "ichan: " << iChan << " " << trim << " " << val << "\n";
	//msg()(sx,"ConfigHandlerTDS::LoadTDSChannelConfig"); sx.str("");


      }
    }
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Loading TDS registers from requested config file, exception thrown: "
       << e.what();
    msg()(sx,"ConfigHandlerTDS::LoadTDSChannelConfig"); sx.str("");
    for(int ichan = 0; ichan < (int)channels.size(); ichan++) {
      channels.at(ichan).ok = false;
    }
  } // catch

  m_tds_channels = channels;

  return channels;

}

//// ------------------------------------------------------------------------ //
std::vector<TDS_LUT> ConfigHandlerTDS::LoadTDSLutConfig(const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;
  using boost::format;

  // clear the current channels	
  m_tds_luts.clear();
  std::vector<TDS_LUT> luts;
  stringstream sx;
  stringstream ss;
  stringstream where;
  
  try {
    for(int ilut = 0; ilut < 16; ilut++) {
      bool lut_ok = true;
      ss.str("");
      ss << "lut_" << format("%02i") % ilut;
      for(const auto& conf : pt.get_child("configuration.lut_registers")) {
	size_t find = conf.first.find(ss.str());
	if(find==string::npos) continue;
	where.str("");
	where << "(lut. " << format("%02i") % ilut;
	string value = "";
	
	TDS_LUT lut;
	lut.number = ilut;
	
	int trim;
	trim = conf.second.get<int>("BandID");
	lut.BandID = trim;
	
	trim = conf.second.get<int>("LeadStrip");
	lut.LeadStrip = trim;
	
	lut.ok = lut_ok;
	luts.push_back(lut);
	
      }
    }
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Loading TDS registers from requested config file, exception thrown: "
       << e.what();
    msg()(sx,"ConfigHandlerTDS::LoadTDSChannelConfig"); sx.str("");
    for(int ilut = 0; ilut < (int)luts.size(); ilut++) {
      luts.at(ilut).ok = false;
    }
  } 

  m_tds_luts = luts;

  return luts;


}
QString ConfigHandlerTDS::TDSMask(){

  QString MapString = "0000000000000000";
  QString str;
  int pos = 0;

  str = QString("%1").arg(m_tds_globalSettings.board_Type, 2, 2, QChar('0'));
  MapString.replace(1, str.size(), str);
  pos+=1+str.size();

  str = QString("%1").arg(m_tds_globalSettings.boardID, 3, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=pos+str.size();

  str = QString("%1").arg(m_tds_globalSettings.ASIC_Type, 2, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(m_tds_globalSettings.chipID, 8, 2, QChar('0'));

  /*
  if ( m_tds_globalSettings.chipID == 1 ) str = "00000001";
  if ( m_tds_globalSettings.chipID == 2 ) str = "00000010";
  if ( m_tds_globalSettings.chipID == 3 ) str = "00000011";
  if ( m_tds_globalSettings.chipID == 4 ) str = "00000100";
  if ( m_tds_globalSettings.chipID == 5 ) str = "00000101";
  if ( m_tds_globalSettings.chipID == 6 ) str = "00000110";
  if ( m_tds_globalSettings.chipID == 7 ) str = "00000111";
  if ( m_tds_globalSettings.chipID == 8 ) str = "00001000";
  */

  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  return MapString;

}
//// ------------------------------------------------------------------------ //
void ConfigHandlerTDS::LoadTDSChipConfiguration(TDSGlobalSetting& global,
						std::vector<TDS_Channel>& channels,
						std::vector<TDS_LUT>& luts)
{
  m_tds_globalSettings = global;

  m_tds_channels.clear();
  for(int i = 0; i < (int)channels.size(); i++) {
    m_tds_channels.push_back(channels[i]);
    //m_channels[i].print();                                                                                                                                                                                   
  }
  //m_globalSettings.print();                                                                                                                                                                                    

  m_tds_luts.clear();
  for(int i = 0; i < (int)luts.size(); i++) {
    m_tds_luts.push_back(luts[i]);
  }


}

