
// vmm
#include "socket_handler.h"

// Qt
#include <QProcess>
#include <QByteArray>

// std/stl
#include <iostream>
using namespace std;

////////////////////////////////////////////////////////////////////////////
// ---------------------------------------------------------------------- //
//  SocketHandler
// ---------------------------------------------------------------------- //
////////////////////////////////////////////////////////////////////////////

SocketHandler::SocketHandler(QObject* parent) :
    QObject(parent),
    m_dbg(false),
    m_msg(0),
    m_sTGCSocket(0),
    m_stgcSetup(false),
    m_daqSocket(0),
    m_daqSetup(false)
{
    cout << "SocketHandler::SocketHandler()" << endl;
    //    m_skipProcessing = false;
}
// ---------------------------------------------------------------------- //
void SocketHandler::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
// ---------------------------------------------------------------------- //
void SocketHandler::addSocket(const QHostAddress &IPaddress, std::string name, 
			      quint16 bindingPort, QAbstractSocket::BindMode mode)
{
    bool bind = true;
    QString lname = QString::fromStdString(name).toLower();

    if(lname=="stgc") {
        if(m_stgcSetup) return;
        m_sTGCSocket = new sTGCSocket();
        m_sTGCSocket->LoadMessageHandler(msg());
        m_sTGCSocket->setDebug(dbg());
        m_sTGCSocket->setName(name);
        m_sTGCSocket->setBindingPort(bindingPort);
	m_sTGCSocket->setBindingIp(IPaddress.toString());

        msg()("Attemping to bind sTGCSocket:","SocketHandler::addSocket");
	bind = m_sTGCSocket->bindSocket(IPaddress, bindingPort, mode);

        if(bind) m_stgcSetup = true;
        msg()("Socket Status:","SocketHandler::addSocket");
        m_sTGCSocket->Print();

    } // stgc
    else if(lname=="daq") {
        if(m_daqSetup) return;
        m_daqSocket = new sTGCSocket();
        m_daqSocket->LoadMessageHandler(msg());
        m_daqSocket->setDebug(dbg());
        m_daqSocket->setName(name);
        m_daqSocket->setBindingPort(bindingPort);
	m_daqSocket->setBindingIp(IPaddress.toString());

        msg()("Attempting to add DaqSocket:","SocketHandler::addSocket");
	bind = m_daqSocket->bindSocket(IPaddress, bindingPort, mode);

        if(bind) m_stgcSetup = true;
        msg()("Socket Status:","SocketHandler::addSocket");
        m_daqSocket->Print();

    } // stgc
    else {
        stringstream sx;
        sx << "FATAL Currently you can only add the 'stgc' sockets\n"
           << "FATAL You have attemped to add a socket named: " << name;
        msg()(sx,"SocketHandler::addSocket",true);
        exit(1);
    }
}
// ---------------------------------------------------------------------- //
bool SocketHandler::stgcSocketOK()
{
    bool status = true;
    if (!m_sTGCSocket) status = false;
    else {
      status = m_sTGCSocket->isBound();
    }
    return status;
}
bool SocketHandler::daqSocketOK()
{
    bool status = true;
    if (!m_daqSocket) status = false;
    else {
      status = m_sTGCSocket->isBound();
    }
    return status;
}
bool SocketHandler::SocketOK( const QString & whichSocket ) {

  return SocketOK( whichSocket.toStdString() );

}
bool SocketHandler::SocketOK( std::string whichSocket ) {
  bool status = true;
  if ( whichSocket == "stgc" ) {
    status = stgcSocketOK();
  }
  else if ( whichSocket == "daq" ) {
    status = daqSocketOK();
  }
  else {
    status = false;
  }
  return status;
}

// ---------------------------------------------------------------------- //
bool SocketHandler::SendDatagram(const QByteArray& datagram,const QHostAddress &IPaddress,
				 const quint16& destPort, const QString& whichSocket)
{
  bool send_ok = true;
  std::stringstream sx;

  if ( !SocketOK(whichSocket) ) {
    sx.str("");
    sx << "Error: stgc socket have not been defined.  Open Communications first. Abort send command";
    msg()(sx, "SocketHandler::SendDatagram");
    return false;
  }

  // get the requested sTGCSocket
  sTGCSocket& socket = getSocket(whichSocket.toStdString());
  
  if(dbg()) {
    stringstream sx;

    sx << " // ----------------------------------------------------------------------------------------------// \n "
       << " Data from socket '" << socket.getName() << "' sent to "
       << "(IP,port) = (" << IPaddress.toString().toStdString() << ", " << destPort << ") :\n"
       << datagram.toHex().toStdString() << "\n";
    msg()(sx,"SocketHandler::SendDatagram");

  }
  socket.writeDatagram(datagram, IPaddress, destPort);

  return send_ok;
}
// ---------------------------------------------------------------------- //
//bool SocketHandler::waitForReadyRead(std::string name, int msec, quint32 cmd_delay)
bool SocketHandler::waitForReadyRead(std::string name, int msec)
{

  std::stringstream sx;

  if ( !SocketOK(name) ) {
    sx.str("");
    sx << "Error: daq socket have not been defined.  Open Communications first. Abort send command";
    msg()(sx, "SocketHandler::waitForReadyRead");
    return false;
  }

  bool status = false;
  sTGCSocket& stgcsocket = getSocket(name);
  status = stgcsocket.socket().waitForReadyRead(msec);
  
  return status;
}
// ---------------------------------------------------------------------- //
QByteArray SocketHandler::processReply(std::string name, QHostAddress* target_ip, 
				       quint16 *target_port)
{

  std::stringstream sx;

  if ( !SocketOK(name) ) {
    sx.str("");
    sx << "Error: daq socket have not been defined.  Open Communications first. Abort send command";
    msg()(sx, "SocketHandler::processReply");
    return false;
  }

  QByteArray outbuffer;
    //    quint32 count = commandCounter();

    sTGCSocket& socket = getSocket(name);
    outbuffer = socket.processReply(target_ip, target_port);
    return outbuffer;
}
// ---------------------------------------------------------------------- //
void SocketHandler::closeAndDisconnect(std::string name, std::string callingFn)
{

  sTGCSocket& stgcsocket = getSocket(name);
  stgcsocket.closeAndDisconnect(callingFn);
  if ( name == "stgc" ) m_stgcSetup = false;
  if ( name == "daq"  ) m_daqSetup  = false;

}
// ---------------------------------------------------------------------- //
QByteArray SocketHandler::buffer(std::string name)
{
  sTGCSocket& stgcsocket = getSocket(name);
  return stgcsocket.buffer(); 
}
// ---------------------------------------------------------------------- //
sTGCSocket& SocketHandler::getSocket(std::string whichSocket)
{
    stringstream sx;
    if(whichSocket=="") {
        sx.str("");
        sx << "FATAL This method must be passed a string containing the name of "
           << "the desired socket";
        msg()(sx,"SocketHandler::getSocket",true);
        exit(1);
    }
    QString lname = QString::fromStdString(whichSocket).toLower();

    if(lname=="stgc") {
        if(m_sTGCSocket) return *m_sTGCSocket;
        else {
            msg()("FATAL Requested socket (stgc) is null!","SocketHandler::getSocket",true);
            exit(1);
        }
    }
    if(lname=="daq") {
        if(m_daqSocket) return *m_daqSocket;
        else {
            msg()("FATAL Requested socket (daq) is null!","SocketHandler::getSocket",true);
            exit(1);
        }
    }
    else {
        sx.str("");
        sx << "FATAL Currently you can only retrieve the 'stgc' sockets.\n"
           << "FATAL You have attempted to retrieve a socket named: " << whichSocket;
        msg()(sx, "SocketHandler::getSocket",true);
        exit(1);
    }
}
// ---------------------------------------------------------------------- //
void SocketHandler::Print()
{
    if(!m_sTGCSocket) {
        if(dbg())
            msg()("SocketHandler currently holds no sockets!",
		  "SocketHandler::Print");
    }
    if(m_sTGCSocket)
      sTGCSocket().Print();
    return;
}
