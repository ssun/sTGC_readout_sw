
#include "config_module_board.h"
#include "reply_type.h"
//#include "data_handler.h" // for reverse32

// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// Qt
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QBitArray>
#include <QStringList>

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Configuration
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
Config_Board::Config_Board(Config_Base *parent) :
    Config_Base(parent),
    reg_addr_Query_ELink    ("000000D0"),
    reg_addr_set_elink_0to3 ("000000D1"),
    reg_addr_set_elink_4to7 ("000000D2"),
    reg_addr_Query_TTC_Lock ("000000DC"),
    reg_addr_Query_EVID     ("000000DE"),
    reg_addr_Global_Reset   ("000000AF"),
    reg_addr_Global_TrigMode("000000AB"),
    reg_addr_Global_DaqEn   ("0000000F"),
    reg_addr_TTC_startBit   ("000000E0"),
    reg_addr_TTC_Training   ("000000E1"),
    reg_addr_Gate_nBC       ("000000E2"),
    reg_addr_TTC_TrigLatency("000000E3"),
    reg_addr_SCA_reset      ("000000E4"),
    reg_addr_TTC_Evt_Reset  ("000000E5"),
    reg_addr_TTC_BC_Reset   ("000000E6"),
    reg_addr_TTC_Sync_Reset ("000000E7"),
    reg_addr_TP_CKTK_nMax   ("000000C1"),
    reg_addr_TP_Freq        ("000000C2"),
    reg_addr_TP_CKTP_nMax   ("000000C3"),
    reg_addr_TP_CKTP_Skew   ("000000C4"),
    reg_addr_TP_CKTP_Period ("000000C5"),
    reg_addr_TP_CKTP_Width  ("000000C6"),
    reg_addr_TP_CKBC_nMax   ("000000C7"),
    reg_addr_TP_CKTP_Spacing("000000C8"),
    reg_addr_Global_ReadEn  ("000000CD"),
    //    m_dbg(false),
    //    m_socketHandler(0),
    m_configHandler(0)//,
    //    m_msg(0)
{
}
// ------------------------------------------------------------------------ //
Config_Board& Config_Board::LoadConfig(ConfigHandlerBoard& config)
{
    m_configHandler = &config;
    if(!m_configHandler) {
        msg()("FATAL ConfigHandler instance is null", "Config_Board::LoadConfig", true);
        exit(1);
    }
    else if(dbg()) {
        msg()("ConfigHandler instance loaded", "Config_Board::LoadConfig");
    }
    return *this;
}
//--------------------------------------------------------------------------//
bool Config_Board::config_SCA(int send_to_port, const QHostAddress &target_ip, int boardType, int boardID)
{
  QString command = default_header;
  
  command += Build_Mask( boardType, boardID, 0, 0 );

  command = command + cmd_SCA_init;

  decoder()->sent_SCA_init = true;

  bool send_ok = true;
  send_ok = SendConfigN(send_to_port, target_ip, command, 3);

  decoder()->sent_SCA_init = false;

  if ( send_ok ) m_current_SCA_ID = decoder()->current_SCA_ID();

  return send_ok;
}

// ------------------------------------------------------------------------ //
bool Config_Board::setELinkDelay0to3(int send_to_port, const QHostAddress &target_ip, std::vector<int> delay)
{

  //----------------------------------------------//
  //   command plus delay amount for elink 0-3      
  //----------------------------------------------//
  QString command="decafbad00000400000000D1";
  command += QString("%1").arg(delay.at(3), 2, 16, QChar('0'));
  command += QString("%1").arg(delay.at(2), 2, 16, QChar('0'));
  command += QString("%1").arg(delay.at(1), 2, 16, QChar('0'));
  command += QString("%1").arg(delay.at(0), 2, 16, QChar('0'));

  bool send_ok = SendConfigN(send_to_port, target_ip, command, 3);
  return send_ok;
}

// ------------------------------------------------------------------------ //
bool Config_Board::setELinkDelay4to7(int send_to_port, const QHostAddress &target_ip, std::vector<int> delay)
{

  //----------------------------------------------//
  //   command plus delay amount for elink 4-7      
  //----------------------------------------------//
  QString command="decafbad00000400000000D2";
  command += QString("%1").arg(delay.at(3), 2, 16, QChar('0'));
  command += QString("%1").arg(delay.at(2), 2, 16, QChar('0'));
  command += QString("%1").arg(delay.at(1), 2, 16, QChar('0'));
  command += QString("%1").arg(delay.at(0), 2, 16, QChar('0'));

  bool send_ok = SendConfigN(send_to_port, target_ip, command, 3);
  return send_ok;

}
bool Config_Board::SetFinalELinkDelay(int send_to_port, const QHostAddress &target_ip, std::vector<int> ELink_BestLockLoc)
{
  std::vector<int> first4Links;
  std::vector<int> last4Links;

  for ( uint i=0; i<ELink_BestLockLoc.size(); i++ ) {
    if ( i < 4 ) {
      if ( ELink_BestLockLoc.at(i) >= 0 ) first4Links.push_back( ELink_BestLockLoc.at(i) );
      else                                first4Links.push_back( 0 );
    }
    else if ( i >= 4 ) {
      if ( ELink_BestLockLoc.at(i) >= 0 ) last4Links.push_back( ELink_BestLockLoc.at(i) );
      else                                last4Links.push_back( 0 );
    }
  }

  bool send_ok = true;
  send_ok = setELinkDelay0to3(send_to_port, target_ip, first4Links);
  if ( !send_ok ) return send_ok;
  send_ok = setELinkDelay4to7(send_to_port, target_ip, last4Links);
  return send_ok;

}
// ------------------------------------------------------------------------ //
vector<bool> Config_Board::QueryELink(int send_to_port, const QHostAddress &target_ip, int nLinks, bool also_decode)
{

  std::stringstream m_sx;

  vector<bool> m_locked_elink;
  m_locked_elink.resize(0);

  //---------------------------------------------//
  //      Query all 8 links ( 0b8 = 0xFF )         
  //---------------------------------------------//

  int linkMask = (1 << nLinks) - 1; // 2^nLinks - 1, generates linkMask of (0b11111111) if 8 ELinks

  // append link mask to command                 
  QString command="decafbad00000400000000D0"; //+"000000FF";
  command += QString("%1").arg(linkMask,8,16,QChar('0'));

  //---------------------------------------------//
  //              Send in command                  
  //---------------------------------------------//
  bool send_ok = true;

  send_ok = SendConfigN(send_to_port, target_ip, command, 3, also_decode, true, TTC_query_elink, 200);
  if ( !send_ok ) return m_locked_elink;

  //---------------------------------------------//
  //        wait for a bit for an reply            
  //---------------------------------------------//
  boost::this_thread::sleep(boost::posix_time::milliseconds(100));

  if ( send_ok ) m_locked_elink = decoder()->locked_elink();

  return m_locked_elink;
}

//-------------------------------------------------------------//

int Config_Board::Query_EVID(int send_to_port, const QHostAddress &target_ip)
{
  int m_EVID = -1;

  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000DE";
  QString reg_val  = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true, true, TTC_query_EVID);

  if ( send_ok ) m_EVID = (int) decoder()->current_EVID();
  
  return m_EVID;
}

//---------------------------------------------------------------------------------------------------//

int Config_Board::Query_TTC_Lock(int send_to_port, const QHostAddress &target_ip)
{
  int locked = -1;

  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000DC";
  QString reg_val  = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true, true, TTC_query_TTC);

  if ( send_ok ) locked = (int) decoder()->current_TTC_status();

  return locked;
}

//---------------------------------------------------------------------------------------------------//

bool Config_Board::eFIFO_disable(int send_to_port, const QHostAddress &target_ip, uint32_t elink_mask)
{

  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000D3";
  QString reg_val  = QString("%1").arg(elink_mask, 8, 16, QChar('0'));;

  cmd = cmd + reg_addr + reg_val;

  std::cout << cmd.toStdString() << std::endl;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3);

  return send_ok;
}


bool Config_Board::TTC_F1(int send_to_port, const QHostAddress &target_ip, bool onoff)
{

  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000F1";
  QString reg_val  = QString("%1").arg(onoff, 8, 16, QChar('0'));;

  cmd = cmd + reg_addr + reg_val;

  std::cout << cmd.toStdString() << std::endl;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1);

  return send_ok;
}

bool Config_Board::TTC_F1(int send_to_port, const QHostAddress &target_ip)
{

  bool send_ok;

  send_ok = TTC_F1(send_to_port, target_ip, 1);
  send_ok = TTC_F1(send_to_port, target_ip, 0);

  return send_ok;
}

bool Config_Board::TTC_F2(int send_to_port, const QHostAddress &target_ip, bool onoff)
{

  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000F2";
  QString reg_val  = QString("%1").arg(onoff, 8, 16, QChar('0'));;

  cmd = cmd + reg_addr + reg_val;

  std::cout << cmd.toStdString() << std::endl;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1);

  return send_ok;
}

bool Config_Board::TTC_F2(int send_to_port, const QHostAddress &target_ip)
{

  bool send_ok;

  send_ok = TTC_F2(send_to_port, target_ip, 1);
  send_ok = TTC_F2(send_to_port, target_ip, 0);

  return send_ok;
}

//--------------------------------------------------------------------------------------------------//

std::vector<uint32_t> Config_Board::current_TTC_reg_address() {
  return decoder()->current_TTC_reg_address(); 
}
std::vector<uint32_t> Config_Board::current_TTC_reg_value() {
  return decoder()->current_TTC_reg_value(); 
}
uint32_t Config_Board::TTC_firmware_version() {
  return decoder()->TTC_firmware_version();
}

//--------------------------------------------------------------------------------------------------//

bool Config_Board::GlobalCtr_KC705Reset(int send_to_port, const QHostAddress &target_ip)   
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000AF";
  QString reg_val  = "000000AA";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);
  if ( !send_ok ) return send_ok;

  /*
  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  cmd      = default_header+"0000"+cmd_TTC_config;
  reg_addr = "000000AF";
  reg_val  = "00000000";

  cmd = cmd + reg_addr + reg_val;

  send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);
  if ( !send_ok) return send_ok;
  */

  return send_ok;
}

bool Config_Board::GlobalCtr_TrigMode(int send_to_port, const QHostAddress &target_ip, bool internal_trig)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000AB";
  QString reg_val;

  if ( internal_trig ) reg_val = "00000007";
  else                 reg_val = "00000004";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::GlobalCtr_DAQ_Enabled(int send_to_port, const QHostAddress &target_ip, bool enabled)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "0000000F";
  QString reg_val;

  if ( enabled ) reg_val = "00000001";
  else           reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::GlobalCtr_Readout_Enabled(int send_to_port, const QHostAddress &target_ip, bool enabled)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000CD";
  QString reg_val;

  if ( enabled ) reg_val = "00000001";
  else           reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::GlobalCtr_TTC_StartBit(int send_to_port, const QHostAddress &target_ip, int bit)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E0";
  QString reg_val  = QString("%1").arg(bit, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

//-------------------------------------------------------------------------------------------------//

bool Config_Board::TTC_Ctr_Training(int send_to_port, const QHostAddress &target_ip, int elink_mask)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E1";
  QString reg_val  = QString("%1").arg(elink_mask, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::FEB_Soft_Reset_GPIO_Low(int send_to_port, const QHostAddress &target_ip, 
					   int boardType, int boardID){

  QString cmd = default_header;
  cmd = cmd + Build_Mask( boardType, boardID, ASIC_Type_VMM, 0b11111111 ); 
  cmd = cmd + cmd_ASIC_reset;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1, true);
  
  return send_ok;

}

bool Config_Board::FEB_Soft_Reset_GPIO_High(int send_to_port, const QHostAddress &target_ip,
					    int boardType, int boardID){

  QString cmd = default_header;
  cmd = cmd + Build_Mask( boardType, boardID, ASIC_Type_VMM, 0b11111111 );
  cmd = cmd + cmd_ASIC_reset_GPIO_enable;
 
  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1, true);

  return send_ok;

}

bool Config_Board::TTC_Ctr_Gate_nBC(int send_to_port, const QHostAddress &target_ip, int nBC)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E2";
  QString reg_val  = QString("%1").arg(nBC, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::TTC_Ctr_TrigLatency(int send_to_port, const QHostAddress &target_ip, int latency)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E3";
  QString reg_val  = QString("%1").arg(latency, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::TTC_Ctr_SCA_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E4";
  QString reg_val;
  if ( reset ) reg_val  = "00000001";
  else         reg_val  = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);
  //  if ( !send_ok) return send_ok;
  return send_ok;

}

bool Config_Board::TTC_Ctr_SCA_Reset(int send_to_port, const QHostAddress &target_ip)
{

  bool send_ok = TTC_Ctr_SCA_Reset(send_to_port, target_ip, true);
  //  if ( !send_ok ) return send_ok;

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_SCA_Reset(send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}

bool Config_Board::TTC_Ctr_Evt_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E5";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;

}

bool Config_Board::TTC_Ctr_Evt_Reset(int send_to_port, const QHostAddress &target_ip) {
  
  bool send_ok = TTC_Ctr_Evt_Reset(send_to_port, target_ip, true);
  //  if ( !send_ok ) return send_ok;

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_Evt_Reset(send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}

bool Config_Board::TTC_Ctr_BC_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E6";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::TTC_Ctr_BC_Reset(int send_to_port, const QHostAddress &target_ip) 
{
  bool send_ok = TTC_Ctr_BC_Reset( send_to_port, target_ip, true );
  //  if ( !send_ok ) return send_ok;

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_BC_Reset( send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}

bool Config_Board::TTC_Ctr_VMM_Soft_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E7";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1, true);

  return send_ok;
}

bool Config_Board::TTC_Ctr_VMM_Soft_Reset(int send_to_port, const QHostAddress &target_ip)
{
  bool send_ok = TTC_Ctr_VMM_Soft_Reset( send_to_port, target_ip, true );
  //  if ( !send_ok ) return send_ok;                                                                                                                                               

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_VMM_Soft_Reset( send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}

bool Config_Board::TTC_Ctr_L0_Evt_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E8";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;

}

bool Config_Board::TTC_Ctr_L0_Evt_Reset(int send_to_port, const QHostAddress &target_ip) {

  bool send_ok = TTC_Ctr_L0_Evt_Reset(send_to_port, target_ip, true);
  //  if ( !send_ok ) return send_ok; 

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_L0_Evt_Reset(send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}

bool Config_Board::TTC_Ctr_L0_Accept_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000E9";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;

}

bool Config_Board::TTC_Ctr_L0_Accept_Reset(int send_to_port, const QHostAddress &target_ip) {

  bool send_ok = TTC_Ctr_L0_Accept_Reset(send_to_port, target_ip, true);

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_L0_Accept_Reset(send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}


bool Config_Board::TTC_Ctr_L1_Accept_Reset(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000EA";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;

}

bool Config_Board::TTC_Ctr_L1_Accept_Reset(int send_to_port, const QHostAddress &target_ip) {

  bool send_ok = TTC_Ctr_L1_Accept_Reset(send_to_port, target_ip, true);

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_L1_Accept_Reset(send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}


bool Config_Board::TTC_Ctr_VMM_TP(int send_to_port, const QHostAddress &target_ip, bool reset)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000EB";
  QString reg_val;
  if ( reset ) reg_val = "00000001";
  else         reg_val = "00000000";

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;

}

bool Config_Board::TTC_Ctr_VMM_TP(int send_to_port, const QHostAddress &target_ip) {

  bool send_ok = TTC_Ctr_VMM_TP(send_to_port, target_ip, true);

  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  send_ok = TTC_Ctr_VMM_TP(send_to_port, target_ip, false);
  if ( !send_ok ) return send_ok;

  return send_ok;
}

bool Config_Board::TTC_Ctr_TTC_pulse_lv(int send_to_port, const QHostAddress &target_ip, uint32_t val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000EE";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;

}



//------------------------------------------------------------------------------------------//

bool Config_Board::VMM_TestPulse_CKTK_nMax(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C1";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_Freq(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C2";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_CKTP_nMax(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C3";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_CKTP_Skew(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C4";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_CKTP_Period(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C5";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_CKTP_Width(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C6";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_CKBC_nMax(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C7";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

bool Config_Board::VMM_TestPulse_CKTP_Spacing(int send_to_port, const QHostAddress &target_ip, int val)
{
  QString cmd      = default_header+"0000"+cmd_TTC_config;
  QString reg_addr = "000000C8";
  QString reg_val  = QString("%1").arg(val, 8, 16, QChar('0'));

  cmd = cmd + reg_addr + reg_val;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 3, true);

  return send_ok;
}

//------------------------------------------------------------------------//

bool Config_Board::Set_SCA_GPIO( int send_to_port, const QHostAddress &target_ip,
				 uint32_t boardType, uint32_t boardID, 
				 uint32_t dir_mask,  uint32_t dout_mask)
{

  QString cmd = default_header; 
  cmd = cmd + Build_Mask( boardType, boardID, ASIC_Type_SCA, 0b0 );
  cmd = cmd + cmd_SCA_GPIO;

  // 8 x base 16 = 32 bits
  QString dir_mask_str  = QString("%1").arg( dir_mask,  8, 16, QChar('0') );
  QString dout_mask_str = QString("%1").arg( dout_mask, 8, 16, QChar('0') );

  cmd += dir_mask_str;
  cmd += dout_mask_str;

  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1, true);

  return send_ok;


}

std::vector<uint32_t> Config_Board::Query_SCA_ADC(int send_to_port, const QHostAddress &target_ip,
						  uint32_t boardType, uint32_t boardID, 
						  uint32_t chan_mux,  uint32_t current)
{
  QString cmd = default_header;
  cmd = cmd + Build_Mask( boardType, boardID, ASIC_Type_SCA, 0b0 );
  cmd = cmd + cmd_SCA_ADC;


  QString chan_mask   = QString("%1").arg( chan_mux, 8, 16, QChar('0') );
  QString current_str = QString("%1").arg( current,  8, 16, QChar('0') );

  cmd += chan_mask;
  cmd += current_str;

  decoder()->set_Sent_SCA_ADC( true );

  // wait a long time for ADC reply
  bool send_ok = SendConfigN(send_to_port, target_ip, cmd, 1, true, false, SCA_normal, 5000);

  boost::this_thread::sleep(boost::posix_time::milliseconds(100));

  std::vector<uint32_t> m_ADC;
  m_ADC.resize(0);

  if ( send_ok ) m_ADC = decoder()->current_SCA_ADC();
  else { std::cout << "failed to send SCA ADC query command " << std::endl; }

  decoder()->sent_SCA_init = false;

  return m_ADC;

}

std::vector<uint32_t> Config_Board::Query_SCA_ADC_VMM( int send_to_port, const QHostAddress &target_ip,
						       uint32_t boardType, uint32_t boardID,
						       uint32_t ivmm,  uint32_t current)
{

  std::vector<uint32_t> m_adc;
  m_adc.resize(0);

  uint32_t chan_mux = 999999;

  if      ( boardType == Board_Type_pFEB ) { 
    if ( ivmm == 0 ) { chan_mux = m_ADC_ch_pFEB_v21_vmm0; }
    if ( ivmm == 1 ) { chan_mux = m_ADC_ch_pFEB_v21_vmm1; }
    if ( ivmm == 2 ) { chan_mux = m_ADC_ch_pFEB_v21_vmm2; }
  }
  else if ( boardType == Board_Type_sFEB ) {
    if ( ivmm == 0 ) chan_mux = m_ADC_ch_sFEB_v21_vmm0;
    if ( ivmm == 1 ) chan_mux = m_ADC_ch_sFEB_v21_vmm1;
    if ( ivmm == 2 ) chan_mux = m_ADC_ch_sFEB_v21_vmm2;
    if ( ivmm == 3 ) chan_mux = m_ADC_ch_sFEB_v21_vmm3;
    if ( ivmm == 4 ) chan_mux = m_ADC_ch_sFEB_v21_vmm4;
    // adc channel 5 and adc channel 7 is swapped for sFEB 2.1
    if ( ivmm == 5 ) chan_mux = m_ADC_ch_sFEB_v21_vmm7;
    if ( ivmm == 6 ) chan_mux = m_ADC_ch_sFEB_v21_vmm6;
    // adc channel 5 and adc channel 7 is swapped for sFEB 2.1
    if ( ivmm == 7 ) chan_mux = m_ADC_ch_sFEB_v21_vmm5;
  }
  else if ( boardType == Board_Type_pFEB_v22 ) {
    if ( ivmm == 0 ) { chan_mux = m_ADC_ch_pFEB_v22_vmm0; }
    if ( ivmm == 1 ) { chan_mux = m_ADC_ch_pFEB_v22_vmm1; }
    if ( ivmm == 2 ) { chan_mux = m_ADC_ch_pFEB_v22_vmm2; }
  }
  else if ( boardType == Board_Type_sFEB_v22 ) {
    if ( ivmm == 0 ) chan_mux = m_ADC_ch_sFEB_v22_vmm0;
    if ( ivmm == 1 ) chan_mux = m_ADC_ch_sFEB_v22_vmm1;
    if ( ivmm == 2 ) chan_mux = m_ADC_ch_sFEB_v22_vmm2;
    if ( ivmm == 3 ) chan_mux = m_ADC_ch_sFEB_v22_vmm3;
    if ( ivmm == 4 ) chan_mux = m_ADC_ch_sFEB_v22_vmm4;
    if ( ivmm == 5 ) chan_mux = m_ADC_ch_sFEB_v22_vmm5;
    if ( ivmm == 6 ) chan_mux = m_ADC_ch_sFEB_v22_vmm6;
    if ( ivmm == 7 ) chan_mux = m_ADC_ch_sFEB_v22_vmm7;
  }

  if ( chan_mux <= 31 ) {
    m_adc = Query_SCA_ADC(send_to_port, target_ip,
			  boardType, boardID,
			  chan_mux,  current);
  }
  
  return m_adc;

}

//-------------------------------------------------------------------------//

std::vector<uint32_t> Config_Board::Query_SCA_ADC_temp( int send_to_port, const QHostAddress &target_ip,
							uint32_t boardType, uint32_t boardID, uint32_t current)
{

  uint32_t chan_mux = m_ADC_ch_pFEB_v21_SCA_Temp; // its always channel 31 so it doesn't matter

  return Query_SCA_ADC(send_to_port, target_ip,
		       boardType, boardID,
		       chan_mux,  current);

}

//-------------------------------------------------------------------------//

