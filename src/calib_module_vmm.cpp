#include "calib_module_vmm.h"

using namespace std;

Calib_VMM::Calib_VMM()
{
  m_dbg = false;
}

void Calib_VMM::write_ThDAC_Calib_VMM_Histos(QString outfile, int iboard) {

  if ( m_threshold_dac_adc_per_vmm.size() == 0 ||
       m_threshold_dac_val_per_vmm.size() == 0 ) return;

  TFile out(outfile.toStdString().c_str(),"RECREATE");

  //------------------------------------------------------------------------------//

  TGraph h_graph;
  h_graph = TGraph(m_threshold_dac_adc_per_vmm.size());

  for ( uint ivmm=0; ivmm < m_threshold_dac_adc_per_vmm.size(); ivmm++ ) {

    m_sx.str("");
    m_sx << "h_board" << iboard << "_vmm" << ivmm << "_ThDAC";
    h_graph.SetTitle( m_sx.str().c_str());
    h_graph.SetName( m_sx.str().c_str() );

    h_graph.Set(m_threshold_dac_adc_per_vmm.at(ivmm).size());

    for( uint istep=0; istep < m_threshold_dac_adc_per_vmm.at(ivmm).size(); istep++ ) {
      h_graph.SetPoint( istep, 
			m_threshold_dac_val_per_vmm.at(ivmm).at(istep),
			m_threshold_dac_adc_per_vmm.at(ivmm).at(istep) );
    }

    h_graph.Write();

  }

  //------------------------------------------------------------------------------//

  if ( m_threshold_dac_adc_per_vmm_trials.size() > 0 ) {

    for ( uint ivmm=0; ivmm < m_threshold_dac_adc_per_vmm_trials.size(); ivmm++ ) {
      
      for( uint istep=0; istep < m_threshold_dac_adc_per_vmm_trials.at(ivmm).size(); istep++ ) {
	
	m_sx.str("");
	m_sx << "h_board" << iboard << "_vmm" << ivmm << "_thDAC " << m_threshold_dac_val_per_vmm.at(ivmm).at(istep) << "_adc";

	TH1F h(m_sx.str().c_str(), m_sx.str().c_str(), 4096, -0.5, 4095.5);
	
	for( uint j=0; j < m_threshold_dac_adc_per_vmm_trials.at(ivmm).at(istep).size(); j++ ) {
	  if ( m_threshold_dac_adc_per_vmm_trials.at(ivmm).at(istep).at(j) == 0xffff ) continue;
	  h.Fill( m_threshold_dac_adc_per_vmm_trials.at(ivmm).at(istep).at(j) );
	}

	h.Write();
      }
    }

  }

  out.Close();

}

void Calib_VMM::write_Calib_VMM_Histos(QString outfile, int iboard) {

  TFile out(outfile.toStdString().c_str(),"RECREATE");

  if ( m_chan_baseline_trials.size() == 0 ) return;

  Calc_Baseline_Avg();
  Calc_Baseline_RMS();

  //------------------------------------------------------------------------------//

  for ( uint ivmm=0; ivmm < m_chan_baseline_mean.size(); ivmm++ ) {

    m_sx.str("");
    m_sx << "h_board" << iboard << "_vmm" << ivmm << "_baseline_adc_avg";
    TH1F h_avg(m_sx.str().c_str(), m_sx.str().c_str(), 64, -0.5, 63.5);

    m_sx.str("");
    m_sx << "h_board" << iboard << "_vmm" << ivmm << "_baseline_adc_rms";
    TH1F h_rms(m_sx.str().c_str(), m_sx.str().c_str(), 64, -0.5, 63.5);

    m_sx.str("");
    m_sx << "h_board" << iboard << "_vmm" << ivmm << "_5rms_above_avg_baseline_adc";
    TH1F h_5rms_aboveAvg(m_sx.str().c_str(), m_sx.str().c_str(), 64, -0.5, 63.5);
    
    for( uint ichan=0; ichan < m_chan_baseline_mean.at(ivmm).size(); ichan++ ) {
      h_avg.SetBinContent( ichan+1, m_chan_baseline_mean.at(ivmm).at(ichan)/4095. );

      h_rms.SetBinContent( ichan+1, m_chan_baseline_rms.at(ivmm).at(ichan)/4095. );

      h_5rms_aboveAvg.SetBinContent( ichan+1, (m_chan_baseline_mean.at(ivmm).at(ichan) + 5 * m_chan_baseline_rms.at(ivmm).at(ichan))/4095. );
    }

    h_avg.Write();
    h_rms.Write();
    h_5rms_aboveAvg.Write();
    
  }

  //------------------------------------------------------------------------------//

  //------------------------------------------------------------------------------//

  for ( uint ivmm=0; ivmm < m_chan_baseline_trials.at(0).size(); ivmm++ ) {

    for( uint ichan=0; ichan < m_chan_baseline_trials.size(); ichan++ ) {
      
      m_sx.str("");
      m_sx << "h_board" << iboard << "_vmm" << ivmm << "_chan" << ichan << "_baseline_adc"; 
      TH1F h(m_sx.str().c_str(), m_sx.str().c_str(), 4095, -0.5, 4095.5);
      
      for( uint j=0; j < m_chan_baseline_trials.at(ichan).at(ivmm).size(); j++ ) {
        if ( m_chan_baseline_trials.at(ichan).at(ivmm).at(j) == 0xffff ) continue;
	h.Fill( m_chan_baseline_trials.at(ichan).at(ivmm).at(j) );
      }
      
      h.Write();
    }
  }

  out.Close();

}

void Calib_VMM::Calc_Baseline_Avg() {

  if ( m_chan_baseline_trials.size() == 0 ) return;

  //-------------------------------------------------------------------------//

  m_chan_baseline_mean.resize(0);

  for ( uint ivmm=0; ivmm < m_chan_baseline_trials.at(0).size(); ivmm++ ) {

    std::vector<float> ivmm_chan_baseline_avg;
    ivmm_chan_baseline_avg.resize(0);
    m_chan_baseline_mean.push_back( ivmm_chan_baseline_avg );
    
  }

  //------------------------------------------------------------------------//

  for ( uint ivmm=0; ivmm < m_chan_baseline_trials.at(0).size(); ivmm++ ) {

    for( uint ichan=0; ichan < m_chan_baseline_trials.size(); ichan++ ) {

      int ntrials = 0;
      float ichan_tot = 0.0;

      for( uint j=0; j < m_chan_baseline_trials.at(ichan).at(ivmm).size(); j++ ) {

	if ( m_chan_baseline_trials.at(ichan).at(ivmm).at(j) == 0xffff ) continue;

        ntrials++;
	ichan_tot += m_chan_baseline_trials.at(ichan).at(ivmm).at(j);

      } // end of j trials

      m_chan_baseline_mean.at(ivmm).push_back( ichan_tot/ntrials );

    } // end of ichan
  } // end of ivmm

  //-------------------------------------------------------------------//

}

void Calib_VMM::Calc_Baseline_RMS() {

  if ( m_chan_baseline_trials.size() == 0 ) return;

  Calc_Baseline_Avg();

  //----------------------------------------------------------------//

  m_chan_baseline_rms.resize(0);

  for ( uint ivmm=0; ivmm < m_chan_baseline_trials.at(0).size(); ivmm++ ) {

    std::vector<float> ivmm_chan_baseline_rms;
    ivmm_chan_baseline_rms.resize(0);
    m_chan_baseline_rms.push_back( ivmm_chan_baseline_rms );

  }

  //---------------------------------------------------------------//

  for ( uint ivmm=0; ivmm < m_chan_baseline_trials.at(0).size(); ivmm++ ) {

    for( uint ichan=0; ichan < m_chan_baseline_trials.size(); ichan++ ) {

      int ntrials = 0;
      float ichan_sqr_tot = 0.0;

      for( uint j=0; j < m_chan_baseline_trials.at(ichan).at(ivmm).size(); j++ ) {
        if ( m_chan_baseline_trials.at(ichan).at(ivmm).at(j) == 0xffff ) continue;

	ntrials++;
	ichan_sqr_tot += ( ( m_chan_baseline_trials.at(ichan).at(ivmm).at(j) - m_chan_baseline_mean.at(ivmm).at(ichan) ) *
			   ( m_chan_baseline_trials.at(ichan).at(ivmm).at(j) - m_chan_baseline_mean.at(ivmm).at(ichan) ) );
	
      } // end of j trials

      m_chan_baseline_rms.at(ivmm).push_back( sqrt(ichan_sqr_tot / ntrials) ) ;
    } // end of ichan

  } // end of ivmm

}

std::vector<uint32_t> Calib_VMM::find_Opt_Trim_Bit() {
  
  uint32_t min_threshold = 1000;
  uint32_t max_threshold = 0;

  int nchan = m_chan_baseline_vs_trim.size();

  m_opt_trim_bit.resize(0);

  if ( nchan == 0 ) return m_opt_trim_bit;

  int n_trim_bit = 0; //m_chan_baseline

  if ( nchan > 0 ) {
    n_trim_bit = m_chan_baseline_vs_trim.at(0).size();
    if ( n_trim_bit > 25 ) n_trim_bit = 25;
  }

  for ( int i=0; i<nchan; i++ ) {
    for ( int j=0; j<n_trim_bit; j++ ) {
      if ( m_chan_baseline_vs_trim[i][j] == 0xffff ) continue;
      if ( min_threshold > m_chan_baseline_vs_trim[i][j] ) min_threshold = m_chan_baseline_vs_trim[i][j];
      if ( max_threshold < m_chan_baseline_vs_trim[i][j] ) max_threshold = m_chan_baseline_vs_trim[i][j];
    }
  }

  std::cout << "min " << min_threshold << " max " << max_threshold << std::endl;

  uint32_t ithreshold = min_threshold;
  uint32_t opt_threshold = 0;
  double   min_total_dev = 1000.*1000.*64.;

  while( ithreshold < max_threshold ) {

    vector <uint32_t> ithreshold_opt_trim_bit;
    ithreshold_opt_trim_bit.resize(0);

    //  total deviation for all channels
    double ithreshold_total_dev = 0;

    //  loop over each channel
    for ( int i=0; i<nchan; i++ ) {

      double      ichan_opt_dev = 1000.*1000.;
      uint32_t    ichan_opt_trim_bit = 0;

      //----------------------------------------------------------//
      //  find best trim spot for each channel for ithreshold
      //  least deviation = best trim 
      //----------------------------------------------------------//
      for ( int j=0; j<n_trim_bit; j++ ) {
	if ( ichan_opt_dev > ( ( m_chan_baseline_vs_trim[i][j] - ithreshold )*
			       ( m_chan_baseline_vs_trim[i][j] - ithreshold ) ) ) {
	  ichan_opt_dev = ( ( m_chan_baseline_vs_trim[i][j] - ithreshold )*
			    ( m_chan_baseline_vs_trim[i][j] - ithreshold ) );
	  ichan_opt_trim_bit = j;
	}
      }

      //---------------------------------------------------------------------//
      //  save best trim bit for each channel
      //  add deviation at least trim bit to total deviation for ithreshold
      //---------------------------------------------------------------------//
      ithreshold_opt_trim_bit.push_back(ichan_opt_trim_bit);
      ithreshold_total_dev += ichan_opt_dev;

    }

    //-----------------------------------------------------------------------//
    //  if ithreshold_total_dev is smaller than min_total_dev
    //  then we just found the new best threshold level and new trim bits
    //-----------------------------------------------------------------------//
    if ( ithreshold_total_dev < min_total_dev ) {
      min_total_dev = ithreshold_total_dev;
      m_opt_trim_bit = ithreshold_opt_trim_bit;
      opt_threshold = ithreshold;
    }

    ithreshold += 1;
  }

  std::cout << "optimal threshold trimming bit " << opt_threshold << std::endl;

  return m_opt_trim_bit;

}

