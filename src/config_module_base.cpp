 
// vmm
#include "config_module_base.h"
#include "reply_type.h"
//#include "data_handler.h" // for reverse32

// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// Qt
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QBitArray>
#include <QStringList>

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_Base
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
Config_Base::Config_Base(QObject *parent) :
    QObject(parent),
    //m_dbg(false),
    //m_socketHandler(0),
    //m_running(false),
    m_msg(0)
{
  m_decode = new VMMDecoder();
  m_socketHandler = NULL;
  m_dbg = false;
  m_running = false;
}
// ------------------------------------------------------------------------ //
void Config_Base::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
// ------------------------------------------------------------------------ //
Config_Base& Config_Base::LoadSocket(SocketHandler& socket)
{
    m_socketHandler = &socket;
    if(!m_socketHandler) {
        msg()("FATAL SocketHandler instance is null", "Config_Base::LoadSocket", true);
        exit(1);
    }
    else if(dbg()) {
        msg()("SocketHandler instance loaded", "Config_Base::LoadSocket");
        //m_socketHandler->Print();
    }
        
    return *this;
}
// ------------------------------------------------------------------------ //
QByteArray Config_Base::bitsToBytes(QBitArray bits)
{
  QByteArray outbytes;
  outbytes.resize(bits.count()/8);
  outbytes.fill(0);

  for(int b = 0; b < bits.count(); ++b)
    outbytes[b/8] = ( outbytes.at(b/8) | ( (bits[b]?1:0) << (7-(b%8))));
  return outbytes;
}
// ------------------------------------------------------------------------ //
quint32 Config_Base::reverse32(QString input_bin)
{
    bool ok;
    QBitArray received(32,false);
    for(int i = 0; i < input_bin.size(); i++) {
        QString bit = input_bin.at(i);
        received.setBit(32-input_bin.size()+i, bit.toUInt(&ok,10));
    } // i

    QBitArray reversed(32,false);
    for(int j = 0; j < 32; j++) {
        reversed.setBit(31-j, received[j]);
    } // j

    QByteArray reversed_byte = bitsToBytes(reversed);
    return reversed_byte.toHex().toUInt(&ok,16);
}
// ------------------------------------------------------------------------ //
QString Config_Base::reverseString(QString str)
{
    QString tmp = str;
    QByteArray ba = tmp.toLocal8Bit();
    char *d = ba.data();
    std::reverse(d,d+tmp.length());
    tmp = QString(d);
    return tmp;
}
// ------------------------------------------------------------------------ //
std::vector<uint32_t> Config_Base::QString_to_UInt32_t(QString str)
{
  //  Assume QString is in hex format;
  std::vector<uint32_t> datagram;
  datagram.resize(0);
  
  bool ok;

  //  std::cout << str.toStdString() << std::endl;

  // break up stream                                                                                           
  for( int i=0; i<str.size(); i=i+8) {
    QString tmp_32byte = str.mid(i,8) ;
    datagram.push_back( (uint32_t) tmp_32byte.toUInt(&ok,16));
  }

  return datagram;

}
// ------------------------------------------------------------------------ //
std::vector<uint32_t> Config_Base::QString_to_UInt32_t(std::string str)
{
  QString str1 = QString::fromStdString(str);

  return QString_to_UInt32_t( str1 );

}
// ------------------------------------------------------------------------ //
void Config_Base::set_output_buffer(QString str)
{
  m_buffer = str;
}
// ------------------------------------------------------------------------ //
void Config_Base::set_output_buffer(std::string str)
{
  m_buffer = QString::fromStdString(str);
}
// ------------------------------------------------------------------------ //
void Config_Base::set_output_buffer(char *str)
{
  m_buffer = QString(str);
}
QString Config_Base::Build_Mask( int boardType, int boardID, int ASIC_Type, int ASIC_ID )
{

  bool ok;

  //  Board Mask in binary
  QString BoardMask;

  if ( boardType <= 2 )      BoardMask += QString("%1").arg(boardType, 3, 2, QChar('0'));
  else if ( boardType == 3 ) BoardMask += QString("%1").arg(0, 3, 2, QChar('0')); // pFEB v2.2 so 0
  else if ( boardType == 4 ) BoardMask += QString("%1").arg(1, 3, 2, QChar('0')); // sFEB v2.2 so 1
  else                       BoardMask += QString("%1").arg(boardType, 3, 2, QChar('0'));
  
  BoardMask += QString("%1").arg(boardID, 3, 2, QChar('0'));
  BoardMask += QString("%1").arg(ASIC_Type, 2, 2, QChar('0'));
  BoardMask += QString("%1").arg(ASIC_ID, 8, 2, QChar('0'));

  // convert board mask to hex
  QString mask = QString("%1").arg(BoardMask.toUInt(&ok, 2), 4, 16,QChar('0') );

  return mask;
}

// ------------------------------------------------------------------------ //
bool Config_Base::SendConfig(int send_to_port, const QHostAddress &target_ip, 
			     QString bit_stream, bool listen_for_reply, bool silent, int search_for_message_type, int wait_time)
{

    stringstream sx;
    bool send_ok = true;
    bool ok;
    bool all_ok = true;

    sx.str("");
    sx << "sending message " << bit_stream.toStdString() << "\n";
    if (!silent) msg()(sx, "Config_Base::SendConfig");

    //////////////////////////////////////////////////
    // build the configuration word(s) to be send to
    // the front ends
    //////////////////////////////////////////////////

    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::WriteOnly);
    out.device()->seek(0);

    ////////////////////////////////////////////////////////////
    // Now prepare and send the word
    ////////////////////////////////////////////////////////////

    // break up stream 
    for( int i=0; i<bit_stream.size(); i=i+4) {
      QString tmp_16byte = bit_stream.mid(i,4) ;
      out << (quint16) tmp_16byte.toUInt(&ok,16);
    }
    //    msg()(datagram.data());

    bool readOK = true;
    send_ok = socket().SendDatagram(datagram, target_ip, send_to_port, "stgc");

    all_ok = send_ok;

    //------------------------------------------------------------------------------//
    //    During a run, all replies are received using a separate daq socket
    //             which constantly listens for data.  This socket is not used
    //------------------------------------------------------------------------------//

    if ( listen_for_reply && !m_running ) {

      std::string outstr = "sent datagram: listing replies";
      set_output_buffer(outstr);
      
      int reply_count = 0;

      if(send_ok) {
	readOK = true;
	
	while ( readOK ) {

	  readOK = socket().waitForReadyRead("daq", wait_time);

	  if(readOK) {

	    //-------------------------------------------------------------------------------------//

	    if(dbg()) msg()("Processing replies...","Config_Base::SendConfig");
	    quint16 target_port = (quint16) send_to_port;
	    QHostAddress *host_ip = new QHostAddress(socket().daqSocket().getBindingIP());
	    
	    QByteArray outbuffer = socket().processReply("daq",host_ip, &target_port );

	    //-------------------------------------------------------------------------------------//
	    
	    sx.str("");
	    sx << "Received message from " << host_ip->toString().toStdString() 
	       << " port " << send_to_port << "\n" 
	       << "message: " << outbuffer.toHex().toStdString() << "\n";
	    if(!silent) msg()(sx, "Config_Base::SendConfig");

	    //-------------------------------------------------------------------------------------//
	    //                         Decode reply message
	    //-------------------------------------------------------------------------------------//

	    std::vector<uint32_t> m_datagram = QString_to_UInt32_t(outbuffer.toHex().toStdString());

	    uint32_t udp_id;
	    int message_type;
	    std::string out_str;

	    bool reply_normal = m_decode->decode_reply(m_datagram, udp_id,
						       message_type, &out_str, false);

	    //---------------------------------------------------------//
	    //          If not searching for sepcific reply
	    //---------------------------------------------------------//

	    if ( search_for_message_type < 0 ) {

	      if ( !reply_normal ) all_ok = false;
	      
	      //-------------------------------------------------------------------------------------//
	      //                If reply is an error output, or output if not silent
	      //-------------------------------------------------------------------------------------//
	      
	      sx.str("");
	      sx << out_str << "\n";
	      if      ( !silent && reply_normal ) {
		msg()(sx,"Config_Base");
	      }
	      else if ( !reply_normal ) {
		msg()(sx,"Config_Base");
		
		sx.str("");
		sx << "message: " << outbuffer.toHex().toStdString() << "\n";
		msg()(sx, "Config_Base");
	      }
	      
	      set_output_buffer(outbuffer.toHex().toStdString());

	    }

	    //------------------------------------------------------//
	    //    Search for a single particular message
	    //------------------------------------------------------//

	    else {
	      all_ok = false; // always not ok until message is found

	      sx.str("");
              sx << out_str << "\n";
              if      ( !silent && reply_normal ) {
                msg()(sx,"Config_Base");
              }
              else if ( !reply_normal ) {
                msg()(sx,"Config_Base");

		sx.str("");
                sx << "message: " << outbuffer.toHex().toStdString() << "\n";
		msg()(sx, "Config_Base");
              }

	      std::string outstr = "timeout waiting for replies";
              set_output_buffer( outstr );
	      
	      //---------------------------------------------------------------//
	      // unless we found the correct message, we consider it an error
	      // break as soon as correct message is found
	      //---------------------------------------------------------------//
	      if ( message_type == search_for_message_type ) {
		all_ok = true;
		set_output_buffer(outbuffer.toHex().toStdString());
		reply_count=1; // the only reply that matters
		break;
	      }

	    }

	    //------------------------------------------------------------------------------------//
	    
	    reply_count++;
	    if ( reply_count >= 4 ) break;

	    //------------------------------------------------------------------------------------//
	  }
	}
      }
    
      if ( reply_count == 0 ) {
	std::string outstr = "timeout waiting for replies";
	set_output_buffer( outstr );
	sx.str("");
	sx << "timeout waiting for replies" << "\n";
	msg()(sx,"Config_Base");
	all_ok = false;
      }
    }
    else {
      //always wait 100 milliseconds between commands
      //boost::this_thread::sleep(boost::posix_time::milliseconds(100));

      all_ok = send_ok;
    }
                                                                                                                  
    boost::this_thread::sleep(boost::posix_time::milliseconds(100));

    return all_ok;

}
//---------------------------------------------------------------------------//
bool Config_Base::SendConfigN(int send_to_port, const QHostAddress &target_ip,
			      QString bit_stream, int ntries, bool listen_for_reply, 
			      bool silent, int search_for_message_type, int wait_time)
{

  bool send_ok = true;

  for ( int i=0; i<ntries; i++ ) {
    send_ok = SendConfig(send_to_port, target_ip, bit_stream, 
			 listen_for_reply, silent, search_for_message_type, wait_time);
    if ( send_ok ) break;
  }

  return send_ok;

}
