
// vmm
#include "config_module_vmm.h"
#include "reply_type.h"

// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// Qt
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QBitArray>
#include <QStringList>

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Configuration
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
Config_VMM::Config_VMM(Config_Base *parent) :
    Config_Base(parent),
    //    m_dbg(false),
    //    m_socketHandler(0),
    m_configHandler(0)//,
    //    m_msg(0)
{
}
// ------------------------------------------------------------------------ //
Config_VMM& Config_VMM::LoadConfig(ConfigHandlerVMM& config)
{
    m_configHandler = &config;
    if(!m_configHandler) {
        msg()("FATAL ConfigHandler instance is null", "Config_VMM::LoadConfig", true);
        exit(1);
    }
    else if(dbg()) {
        msg()("ConfigHandler instance loaded", "Config_VMM::LoadConfig");
    }
    return *this;
}
//---------------------------------------------------//
bool Config_VMM::SendVMMConfig(int send_to_port, const QHostAddress &target_ip)
{
    stringstream sx;
    bool send_ok = true;
    bool ok;
    
    sx.str("");
    //    sx << "sending message " << bit_stream.toStdString() << "\n";
    //    msg()(sx, "Config_VMM::SendConfig");

    //////////////////////////////////////////////////
    // build the configuration word(s) to be send to
    // the front ends
    //////////////////////////////////////////////////

    QByteArray datagram;
    QDataStream out(&datagram, QIODevice::WriteOnly);
    out.device()->seek(0);

    ////////////////////////////////////////////////////////////
    // Now prepare and send the word
    ////////////////////////////////////////////////////////////
    //    send_to_port = 6008;
    //    const QString& ip = "192.168.0.1";

    // break up stream 
    /*
    for( int i=0; i<bit_stream.size(); i=i+4) {
      QString tmp_16byte = bit_stream.mid(i,4) ;
      out << (quint16) tmp_16byte.toUInt(&ok,16);
    }
    */
    //    msg()(datagram.data());

    //////////////////////////////////////////////////
    // Global Registers                               
    //////////////////////////////////////////////////
    std::vector<QString> VMMGlobalRegisters;
    VMMGlobalRegisters.clear();
    fillVMMGlobalRegisters(VMMGlobalRegisters);

    //////////////////////////////////////////////////
    // channel registers                              
    //////////////////////////////////////////////////
    std::vector<QString> channelRegisters;
    channelRegisters.clear();
    fillVMMChannelRegisters(channelRegisters);

    out << (quint32) QString::fromStdString(config().VMMGlobalSettings().header).toUInt(&ok,16);
    out << (quint16) Build_Mask(config().VMMGlobalSettings().board_Type, config().VMMGlobalSettings().boardID,
				config().VMMGlobalSettings().ASIC_Type,  config().VMMGlobalSettings().vmmmask).toUInt(&ok,16);
    out << (quint16) QString::fromStdString(config().VMMGlobalSettings().command).toUInt(&ok,16);

    // first bit to write is the last bit of global bank 2
    out << (quint32)(reverseString(VMMGlobalRegisters.at(5)).toUInt(&ok,2));
    out << (quint32)(reverseString(VMMGlobalRegisters.at(4)).toUInt(&ok,2));
    out << (quint32)(reverseString(VMMGlobalRegisters.at(3)).toUInt(&ok,2));

    // send channels
    for(int i = 63; i >= 0; i--) {
      QString first8bits = channelRegisters.at(i).mid(8,8); // 00000000|sc|sl|...
      QString second16bits = channelRegisters.at(i).mid(16,16);

      // |nu|sz6|sz8|...|sl||sc|
      out << (quint8) (first8bits).toUInt(&ok,2);
      out << (quint16)(second16bits).toUInt(&ok,2);
    } // i

    // last bit to write is first bit of global bank 1
    out << (quint32)(reverseString(VMMGlobalRegisters.at(2)).toUInt(&ok,2));
    out << (quint32)(reverseString(VMMGlobalRegisters.at(1)).toUInt(&ok,2));
    out << (quint32)(reverseString(VMMGlobalRegisters.at(0)).toUInt(&ok,2));

    //cout << "----------------------------------------------------" << endl;
    //cout << "configuration packet vmm " << i << endl;
    //qDebug() << datagram.toHex();
    //cout << endl;

    //--------------------------------------------------------------------------//

    QString tmp = QString::fromStdString(datagram.toHex().toStdString());
    send_ok = SendConfigN(send_to_port, target_ip, tmp, 1 );

    //boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

    return send_ok;

    //--------------------------------------------------------------------------//

}
// ------------------------------------------------------------------------ //
void Config_VMM::fillVMMGlobalRegisters(std::vector<QString>& global)
{
  stringstream sx;
  if(dbg()) msg()("Loading global registers","Config_VMM::fillGlobalRegisters");
  global.clear();
  int pos = 0;


  QString bit32_empty = "00000000000000000000000000000000"; 
  QString tmp;

  //////////////////////////////////////////////////////////////////
  // Global Bank 1
  //////////////////////////////////////////////////////////////////


  ////////////////////////////////////////////////////////////////////
  ////////////////// 32-1 [sp:sdt3]
  QString spi1_0 = bit32_empty;
  pos = 0;

  // sp
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sp));
  pos++;

  // sdp
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sdp));
  pos++;

  // sbmx
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sbmx));
  pos++;

  // sbft
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sbft));
  pos++;

  // sbfp
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sbfp));
  pos++;
    
  // sbfm
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sbfm));
  pos++;
    
  // slg
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().slg));
  pos++;

  // sm5-sm0
  tmp = QString("%1").arg(config().VMMGlobalSettings().sm5,6,2,QChar('0'));
  spi1_0.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // scmx
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().scmx));
  pos++;

  // sfa
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sfa));
  pos++;

  // sfam
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sfam));
  pos++;

  // st
  tmp = QString("%1").arg(config().VMMGlobalSettings().st,2,2,QChar('0'));
  spi1_0.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // sfm
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sfm));
  pos++;

  // sg
  tmp = QString("%1").arg(config().VMMGlobalSettings().sg,3,2,QChar('0'));
  spi1_0.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // sng
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sng));
  pos++;

  // stot
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().stot));
  pos++;

  // sttt
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().sttt));
  pos++;

  // ssh
  spi1_0.replace(pos,1,QString::number(config().VMMGlobalSettings().ssh));
  pos++;

  // stc
  tmp = QString("%1").arg(config().VMMGlobalSettings().stc,2,2,QChar('0'));
  spi1_0.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // first 4bits of sdt
  uint32_t sdt_tmp = 0;
  sdt_tmp |= ( (0x3C0 & config().VMMGlobalSettings().sdt_dac) >> 6 );
  //sdt_tmp |= ( 0xF & config().VMMGlobalSettings().sdt_dac );
  tmp = QString("%1").arg(sdt_tmp,4,2,QChar('0'));
  //tmp = reverseString(tmp);
  spi1_0.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  std::cout << "spi1_0 " << spi1_0.toStdString() << std::endl;

  ////////////////////////////////////////////////////////////////////
  ////////////////// 32-1 [sd4:res00]
  QString spi1_1 = bit32_empty;
  pos = 0;

  // last 6 bits of sdt
  sdt_tmp = 0;
  sdt_tmp |= ((config().VMMGlobalSettings().sdt_dac & 0x3F));// & 0x3F;
  //sdt_tmp |= ((config().VMMGlobalSettings().sdt_dac & 0x3F0) >> 4);// & 0x3F;
  tmp = QString("%1").arg(sdt_tmp,6,2,QChar('0'));
  //tmp = reverseString(tmp);
  spi1_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // sdp
  tmp = QString("%1").arg(config().VMMGlobalSettings().sdp_dac,10,2,QChar('0'));
  //tmp = reverseString(tmp);
  spi1_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // sc010b:sc110b
  tmp = QString("%1").arg(config().VMMGlobalSettings().sc10b,2,2,QChar('0'));
  tmp = reverseString(tmp);
  spi1_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // sc08b:sc18b
  tmp = QString("%1").arg(config().VMMGlobalSettings().sc8b,2,2,QChar('0'));
  tmp = reverseString(tmp);
  spi1_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // sc06b:sc26b
  tmp = QString("%1").arg(config().VMMGlobalSettings().sc6b,3,2,QChar('0'));
  tmp = reverseString(tmp);
  spi1_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // s8b
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().s8b));
  pos++;

  // s6b
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().s6b));
  pos++;

  // s10b
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().s10b));
  pos++;

  // sdcks
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sdcks));
  pos++;

  // sdcka
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sdcka));
  pos++;

  // sdck6b
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sdck6b));
  pos++;

  // sdrv
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sdrv));
  pos++;

  // stpp
  spi1_1.replace(pos,1,QString::number(config().VMMGlobalSettings().stpp));
  pos++;

  std::cout << "spi1_1 " << spi1_1.toStdString() << std::endl;

  // res00

  ////////////////////////////////////////////////////////////////////
  ////////////////// 32-2 [res0:reset]
  QString spi1_2 = bit32_empty;
  pos = 0;

  pos += 4; // first 4 reserved

  // slvs
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvs));
  pos++;

  // s32
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().s32));
  pos++;

  // stcr
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().stcr));
  pos++;

  // ssart
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().ssart));
  pos++;

  // srec
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().srec));
  pos++;

  // stlc
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().stlc));
  pos++;

  // sbip
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().sbip));
  pos++;

  // srat
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().srat));
  pos++;

  // sfrst
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().sfrst));
  pos++;

  // slvsbc
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvsbc));
  pos++;

  // slvstp
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvstp));
  pos++;

  // slvstk
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvstk));
  pos++;

  // slvsdt
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvsdt));
  pos++;

  // slvsart
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvsart));
  pos++;

  // slvstki
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvstki));
  pos++;

  // slvsena
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvsena));
  pos++;

  // slvs6b
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().slvs6b));
  pos++;

  // sL0enaV
  spi1_2.replace(pos,1,QString::number(config().VMMGlobalSettings().sL0enaV));
  pos+=9; // we have 8 nu before the reset bits

  // reset
  uint32_t reset_flags = 0;
  if(config().VMMGlobalSettings().reset == 1) {
    // raise last 2 bits high if doing reset
    reset_flags = 3;
  }
  tmp = QString("%1").arg(reset_flags,2,2,QChar('0'));
  //tmp = reverseString(tmp);
  spi1_2.replace(pos, tmp.size(),tmp);
  
  std::cout << "spi1_2 " << spi1_2.toStdString() << std::endl;

  
  //////////////////////////////////////////////////////////////////
  // Global Bank 2
  //////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////
  ////////////////// 32-0 [nu:nskipm]
  QString spi2_0 = bit32_empty;
  pos = 0;

  // bits [0:30] are not-used
  pos+=31;

  // nskipm
  spi2_0.replace(pos,1,QString::number(config().VMMGlobalSettings().nskipm));
    
  std::cout << "spi2_0 " << spi2_0.toStdString() << std::endl;

  ////////////////////////////////////////////////////////////////////
  ////////////////// 32-1 [sL0cktest:rollover]

  QString spi2_1 = bit32_empty;
  pos = 0;

  // sL0cktest
  spi2_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sL0cktest));
  pos++;

  // sL0dckinv
  spi2_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sL0dckinv));
  pos++;

  // sL0ckinv
  spi2_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sL0ckinv));
  pos++;

  // sL0ena
  spi2_1.replace(pos,1,QString::number(config().VMMGlobalSettings().sL0ena));
  pos++;

  // truncate
  tmp = QString("%1").arg(config().VMMGlobalSettings().truncate,6,2,QChar('0'));
  tmp = reverseString(tmp);
  spi2_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // nskip
  tmp = QString("%1").arg(config().VMMGlobalSettings().nskip,7,2,QChar('0'));
  tmp = reverseString(tmp);
  spi2_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // window
  tmp = QString("%1").arg(config().VMMGlobalSettings().window,3,2,QChar('0'));
  tmp = reverseString(tmp);
  spi2_1.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // rollover
  tmp = QString("%1").arg(config().VMMGlobalSettings().rollover,12,2,QChar('0'));
  tmp = reverseString(tmp);
  spi2_1.replace(pos,tmp.size(),tmp);

  std::cout << "spi2_1 " << spi2_1.toStdString() << std::endl;    

  ////////////////////////////////////////////////////////////////////
  ////////////////// 32-2 [l0offset:offset]

  QString spi2_2 = bit32_empty;
  pos = 0;

  // l0offset
  tmp = QString("%1").arg(config().VMMGlobalSettings().l0offset,12,2,QChar('0'));
  tmp = reverseString(tmp);
  spi2_2.replace(pos,tmp.size(),tmp);
  pos+=tmp.size();

  // offset
  tmp = QString("%1").arg(config().VMMGlobalSettings().offset,12,2,QChar('0'));
  tmp = reverseString(tmp);
  spi2_2.replace(pos,tmp.size(),tmp);

  std::cout << "spi2_2 " << spi2_2.toStdString() << std::endl;

  global.push_back(spi1_0);
  global.push_back(spi1_1);
  global.push_back(spi1_2);
  global.push_back(spi2_0);
  global.push_back(spi2_1);
  global.push_back(spi2_2);
  
}
// ------------------------------------------------------------------------ //
void Config_VMM::fillVMMChannelRegisters(std::vector<QString>& channels)
{
  stringstream sx;
  if(dbg()) msg()("Loading channel configuration", "Config_VMM::fillChannelRegisters");
  
  channels.clear();
  int pos = 0;
  QString tmp;
  QString reg;
  QString bit32_empty = "00000000000000000000000000000000"; 

  for(int i = 0; i < 64; ++i) {
    reg = bit32_empty;
    pos = 0;
    //  pos = 8;
    
    // sc
    reg.replace(pos,1,QString::number(config().VMM_ChannelSettings(i).sc)); 
    pos++;
    
    // sl
    reg.replace(pos,1,QString::number(config().VMM_ChannelSettings(i).sl));
    pos++;
    
    // st
    reg.replace(pos,1,QString::number(config().VMM_ChannelSettings(i).st));
    pos++;
    
    // sth
    reg.replace(pos,1,QString::number(config().VMM_ChannelSettings(i).sth));
    pos++;
    
    // sm
    reg.replace(pos,1,QString::number(config().VMM_ChannelSettings(i).sm));
    pos++;
    
    // smx
    reg.replace(pos,1,QString::number(config().VMM_ChannelSettings(i).smx));
    pos++;
    
    // sd (trim)
    tmp = QString("%1").arg(config().VMM_ChannelSettings(i).sd,5,2,QChar('0')); 
    // sd0:sd4 -- is reversal still needed for trim in VMM3?
    tmp = reverseString(tmp);
    reg.replace(pos,tmp.size(),tmp);
    pos+=tmp.size();
    
    // sz10b 10 bit adc lsb
    tmp = QString("%1").arg(config().VMM_ChannelSettings(i).sz10b,5,2,QChar('0'));
    tmp = reverseString(tmp);
    reg.replace(pos,tmp.size(),tmp);
    pos+=tmp.size();
    
    // sz8b 8 bit adc lsb
    tmp = QString("%1").arg(config().VMM_ChannelSettings(i).sz8b,4,2,QChar('0'));
    tmp = reverseString(tmp);
    reg.replace(pos,tmp.size(),tmp);
    pos+=tmp.size();
    
    // sz6b 6 bit adc lsb
    tmp = QString("%1").arg(config().VMM_ChannelSettings(i).sz6b,3,2,QChar('0'));
    tmp = reverseString(tmp);
    reg.replace(pos,tmp.size(),tmp);
    pos+=tmp.size();
    
    // push back
    reg = reverseString(reg);
    channels.push_back(reg);
    
  } // i
}


