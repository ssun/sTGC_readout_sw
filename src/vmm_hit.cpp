#include "vmm_hit.h"


// std/stl
#include <iostream>
#include <sstream>
using namespace std;

VMMHit::VMMHit() :
    m_flag(0),
    m_pdo(0),
    m_tdo(0),
    m_graycode_bcid(0),
    m_decoded_graycode_bcid(0),
    m_pass_threshold(0),
    m_vmm_channel(0),
    m_mapped_channel(-1),
    m_feb_channel(-1),
    m_board_id(-1)
{
}

/////////////////////////////////////////////////////////////////////////////
void VMMHit::print()
{
    stringstream sx;
    sx << "VMMHit  board: " << m_board_id << " channel: " << m_vmm_channel
       << " pdo: " << m_pdo << " tdo: " << m_tdo << " gray: " << m_graycode_bcid
       << " decided gray: " << m_decoded_graycode_bcid; 
    cout << sx.str() << endl;
}
/////////////////////////////////////////////////////////////////////////////
void VMMHit::clear()
{
    m_flag = 0;
    m_pdo  = 0;
    m_tdo  = 0;
    m_graycode_bcid = 0;
    m_decoded_graycode_bcid = 0;
    m_pass_threshold = 0;
    m_vmm_channel = 0;
    m_mapped_channel = -1;
    m_feb_channel = -1;
    m_board_id = -1;
}
