#include "roc_fpga_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ROC_FPGASetting::ROC_FPGASetting() :
  header(""),
  boardID(0),
  board_Type(0),
  ASIC_Type(2),
  chipID(0),
  command(""),
  UniqueS(""),
  vmm_enable_mask(0),
  cktp_vs_ckbc_skew(0),
  cktp_width(0),
  trigger_window_width(0),
  ok(false)
{
}
void ROC_FPGASetting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Global Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << GlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    cout << ss.str() << endl;
    */

}
