#include "tds_lut.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  LUT
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
TDS_LUT::TDS_LUT() :
    number(0),
    BandID(0),
    LeadStrip(0),
    ok(false)
{
}
void TDS_LUT::print()
{
    stringstream ss;
    ss << " LUT     " << number << endl;
    ss << " BandID :    " << BandID << endl;
    ss << " LeadStrip : " << LeadStrip << endl;
    cout << ss.str() << endl;
}
