#include "board_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  BoardSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
BoardSetting::BoardSetting() :
  boardID(-1),
  boardUN(QString::fromStdString("")),
  ROC_UniqueS(QString::fromStdString("")),
  nVMM(0),
  nTDS(0),
  ok(false)
{
  VMMChip_ID.clear();
  TDSChip_ID.clear();
  VMM_UniqueS_Numbers.clear();
  TDS_UniqueS_Numbers.clear();
  GPIO_dir_enable.clear();
  GPIO_dout_enable.clear();
  GPIO_dir_enable.resize(0);
  GPIO_dout_enable.resize(0);
}
void BoardSetting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Board Settings " << endl;

    ss << "     > test pulse DAC            : "
        << test_pulse_dac << endl;
    ss << "------------------------------------------------------" << endl;

    cout << ss.str() << endl;
    */

}
