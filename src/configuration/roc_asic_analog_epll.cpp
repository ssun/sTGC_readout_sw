#include "roc_asic_analog_epll.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ROC_ASIC_analog_epll::ROC_ASIC_analog_epll() :
  ok(false)
{

  ePllPhase40MHz_0 = 0;
  ePllPhase40MHz_1 = 0;
  ePllPhase40MHz_2 = 0;
  ePllPhase40MHz_3 = 0;

  ePllPhase160MHz_0 = 0;
  ePllPhase160MHz_1 = 0;
  ePllPhase160MHz_2 = 0;
  ePllPhase160MHz_3 = 0;

  ePllInstantLock = false;
  ePllReset = false;
  bypassPll = false;
  ePllLockEn = false;

  ePll_Ref_Freq = 0;
  ePllEnablePhase = 0;
  ePll_lcp = 0;
  ePll_cap = 0;

  tp_phase_0 = 0;
  tp_phase_1 = 0;
  tp_phase_2 = 0;
  tp_phase_3 = 0;
  tp_bypass_mask = 0;

  ctrl_phase_0 = 0;
  ctrl_phase_1 = 0;
  ctrl_phase_2 = 0;
  ctrl_phase_3 = 0;
  ctrl_bypass_mask = 0;

  ctrl_delay_0 = 0;
  ctrl_delay_1 = 0;
  ctrl_delay_2 = 0;
  ctrl_delay_3 = 0;
  ctrl_delay_mask = 0;

  tx_enable = 0;
  tx_csel_enable = 0;

}
void ROC_ASIC_analog_epll::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Global Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << GlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    cout << ss.str() << endl;
    */

}
