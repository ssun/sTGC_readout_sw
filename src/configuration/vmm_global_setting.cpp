#include "vmm_global_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


const QStringList VMMGlobalSetting::all_polarities
    = {"wires", "strips"};
const QStringList VMMGlobalSetting::all_gains
    = {"0.5", "1.0", "3.0", "4.5", "6.0", "9.0", "12.0", "16.0"}; //mV/fC
const QList<int> VMMGlobalSetting::all_peakTimes
    = {200, 100, 50, 25}; //ns
const QList<int> VMMGlobalSetting::all_TACslopes
    = {125, 250, 500, 1000}; //ns
const QStringList VMMGlobalSetting::all_ARTmodes
    = {"threshold", "peak"};
const QStringList VMMGlobalSetting::all_directTimeModes
    = {"TtP", "ToT", "PtP", "PtT"};
const QStringList VMMGlobalSetting::all_ADC10bits
    = {"200ns", "+60ns", "+120ns", "+180ns"};
const QStringList VMMGlobalSetting::all_ADC8bits
    = {"100ns", "+60ns", "+120ns", "+180ns"};
const QStringList VMMGlobalSetting::all_ADC6bits
    = {"low", "middle", "up"};

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
VMMGlobalSetting::VMMGlobalSetting() :
    /////////////////////////////////
    // VMM3 registers
    /////////////////////////////////
  header(""),
  boardID(0),
  ASIC_Type(1),
  vmmmask(0),
  command(""),
    sp(0),
    sdp(0),
    sbmx(0),
    sbft(0),
    sbfp(0),
    sbfm(0),
    slg(1),
    sm5(0),
    scmx(0),
    sfa(0),
    sfam(0),
    st(0),
    sfm(0),
    sg(2),
    sng(0),
    stot(0),
    stpp(0),
    sttt(0),
    ssh(0),
    stc(0),
    sdt_dac(230),
    sdp_dac(250),
    sc10b(0),
    sc8b(0),
    sc6b(0),
    s8b(0),
    s6b(0),
    s10b(1),
    sdcks(0),
    sdcka(0),
    sdck6b(0),
    sdrv(0),
    slvs(0),
    stcr(1),
    ssart(0),
    s32(0),
    stlc(0),
    srec(0),
    sbip(0),
    srat(0),
    sfrst(0),
    slvsbc(0),
    slvstp(0),
    slvstk(0),
    slvsdt(0),
    slvsart(0),
    slvstki(0),
    slvsena(0),
    slvs6b(0),
    sL0enaV(0),
    reset(0),
    sL0ena(0),
    l0offset(0),
    offset(0),
    rollover(0),
    window(0),
    truncate(0),
    nskip(0),
    sL0cktest(0),
    sL0ckinv(0),
    sL0dckinv(0),
    nskipm(0),
    
    /////////////////////////////////
    // VMM2 registers
    /////////////////////////////////
    /*
    polarity(0),
    leakage_current(1),
    analog_tristates(0),
    double_leakage(1),
    gain(2),
    peak_time(0),
    neighbor_trigger(0),
    tac_slope(0),
    disable_at_peak(0),
    art(1),
    art_mode(0),
    dual_clock_art(0),
    out_buffer_mo(0),
    out_buffer_pdo(0),
    out_buffer_tdo(0),
    channel_monitor(0),
    monitoring_control(1),
    monitor_pdo_out(0),
    adcs(1),
    sub_hysteresis(0),
    direct_time(0),
    direct_time_mode(1),
    direct_time_mode0(0),
    direct_time_mode1(1),
    conv_mode_8bit(1),
    enable_6bit(0),
    adc_10bit(0),
    adc_8bit(0),
    adc_6bit(0),
    dual_clock_data(0),
    dual_clock_6bit(0),
    threshold_dac(200),
    test_pulse_dac(300),
    */
    ok(false)
{

  chan_baselines.resize(0);
  chan_baseline_vs_trim.resize(0);
  opt_trim_bits.resize(0);

}
void VMMGlobalSetting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " VMMGlobal Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << VMMGlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    ss << "     > channel leakage current   : "
        << boolalpha << leakage_current << endl;

    ss << "     > analog tristates          : "
        << boolalpha << analog_tristates << endl;

    ss << "     > double leakage            : "
        << boolalpha << double_leakage << endl;

    ss << "     > gain                      : "
        << gain << " ("
        << VMMGlobalSetting::all_gains[gain].toStdString() << " mV/fC)" << endl;

    ss << "     > peak time                 : "
        << peak_time << " ("
        << VMMGlobalSetting::all_peakTimes[peak_time] << " ns)" << endl;

    ss << "     > neighbor trigger          : "
        << boolalpha << neighbor_trigger << endl;

    ss << "     > TAC slope adj             : "
        << tac_slope << " ("
        << VMMGlobalSetting::all_TACslopes[tac_slope] << " ns)" << endl;

    ss << "     > disable at peak           : "
        << boolalpha << disable_at_peak << endl;

    ss << "     > ART                       : "
        << boolalpha << art << endl;

    ss << "     > ART mode                  : "
        << art_mode << " ("
        << VMMGlobalSetting::all_ARTmodes[art_mode].toStdString() << ")" << endl;

    ss << "     > dual clock ART            : "
        << boolalpha << dual_clock_art << endl;

    ss << "     > out buffer mo             : "
        << boolalpha << out_buffer_mo << endl;

    ss << "     > out buffer pdo            : "
        << boolalpha << out_buffer_pdo << endl;

    ss << "     > out buffer tdo            : "
        << boolalpha << out_buffer_tdo << endl;

    ss << "     > channel monitoring        : "
        << channel_monitor << endl;

    ss << "     > monitoring control        : "
        << boolalpha << monitoring_control << endl;

    ss << "     > monitor pdo out           : "
        << boolalpha << monitor_pdo_out << endl;

    ss << "     > ADCs                      : "
        << boolalpha << adcs << endl;

    ss << "     > sub hysteresis discr      : "
        << boolalpha << sub_hysteresis << endl;

    ss << "     > direct time               : "
        << boolalpha << direct_time << endl;

    ss << "     > direct time mode          : "
        << direct_time_mode << " ("
        << VMMGlobalSetting::all_directTimeModes[direct_time_mode].toStdString()
        << ")" << endl;

    ss << "     > conv mode 8bit            : "
        << boolalpha << conv_mode_8bit << endl;

    ss << "     > enable 6bit               : "
        << boolalpha << enable_6bit << endl;

    ss << "     > ADC 10bit                 : "
        << adc_10bit << " ("
        << VMMGlobalSetting::all_ADC10bits[adc_10bit].toStdString() << ")" << endl;

    ss << "     > ADC 8bit                  : "
        << adc_8bit << " ("
        << VMMGlobalSetting::all_ADC8bits[adc_8bit].toStdString() << ")" << endl;

    ss << "     > ADC 6bit                  : "
        << adc_6bit << " ("
        << VMMGlobalSetting::all_ADC6bits[adc_6bit].toStdString() << ")" << endl;

    ss << "     > dual clock data           : "
        << boolalpha << dual_clock_data << endl;

    ss << "     > dual clock 6bit           : "
        << boolalpha << dual_clock_6bit << endl;

    ss << "     > threshold DAC             : "
        << threshold_dac << endl;

    ss << "     > test pulse DAC            : "
        << test_pulse_dac << endl;
    ss << "------------------------------------------------------" << endl;

    cout << ss.str() << endl;
    */

}
