#include "tds_channel.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Channel
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
TDS_Channel::TDS_Channel() :
    number(0),
    enabled(0),
    delay(0),
    ok(false)
{
}
void TDS_Channel::print()
{
    stringstream ss;
    ss << "Channel " << number << endl;
    ss << "    > enabled     : " << enabled << endl;
    ss << "    > delay       : " << delay << endl;
    cout << ss.str() << endl;
}
