#include "comm_info.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  CommInfo
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
CommInfo::CommInfo() :
    fec_port(6007),
    daq_port(6006),
    vmmasic_port(6603),
    vmmapp_port(6600),
    clocks_port(6602),
    config_filename(""),
    ip_list(""),
    ok(false)
{
}
// ------------------------------------------------------------------------ //
void CommInfo::print()
{
    stringstream ss;

    ss << "------------------------------------------------------" << endl;
    ss << " UDP Settings " << endl;
    ss << "     > FEC port          : "
        << fec_port << endl;
    ss << "     > DAQ port          : "
        << daq_port << endl;
    ss << "     > VMMASIC port      : "
        << vmmasic_port << endl;
    ss << "     > VMMAPP port       : "
        << vmmapp_port << endl;
    ss << "     > clocks port           : "
        << clocks_port << endl;
    ss << "     > IP list           : "
        << ip_list.toStdString()  << endl;
    ss << "------------------------------------------------------" << endl;

    cout << ss.str() << endl;
}
