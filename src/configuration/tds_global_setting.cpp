#include "tds_global_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


/*
const QStringList GlobalSetting::all_polarities
    = {"wires", "strips"};
const QStringList GlobalSetting::all_gains
    = {"0.5", "1.0", "3.0", "4.5", "6.0", "9.0", "12.0", "16.0"}; //mV/fC
const QList<int> GlobalSetting::all_peakTimes
    = {200, 100, 50, 25}; //ns
const QList<int> GlobalSetting::all_TACslopes
    = {125, 250, 500, 1000}; //ns
const QStringList GlobalSetting::all_ARTmodes
    = {"threshold", "peak"};
const QStringList GlobalSetting::all_directTimeModes
    = {"TtP", "ToT", "PtP", "PtT"};
const QStringList GlobalSetting::all_ADC10bits
    = {"200ns", "+60ns", "+120ns", "+180ns"};
const QStringList GlobalSetting::all_ADC8bits
    = {"100ns", "+60ns", "+120ns", "+180ns"};
const QStringList GlobalSetting::all_ADC6bits
    = {"low", "middle", "up"};
*/

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
TDSGlobalSetting::TDSGlobalSetting() :
    /////////////////////////////////
    // VMM3 registers
    /////////////////////////////////
  header(""),
  boardID(0),
  ASIC_Type(2),
  chipID(0),
  command(""),
  bcid_offset(0),
  bcid_rollover(0),
  bcid_clockphase(0),
  match_window(0),
  vmm0_clockphase(0),
  vmm1_clockphase(0),
  SER_PLL_current(0),
  SER_PLL_resistor(0),
  soft_resets(0),
  reject_window(0),
  bypass_prompt(0),
  prompt_circuit(0),
  bypass_trigger(0),
  bypass_scrambler(0),
  test_frame2Router_enable(0),
  stripTDS_globaltest(0),
  PRBS_en(0),
  //  timer(0),
  ok(false)
{
}
void TDSGlobalSetting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Global Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << GlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    cout << ss.str() << endl;
    */

}
