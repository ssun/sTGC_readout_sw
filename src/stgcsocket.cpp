
// stgc
#include "stgcsocket.h"

// Qt
#include <QByteArray>

// std/stl
#include <sstream>
#include <iostream>
using namespace std;

/////////////////////////////////////////////////////////////////////////////
// ----------------------------------------------------------------------- //
//  sTGCSocket
// ----------------------------------------------------------------------- //
/////////////////////////////////////////////////////////////////////////////

sTGCSocket::sTGCSocket(QObject* parent) :
    QObject(parent),
    m_dbg(false),
    m_msg(0),
    m_name(""),
    //m_HostIp("0.0.0.0"),
    //m_bindingPort(0),
    m_socket(0)
{
    m_socket = new QUdpSocket(this);
    connect(m_socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    
    m_HostIp = QString("0.0.0.0");
    m_bindingPort = 0;
}
// ----------------------------------------------------------------------- //
void sTGCSocket::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
// ----------------------------------------------------------------------- //
bool sTGCSocket::bindSocket(const QHostAddress &HostIp, quint16 port, QAbstractSocket::BindMode mode)
{
  bool bind = true;
  if(m_socket) {
    bind = m_socket->bind(HostIp, port, mode);
  }
  else {
    msg()("Socket named " + getName() + " is null!",
	  "sTGCSocket::bindSocket",true);
    bind = false;
  }
  if(!bind) {
    msg()("WARNING: Unable to bind socket named " + getName(),
	  "sTGCSocket::bindSocket", true);
  }
  else {

    setBindingIp( HostIp.toString() );
    stringstream sx;
    sx << "Socket named " << getName() << " successfully bound to port " << port << " IP " << m_HostIp.toStdString();
    if(dbg())
      msg()(sx, "sTGCSocket::bindSocket");
    
    cout << "sTGCSocket::bindSocket    " << sx.str() << endl;
  }
  return bind;
}
// ----------------------------------------------------------------------- //
bool sTGCSocket::isBound()
{
    return socket().state() == 4;
}
// ----------------------------------------------------------------------- //
bool sTGCSocket::hasPendingDatagrams()
{
    return m_socket->hasPendingDatagrams();
}
// ----------------------------------------------------------------------- //
quint64 sTGCSocket::pendingDatagramSize()
{
    return m_socket->pendingDatagramSize();
}
// ----------------------------------------------------------------------- //
quint64 sTGCSocket::readDatagram(char* data, quint64 maxSize,
				 QHostAddress* address, quint16* port)
{
    return m_socket->readDatagram(data, maxSize, address, port);
}
// ----------------------------------------------------------------------- //
void sTGCSocket::readyRead()
{
    if(dbg()) msg()(getName() + " socket receiving data...",
		    "sTGCSocket::readyRead");

    if     (getName()=="stgc" || getName()=="STGC")
        emit dataReady();
}
// ----------------------------------------------------------------------- //
quint64 sTGCSocket::writeDatagram(const QByteArray& datagram,
				  const QHostAddress& host, quint16 port)
{
    return m_socket->writeDatagram(datagram, host, port);
}
// ----------------------------------------------------------------------- //
bool sTGCSocket::checkAndReconnect(std::string callingFn)
{
    stringstream sx;
    bool status = true;
    if(m_socket->state() == QAbstractSocket::UnconnectedState) {
        status = false;
        string fn = "";
        if(callingFn!="") fn = "(" + callingFn + ") ";

        if(dbg()) {
            sx << fn << "About to rebind socket named " << getName() << " to port "
               << getBindingPort();
            msg()(sx, "sTGCSocket::checkAndReconnect");
        }
        bool bnd = m_socket->bind(getBindingPort(), QUdpSocket::ShareAddress);
        if(!bnd) {
            status = false;
            sx.str("");
            sx << fn << "ERROR Unable to re-bind socket named " << getName() << "to port "
               << getBindingPort();
            msg()(sx, "sTGCSocket::checkAndReconnect");
            closeAndDisconnect(callingFn);
        }
        else {
            status = true;
            if(dbg()) { 
                sx.str("");
                sx << fn << "Socket named " << getName() << " successfully rebound to port "
                   << getBindingPort();
                msg()(sx,"sTGCSocket::checkAndReconnect");
            }
        }
    }
    return status;
}
// ----------------------------------------------------------------------- //
void sTGCSocket::closeAndDisconnect(std::string callingFn)
{
    stringstream sx;
    string fn = "";
    if(callingFn!="") fn = "(" + callingFn + ") ";
    if(dbg())
        sx << fn << "Closing and disconnecting from host the socket"
           << " named " << getName() << " (bound on port " << getBindingPort()
           << ")";
        msg()(sx,"sTGCSocket::closeAndDisconnect");
    m_socket->close();
    m_socket->disconnectFromHost();
}
// ----------------------------------------------------------------------- //
QByteArray sTGCSocket::processReply(QHostAddress *target_ip, quint16 *target_port)
{
    stringstream sx;
    //debug
    sx << getName() << " socket processing replies for IP: " + target_ip->toString().toStdString();
    if(dbg()) msg()("Processing datagram replies for IP: " + target_ip->toString().toStdString(),
		    "sTGCSocket::processReply");

    //    bool ok;
    QString datagram_hex;

    QByteArray datagram;
    datagram.clear();

    // added a protection to avoid reading if continuous output by VMM;
    int n_datagram = 0 ;
    int n_maxdatagram = 200;

    // wait for a reply
    while(socket().hasPendingDatagrams()) {

      if ( n_datagram > n_maxdatagram ) {
	sx.str("");
	sx << "Error: socket received more than " << n_maxdatagram 
	   << " maximum number of datagrams in one sitting, Aborting \n";
	msg()(sx,"sTGCSocket::processReply");sx.str("");
	break;
      }

        //debug
        if(dbg()) {
            sx.str("");
            sx << "socket " << getName() << " has datagrams";
            msg()(sx,"sTGCSocket::processReply");sx.str("");
        }

        // read the datagram in reply
        datagram.resize(socket().pendingDatagramSize());
        socket().readDatagram(datagram.data(), datagram.size(), target_ip, target_port);
	n_datagram++;

        if(dbg()) {
            //debug
            sx.str("");
            sx << "Received datagram (hex): \n"
               << datagram.toHex().toStdString() << "\n"
               //<< "from stgc with IP: " << vmm_ip 
               << "from stgc with IP: " << target_ip->toString().toStdString()
               << " and message size is " << datagram.size();
            msg()(sx,"sTGCSocket::processReply");sx.str("");
        }

        datagram_hex.clear();
        //debug:  First part of message should match 
	//        the command that was sent that its replying to
        datagram_hex = datagram.mid(0,4).toHex();
        //datagram_hex = buffer().mid(0,4).toHex();

        //quint32 received = datagram_hex.toUInt(&ok,16);

	// Disable check for now -ssun 15/06/2017
	/*
        if(received != cmd_cnt_to_check) {
            sx.str("");
            sx << "WARNING Command number received (" << received << ") does not match "
               << "internal command counter expected (" << cmd_cnt_to_check
               << ")";
            msg()(sx, "sTGCSocket::processReply");
        }
	*/

    }//while

    return datagram;

}
// ----------------------------------------------------------------------- //
void sTGCSocket::Print()
{
    stringstream ss;
    ss << "    Name          : " << getName() << "\n"
       << "    Bound to port : " << getBindingPort() << "\n"
       << "    Bound to IP   : " << getBindingIP().toStdString() << "\n";

    if ( m_socket->state() == 0 ) ss << "    Status        :  Unconnected \n";
    if ( m_socket->state() == 1 ) ss << "    Status        :  Looking up host \n";
    if ( m_socket->state() == 2 ) ss << "    Status        :  Currently Connecting \n";
    if ( m_socket->state() == 3 ) ss << "    Status        :  Connected \n";
    if ( m_socket->state() == 4 ) ss << "    Status        :  Bound \n";
    if ( m_socket->state() == 5 ) ss << "    Status        :  Listening \n";
    if ( m_socket->state() == 6 ) ss << "    Status        :  Currently Closing \n";

    msg()(ss,"sTGCSocket::Print");
}
