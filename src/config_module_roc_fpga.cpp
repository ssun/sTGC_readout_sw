
// vmm
#include "config_module_roc_fpga.h"
#include "vmm_decoder.h"
// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// Qt
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QBitArray>
#include <QStringList>

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_Roc_FPGA
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
Config_RocFPGA::Config_RocFPGA(Config_Base *parent) :
  Config_Base(parent),
  m_configHandler(0)
{
}
// ------------------------------------------------------------------------ //
Config_RocFPGA& Config_RocFPGA::LoadConfig(ConfigHandlerRocFPGA& config)
{
    m_configHandler = &config;
    if(!m_configHandler) {
        msg()("FATAL ConfigHandler instance is null", "Config_RocFPGA::LoadConfig", true);
        exit(1);
    }
    else if(dbg()) {
        msg()("ConfigHandler instance loaded", "Config_RocFPGA::LoadConfig");
    }
    return *this;
}
//---------------------------------------------------// 
bool Config_RocFPGA::SendRocFPGAConfig(int send_to_port, const QHostAddress &target_ip)
{
  stringstream sx;
  bool send_ok = true;
  bool ok;

  sx.str("");
  //    sx << "sending message " << bit_stream.toStdString() << "\n";
  //    msg()(sx, "Config_RocFPGA::SendConfig");

  //////////////////////////////////////////////////
  // build the configuration word(s) to be send to  
  // the front ends                                 
  //////////////////////////////////////////////////

  QByteArray datagram;
  QDataStream out(&datagram, QIODevice::WriteOnly);
  out.device()->seek(0);

  //////////////////////////////////////////////////
  // Global Registers                               
  //////////////////////////////////////////////////
  std::vector<QString> RocFPGA_GlobalRegisters;
  RocFPGA_GlobalRegisters.clear();
  fillRocFPGAGlobalRegisters(RocFPGA_GlobalRegisters);

  //----------------------------------------------//

  out << (quint32) QString::fromStdString(config().ROC_FPGASettings().header).toUInt(&ok,16);
  out << (quint16) Build_Mask(config().ROC_FPGASettings().board_Type, config().ROC_FPGASettings().boardID,
			      config().ROC_FPGASettings().ASIC_Type,  config().ROC_FPGASettings().chipID).toUInt(&ok,16);
  out << (quint16) QString::fromStdString(config().ROC_FPGASettings().command).toUInt(&ok,16);

  //  sx.str("");
  //  sx << config().RocFPGAMap().toStdString() << "\n";
  //  msg()(sx,"Config_RocFPGA::SendConfig");

  //------------------------------------------------------//
  //    128 bit register for ROC FPGA
  //------------------------------------------------------//

  // 32 bits of vmm_enable_mask, cktp_vs_ckbc_skew, cktp width (MSB first)
  out << (quint32)(RocFPGA_GlobalRegisters.at(0).toUInt(&ok,2));
  // 16 bits of trigger_window_width (MSB first)
  out << (quint16)(RocFPGA_GlobalRegisters.at(1).toUInt(&ok,2));

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  // 80 bits of filler (MSB first)
  out << (quint16)(bit16_empty.toUInt(&ok,2));
  out << (quint32)(bit32_empty.toUInt(&ok,2));
  out << (quint32)(bit32_empty.toUInt(&ok,2));

  //--------------------------------------------------------------//

  QString tmp = QString::fromStdString( datagram.toHex().toStdString() );
  send_ok = SendConfigN(send_to_port, target_ip, tmp, 1 );

  //boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

  return send_ok;

  //--------------------------------------------------------------//

}
// ------------------------------------------------------------------------ //
void Config_RocFPGA::fillRocFPGAGlobalRegisters(std::vector<QString> &global)
{
  stringstream sx;
  if(dbg()) msg()("Loading global registers","Config_RocFPGA::fillGlobalRegisters");
  global.clear();
  int pos = 0;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  //-----------------------------------//
  //          first 32 bits
  //-----------------------------------//

  QString spi1_0 = bit32_empty;
  pos = 0;

  QString str;

  //------------------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_FPGASettings().vmm_enable_mask, 8, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //-----------------------------------------------------------------//

  str = QString("%1").arg(config().ROC_FPGASettings().cktp_vs_ckbc_skew, 8, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(config().ROC_FPGASettings().cktp_width, 16, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  //-----------------------------------//  
  //          last 16 bits               
  //-----------------------------------//  

  QString spi1_1 = bit16_empty;
  pos = 0;

  str = QString("%1").arg(config().ROC_FPGASettings().trigger_window_width, 16, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  global.push_back(spi1_0);
  global.push_back(spi1_1);
  
}

