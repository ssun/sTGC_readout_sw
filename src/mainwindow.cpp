/////////////////////////////////////////
//
// mainwindow
//
// Main GUI for sTGC DAQ/DCS
//
// siyuan.sun@cern.ch,
// June 2017
//
// based on code written by daniel.joseph.antrim@cern.ch 
// for vmm_readout_sw
//////////////////////////////////////////

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "reply_type.h"

// qt
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>
#include <QFileDialog>
#include <QTime>
#include <QScrollBar>
#include <QInputDialog>
#include <QMessageBox>
// multiselect
#include <QListWidget>

// std/stl
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>

//enum setupSteps { setupAll, configuringSCA, configuringChips, configuringTTC, enablingDAQ, monitoringReadout };
enum calib_type { type_trimming, type_baseline, type_ThDAC };

using namespace std;


// ------------------------------------------------------------------------- //
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_dbg(true),
    HOSTPORT(7201),
    TARGETPORT(6008),
    HostIp("0.0.0.0"),
    TargetIp("0.0.0.0")
    //boardConfigHandler(0),
    //vmmConfigHandler(0),
    //tdsConfigHandler(0),
    //rocFPGAConfigHandler(0),
    //stgcSocketHandler(0),
    //boardConfigModule(0),
    //vmmConfigModule(0),
    //tdsConfigModule(0),
    //rocFPGAConfigModule(0),
    //stgcMessageHandler(0),
    //vmmDataHandler(0),
    //vmmRunModule(0),
    //ignore_channel_update(false),
    //ignore_tds_channel_update(false),
    //reply_category(-1),
    //m_is_running(false),
    //m_commOK(false),
    //stgc_bound(false),
    //daq_bound(false),
    //data_bound(false),
    //prev_mon_chan(-1)
{

  ignore_channel_update = false;
  ignore_tds_channel_update = false;
  reply_category = -1;
  m_is_running = false;
  m_commOK     = false;
  stgc_bound   = false;
  daq_bound    = false;
  data_bound   = false;
  prev_mon_chan = -1; 

    ui->setupUi(this);

    stgcMessageHandler = new MessageHandler();
    stgcMessageHandler->setMessageSize(116);
    stgcMessageHandler->setGUI(true);
    connect(stgcMessageHandler, SIGNAL(logReady()), this, SLOT(readLog()));

    //    this->setFixedSize(1200*1.03,720); // 1400, 725
    //this->setFixedSize(1200,720); // 1400, 725
    //this->setMinimumHeight(720);
    //this->setMinimumWidth(1200);
    Font.setFamily("Arial");
    ui->loggingScreen->setReadOnly(true);

    ui->tabWidget->setStyleSheet("QTabBar::tab { height: 25px; width: 156px; }");
    //ui->tabWidget->setStyleSheet("QTabBar::tab { height: 18px; width: 100px; }");
    //ui->tabWidget->setTabText(0,"Information");
    //ui->tabWidget->setTabText(1, "Channels");
    //ui->tabWidget->setTabText(2, "Calibration");

    //-----------------------------------------------------------------//
    //                        Setup Clock
    //-----------------------------------------------------------------//

    DisplayDateTime();

    QTimer *timer = new QTimer(this);
    connect( timer, SIGNAL(timeout()), this, SLOT(DisplayDateTime()));
    connect( timer, SIGNAL(timeout()), this, SLOT(QueryRunStatus()));
    timer->start(1000); // update every second
    //timer->start(600000);

    //-----------------------------------------------------------------//

    ui->SetupProgressBar->setMinimum(   0 );
    ui->SetupProgressBar->setMaximum( 500 );
    ui->SetupProgressBar->setValue(0);

    /////////////////////////////////////////////////////////////////////
    //-----------------------------------------------------------------//
    // STGC handles
    //-----------------------------------------------------------------//
    /////////////////////////////////////////////////////////////////////
    miniDAQConfigHandler = new ConfigHandlerMiniDAQ();
    boardConfigHandler   = new ConfigHandlerBoard();
    vmmConfigHandler     = new ConfigHandlerVMM();
    tdsConfigHandler     = new ConfigHandlerTDS();
    rocFPGAConfigHandler = new ConfigHandlerRocFPGA();
    rocASICConfigHandler = new ConfigHandlerRocASIC();

    stgcSocketHandler = new SocketHandler();

    baseConfigModule    = new Config_Base();
    boardConfigModule   = new Config_Board();
    vmmConfigModule     = new Config_VMM();
    tdsConfigModule     = new Config_TDS();
    rocFPGAConfigModule = new Config_RocFPGA();
    rocASICConfigModule = new Config_RocASIC();

    vmmCalibModule    = new Calib_VMM();

    //    vmmRunModule      = new RunModule();
    vmmDataHandler    = new DataHandler();
    vmmDataHandler->initialize();

    boardConfigHandler  ->LoadMessageHandler(msg());
    vmmConfigHandler    ->LoadMessageHandler(msg());
    tdsConfigHandler    ->LoadMessageHandler(msg());
    rocFPGAConfigHandler->LoadMessageHandler(msg());
    rocASICConfigHandler->LoadMessageHandler(msg());

    stgcSocketHandler->LoadMessageHandler(msg());

    baseConfigModule   ->LoadMessageHandler(msg());
    vmmConfigModule    ->LoadMessageHandler(msg());
    tdsConfigModule    ->LoadMessageHandler(msg());
    boardConfigModule  ->LoadMessageHandler(msg());
    rocFPGAConfigModule->LoadMessageHandler(msg());
    rocASICConfigModule->LoadMessageHandler(msg());

    vmmDataHandler  ->LoadMessageHandler(msg());
    //    vmmRunModule    ->LoadMessageHandler(msg());

    m_dbg = true;

    miniDAQConfigHandler->setDebug(m_dbg);
    boardConfigHandler  ->setDebug(m_dbg);
    vmmConfigHandler    ->setDebug(m_dbg);
    tdsConfigHandler    ->setDebug(m_dbg);
    rocFPGAConfigHandler->setDebug(m_dbg);
    rocASICConfigHandler->setDebug(m_dbg);

    stgcSocketHandler  ->setDebug(m_dbg);

    baseConfigModule   ->setDebug(m_dbg);
    boardConfigModule  ->setDebug(m_dbg);
    vmmConfigModule    ->setDebug(m_dbg);
    tdsConfigModule    ->setDebug(m_dbg);
    rocFPGAConfigModule->setDebug(m_dbg);
    rocASICConfigModule->setDebug(m_dbg);

    vmmCalibModule    ->setDebug(m_dbg);

    //    vmmRunModule     ->setDebug(m_dbg);
    vmmDataHandler   ->setDebug(m_dbg);

    // set dry run for testing
    //msg()(" !! SOCKETHANDLER SET FOR DRY RUN !! ");
    //stgcSocketHandler->setDryRun();


    // load the things
    baseConfigModule ->LoadSocket(*stgcSocketHandler);
    
    boardConfigModule->LoadConfig(*boardConfigHandler);
    boardConfigModule->LoadSocket(*stgcSocketHandler);

    vmmConfigModule  ->LoadConfig(*vmmConfigHandler);
    vmmConfigModule  ->LoadSocket(*stgcSocketHandler);

    tdsConfigModule  ->LoadConfig(*tdsConfigHandler);
    tdsConfigModule  ->LoadSocket(*stgcSocketHandler);

    rocFPGAConfigModule->LoadConfig(*rocFPGAConfigHandler);
    rocFPGAConfigModule->LoadSocket(*stgcSocketHandler);

    rocASICConfigModule->LoadConfig(*rocASICConfigHandler);
    rocASICConfigModule->LoadSocket(*stgcSocketHandler);


    /////////////////////////////////////////////////////////////////////
    //-----------------------------------------------------------------//
    // make widget connections
    //-----------------------------------------------------------------//
    /////////////////////////////////////////////////////////////////////

    VMMchannelGridLayout = new QGridLayout(this);
    dummy = new QWidget(ui->groupBox_22);
    CreateVMMChannelsFieldsMulti();

    TDSchannelGridLayout = new QGridLayout(this);
    dummy = new QWidget(ui->groupBox_24);
    CreateTDSChannelsFieldsMulti();

    GPIOchanGridLayout = new QGridLayout(this);
    dummy = new QWidget(ui->groupBox_24);
    CreateGPIOChanFieldsMulti();

    //-----------------------------------------------------------------//

    // set number of FECs
    // ping the boards and connect sockets

    connect(ui->openConnection,   SIGNAL(pressed()),
	    this, SLOT(Connect()));
    connect(ui->openConnection_2, SIGNAL(pressed()),
            this, SLOT(Connect()));
    connect(ui->setupSCA,         SIGNAL(pressed()),
	    this, SLOT(configSCA()));
    //    ui->setupSCA->setEnabled(false);

    // if the IP changes in anyway, we must re-connect
    connect(ui->HostIp_1, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));
    connect(ui->HostIp_2, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));
    connect(ui->HostIp_3, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));
    connect(ui->HostIp_4, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));

    connect(ui->TargetIp_1, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));
    connect(ui->TargetIp_2, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));
    connect(ui->TargetIp_3, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));
    connect(ui->TargetIp_4, SIGNAL(textChanged(QString)),
	    this, SLOT(IPchanged(QString)));

    // prepare the board configuration and send
    connect(ui->SendConfiguration, SIGNAL(pressed()),
	    this, SLOT(prepareAndSendInputConfig()));
    //ui->SendConfiguration->setEnabled(false);

    ui->Config_Output->setReadOnly(true);
    
    //    connect(baseConfigModule, SIGNAL(config_reply()), this, SLOT(config_reply_output()));
    //    connect(boardConfigModule, SIGNAL(config_reply()), this, SLOT(config_reply_output()));
    //    connect(tdsConfigModule, SIGNAL(config_reply()), this, SLOT(config_reply_output()));
    //    connect(vmmConfigModule, SIGNAL(config_reply()), this, SLOT(config_reply_output()));

    //-----------------------------------------------------------------------//
    // select the configuration xml file (if you want to load a pre-existing
    // configuration)
    //-----------------------------------------------------------------------//

    connect(ui->selectDir_config,          SIGNAL(clicked()),
	    this, SLOT(selectDirConfig()));

    connect(ui->select_config_vmm,         SIGNAL(clicked()),
	    this, SLOT(selectVMMConfigXMLFile()));

    connect(ui->outputFile_config_dir,     SIGNAL(clicked()),
	    this, SLOT(selectOutputDir()));

    ui->start_run_reset_vmms->isChecked();
    ui->start_run_reset_vmms->setEnabled(true);

    //-----------------------------------------------------------//

    connect(ui->load_current_config_board, SIGNAL(clicked()),
	    this, SLOT(loadCurrentConfigBoard()));

    connect(ui->load_current_config_vmm, SIGNAL(clicked()),
            this, SLOT(loadCurrentConfigVMM()));

    connect(ui->load_current_config_tds, SIGNAL(clicked()),
            this, SLOT(loadCurrentConfigTDS()));

    connect(ui->load_current_config_ROC_FPGA, SIGNAL(clicked()),
            this, SLOT(loadCurrentConfigRocFPGA()));

    connect(ui->load_current_config_ROC_ASIC, SIGNAL(clicked()),
            this, SLOT(loadCurrentConfigRocASIC()));

    //-----------------------------------------------------------//

    connect(ui->configVMMXmlFilenameField, SIGNAL(returnPressed()),
	    this, SLOT(checkRequestedFile()));

    connect(ui->load_config_vmm,           SIGNAL(clicked()),
	    this, SLOT(loadVMMConfigurationFromFile()));

    connect(ui->write_config_vmm,          SIGNAL(clicked()),
	    this, SLOT(writeVMMConfigurationToFile()));

    //------------------------------------------------------------//

    connect(ui->select_config_tds,         SIGNAL(clicked()),
            this, SLOT(selectTDSConfigXMLFile()));

    connect(ui->configTDSXmlFilenameField, SIGNAL(returnPressed()),
            this, SLOT(checkRequestedFile()));

    connect(ui->load_config_tds,           SIGNAL(clicked()),
            this, SLOT(loadTDSConfigurationFromFile()));

    connect(ui->write_config_tds,          SIGNAL(clicked()),
            this, SLOT(writeTDSConfigurationToFile()));

    //------------------------------------------------------------//

    connect(ui->select_config_ROC_FPGA,         SIGNAL(clicked()),
            this, SLOT(selectRocFPGAConfigXMLFile()));

    connect(ui->configRocFPGAXmlFilenameField, SIGNAL(returnPressed()),
            this, SLOT(checkRequestedFile()));

    connect(ui->load_config_ROC_FPGA,           SIGNAL(clicked()),
            this, SLOT(loadRocFPGAConfigurationFromFile()));

    connect(ui->write_config_ROC_FPGA,          SIGNAL(clicked()),
            this, SLOT(writeRocFPGAConfigurationToFile()));

    //------------------------------------------------------------//

    connect(ui->ReconfigureVMM, SIGNAL(clicked()),
	    this, SLOT(prepareAndSendVMMConfig()));
    connect(ui->ReconfigureTDS, SIGNAL(clicked()),
            this, SLOT(prepareAndSendTDSConfig()));
    connect(ui->ReconfigureRocFPGA, SIGNAL(clicked()),
            this, SLOT(prepareAndSendRocFPGAConfig()));

    //------------------------------------------------//

    InitializeVMMChannelArrayValues();

    //-----------------------------------------------//

    ui->ROC_ASIC_Type_2->setCurrentIndex(2);
    ui->ROC_ASIC_Type_2->setEnabled(false);

    ui->ROC_ASIC_Type->setCurrentIndex(2);
    ui->ROC_ASIC_Type->setEnabled(false);

    ui->TDS_ASIC_Type->setCurrentIndex(1);
    ui->TDS_ASIC_Type->setEnabled(false);

    ui->VMM_ASIC_Type->setCurrentIndex(0);
    ui->VMM_ASIC_Type->setEnabled(false);

    //----------------------------------------------//
    
    //    connect(ui->Send_Config, SIGNAL(clicked()),
    //	    this, SLOT(sendCommand()));

    connect(ui->cmd_search_elink,       SIGNAL(clicked()),
            this, SLOT(sendCommand()));

    connect(ui->cmd_elink_phase_matrix, SIGNAL(clicked()),
            this, SLOT(getElinkPhaseMatrix()));

    connect(ui->cmd_query_EVID,         SIGNAL(clicked()),
            this, SLOT(sendCommand()));

    connect(ui->QueryCurrentTTCRegVal,  SIGNAL(clicked()),
	    this, SLOT(sendCommand()));

    connect(ui->cmd_elink_fifo_disable, SIGNAL(clicked()),
	    this, SLOT(sendCommand()));
    connect(ui->ManualSetELink,         SIGNAL(clicked()),
	    this, SLOT(sendCommand()));

    connect(ui->cmd_FPGA_reset,     SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_Ext_Trigger,    SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_DAQ_Enable,     SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_ReadOut_Enable, SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_TTC_start,      SIGNAL(clicked()),
            this, SLOT(sendCommand()));


    connect(ui->cmd_soft_reset_low,   SIGNAL(clicked()),
	    this, SLOT(sendCommand()));
    connect(ui->cmd_soft_reset_high,  SIGNAL(clicked()),
            this, SLOT(sendCommand()));

    connect(ui->cmd_Gate_nBC,      SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_latency_setup, SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_SCA_Reset,     SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_EVID_Reset,    SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_BC_Reset,      SIGNAL(clicked()),
            this, SLOT(sendCommand()));

    connect( ui->cmd_L0_Evt_Reset, SIGNAL(clicked()),
             this, SLOT(sendCommand()) );
    connect( ui->cmd_L0_Accept, SIGNAL(clicked()),
	     this, SLOT(sendCommand()) );
    connect( ui->cmd_L1_Accept, SIGNAL(clicked()),
	     this, SLOT(sendCommand()) );
    connect( ui->cmd_VMM_Soft_Reset, SIGNAL(clicked()),
	     this, SLOT(sendCommand()) );
    connect( ui->cmd_VMM_TP, SIGNAL(clicked()),
	     this, SLOT(sendCommand()) );
    connect( ui->cmd_TTC_pulse_lv, SIGNAL(clicked()),
	     this, SLOT(sendCommand()) );

    connect( ui->cmd_set_SCA_GPIO, SIGNAL(clicked()),
	     this, SLOT(sendCommand()) );

    connect(ui->cmd_CKTP_NMax,    SIGNAL(clicked()),
	    this, SLOT(sendCommand()));
    connect(ui->cmd_CKBC_NMax,    SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_CKTP_Spacing, SIGNAL(clicked()),
            this, SLOT(sendCommand()));

    connect(ui->CKTP_NMax_max,     SIGNAL(clicked()),
            this, SLOT(setMax()));
    connect(ui->CKBC_NMax_max,     SIGNAL(clicked()),
            this, SLOT(setMax()));
    connect(ui->CKTP_Spacing_max,  SIGNAL(clicked()),
            this, SLOT(setMax()));

    connect(ui->Gate_nBC_Max,      SIGNAL(clicked()),
            this, SLOT(setMax()));
    connect(ui->Latency_nBC_Max,   SIGNAL(clicked()),
	    this, SLOT(setMax()));

    connect(ui->cmd_TTC_F1, SIGNAL(clicked()),
            this, SLOT(sendCommand()));
    connect(ui->cmd_TTC_F2, SIGNAL(clicked()),
            this, SLOT(sendCommand()));


    connect(ui->cmd_SCA_GO, SIGNAL(clicked()),
	    this, SLOT(sendCommand()));

    ui->Ext_Trigger_En->setCurrentIndex(0);
    ui->CKTP_NMax->setValue(0);

    ui->cmd_CKBC_NMax->setEnabled(false);

    ui->CKBC_NMax->setEnabled(false);
    ui->CKBC_NMax_max->setEnabled(false);

    connect(ui->setup_mini_daq, SIGNAL(clicked()),
	    this, SLOT(setupMiniDAQ()));

    connect(ui->ConfiguringFEBs, SIGNAL(clicked()),
	    this, SLOT(prepareAndSendConfigDir()));

    //---------------------------------------------//

    connect(ui->configRocASICXmlFilenameField, SIGNAL(returnPressed()),
            this, SLOT(checkRequestedFile()));

    connect(ui->select_config_roc_asic, SIGNAL(clicked()),
            this, SLOT(selectRocASICConfigXMLFile()));

    connect(ui->ReconfigureROC_ASIC, SIGNAL(clicked()),
            this, SLOT(prepareAndSendRocASICConfig()));

    connect(ui->load_config_roc_asic, SIGNAL(clicked()),
            this, SLOT(loadRocASICConfigurationFromFile()));

    connect(ui->write_config_roc_asic, SIGNAL(clicked()),
            this, SLOT(writeRocASICConfigurationToFile()));

    //---------------------------------------------//

    connect(ui->configBoardXmlFilenameField, SIGNAL(returnPressed()),
            this, SLOT(checkRequestedFile()));

    connect(ui->select_config_board, SIGNAL(clicked()),
            this, SLOT(selectBoardConfigXMLFile()));

    connect(ui->ReconfigureBoard, SIGNAL(clicked()),
            this, SLOT(prepareAndSendBoardConfig()));

    connect(ui->load_config_board, SIGNAL(clicked()),
            this, SLOT(loadBoardConfigurationFromFile()));

    connect(ui->write_config_board, SIGNAL(clicked()),
            this, SLOT(writeBoardConfigurationToFile()));

    //-----------------------------------------------//

    connect(ui->FE_Board_Type, SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateFEBType()) );

    connect(ui->ManualReset_hard, SIGNAL(clicked()),
	    this, SLOT(configDir_hardReset()) );
    connect(ui->ManualReset_stcr, SIGNAL(clicked()),
            this, SLOT(configDir_stcrReset()) );

    connect(ui->config_board_stcr_reset_all, SIGNAL(clicked()),
	    this, SLOT(configBoard_stcrReset()) );
    connect(ui->config_board_hard_reset_all, SIGNAL(clicked()),
            this, SLOT(configBoard_hardReset()) );

    //----------------------------------------------------//

    ui->vmm_select_1->setChecked(false);
    ui->vmm_select_2->setChecked(false);
    ui->vmm_select_3->setChecked(false);
    ui->vmm_select_4->setChecked(false);
    ui->vmm_select_5->setChecked(false);
    ui->vmm_select_6->setChecked(false);
    ui->vmm_select_7->setChecked(false);
    ui->vmm_select_8->setChecked(false);

    ui->gpio_select_0->setChecked(true);
    ui->gpio_select_1->setChecked(true);
    ui->gpio_select_2->setChecked(true);
    ui->gpio_select_3->setChecked(true);
    ui->gpio_select_4->setChecked(true);
    ui->gpio_select_5->setChecked(true);
    ui->gpio_select_6->setChecked(true);
    ui->gpio_select_7->setChecked(true);

    ui->tds_select_1->setChecked(false);
    ui->tds_select_2->setChecked(false);
    ui->tds_select_3->setChecked(false);
    ui->tds_select_4->setChecked(false);

    ui->VMM_UniqueS_1->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_2->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_3->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_4->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_5->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_6->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_7->setStyleSheet("background-color: lightGray");
    ui->VMM_UniqueS_8->setStyleSheet("background-color: lightGray");
    ui->TDS_UniqueS_1->setStyleSheet("background-color: lightGray");
    ui->TDS_UniqueS_2->setStyleSheet("background-color: lightGray");
    ui->TDS_UniqueS_3->setStyleSheet("background-color: lightGray");
    ui->TDS_UniqueS_4->setStyleSheet("background-color: lightGray");

    ui->VMM_UniqueS_1->setEnabled(false);
    ui->VMM_UniqueS_2->setEnabled(false);
    ui->VMM_UniqueS_3->setEnabled(false);
    ui->VMM_UniqueS_4->setEnabled(false);
    ui->VMM_UniqueS_5->setEnabled(false);
    ui->VMM_UniqueS_6->setEnabled(false);
    ui->VMM_UniqueS_7->setEnabled(false);
    ui->VMM_UniqueS_8->setEnabled(false);
    ui->TDS_UniqueS_1->setEnabled(false);
    ui->TDS_UniqueS_2->setEnabled(false);
    ui->TDS_UniqueS_3->setEnabled(false);
    ui->TDS_UniqueS_4->setEnabled(false);

    updateFEBType();

    connect(ui->vmm_select_1, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_2, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_3, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_4, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_5, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_6, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_7, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->vmm_select_8, SIGNAL(toggled(bool)), this, SLOT(configBoard()));

    connect(ui->tds_select_1, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->tds_select_2, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->tds_select_3, SIGNAL(toggled(bool)), this, SLOT(configBoard()));
    connect(ui->tds_select_4, SIGNAL(toggled(bool)), this, SLOT(configBoard()));

    connect( ui->tds_select2_1, SIGNAL(toggled(bool)), this, SLOT(clickedTDS(bool)) );
    connect( ui->tds_select2_2, SIGNAL(toggled(bool)), this, SLOT(clickedTDS(bool)) );
    connect( ui->tds_select2_3, SIGNAL(toggled(bool)), this, SLOT(clickedTDS(bool)) );
    connect( ui->tds_select2_4, SIGNAL(toggled(bool)), this, SLOT(clickedTDS(bool)) );

    connect(ui->Open_VMM_Config, SIGNAL(pressed()), this, SLOT(openChipConfig()));
    connect(ui->Open_TDS_Config, SIGNAL(pressed()), this, SLOT(openChipConfig()));

   //-------------------------------------//

    ui->writeData->setChecked(true);
    ui->writeData->setEnabled(false);

    connect(ui->startRunning, SIGNAL(pressed()), this, SLOT(startRun()));
    connect(ui->enableDAQ,    SIGNAL(pressed()), this, SLOT(enableDAQ()));
    connect(ui->stopRunning,  SIGNAL(pressed()), this, SLOT(stopRun()));

    //------------------------------------//

    ui->CKTP_Spacing->setDisplayIntegerBase(16);
    ui->CKTP_Spacing->setRange(0xffff,0xffffff);

    //------------------------------------//
    
    BoardID_Linked.resize(0);
    BoardID_Linked.push_back( ui->board_lked_1 );
    BoardID_Linked.push_back( ui->board_lked_2 );
    BoardID_Linked.push_back( ui->board_lked_3 );
    BoardID_Linked.push_back( ui->board_lked_4 );
    BoardID_Linked.push_back( ui->board_lked_5 );
    BoardID_Linked.push_back( ui->board_lked_6 );
    BoardID_Linked.push_back( ui->board_lked_7 );
    BoardID_Linked.push_back( ui->board_lked_8 );

    //------------------------------------//

    ELink_Manual_Phase.resize(0);
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_1 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_2 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_3 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_4 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_5 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_6 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_7 );
    ELink_Manual_Phase.push_back( ui->ELink_1_phase_8 );

    for ( uint i=0; i < ELink_Manual_Phase.size(); i++ ) {
      ELink_Manual_Phase.at(i)->setRange(0,63);
    }

    //------------------------------------//

    ELink_stat.resize(0);
    ELink_stat.push_back( ui->ELink_stat_1 );
    ELink_stat.push_back( ui->ELink_stat_2 );
    ELink_stat.push_back( ui->ELink_stat_3 );
    ELink_stat.push_back( ui->ELink_stat_4 );
    ELink_stat.push_back( ui->ELink_stat_5 );
    ELink_stat.push_back( ui->ELink_stat_6 );
    ELink_stat.push_back( ui->ELink_stat_7 );
    ELink_stat.push_back( ui->ELink_stat_8 );

    //------------------------------------//
    
    elink_lked.resize(0);
    elink_lked.push_back( ui->elink_lked_1 );
    elink_lked.push_back( ui->elink_lked_2 );
    elink_lked.push_back( ui->elink_lked_3 );
    elink_lked.push_back( ui->elink_lked_4 );
    elink_lked.push_back( ui->elink_lked_5 );
    elink_lked.push_back( ui->elink_lked_6 );
    elink_lked.push_back( ui->elink_lked_7 );
    elink_lked.push_back( ui->elink_lked_8 );

    connect(ui->board_change_all_vmm_thresholds,  SIGNAL(clicked()),
            this, SLOT(configBoard_all_vmms()) );
    connect(ui->board_change_all_vmm_gain,  SIGNAL(clicked()),
            this, SLOT(configBoard_all_vmms()) );
    connect(ui->board_open_all_channels,  SIGNAL(clicked()),
            this, SLOT(configBoard_all_vmms()) );
    connect(ui->board_mask_all_channels,  SIGNAL(clicked()),
            this, SLOT(configBoard_all_vmms()) );
    connect(ui->board_change_mon_chan,  SIGNAL(clicked()),
            this, SLOT(configBoard_all_vmms()) );

    EnableConfiguration();

    ui->ROC_cktp_width->setValue( 1023 );
    ui->ROC_trigger_width->setValue( 4800 );
    ui->ROC_config_command->setEnabled( false );
    ui->ROC_config_header->setEnabled( false );
    ui->VMM_config_header->setEnabled( false );
    ui->VMM_config_command->setEnabled( false );
    ui->TDS_config_header->setEnabled( false );
    ui->TDS_config_command->setEnabled( false );

    ui->global_sdp_dac->setValue( 120 );
    ui->global_sdt_dac->setValue( 175 );
    ui->global_sp->setCurrentIndex(1);
    ui->global_sg->setCurrentIndex(2);
    ui->global_st->setCurrentIndex(2);
    ui->global_sbip->setCurrentIndex(1);
    ui->global_slvsbc->setCurrentIndex(1);
    ui->global_slvstp->setCurrentIndex(1);
    ui->global_slvstk->setCurrentIndex(1);
    ui->global_slvsdt->setCurrentIndex(1);

    //--------------------------------------//

    connect( ui->vmm_calib_single, SIGNAL(clicked()), 
	     this, SLOT( CalibrateVMM() ) );
    connect( ui->vmm_scan_baseline, SIGNAL(clicked()),
	     this, SLOT( FindBaseline() ) );
    connect( ui->vmm_display_baseline, SIGNAL(clicked()),
	     this, SLOT( Display_Baseline_Scan() ) );
    connect( ui->display_calib_single, SIGNAL(clicked()),
             this, SLOT( Display_Trimming_Matrix() ) );

    //--------------------------------------//

    InitRocGUI();

    connect( ui->config_board_calibrateVMMs, SIGNAL(clicked()),
	     this, SLOT( FindBoardCalibration() ) );
    connect( ui->config_board_baselineVMMs, SIGNAL(clicked()),
             this, SLOT( FindBoardBaseline() ) );
    connect( ui->config_board_baselineVMMs_detailed, SIGNAL(clicked()),
             this, SLOT( FindBoardBaselineDetailed() ) );
    connect( ui->config_board_calibrateThDAC, SIGNAL(clicked()),
	     this, SLOT( FindBoardThDacCalibration() ) );

    connect( ui->Calib_whole_dir, SIGNAL(clicked()),
             this, SLOT( CalibWholeDir() ) );
    
}

// ------------------------------------------------------------------------- //
MainWindow::~MainWindow()
{
  if ( daq_bound ) socketHandle().closeAndDisconnect("daq");
  if ( stgc_bound ) socketHandle().closeAndDisconnect("stgc");

  if ( data_bound ) {
    stopRun();
  }

  delete miniDAQConfigHandler;
  delete boardConfigHandler;
  delete vmmConfigHandler;
  delete tdsConfigHandler;
  delete rocFPGAConfigHandler;

  delete stgcSocketHandler;

  delete baseConfigModule;
  delete boardConfigModule;
  delete vmmConfigModule;
  delete tdsConfigModule;
  delete rocFPGAConfigModule;

  delete vmmCalibModule;
  delete vmmDataHandler;

  delete ui;
}

// ------------------------------------------------------------------------ //
void MainWindow::PopUpError(QString str, QString info = "") {
  msgBox.setText(str);
  if ( info != "" ) msgBox.setInformativeText(info);
  msgBox.setIcon(QMessageBox::Critical);
  msgBox.exec();
}

void MainWindow::PopUpError(const char* str, const char *info) {
  PopUpError( QString(str),
              QString(info) );
}
// ------------------------------------------------------------------------ //
void MainWindow::PopUpWarning(QString str, QString info = "") {
  msgBox.setText(str);
  if ( info != "" ) msgBox.setInformativeText(info);
  msgBox.setIcon(QMessageBox::Warning);
  msgBox.exec();
}

void MainWindow::PopUpWarning(const char* str, const char *info) {
  PopUpWarning( QString(str),
                QString(info) );
}
// ------------------------------------------------------------------------ //
void MainWindow::PopUpInfo(QString str, QString info = "") {
  msgBox.setText(str);
  if ( info != "" ) msgBox.setInformativeText(info);
  msgBox.setIcon(QMessageBox::Information);
  msgBox.exec();
}

void MainWindow::PopUpInfo(const char* str, const char *info) {
  PopUpInfo( QString(str),
             QString(info) );
}



// ------------------------------------------------------------------------- //
void MainWindow::clickedTDS(bool checked) {

  if ( QObject::sender() == ui->tds_select2_1 ) {
    if ( checked ) {
      ui->tds_select2_2->setChecked(false);
      ui->tds_select2_3->setChecked(false);
      ui->tds_select2_4->setChecked(false);
    }
  }
  if ( QObject::sender() == ui->tds_select2_2 ) {
    if ( checked ) {
      ui->tds_select2_1->setChecked(false);
      ui->tds_select2_3->setChecked(false);
      ui->tds_select2_4->setChecked(false);
    }
  }
  if ( QObject::sender() == ui->tds_select2_3 ) {
    if ( checked ) {
      ui->tds_select2_1->setChecked(false);
      ui->tds_select2_2->setChecked(false);
      ui->tds_select2_4->setChecked(false);
    }
  }
  if ( QObject::sender() == ui->tds_select2_4 ) {
    if ( checked ) {
      ui->tds_select2_1->setChecked(false);
      ui->tds_select2_2->setChecked(false);
      ui->tds_select2_3->setChecked(false);
    }
  }

}

void MainWindow::configBoard()
{

  if ( QObject::sender() == ui->vmm_select_1 || QObject::sender() == ui->vmm_select_2 ||
       QObject::sender() == ui->vmm_select_3 || QObject::sender() == ui->vmm_select_4 ||
       QObject::sender() == ui->vmm_select_5 || QObject::sender() == ui->vmm_select_6 ||
       QObject::sender() == ui->vmm_select_7 || QObject::sender() == ui->vmm_select_8 ) {
    ui->VMMChipSelect->clear();
    if ( ui->vmm_select_1->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 1");
      ui->VMM_UniqueS_1->setEnabled(true);
      ui->VMM_UniqueS_1->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_1->setText("");
      ui->VMM_UniqueS_1->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_1->setEnabled(false);
    }
    if ( ui->vmm_select_2->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 2");
      ui->VMM_UniqueS_2->setEnabled(true);
      ui->VMM_UniqueS_2->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_2->setText("");
      ui->VMM_UniqueS_2->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_2->setEnabled(false);
    }
    if ( ui->vmm_select_3->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 3");
      ui->VMM_UniqueS_3->setEnabled(true);
      ui->VMM_UniqueS_3->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_3->setText("");
      ui->VMM_UniqueS_3->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_3->setEnabled(false);
    }
    if ( ui->vmm_select_4->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 4");
      ui->VMM_UniqueS_4->setEnabled(true);
      ui->VMM_UniqueS_4->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_4->setText("");
      ui->VMM_UniqueS_4->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_4->setEnabled(false);
    }
    if ( ui->vmm_select_5->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 5");
      ui->VMM_UniqueS_5->setEnabled(true);
      ui->VMM_UniqueS_5->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_5->setText("");
      ui->VMM_UniqueS_5->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_5->setEnabled(false);
    }
    if ( ui->vmm_select_6->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 6");
      ui->VMM_UniqueS_6->setEnabled(true);
      ui->VMM_UniqueS_6->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_6->setText("");
      ui->VMM_UniqueS_6->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_6->setEnabled(false);
    }
    if ( ui->vmm_select_7->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 7");
      ui->VMM_UniqueS_7->setEnabled(true);
      ui->VMM_UniqueS_7->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_7->setText("");
      ui->VMM_UniqueS_7->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_7->setEnabled(false);
    }
    if ( ui->vmm_select_8->isChecked() ) {
      ui->VMMChipSelect->addItem("chip 8");
      ui->VMM_UniqueS_8->setEnabled(true);
      ui->VMM_UniqueS_8->setStyleSheet("background-color: white");
    }
    else {
      ui->VMM_UniqueS_8->setText("");
      ui->VMM_UniqueS_8->setStyleSheet("background-color: lightGray");
      ui->VMM_UniqueS_8->setEnabled(false);
    }
  }

  if ( QObject::sender() == ui->tds_select_1 || QObject::sender() == ui->tds_select_2 ||
       QObject::sender() == ui->tds_select_3 || QObject::sender() == ui->tds_select_4 ) {
    ui->TDSChipSelect->clear();
    if ( ui->tds_select_1->isChecked() ) {
      ui->TDSChipSelect->addItem("chip 1");
      ui->TDS_UniqueS_1->setEnabled(true);
      ui->TDS_UniqueS_1->setStyleSheet("background-color: white");
    }
    else {
      ui->TDS_UniqueS_1->setText("");
      ui->TDS_UniqueS_1->setStyleSheet("background-color: lightGray");
      ui->TDS_UniqueS_1->setEnabled(false);
    }
    if ( ui->tds_select_2->isChecked() ) {
      ui->TDSChipSelect->addItem("chip 2");
      ui->TDS_UniqueS_2->setEnabled(true);
      ui->TDS_UniqueS_2->setStyleSheet("background-color: white");
    }
    else {
      ui->TDS_UniqueS_2->setText("");
      ui->TDS_UniqueS_2->setStyleSheet("background-color: lightGray");
      ui->TDS_UniqueS_2->setEnabled(false);
    }
    if ( ui->tds_select_3->isChecked() ) {
      ui->TDSChipSelect->addItem("chip 3");
      ui->TDS_UniqueS_3->setEnabled(true);
      ui->TDS_UniqueS_3->setStyleSheet("background-color: white");
    }
    else {
      ui->TDS_UniqueS_3->setText("");
      ui->TDS_UniqueS_3->setStyleSheet("background-color: lightGray");
      ui->TDS_UniqueS_3->setEnabled(false);
    }
    if ( ui->tds_select_4->isChecked() ) {
      ui->TDSChipSelect->addItem("chip 4");
      ui->TDS_UniqueS_4->setEnabled(true);
      ui->TDS_UniqueS_4->setStyleSheet("background-color: white");
    }
    else {
      ui->TDS_UniqueS_4->setText("");
      ui->TDS_UniqueS_4->setStyleSheet("background-color: lightGray");
      ui->TDS_UniqueS_4->setEnabled(false);
    }
  }

  /*
  QMessageBox::information( this, 
			    tr("Application Name"), 
			    tr("An information message.") );
  */

}
// ------------------------------------------------------------------------- //
void MainWindow::readLog()
{
    string buff = msg().buffer();
    ui->loggingScreen->append(QString::fromStdString(buff));
    //ui->loggingScreen->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
    msg().clear(); 
}
// ------------------------------------------------------------------------- //
void MainWindow::updateLogScreen()
{
  ui->loggingScreen->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
  ui->loggingScreen->verticalScrollBar()->setValue(ui->loggingScreen->verticalScrollBar()->maximum());
}
// ------------------------------------------------------------------------- //
// ------------------------------------------------------------------------- //
void MainWindow::IPaddressChange()
{
    QString message = "Re-establish Connection";
    QString initial = "Establish Comms";
    if(ui->connectionLabel->text()!=message && ui->connectionLabel->text()!=initial) {
        msg()("IP address change signaled. Re-establish connection to ensure proper connectivity.");
        ui->connectionLabel->setText(message);
        ui->connectionLabel->setStyleSheet("background-color: lightGray");
    }
    return;
}
// ------------------------------------------------------------------------- //
void MainWindow::IPchanged(QString /*string*/)
{
    IPaddressChange();
}
// ------------------------------------------------------------------------- //
void MainWindow::IPchanged(int /*value*/)
{
    IPaddressChange();
}
// ------------------------------------------------------------------------- //
void MainWindow::buildHostIP()
{
  QString ip_host = ui->HostIp_1->text() + "." + ui->HostIp_2->text() 
      + "." + ui->HostIp_3->text() + "." + ui->HostIp_4->text();

  HostIp.setAddress( ip_host );

  return;
}
// -------------------------------------------------------------------------- //
void MainWindow::buildTargetIP()
{
  QString ip_target = ui->TargetIp_1->text() + "." + ui->TargetIp_2->text() 
      + "." + ui->TargetIp_3->text() + "." + ui->TargetIp_4->text();

  TargetIp.setAddress( ip_target );

  return;
}

// ------------------------------------------------------------------------- //
void MainWindow::Connect()
{
  stringstream sx;

    // ------------------------------------------------- //
    //  "comm info"
    // ------------------------------------------------- //
    CommInfo commInfo;
    commInfo.host_port      = HOSTPORT;
    commInfo.target_port    = TARGETPORT;

    buildHostIP();
    buildTargetIP();

    msg()(" ");
    sx << "Connecting sockets to eithernet port: \n" ;
    msg()(sx);

    BoardConfigHandle().LoadCommInfo(commInfo);

    if (!socketHandle().stgcSocketOK()) socketHandle().addSocket(HostIp, "stgc", HOSTPORT,   QUdpSocket::ShareAddress);
    if (!socketHandle().daqSocketOK()) socketHandle().addSocket(HostIp, "daq",  TARGETPORT, QUdpSocket::ShareAddress);

    
    if ( !socketHandle().stgcSocketOK() &&
	 !socketHandle().daqSocketOK() ) {
      ui->connectionLabel->setText("Error: sockets failed to connect");
      PopUpError( "Error: sockets stgc and daq failed to connect" );
      stgc_bound = false;
      daq_bound = false;
    }
    else if ( !socketHandle().daqSocketOK() ) {
      ui->connectionLabel->setText("Error: sockets failed to connect");
      PopUpError( "Error: socket daq failed to connect" );
      stgc_bound = true;
      daq_bound = false;
    }
    else if ( !socketHandle().stgcSocketOK() ) {
      ui->connectionLabel->setText("Error: sockets failed to connect");
      PopUpError( "Error: socket stgc failed to connect" );
      stgc_bound = false;
      daq_bound = true;
    }
    else {
      ui->connectionLabel->setText("connected");

      daq_bound = true;
      stgc_bound = true;

      ui->openConnection->setEnabled(false);
      ui->openConnection_2->setEnabled(false);

    }

    return;
}
//---------------------------------------------------------//
void MainWindow::configSCA()
{
  msg()(" ");
  msg()("configuring SCA");
  
  buildTargetIP();
  
  bool send_ok = true;
  for ( uint i = 0; i < BoardID_Linked.size(); i++ ) {
    
    if ( BoardID_Linked.at(i)->isChecked() ) {
      std::cout << "resetting board " << i << std::endl;

      bool send_ok_i = BoardConfigModule().config_SCA(TARGETPORT, TargetIp, 
						      ui->cmd_boardType->currentIndex(), i);
      if ( !send_ok_i ) {
	send_ok = false;

	stringstream m_sx;

	m_sx.str("");
	m_sx << "SCA init failed for board " << i << " \n";
	msg()(m_sx);

	PopUpError( m_sx.str().c_str() );

      }
    }

  }

  //  if ( send_ok ) ui->connectionLabel->setText("SCA configured");
  //  else           ui->connectionLabel->setText("SCA config error");

  m_reply_error = send_ok;

}
//---------------------------------------------------------//
void MainWindow::prepareAndSendInputConfig()
{
  msg()(" ");
  msg()("Sending Manual Configuration Byte Stream...");
  
  QString config_bit_stream = ui->Config_Input->toPlainText();
  
  buildTargetIP();
  
  BaseConfigModule().SendConfig(TARGETPORT, TargetIp, config_bit_stream);

  QString out_buffer = BaseConfigModule().output_buffer();
  ui->Config_Output->setText(out_buffer);
}
//---------------------------------------------------------//
void MainWindow::CalibWholeDir()
{

  if( ui->Calib_whole_dir == QObject::sender() ) {
    icommand = 0;
    ui->SetupProgressBar->setValue( icommand );
    ui->connectionLabel->setText("Calibrating all VMM Channel PDO:  Will take a long time ~0.5-1hr" );
  }

  miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask = 0;

  //------------------------------------------//

  stringstream m_sx;

  //------------------------------------------//                                         
  //       Find Directory from Config         //                                         
  //------------------------------------------//                                         
  QDir Config_Dir(ui->configXmlDirectoryField->text());


  QStringList board_config_filter;
  board_config_filter << "BOARD_*.xml";
  QFileInfoList Board_Config_List = Config_Dir.entryInfoList( board_config_filter );

  //------------------------------------------//                                         
  //         Load All Configurations          //                                         
  //------------------------------------------//                                         

  m_sx.str("");
  m_sx << "Found " << Board_Config_List.size() << " boards configurations \n ";
  msg()(m_sx);

  int nboards = miniDAQConfigHandle().nElinks();

  double nBoardsDone = 0.0;

  for ( int i=0; i<Board_Config_List.size(); i++ ) {

    m_sx.str("");
    m_sx << "Loading configuration for board "
         << Board_Config_List.at(i).fileName().toStdString() << " \n";
    msg()(m_sx);

    ui->configBoardXmlFilenameField->setText( Board_Config_List.at(i).filePath() );
    loadBoardConfigurationFromFile();
    uint32_t boardID = (uint32_t) BoardConfigHandle().BoardSettings().boardID;

    uint32_t boardMask = ( 0b1 << boardID );

    if ( boardMask & miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask ) {

      m_sx.str("");
      m_sx << "Warning: Board " << boardID << " is being calibrated twice \n ";
      m_sx << "The new configuration from " << Board_Config_List.at(i).fileName().toStdString()
           << " will override the first configuration \n ";

      PopUpWarning( m_sx.str().c_str() );

    }


    // only attempt for boards with stable elinks                                                                                              
    if ( (boardMask & miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask ) != 0 ) {
      FindBoardBaselineDetailed();
      nBoardsDone += 0.45;
      ui->SetupProgressBar->setValue( nBoardsDone/nboards*500. );


      FindBoardCalibration();
      nBoardsDone += 0.45;
      ui->SetupProgressBar->setValue( nBoardsDone/nboards*500. );

      FindBoardThDacCalibration();
      nBoardsDone += 0.1;
      ui->SetupProgressBar->setValue( nBoardsDone/nboards*500. );

      miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask += ( 0b1 << boardID );
    }

  }
  
  //---------------------------------------------------------------------//


  if ( miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask == 0 ) {
    m_sx.str("");
    m_sx << "No elinks are locked: Please scan for elinks \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("No elinks are locked:  Please first Scan for elinks");
    PopUpError("Error: No elinks are locked: Please check connection and scan for elinks");
  }
  else if ( miniDAQConfigHandle().All_Boards_Configed() ) {
    m_sx.str("");
    m_sx << "All detected board's VMMs have been calibrated.  Baselines have been scanned and per channel thresholds equalized \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("All FEB VMMs Calibrated: Ready to Configure FEB ASICs");

  }
  else {

    QString FailedBoard_mask = QString("%1").arg( (miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask &
                                                   ~miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask),
                                                  8, 2, QChar('0'));

    m_sx.str("");
    m_sx << "Error: VMM Calibration failed for board "
         << FailedBoard_mask.toStdString() << "\n";
    msg()(m_sx);

    PopUpError(m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Board(s) " << FailedBoard_mask.toStdString() << " failed calibration";
    ui->connectionLabel->setText(QString::fromStdString( m_sx.str() ) );
  }

}
//---------------------------------------------------------//
void MainWindow::prepareAndSendConfigDir(bool resetVMM_hard, bool resetVMM_auto, bool board_sync_soft_reset, 
					 bool board_renable_gpio, bool isSetup, bool onlyPFEB)
{


  if( ui->ConfiguringFEBs == QObject::sender() ) {
    icommand = 200;
    ui->SetupProgressBar->setValue( icommand );
  }

  miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask = 0;

  std::cout << "configuring dir" << std::endl;

  stringstream m_sx;

  //------------------------------------------//
  //       Find Directory from Config         //
  //------------------------------------------//
  QDir Config_Dir(ui->configXmlDirectoryField->text());


  QStringList board_config_filter;
  board_config_filter << "BOARD_*.xml";
  QFileInfoList Board_Config_List = Config_Dir.entryInfoList( board_config_filter );
  
  buildTargetIP();

  //------------------------------------------//
  //         Load All Configurations          //
  //------------------------------------------//

  m_sx.str("");
  m_sx << "Found " << Board_Config_List.size() << " boards configurations \n ";
  msg()(m_sx);

  if ( ui->ConfiguringFEBs == QObject::sender() ) {
    ui->connectionLabel->setText("Configuring all FEB ASIC" );
  }
  if ( resetVMM_hard ) ui->connectionLabel->setText("Hard Resetting all VMMs" );
  if ( resetVMM_auto ) ui->connectionLabel->setText("Auto Resetting all VMMs" );

  for ( int i=0; i<Board_Config_List.size(); i++ ) {

    m_sx.str("");
    m_sx << "Loading configuration for board " 
	 << Board_Config_List.at(i).fileName().toStdString() << " \n";
    msg()(m_sx);

    ui->configBoardXmlFilenameField->setText( Board_Config_List.at(i).filePath() );
    loadBoardConfigurationFromFile();
    uint32_t boardID   = (uint32_t) BoardConfigHandle().BoardSettings().boardID;
    uint32_t boardType = (uint32_t) BoardConfigHandle().BoardSettings().boardType;

    uint32_t boardMask = ( 0b1 << boardID );

    if ( boardMask & miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask ) {

      m_sx.str("");
      m_sx << "Warning: Board " << boardID << " is being configured twice \n ";
      m_sx << "The new configuration from " << Board_Config_List.at(i).fileName().toStdString() 
	   << " will override the first configuration \n ";

      PopUpWarning( m_sx.str().c_str() );

    }

    // for testing only
    //    miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask = 0b1110;

    //------------------------------------------------------//
    //      only attempt for boards with stable elinks
    //------------------------------------------------------//

    if ( (boardMask & miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask ) != 0 ) {

      if ( board_sync_soft_reset ) {
	BoardConfigModule().FEB_Soft_Reset_GPIO_Low( TARGETPORT, TargetIp, boardType, boardID );

	if ( ( 0b1 << boardID ) &
	     ( ~miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask ) ) {
	  miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask += ( 0b1 << boardID ) ;
	}

	m_sx.str("");
	m_sx << "Board " << boardID << " gpio set to low in preparation for synchronous reset \n";
	msg()(m_sx);

      }
      else if ( board_renable_gpio ) {
        BoardConfigModule().FEB_Soft_Reset_GPIO_High( TARGETPORT, TargetIp, boardType, boardID );

	if ( ( 0b1 << boardID ) &
             ( ~miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask ) ) {
          miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask += ( 0b1 << boardID ) ;
        }

	m_sx.str("");
	m_sx <<"Board " << boardID << " gpio set to high after synchronous reset \n";
	msg()(m_sx);

      }
      else {
	prepareAndSendBoardConfig( resetVMM_hard, resetVMM_auto, onlyPFEB);
      }

    }
      
    if ( isSetup ) {
      if ( ui->ConfiguringFEBs == QObject::sender() ) icommand+=12;
      else                                            icommand+= 4;

      if( ui->ConfiguringFEBs == QObject::sender() ) { 
	if ( icommand > 300 ) icommand = 300;
      }
      else if ( icommand > 450 ) {
	icommand = 450;
      }

      ui->SetupProgressBar->setValue(icommand );
    }

  }

  if ( miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask == 0 ) {
    m_sx.str("");
    m_sx << "No elinks are locked: Please scan for elinks \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("No elinks are locked:  Please first Scan for elinks");
    PopUpError("Error: No elinks are locked: Please check connection and scan for elinks");
  }
  else if ( miniDAQConfigHandle().All_Boards_Configed() ) {
    m_sx.str("");
    m_sx << "All detected boards are configured \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("All FEB ASICs Configured: Ready to Prepare Run");

    if( ui->ConfiguringFEBs == QObject::sender() ) {
      icommand = 300;
      ui->SetupProgressBar->setValue( icommand );
    }

  }
  else {

    QString FailedBoard_mask = QString("%1").arg( (miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask &
						   ~miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask), 
						  8, 2, QChar('0'));
    
    m_sx.str("");
    m_sx << "Error: Board configuration failed for board " 
	 << FailedBoard_mask.toStdString() << "\n";
    msg()(m_sx);
    
    PopUpError(m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Board(s) " << FailedBoard_mask.toStdString() << " failed configuration";
    ui->connectionLabel->setText(QString::fromStdString( m_sx.str() ) );
  }

  std::cout << "n command sent: " << icommand << std::endl;

}
//---------------------------------------------------------//
void MainWindow::getElinkPhaseMatrix() {
  std::vector< std::vector<bool> > LinkOffsets = miniDAQConfigHandle().miniDAQ_Settings().elink_matrix ;

  stringstream m_sx;

  m_sx.str("");

  m_sx << "ELink Phase Matrix \n ";

  for ( uint j=0; j<LinkOffsets.at(0).size(); j++ ) {

    m_sx << "Board " << j << " : ";

    for ( uint i=0; i < LinkOffsets.size(); i++ ) {

      if ( LinkOffsets.at(i).at(j) ) {
	m_sx << 1 << " " ;
      }
      else {
	m_sx << 0 << " ";
      }
    }

    m_sx << " \n";

  }

  PopUpInfo( m_sx.str().c_str() );

}
//---------------------------------------------------------//
void MainWindow::updateFEBType(){

  if ( ui->FE_Board_Type->currentIndex() == board_type_pFEB ||
       ui->FE_Board_Type->currentIndex() == board_type_pFEB_v22 ) {
    ui->vmm_select_1->setEnabled(true);
    ui->vmm_select_2->setEnabled(true);
    ui->vmm_select_3->setEnabled(true);
    if ( ui->vmm_select_4->isChecked() ) ui->vmm_select_4->setChecked(false);
    if ( ui->vmm_select_5->isChecked() ) ui->vmm_select_5->setChecked(false);
    if ( ui->vmm_select_6->isChecked() ) ui->vmm_select_6->setChecked(false);
    if ( ui->vmm_select_7->isChecked() ) ui->vmm_select_7->setChecked(false);
    if ( ui->vmm_select_8->isChecked() ) ui->vmm_select_8->setChecked(false);
    ui->vmm_select_4->setEnabled(false);
    ui->vmm_select_5->setEnabled(false);
    ui->vmm_select_6->setEnabled(false);
    ui->vmm_select_7->setEnabled(false);
    ui->vmm_select_8->setEnabled(false);

    ui->tds_select_1->setEnabled(true);
    if ( ui->tds_select_2->isChecked() ) ui->vmm_select_2->setChecked(false);
    if ( ui->tds_select_3->isChecked() ) ui->vmm_select_3->setChecked(false);
    if ( ui->tds_select_4->isChecked() ) ui->vmm_select_4->setChecked(false);
    ui->tds_select_2->setEnabled(false);
    ui->tds_select_3->setEnabled(false);
    ui->tds_select_4->setEnabled(false);

  }
  else if ( ui->FE_Board_Type->currentIndex() == board_type_sFEB ) {
    if ( !ui->vmm_select_1->isChecked() ) ui->vmm_select_1->setChecked(true);
    if ( !ui->vmm_select_2->isChecked() ) ui->vmm_select_2->setChecked(true);
    if ( !ui->vmm_select_3->isChecked() ) ui->vmm_select_3->setChecked(true);
    if ( !ui->vmm_select_4->isChecked() ) ui->vmm_select_4->setChecked(true);
    if ( !ui->vmm_select_5->isChecked() ) ui->vmm_select_5->setChecked(true);
    if ( !ui->vmm_select_6->isChecked() ) ui->vmm_select_6->setChecked(true);
    if ( !ui->vmm_select_7->isChecked() ) ui->vmm_select_7->setChecked(true);
    if ( !ui->vmm_select_8->isChecked() ) ui->vmm_select_8->setChecked(true);
    ui->vmm_select_1->setEnabled(false);
    ui->vmm_select_2->setEnabled(false);
    ui->vmm_select_3->setEnabled(false);
    ui->vmm_select_4->setEnabled(false);
    ui->vmm_select_5->setEnabled(false);
    ui->vmm_select_6->setEnabled(false);
    ui->vmm_select_7->setEnabled(false);
    ui->vmm_select_8->setEnabled(false);


    ui->tds_select_1->setEnabled(true);
    ui->tds_select_2->setEnabled(true);
    ui->tds_select_3->setEnabled(true);
    ui->tds_select_4->setEnabled(true);
  }
  else if ( ui->FE_Board_Type->currentIndex() == board_type_sFEB_v22 ) {
    if ( ui->vmm_select_1->isChecked() ) ui->vmm_select_1->setChecked(false);
    if ( ui->vmm_select_2->isChecked() ) ui->vmm_select_2->setChecked(false);
    if ( ui->vmm_select_3->isChecked() ) ui->vmm_select_3->setChecked(false);
    if ( ui->vmm_select_4->isChecked() ) ui->vmm_select_4->setChecked(false);
    if ( ui->vmm_select_5->isChecked() ) ui->vmm_select_5->setChecked(false);
    if ( ui->vmm_select_6->isChecked() ) ui->vmm_select_6->setChecked(false);
    if ( ui->vmm_select_7->isChecked() ) ui->vmm_select_7->setChecked(false);
    if ( ui->vmm_select_8->isChecked() ) ui->vmm_select_8->setChecked(false);
    ui->vmm_select_1->setEnabled(true);
    ui->vmm_select_2->setEnabled(true);
    ui->vmm_select_3->setEnabled(true);
    ui->vmm_select_4->setEnabled(true);
    ui->vmm_select_5->setEnabled(true);
    ui->vmm_select_6->setEnabled(true);
    ui->vmm_select_7->setEnabled(true);
    ui->vmm_select_8->setEnabled(true);

    ui->tds_select_1->setEnabled(true);
    ui->tds_select_2->setEnabled(true);
    ui->tds_select_3->setEnabled(true);
    ui->tds_select_4->setEnabled(true);
  }

}
//---------------------------------------------------------//

void MainWindow::configDir_hardReset() {
  prepareAndSendConfigDir(true,  false, false, false);
  prepareAndSendConfigDir(false, false, false, false);
}
void MainWindow::configDir_stcrReset() {
  prepareAndSendConfigDir(false, true,  false, false);
  bool send_ok = BoardConfigModule().VMM_TestPulse_CKTP_Spacing(TARGETPORT, TargetIp, 0xffff );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set training pulse spacing to 0xffff, Aborting Config MiniDAQ");
    return;
  }
  send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp, 20 );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set training pulse frequency to 0, Aborting Config MiniDAQ");
    return;
  }
  boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
  prepareAndSendConfigDir(false, false, false, false);
}

void MainWindow::configBoard_stcrReset() {
  prepareAndSendBoardConfig(false, true);
  bool send_ok = BoardConfigModule().VMM_TestPulse_CKTP_Spacing(TARGETPORT, TargetIp, 0xffff );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set training pulse spacing to 0xffff, Aborting Config MiniDAQ");
    return;
  }
  send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp, 20 );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set training pulse frequency to 0, Aborting Config MiniDAQ");
    return;
  }
  boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
  prepareAndSendBoardConfig(false, false);
}
void MainWindow::configBoard_hardReset(){
  prepareAndSendBoardConfig(true, false);
  prepareAndSendBoardConfig(false, false);

}

//--------------------------------------------------------------------//
void MainWindow::prepareAndSendBoardConfig(bool resetVMM_hard, bool resetVMM_auto, bool onlyPFEB)
{
  
  msg()(" ");
  
  stringstream m_sx;
  
  bool all_ok = true;
  bool load_calib = false;

  m_sx.str("");
  m_sx << "Sending configuration for all chips on board " 
       << ui->FE_Board_ID->value() << " with Unique ID "
       << ui->FE_Board_UniqueS->text().toStdString() << std::endl;
  msg()(m_sx);

  //-------------------------------------------//

  LoadBoardConfig_to_Handler();
  buildTargetIP();

  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;
  int nTDS = BoardConfigHandle().BoardSettings().nTDS;

  //-------------------------------------------//

  m_sx << "Board contains " << BoardConfigHandle().BoardSettings().nVMM 
       << " nVMMs and "     << BoardConfigHandle().BoardSettings().nTDS
       << " nTDSs: Configuring each now "  << std::endl;
  msg()(m_sx);

  //-------------------------------------------------------//
  // enable all VMM
  //-------------------------------------------------------//

  if( !(BoardConfigHandle().BoardSettings().ROC_UniqueS == "" ) ) {

    RocFPGAConfigHandle().Set_ROC_Registered_to_Default(boardID, boardType);
    
    bool send_ok = RocFPGAConfigModule().SendRocFPGAConfig(TARGETPORT, TargetIp);
    
    if ( !send_ok ) {
      m_sx.str("");
      m_sx << "failed to enable all VMM before configuring for board "
	   << boardID << " with Unique ID "
	   << BoardConfigHandle().BoardSettings().boardUN.toStdString() << "\n";
      msg()(m_sx);
      
      PopUpError( m_sx.str().c_str() );
      return;
    }
    else {
      
      m_sx.str("");
      m_sx << "../data_temp/temp/board_" << RocFPGAConfigHandle().ROC_FPGASettings().boardID ;
      m_sx << "_roc_current_tmp.xml";
      
      RocFPGAConfigHandle().Write_ROC_FPGAConfig_to_File(QString::fromStdString(m_sx.str()));
      
      m_sx.str("");
      m_sx << "Board " << RocFPGAConfigHandle().ROC_FPGASettings().boardID;
      
      QString ROC_name = QString::fromStdString( m_sx.str() );
      bool new_ROC = true;
      for ( int i=0; i<ui->current_ROC_FPGA_Config->count(); i++ ) {
	if ( ui->current_ROC_FPGA_Config->itemText(i) == ROC_name ) new_ROC = false;
      }
      
      if ( new_ROC ) ui->current_ROC_FPGA_Config->addItem(ROC_name);
      
    }
    
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

  }

  //--------------------------------------------------------//
  //                   Config Board ASICs
  //--------------------------------------------------------//

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());
  

  //---------------------------------------------------------------//                       
  //                       Configure TDS                                                    
  //---------------------------------------------------------------//                       

  for ( int i=0; i<nTDS; i++ ) {

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/TDS_"+BoardConfigHandle().BoardSettings().TDS_UniqueS_Numbers.at(i)+".xml";

    bool ok;

    QFile test(file_name);
    if ( test.exists() ) {

      for ( int j=0; j<1; j++ ) {
        ok = TDSConfigHandle().LoadTDSConfig_from_File(file_name);

        TDSConfigHandle().SetBoardID( BoardConfigHandle().BoardSettings().boardID );
        TDSConfigHandle().SetBoardType(BoardConfigHandle().BoardSettings().boardType );

	if ( BoardConfigHandle().BoardSettings().boardType == board_type_sFEB &&
	     onlyPFEB ) continue;

        if(ok) {
          msg()(("Configuration loaded successfully for " + file_name).toStdString());
        }
        else {
          msg()("problem loading configuration from file, retrying");
	  continue;
        }

        bool send_ok = TDSConfigModule().SendTDSConfig(TARGETPORT, TargetIp);

	// for testing                                                                                                                                                                   
        //      send_ok = true;                                                                                                                                                          

        if ( send_ok ) {
          msg()("TDS properly configured");

          m_sx.str("");
          m_sx << "../data_temp/temp/board_" << TDSConfigHandle().TDSGlobalSettings().boardID ;
          m_sx << "_tds_"                    << TDSConfigHandle().TDSGlobalSettings().chipID <<"_current_tmp.xml";
          TDSConfigHandle().WriteTDSConfig_to_File(QString::fromStdString(m_sx.str()));

          m_sx.str("");
          m_sx << "Board " << TDSConfigHandle().TDSGlobalSettings().boardID
               << " TDS "  << TDSConfigHandle().TDSGlobalSettings().chipID;

          QString TDS_name = QString::fromStdString( m_sx.str() );
          bool new_TDS = true;
          for ( int i=0; i<ui->current_TDS_Config->count(); i++ ) {
            if ( ui->current_TDS_Config->itemText(i) == TDS_name ) new_TDS = false;
          }

          if ( new_TDS ) ui->current_TDS_Config->addItem(TDS_name);

          break;
        }
        else {
          if ( j != 0 ) {
            msg()("error when configuring TDS, retrying");
            continue;
          }
          else {

            m_sx.str("");
            m_sx << "Board ID " << TDSConfigHandle().TDSGlobalSettings().boardID;
            m_sx << " tds "     << TDSConfigHandle().TDSGlobalSettings().chipID;

            PopUpError("Error: TDS failed to configure after 1 tries", m_sx.str().c_str());
            //all_ok = false;
          }
        }
      }

    }
    else {
      msg()(("Configuration file " + file_name + " does not exist").toStdString());
      PopUpError( ("Configuration file " + file_name + " does not exist").toStdString().c_str() );
      return;
    }

  }

  //--------------------------------------------------------//
  //                     Configure VMM
  //--------------------------------------------------------//

  //---------------------------------------------------------------------------//
  //   Start from highest VMM ID first to configure in daisy chain for sFEB
  //   pFEBs don't care what order VMMs are configured so configuring last
  //         first also works for pFEBs
  //---------------------------------------------------------------------------//

  for ( int i=nVMM-1; i>=0; i-- ) {

    if ( BoardConfigHandle().BoardSettings().boardType == board_type_sFEB &&
	 onlyPFEB ) continue;
    //    if ( BoardConfigHandle().BoardSettings().boardType == board_type_sFEB &&
    //	 ( i == 0 || i == 7 ) ) continue;

    bool ok;
    
    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(i)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {

      for ( int j=0; j<1; j++ ) {
	ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);
	if ( resetVMM_hard ) VMMConfigHandle().setResetHardBit( true ) ;
	if ( resetVMM_auto ) VMMConfigHandle().setResetAutoBit( true );
	VMMConfigHandle().SetBoardID( BoardConfigHandle().BoardSettings().boardID );
	VMMConfigHandle().SetBoardType( BoardConfigHandle().BoardSettings().boardType );

        if ( load_calib ) {
	  Scan_Baseline( i, 
			 BoardConfigHandle().BoardSettings().boardID, 
			 BoardConfigHandle().BoardSettings().boardType );
	  Find_Optimal_Trimming( i, 
				 BoardConfigHandle().BoardSettings().boardID,
				 BoardConfigHandle().BoardSettings().boardType );
	  VMMConfigHandle().WriteVMMConfig_to_File(file_name);
	}

	// sFEB configuration always 8 VMM in daisy chain.
	// each VMM configuration only have VMM_ID = ith VMM.
	// pFEB configuration can send multiple VMMs at the same time

	if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
	  std::cout << "daisy chain config,setting VMMmask to 0b11111111" << std::endl;
	  VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
	  // always hard reset VMM 0,1,2,3 on sFEB
	  //if ( i == 0 || i == 1 || i == 2 || i == 3 ) VMMConfigHandle().setResetHardBit( true ) ;
	}
	else {
	  std::cout << "point to point FEB config, setting VMMmask to " << i << std::endl;
          VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << i );
        }

	if(ok) {
	  msg()(("Configuration loaded successfully for " + file_name).toStdString());
	}
	else {
	  msg()("problem loading configuration from file, retrying");
	  continue;
	}

	bool send_ok;
	send_ok = VMMConfigModule().SendVMMConfig(TARGETPORT, TargetIp);
	
	//for testing
	//	send_ok = true;

	if ( send_ok ) {
	  msg()("VMM properly configured");

	  m_sx.str("");
	  m_sx << "../data_temp/temp/board_" << VMMConfigHandle().VMMGlobalSettings().boardID ;

	  QString vmmMask;

	  vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
				      8, 2, QChar('0'));

	  m_sx << "_vmm_" << i ; //vmmMask.toStdString() 
	  m_sx << "_current_tmp.xml";

	  VMMConfigHandle().WriteVMMConfig_to_File(QString::fromStdString(m_sx.str()));

	  m_sx.str("");
	  m_sx << "Board " << VMMConfigHandle().VMMGlobalSettings().boardID
	       << " VMM " << i; //vmmMask.toStdString() ;

	  QString VMM_name = QString::fromStdString( m_sx.str() );
	  bool new_VMM = true;
	  for( int i=0; i<ui->current_VMM_Config->count(); i++ ) {
	    if ( ui->current_VMM_Config->itemText(i) == VMM_name ) new_VMM = false;
	  }

	  if ( new_VMM ) ui->current_VMM_Config->addItem(VMM_name);

	  //boost::this_thread::sleep(boost::posix_time::milliseconds(500));

	  break;
	}
	else {
	  if ( j != 0 ) {
	    msg()("error when configuring VMM, retrying");
	    continue;
	  }
	  else {
	    m_sx.str("");
	    m_sx << "Board ID " << VMMConfigHandle().VMMGlobalSettings().boardID;
	    
	    QString vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
						8, 2, QChar('0'));
	    m_sx << " VMM mask "  << vmmMask.toStdString();
	      
	    PopUpError("Error: VMM failed to configure after 1 tries", m_sx.str().c_str());
	    all_ok = false;
	  }
	}
	
      }
    }
    else {
      msg()(("Configuration file " + file_name + " does not exist").toStdString());
      PopUpError( ("Configuration file " + file_name + " does not exist").toStdString().c_str() );
      return;
    }

  }
 
  //--------------------------------------------------------------//
  //   Set VMM enable mask to value saved in xml file
  //--------------------------------------------------------------//

  boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

  if( !(BoardConfigHandle().BoardSettings().ROC_UniqueS == "" ) ) {
    QString filename_roc = boardConfigFile.absoluteDir().absolutePath() +
      "/ROC_"+BoardConfigHandle().BoardSettings().ROC_UniqueS+".xml";

    RocFPGAConfigHandle().Load_ROC_FPGAConfig_from_File(filename_roc);
    RocFPGAConfigHandle().SetBoardID(boardID);
    RocFPGAConfigHandle().SetBoardType(boardType);

    bool send_ok = RocFPGAConfigModule().SendRocFPGAConfig(TARGETPORT, TargetIp);
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
    
    if ( !send_ok ) {
      m_sx.str("");
      m_sx << "failed to set VMM enabled mask for board "
	   << boardID << " with Unique ID "
	   << BoardConfigHandle().BoardSettings().boardUN.toStdString() << "\n";
      msg()(m_sx);
      
      PopUpError( m_sx.str().c_str() );
      return;
    }
    else {
      
      m_sx.str("");
      m_sx << "../data_temp/temp/board_" << RocFPGAConfigHandle().ROC_FPGASettings().boardID ;
      m_sx << "_roc_current_tmp.xml";
      
      RocFPGAConfigHandle().Write_ROC_FPGAConfig_to_File(QString::fromStdString(m_sx.str()));
      
      m_sx.str("");
      m_sx << "Board " << RocFPGAConfigHandle().ROC_FPGASettings().boardID;
      
      QString ROC_name = QString::fromStdString( m_sx.str() );
      bool new_ROC = true;
      for ( int i=0; i<ui->current_ROC_FPGA_Config->count(); i++ ) {
	if ( ui->current_ROC_FPGA_Config->itemText(i) == ROC_name ) new_ROC = false;
      }
      
      if ( new_ROC ) ui->current_ROC_FPGA_Config->addItem(ROC_name);
      
    }
  }

  //-------------------------------------------------------------//

  // for testing only
  //  all_ok = true;

  if ( all_ok ) {

    m_sx.str("");
    m_sx<<"../data_temp/temp/board_" << boardID << "_current_tmp.xml";

    BoardConfigHandle().WriteBoardConfig_to_File(QString::fromStdString(m_sx.str()));

    if ( ( 0b1 << boardID ) &
	 ( ~miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask ) ) {
      miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask += ( 0b1 << boardID ) ;

      m_sx.str("");
      m_sx<<"Board " << boardID ;
      ui->current_Board_Config->addItem(QString::fromStdString(m_sx.str()));

    }

    QString Board_mask = QString("%1").arg(miniDAQConfigHandle().miniDAQ_Settings().configured_board_mask,
					   8, 2, QChar('0'));

    m_sx.str("");
    m_sx << "board(s) " << Board_mask.toStdString() ;
    if ( !resetVMM_hard && !resetVMM_auto ) m_sx << " configured successfully \n";
    else                                    m_sx << " reset successfully \n";

    if ( !resetVMM_hard && !resetVMM_auto ) ui->connectionLabel->setText("board(s) "+Board_mask+" configured successfully");
    else                                    ui->connectionLabel->setText("board(s) "+Board_mask+" reset successfully");
    msg()(m_sx);

    if ( ui->ReconfigureBoard == QObject::sender() ) { 
      PopUpInfo( m_sx.str().c_str() );
    }
    
  }
  else          ui->connectionLabel->setText("board config error");

}

void MainWindow::configBoard_all_vmms() {

  msg()(" ");

  stringstream m_sx;

  //  bool all_ok = true;

  m_sx.str("");
  m_sx << "Writing new configuration for all chips on board "
       << ui->FE_Board_ID->value() << " with Unique ID "
       << ui->FE_Board_UniqueS->text().toStdString() << std::endl;
  msg()(m_sx);

  //-------------------------------------------//

  LoadBoardConfig_to_Handler();
  buildTargetIP();

  //int boardID = BoardConfigHandle().BoardSettings().boardID;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;
  //int nTDS = BoardConfigHandle().BoardSettings().nTDS;

  //-------------------------------------------//

  m_sx << "Board contains " << BoardConfigHandle().BoardSettings().nVMM
       << " nVMMs and "     << BoardConfigHandle().BoardSettings().nTDS
       << " nTDSs: Configuring each now "  << std::endl;
  msg()(m_sx);

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());

  //------------------------------------------//

  for ( int i=nVMM-1; i>=0; i-- ) {

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(i)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {

      bool ok;

      ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);

      if ( !ok ) {
	m_sx.str("");
	m_sx << "failed to load vmm xml file " << file_name.toStdString() << "\n";
	PopUpError(m_sx.str().c_str());
      }

      if ( ui->board_change_all_vmm_thresholds == QObject::sender() ) { 
	VMMConfigHandle().VMMGlobalSettings().sdt_dac = ui->board_vmm_threshold_global_sdt_dac->value();
      }
      if ( ui->board_change_all_vmm_gain == QObject::sender() )       { 
	VMMConfigHandle().VMMGlobalSettings().sg      = ui->board_vmm_gain_global_sg->currentIndex();
      }
      if ( ui->board_mask_all_channels == QObject::sender() )         { 
	for(int i = 0; i < 64; i++) {
	  VMMConfigHandle().VMM_ChannelSettings(i).sm  = true;
	} // i    
      }
      if ( ui->board_open_all_channels == QObject::sender() )         {
	for(int i = 0; i < 64; i++) {
          VMMConfigHandle().VMM_ChannelSettings(i).sm  = false;
	} 
      }
      if ( ui->board_change_mon_chan == QObject::sender() )   {
	int ichan = ui->board_vmm_mon_chan_global_sm5->currentIndex();
	VMMConfigHandle().VMM_ChannelSettings(ichan).sm = false;
	VMMConfigHandle().VMM_ChannelSettings(ichan).st = true;
	VMMConfigHandle().VMM_ChannelSettings(ichan).sth = true;
	VMMConfigHandle().VMMGlobalSettings().sm5 = ichan;

	if ( ui->board_mask_prev_mon_chan->isChecked() && prev_mon_chan >= 0 ) {
	  VMMConfigHandle().VMM_ChannelSettings(prev_mon_chan).sm = true;
	  VMMConfigHandle().VMM_ChannelSettings(prev_mon_chan).st = false;
	  VMMConfigHandle().VMM_ChannelSettings(prev_mon_chan).sth = false;
	}

	prev_mon_chan = ichan;

      }

      VMMConfigHandle().SetBoardID( BoardConfigHandle().BoardSettings().boardID );
      VMMConfigHandle().SetBoardType( BoardConfigHandle().BoardSettings().boardType );

      VMMConfigHandle().WriteVMMConfig_to_File(file_name);

    }
  }

}

//---------------------------------------------------------//
void MainWindow::prepareAndSendVMMConfig()
{

  stringstream m_sx;

  m_sx.str(" ");
  msg()(m_sx);

  m_sx << "Sending Configuration for VMM number " ;
  if ( ui->vmm_select2_1->isChecked() ) m_sx <<  "0,";
  if ( ui->vmm_select2_2->isChecked() ) m_sx <<  "1,";
  if ( ui->vmm_select2_3->isChecked() ) m_sx <<  "2,";
  if ( ui->vmm_select2_4->isChecked() ) m_sx <<  "3,";
  if ( ui->vmm_select2_5->isChecked() ) m_sx <<  "4,";
  if ( ui->vmm_select2_6->isChecked() ) m_sx <<  "5,";
  if ( ui->vmm_select2_7->isChecked() ) m_sx <<  "6,";
  if ( ui->vmm_select2_8->isChecked() ) m_sx <<  "7,";
  m_sx << " on board " << ui->VMM_boardID->value() << "\n";
  msg()(m_sx);

  LoadVMMConfig_to_Handler();

  buildTargetIP();

  bool send_ok = true;
  send_ok = VMMConfigModule().SendVMMConfig(TARGETPORT, TargetIp);

  // for testing
  //  send_ok = true;

  if ( send_ok ) {

    if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_pFEB || 
	 VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_pFEB_v22 || 
	 VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB_v22 ) {

      int nVMM_on_FEB;

      if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_pFEB ||
	   VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_pFEB_v22 ) {
	nVMM_on_FEB = 3;
      }
      else {
	nVMM_on_FEB = 8;
      }

      for ( int i=0; i<nVMM_on_FEB; i++ ) {

	if ( VMMConfigHandle().VMMGlobalSettings().vmmmask & ( 0b1 << i ) ) {
	  
	  m_sx.str("");
	  m_sx << "../data_temp/temp/board_" << VMMConfigHandle().VMMGlobalSettings().boardID ; 
	  
	  QString vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
					      8, 2, QChar('0'));
	  
	  m_sx << "_vmm_" << i << "_current_tmp.xml"; //vmmMask.toStdString() << "_current_tmp.xml";
    
	  VMMConfigHandle().WriteVMMConfig_to_File(QString::fromStdString(m_sx.str()));

	  m_sx.str("");
	  m_sx << "Board " << VMMConfigHandle().VMMGlobalSettings().boardID
	       << " VMM " << i; //vmmMask.toStdString() ;

	  QString VMM_name = QString::fromStdString( m_sx.str() );
	  bool new_VMM = true;
	  for( int i=0; i<ui->current_VMM_Config->count(); i++ ) {
	    if ( ui->current_VMM_Config->itemText(i) == VMM_name ) new_VMM = false;
	  }

	  if ( new_VMM ) ui->current_VMM_Config->addItem(VMM_name);

	}
      }

      if ( ui->ReconfigureVMM == QObject::sender() ) {

	QString vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
					    8, 2, QChar('0'));
	m_sx.str("");
	m_sx << "Board " << VMMConfigHandle().VMMGlobalSettings().boardID 
	     << " VMM "  << vmmMask.toStdString()
	     << " has been successfully configured";
	PopUpInfo( m_sx.str().c_str() );
      }

    }
    if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
      PopUpWarning("Warning: VMM configuration was succesfully sent BUT: \n       sFEB use daisy chain configuration and should be configured using entire board configuration. \n       Manual configurations will not be saved to current VMM configs \n       Proceed with caution");
    }
  }
  else {

    QString vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
					8, 2, QChar('0'));

    m_sx.str("");
    m_sx << "Board " << VMMConfigHandle().VMMGlobalSettings().boardID
	 << " VMM "  << vmmMask.toStdString()
	 << " configuration failed";
    PopUpError( m_sx.str().c_str() );
  }

}
//---------------------------------------------------------//
void MainWindow::prepareAndSendTDSConfig()
{
  stringstream m_sx;

  m_sx.str(" ");
  msg()(m_sx);

  m_sx << "Sending Configuration for TDS number " ;
  if ( ui->tds_select2_1->isChecked() ) m_sx <<  "0,";
  if ( ui->tds_select2_2->isChecked() ) m_sx <<  "1,";
  if ( ui->tds_select2_3->isChecked() ) m_sx <<  "2,";
  if ( ui->tds_select2_4->isChecked() ) m_sx <<  "3,";
  m_sx << " on board " << ui->TDS_boardID->value() << "\n";
  msg()(m_sx);

  LoadTDSConfig_to_Handler();

  buildTargetIP();

  bool send_ok = true;
  send_ok = TDSConfigModule().SendTDSConfig(TARGETPORT, TargetIp);

  // for testing
  //  send_ok = true;

  if ( send_ok ) {
    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << TDSConfigHandle().TDSGlobalSettings().boardID ;
    m_sx << "_tds_"                    << TDSConfigHandle().TDSGlobalSettings().chipID <<"_current_tmp.xml";
    TDSConfigHandle().WriteTDSConfig_to_File(QString::fromStdString(m_sx.str()));

    m_sx.str("");
    m_sx << "Board " << TDSConfigHandle().TDSGlobalSettings().boardID 
	 << " TDS " << TDSConfigHandle().TDSGlobalSettings().chipID;

    QString TDS_name = QString::fromStdString( m_sx.str() );
    bool new_TDS = true;
    for ( int i=0; i<ui->current_TDS_Config->count(); i++ ) {
      if ( ui->current_TDS_Config->itemText(i) == TDS_name ) new_TDS = false;
    }

    if ( new_TDS ) ui->current_TDS_Config->addItem(TDS_name);

    m_sx.str("");
    m_sx << "Board " << TDSConfigHandle().TDSGlobalSettings().boardID
	 << " TDS "  << TDSConfigHandle().TDSGlobalSettings().chipID
	 <<" has been successfully configured";
    PopUpInfo( m_sx.str().c_str() );

  }
  else {

    m_sx.str("");
    m_sx << "Board " << TDSConfigHandle().TDSGlobalSettings().boardID
	 << " tds "  << TDSConfigHandle().TDSGlobalSettings().chipID
	 << " configuration failed";
    PopUpError( m_sx.str().c_str() );

  }


}
//---------------------------------------------------------//
void MainWindow::prepareAndSendRocFPGAConfig()
{
  stringstream m_sx;

  m_sx.str(" ");
  msg()(m_sx);

  m_sx << "Sending Configuration for ROC " ;
  m_sx << " on board " << ui->ROC_boardID->value() << "\n";
  msg()(m_sx);

  Load_ROC_FPGA_Config_to_Handler();

  buildTargetIP();

  bool send_ok = true;
  send_ok = RocFPGAConfigModule().SendRocFPGAConfig(TARGETPORT, TargetIp);

  // for testing                                                                                                               
  //  send_ok = true;                                                                                                          
  if ( send_ok ) {
    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << RocFPGAConfigHandle().ROC_FPGASettings().boardID ;
    m_sx << "_roc_current_tmp.xml";

    RocFPGAConfigHandle().Write_ROC_FPGAConfig_to_File(QString::fromStdString(m_sx.str()));

    m_sx.str("");
    m_sx << "Board " << RocFPGAConfigHandle().ROC_FPGASettings().boardID;

    QString ROC_name = QString::fromStdString( m_sx.str() );
    bool new_ROC = true;
    for ( int i=0; i<ui->current_ROC_FPGA_Config->count(); i++ ) {
      if ( ui->current_ROC_FPGA_Config->itemText(i) == ROC_name ) new_ROC = false;
    }

    if ( new_ROC ) ui->current_ROC_FPGA_Config->addItem(ROC_name);

    m_sx.str("");
    m_sx << "Board " << RocFPGAConfigHandle().ROC_FPGASettings().boardID
         << " FPGA ROC has been successfully configured";
    PopUpInfo( m_sx.str().c_str() );

  }
  else {
    m_sx.str("");
    m_sx << "Board " << RocFPGAConfigHandle().ROC_FPGASettings().boardID
         << " FPGA ROC configuration failed";
    PopUpError( m_sx.str().c_str() );
  }

}
//---------------------------------------------------------//
void MainWindow::prepareAndSendRocASICConfig()
{
  stringstream m_sx;

  m_sx.str(" ");
  msg()(m_sx);

  m_sx << "Sending Configuration for ROC " ;
  m_sx << " on board " << ui->ROC_ASIC_boardID->value() << "\n";
  msg()(m_sx);

  Load_ROC_ASIC_Config_to_Handler();

  buildTargetIP();

  bool send_ok = true;
  send_ok = RocASICConfigModule().SendRocASIC_DigitalConfig(TARGETPORT, TargetIp);

  // for testing 
  //  send_ok = true;  
  if ( send_ok ) {

    m_sx.str("");
    m_sx << "Board " << RocASICConfigHandle().ROC_ASIC_Settings().boardID
         << " ASIC ROC digital has been successfully configured";
    PopUpInfo( m_sx.str().c_str() );

  }
  else {
    m_sx.str("");
    m_sx << "Board " << RocASICConfigHandle().ROC_ASIC_Settings().boardID
         << " ASIC ROC digital configuration failed";
    PopUpError( m_sx.str().c_str() );
    return;
  }

  send_ok = RocASICConfigModule().SendRocASIC_AnalogConfig(TARGETPORT, TargetIp);

  if ( send_ok ) {
    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << RocASICConfigHandle().ROC_ASIC_Settings().boardID ;
    m_sx << "_roc_asic_current_tmp.xml";

    RocASICConfigHandle().Write_ROC_ASICConfig_to_File(QString::fromStdString(m_sx.str()));

    m_sx.str("");
    m_sx << "Board " << RocASICConfigHandle().ROC_ASIC_Settings().boardID;

    QString ROC_name = QString::fromStdString( m_sx.str() );
    bool new_ROC = true;
    for ( int i=0; i<ui->current_ROC_ASIC_Config->count(); i++ ) {
      if ( ui->current_ROC_ASIC_Config->itemText(i) == ROC_name ) new_ROC = false;
    }

    if ( new_ROC ) ui->current_ROC_ASIC_Config->addItem(ROC_name);

    m_sx.str("");
    m_sx << "Board " << RocASICConfigHandle().ROC_ASIC_Settings().boardID
         << " ASIC ROC analog has been successfully configured";
    PopUpInfo( m_sx.str().c_str() );

  }
  else {
    m_sx.str("");
    m_sx << "Board " << RocASICConfigHandle().ROC_ASIC_Settings().boardID
         << " ASIC ROC analog configuration failed";
    PopUpError( m_sx.str().c_str() );
  }


}
//---------------------------------------------------------//
int MainWindow::Convert_VMM_Mask_to_Value(){
  int VMMMask = 0;

  if ( ui->vmm_select2_1->isChecked() ) {
    VMMMask += 0b00000001;
  }
  if ( ui->vmm_select2_2->isChecked() ) {
    VMMMask += 0b00000010;
  }
  if ( ui->vmm_select2_3->isChecked() ) {
    VMMMask += 0b00000100;
  }
  if ( ui->vmm_select2_4->isChecked() ) {
    VMMMask += 0b00001000;
  }
  if ( ui->vmm_select2_5->isChecked() ) {
    VMMMask += 0b00010000;
  }
  if ( ui->vmm_select2_6->isChecked() ) {
    VMMMask += 0b00100000; 
  }
  if ( ui->vmm_select2_7->isChecked() ) {
    VMMMask += 0b01000000;
  }
  if ( ui->vmm_select2_8->isChecked() ) {
    VMMMask += 0b10000000;
  }

  return VMMMask;
}
//---------------------------------------------------------//
void MainWindow::Display_VMM_Mask(uint32_t VMMMask){

  if ( ( VMMMask & 0b00000001 ) == 0b00000001 ) ui->vmm_select2_1->setChecked(true);
  else                                          ui->vmm_select2_1->setChecked(false);
  if ( ( VMMMask & 0b00000010 ) == 0b00000010 ) ui->vmm_select2_2->setChecked(true);
  else                                          ui->vmm_select2_2->setChecked(false);
  if ( ( VMMMask & 0b00000100 ) == 0b00000100 ) ui->vmm_select2_3->setChecked(true);
  else                                          ui->vmm_select2_3->setChecked(false);
  if ( ( VMMMask & 0b00001000 ) == 0b00001000 ) ui->vmm_select2_4->setChecked(true);
  else                                          ui->vmm_select2_4->setChecked(false);
  if ( ( VMMMask & 0b00010000 ) == 0b00010000 ) ui->vmm_select2_5->setChecked(true);
  else                                          ui->vmm_select2_5->setChecked(false);
  if ( ( VMMMask & 0b00100000 ) == 0b00100000 ) ui->vmm_select2_6->setChecked(true);
  else                                          ui->vmm_select2_6->setChecked(false);
  if ( ( VMMMask & 0b01000000 ) == 0b01000000 ) ui->vmm_select2_7->setChecked(true);
  else                                          ui->vmm_select2_7->setChecked(false);
  if ( ( VMMMask & 0b10000000 ) == 0b10000000 ) ui->vmm_select2_8->setChecked(true);
  else                                          ui->vmm_select2_8->setChecked(false);

}
//---------------------------------------------------------//
/*
int MainWindow::Convert_TDS_Mask_to_Value(){
  int TDSMask = 0;

  if ( ui->tds_select2_1->isChecked() ) {
    TDSMask += 0b0001;           // (binary 0001)
  }
  if ( ui->tds_select2_2->isChecked() ) {
    TDSMask += 0b0010;           // (binary 0010)
  }
  if ( ui->tds_select2_3->isChecked() ) {
    TDSMask += 0b0100;           // (binary 0100)
  }
  if ( ui->tds_select2_4->isChecked() ) {
    TDSMask += 0b1000;           // (binary 1000)
  }

  return TDSMask;
}
*/
//---------------------------------------------------------//
int MainWindow::Convert_TDS_Mask_to_Value(){
  int TDSMask = 0;

  if ( ui->tds_select2_1->isChecked() ) {
    TDSMask = 0;          
  }
  if ( ui->tds_select2_2->isChecked() ) {
    TDSMask = 1;          
  }
  if ( ui->tds_select2_3->isChecked() ) {
    TDSMask = 2;          
  }
  if ( ui->tds_select2_4->isChecked() ) {
    TDSMask = 3;          
  }

  return TDSMask;
}
//----------------------------------------------------------//
/*
void MainWindow::Display_TDS_Mask(uint32_t TDSMask){

  if ( ( TDSMask & 0b0001 ) == 0b0001 ) ui->tds_select2_1->setChecked(true);
  else                                  ui->tds_select2_1->setChecked(false);
  if ( ( TDSMask & 0b0010 ) == 0b0010 ) ui->tds_select2_2->setChecked(true);
  else                                  ui->tds_select2_2->setChecked(false);
  if ( ( TDSMask & 0b0100 ) == 0b0100 ) ui->tds_select2_3->setChecked(true);
  else                                  ui->tds_select2_3->setChecked(false);
  if ( ( TDSMask & 0b1000 ) == 0b1000 ) ui->tds_select2_4->setChecked(true);
  else                                  ui->tds_select2_4->setChecked(false);

}
*/
//---------------------------------------------------------//
void MainWindow::Display_TDS_Mask(uint32_t TDS_ID){

  if ( TDS_ID == 0 ) ui->tds_select2_1->setChecked(true);
  else              ui->tds_select2_1->setChecked(false);
  if ( TDS_ID == 1 ) ui->tds_select2_2->setChecked(true);
  else              ui->tds_select2_2->setChecked(false);
  if ( TDS_ID == 2 ) ui->tds_select2_3->setChecked(true);
  else              ui->tds_select2_3->setChecked(false);
  if ( TDS_ID == 3 ) ui->tds_select2_4->setChecked(true);
  else              ui->tds_select2_4->setChecked(false);

}
//---------------------------------------------------------//
void MainWindow::setupMiniDAQ() 
{
  //  int total_tries = 3;
  stringstream sx;
  buildTargetIP();

  bool send_ok = true;

  icommand = 0;

  //-------------------------------------------------------------------------//

  ui->connectionLabel->setText("resetting KC705");
  send_ok = BoardConfigModule().GlobalCtr_KC705Reset(TARGETPORT, TargetIp);
  if ( !send_ok ) {
    PopUpError("Error: Failed to reset KC705, Aborting MiniDAQ");
    return;
  }
  icommand++;
  if ( icommand > 200 ) icommand = 200;
  ui->SetupProgressBar->setValue( icommand );
  ui->connectionLabel->setText("KC705 reset");

  //  miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask = 0;

  //-------------------------------------------------------------------------//
  
  ui->connectionLabel->setText("reseting MiniDAQ registers to default");
  send_ok = ConfigureTTC_toDefault();
  if ( !send_ok ) {
    // already has pop up errors in ConfigureTTC_toDefault don't make double
    return;
  }
  icommand++;
  if ( icommand > 200 ) icommand = 200;
  ui->SetupProgressBar->setValue(icommand );
  ui->connectionLabel->setText("MiniDAQ registers set to default");

  //-------------------------------------------------------------------------//
  // reset all SCAs
  ui->connectionLabel->setText("configuring FEBs SCA");
  for ( uint i = 0; i < BoardID_Linked.size(); i++ ) {
    // ignore timeout errors
    BoardConfigModule().config_SCA(TARGETPORT, TargetIp,
				   ui->cmd_boardType->currentIndex(), i);
    icommand++;
    if ( icommand > 200 ) icommand = 200;

    ui->SetupProgressBar->setValue(icommand );
    m_current_SCA_ID = BoardConfigModule().current_SCA_ID();

  }
  ui->connectionLabel->setText("All FEBs SCA configured");

  //-------------------------------------------------------------------------// 
  // enable all VMM for configuration
  //ui->connectionLabel->setText("enable all VMM for configuration");
  //for ( uint i = 0; i < BoardID_Linked.size(); i++ ) {
  //  RocFPGAConfigHandle().Set_ROC_Registered_to_Default(i,0);
    // ignore timeout errors                                                    
  //  RocFPGAConfigModule().SendRocFPGAConfig(TARGETPORT, TargetIp);
  //  icommand++;
  //  ui->SetupProgressBar->setValue(icommand );

  //}
  //ui->connectionLabel->setText("All GPIO enabled");

  //-------------------------------------------------------------------------//

  if (  ( miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask & 0b11111111 ) == 0 ) {

    ui->connectionLabel->setText("Scanning ELinks: takes a minute");
    QueryAllELinks( true );
    ui->connectionLabel->setText("ELinks Locked");
    
  }
  else if ( ui->scan_twinax->isChecked() ) {
    ui->connectionLabel->setText("Scanning ELinks: takes a minute");
    QueryAllELinks( true );
    ui->connectionLabel->setText("ELinks Locked");
  }
  else {
    ui->connectionLabel->setText("using existing elink locked mask");
  }

  if (  ( miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask & 0b11111111 ) == 0 ) {
    ui->connectionLabel->setText("No boards detected: Please check Twinax cable connection");
    PopUpError("No FE Boards Detected: Please check Twinax cable connection and FEB power and reconfigure MiniDaq");
    return;
  }

  //-------------------------------------------------------------------------//
  //                        Enable TTC training
  //-------------------------------------------------------------------------//
  int elink_mask = 0;
  
  if ( ui->elink_lked_1->isChecked() ) elink_mask += 0b1;
  if ( ui->elink_lked_2->isChecked() ) elink_mask += 0b10;
  if ( ui->elink_lked_3->isChecked() ) elink_mask += 0b100;
  if ( ui->elink_lked_4->isChecked() ) elink_mask += 0b1000;
  if ( ui->elink_lked_5->isChecked() ) elink_mask += 0b10000;
  if ( ui->elink_lked_6->isChecked() ) elink_mask += 0b100000;
  if ( ui->elink_lked_7->isChecked() ) elink_mask += 0b1000000;
  if ( ui->elink_lked_8->isChecked() ) elink_mask += 0b10000000;
  
  //  ui->connectionLabel->setText("sending TTC training pattern");
  //  send_ok = BoardConfigModule().TTC_Ctr_Training(TARGETPORT, TargetIp, elink_mask);
  //  if ( !send_ok ) {
  //    PopUpError("Error: Send TTC Training Failed");
  //    return;
  //  }

  icommand++;
  if ( icommand > 200 ) icommand = 200;
  ui->SetupProgressBar->setValue(icommand );

  //---------------------------------------------------------------------//
  //                Disable output from not locked elinks
  //---------------------------------------------------------------------//

  elink_mask=0;

  if ( !ui->elink_lked_1->isChecked() ) elink_mask += 0b1;
  if ( !ui->elink_lked_2->isChecked() ) elink_mask += 0b10;
  if ( !ui->elink_lked_3->isChecked() ) elink_mask += 0b100;
  if ( !ui->elink_lked_4->isChecked() ) elink_mask += 0b1000;
  if ( !ui->elink_lked_5->isChecked() ) elink_mask += 0b10000;
  if ( !ui->elink_lked_6->isChecked() ) elink_mask += 0b100000;
  if ( !ui->elink_lked_7->isChecked() ) elink_mask += 0b1000000;
  if ( !ui->elink_lked_8->isChecked() ) elink_mask += 0b10000000;

  send_ok = BoardConfigModule().eFIFO_disable(TARGETPORT, TargetIp,
					      elink_mask);

  icommand++;
  if ( icommand > 200 ) icommand = 200;
  ui->SetupProgressBar->setValue(icommand );

  //---------------------------------------------------------------------//

  //  ui->connectionLabel->setText("Querying TTC lock status");
  //  int TTC_lock = BoardConfigModule().Query_TTC_Lock(TARGETPORT, TargetIp);
  //  icommand++;
  //  if ( icommand > 200 ) icommand = 200;
  //  ui->SetupProgressBar->setValue(icommand );

  //  std::cout << TTC_lock << std::endl;
  
  //  miniDAQConfigHandle().miniDAQ_Settings().TTC_locked_mask = TTC_lock;

  //  std::cout << miniDAQConfigHandle().miniDAQ_Settings().TTC_locked_mask << std::endl;

  //  if ( ( miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask &
  //	 ~miniDAQConfigHandle().miniDAQ_Settings().TTC_locked_mask ) != 0 ) {
  //    sx.str("");
  //    sx << "Error: locked elink " 
  //       << QString("%1").arg( ( miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask & 
  //			       ~miniDAQConfigHandle().miniDAQ_Settings().TTC_locked_mask) , 
  //			     8, 2, QChar('0')).toStdString()
  //	 << " is not ttc locked \n";
  //    msg()(sx);
  //    PopUpError( sx.str().c_str() );
  //    return;
  //  }

  //---------------------------------------------------------------------//

  //  set TTC training to 0;
  //  send_ok = BoardConfigModule().TTC_Ctr_Training( TARGETPORT, TargetIp, 0 );
  //  if ( !send_ok ) {
  //    PopUpError( "Error: Send TTC training failed" );
  //    return;
  //  }
  icommand++;
  icommand = 200;
  ui->SetupProgressBar->setValue(icommand );
  ui->connectionLabel->setText(" MiniDAQ config complete. Next: configure FEB ASICs");

  //---------------------------------------------------------------------//

  loadMiniDAQConfig_fromTTC();

  //  std::cout << "n command sent: " << icommand << std::endl;

  return;


}
//---------------------------------------------------------//
bool MainWindow::ConfigureTTC_toDefault() {

  buildTargetIP();

  bool send_ok = true;

  // external trigger
  send_ok = BoardConfigModule().GlobalCtr_TrigMode(TARGETPORT, TargetIp, false);
  if ( !send_ok )  {
    PopUpError("Error: Failed to set trigger mode to internal, Aborting Config MiniDAQ");
    return send_ok;
  }

  // DAQ disabled
  send_ok = BoardConfigModule().GlobalCtr_DAQ_Enabled(TARGETPORT, TargetIp, false);
  if ( !send_ok)  {
    PopUpError("Error: Failed to set fill DAQ buffer to disabled, Aborting Config MiniDAQ");
    return send_ok;
  }

  // readout disabled
  send_ok = BoardConfigModule().GlobalCtr_Readout_Enabled(TARGETPORT, TargetIp, false);
  if ( !send_ok)  {
    PopUpError("Error: Failed to set readout to disabled, Aborting Config MiniDAQ");
    return send_ok;
  }

  // ttc start bit = 0
  send_ok = BoardConfigModule().GlobalCtr_TTC_StartBit(TARGETPORT, TargetIp, 0 );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set TTC start bit to 0, Aborting Config MiniDAQ");
    return send_ok;
  }

  //--------------------------------------------------------------------------//

  // ttc training to none
  //  send_ok = BoardConfigModule().TTC_Ctr_Training(TARGETPORT, TargetIp, 0 );
  //  if ( !send_ok)  {
  //    PopUpError("Error: Failed to set TTC training to 0, Aborting Config MiniDAQ");
  //    return send_ok;
  //  }

  // block trigger for 0xffff * 25 ns after triggering
  send_ok = BoardConfigModule().TTC_Ctr_Gate_nBC(TARGETPORT, TargetIp, 0x0000 );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set Gate nBC to 0x0000, Aborting Config MiniDAQ");
    return send_ok;
  }

  // trigger latency = 0
  send_ok = BoardConfigModule().TTC_Ctr_TrigLatency(TARGETPORT, TargetIp, 0 );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set trigger latency to 0, Aborting Config MiniDAQ");
    return send_ok;
  }

  // SCA reset = 0
  send_ok = BoardConfigModule().TTC_Ctr_SCA_Reset(TARGETPORT, TargetIp, false);
  if ( !send_ok)  {
    PopUpError("Error: Failed to set SCA reset bit to false, Aborting Config MiniDAQ");
    return send_ok;
  }

  // EVID reset = 0
  send_ok = BoardConfigModule().TTC_Ctr_Evt_Reset(TARGETPORT, TargetIp, false);
  if ( !send_ok)  {
    PopUpError("Error: Failed to set event readout to false, Aborting Config MiniDAQ");
    return send_ok;
  }


  // BCID reset = 0
  send_ok = BoardConfigModule().TTC_Ctr_BC_Reset(TARGETPORT, TargetIp, false);
  if ( !send_ok)  {
    PopUpError("Error: Failed to set BC reset to false, Aborting Config MiniDAQ");
    return send_ok;
  }

  //-----------------------------------------------------------------------//

  // CKTP nMax = 0
  send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp, 0 );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set training pulse frequency to 0, Aborting Config MiniDAQ");
    return send_ok;
  }


  // CKTP spacing = 0xffffff (max)
  send_ok = BoardConfigModule().VMM_TestPulse_CKTP_Spacing(TARGETPORT, TargetIp, 0xffffff );
  if ( !send_ok)  {
    PopUpError("Error: Failed to set training pulse spacing to 0xffffff, Aborting Config MiniDAQ");
    return send_ok;
  }

  return send_ok;

}

//---------------------------------------------------------//
void MainWindow::loadMiniDAQConfig_fromTTC(){

  bool send_ok = true;
  stringstream m_sx;

  buildTargetIP();

  //----------------------------------------------------------//
  //   Send a "safe" command that doesn't do anything  
  //            in this case, reset_sca = false               
  //----------------------------------------------------------//

  send_ok = BoardConfigModule().TTC_Ctr_SCA_Reset(TARGETPORT, TargetIp, false);
  //  send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp, 0 );

  if ( !send_ok ) {
    m_sx.str("");
    m_sx << "problem querying TTC register. Aborting Query \n";
    msg()(m_sx);
    return;
  }

  boost::this_thread::sleep(boost::posix_time::milliseconds(200));

  if ( !m_is_running ) {
    //    std::cout << "load from module" << std::endl;
    m_current_TTC_reg_address = BoardConfigModule().current_TTC_reg_address();
    m_current_TTC_reg_value   = BoardConfigModule().current_TTC_reg_value();
  }
  else {
    //    std::cout << "load from handle" << std::endl;
    m_current_TTC_reg_address = dataHandle().current_TTC_reg_address();
    m_current_TTC_reg_value   = dataHandle().current_TTC_reg_value();
  }

  //  std::cout << "loaded" << std::endl;

  if ( m_current_TTC_reg_address.size() == 0 ||
       m_current_TTC_reg_value.size() == 0   ||
       m_current_TTC_reg_address.size() !=
       m_current_TTC_reg_value.size() ) {
    m_sx.str("");
    m_sx << "problem query TTC register. Aborting Query \n";
    msg()(m_sx);
    return;
  }


  bool ok;
  
  for ( uint i=0; i<m_current_TTC_reg_address.size(); i++ ) {

    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Query_ELink.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_set_elink_0to3.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_set_elink_4to7.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Query_TTC_Lock.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Query_EVID.toUInt( &ok, 16 ) ) {
    }

    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Global_Reset.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Global_TrigMode.toUInt( &ok, 16 ) ) {
      // internal trigger
      if ( m_current_TTC_reg_value.at(i) == 0x7 ) {
	ui->Ext_Trigger_En->setCurrentIndex( 1 );
      } 
      // external
      else if ( m_current_TTC_reg_value.at(i) == 0x4 ) {
        ui->Ext_Trigger_En->setCurrentIndex( 0 );
      }
      else {
	m_sx.str("");
	m_sx << "current TTC trigger bit wrong: " << m_current_TTC_reg_value.at(i) << " \n";
	msg()(m_sx);
      }


    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Global_DaqEn.toUInt( &ok, 16 ) ) {
      ui->Daq_En->setCurrentIndex( m_current_TTC_reg_value.at(i) );
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Global_ReadEn.toUInt( &ok, 16 ) ) {
      ui->ReadOut_En->setCurrentIndex( m_current_TTC_reg_value.at(i) );
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TTC_startBit.toUInt( &ok, 16 ) ) {
      ui->TTC_start_bit->setValue( m_current_TTC_reg_value.at(i) );
    }

    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TTC_Training.toUInt( &ok, 16 ) ) {
      std::cout << "TTC training reg " << m_current_TTC_reg_value.at(i) << std::endl;
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_Gate_nBC    .toUInt( &ok, 16 ) ) {
      ui->Gate_nBC->setValue( m_current_TTC_reg_value.at(i) );
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TTC_TrigLatency .toUInt( &ok, 16 ) ) {
      ui->Latency_nBC->setValue( m_current_TTC_reg_value.at(i) );
    }

    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_SCA_reset.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TTC_Evt_Reset.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TTC_BC_Reset.toUInt( &ok, 16 ) ) {
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TP_CKTP_nMax.toUInt( &ok, 16 ) ) {
      ui->CKTP_NMax->setValue( m_current_TTC_reg_value.at(i) );
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TP_CKBC_nMax.toUInt( &ok, 16 ) ) {
      ui->CKBC_NMax->setValue( m_current_TTC_reg_value.at(i) );
    }
    if ( m_current_TTC_reg_address.at(i) == BoardConfigModule().reg_addr_TP_CKTP_Spacing.toUInt( &ok, 16 ) ){
      ui->CKTP_Spacing->setValue( m_current_TTC_reg_value.at(i) );
    }

  }


}

//---------------------------------------------------------//
void MainWindow::setMax(){

  if ( ui->CKTP_NMax_max == QObject::sender() ) {
    ui->CKTP_NMax->setValue(0xffff);
  }

  if ( ui->CKBC_NMax_max == QObject::sender() ) {
    ui->CKBC_NMax->setValue(0);
  }

  if ( ui->CKTP_Spacing_max == QObject::sender() ) {
    ui->CKTP_Spacing->setValue(0xffffff);
  }

  if ( ui->Gate_nBC_Max == QObject::sender() ) {
    ui->Gate_nBC->setValue(0xffff);
  }

  if ( ui->Latency_nBC_Max == QObject::sender() ) {
    ui->Latency_nBC->setValue(63);
  }

}
//---------------------------------------------------------//
void MainWindow::sendCommand() 
{

  stringstream m_sx;

  buildTargetIP();

  bool send_ok = true;

  //--------------------------------------------------------//
  //                   Query Board Info
  //--------------------------------------------------------//

  if ( ui->cmd_search_elink == QObject::sender() ) {
    int nlinks = QueryAllELinks();
    if ( nlinks == 0 ) {
      PopUpError("No Elinks Detected:  Query Elink Error", "please check FEB to miniDAQ connection and try again");
    }
    else {
      PopUpInfo("Scan elink phase successful.  Elinks locked are displayed and elink phase matrix can be checked.");
    }
  }

  if ( ui->cmd_query_elink_locked == QObject::sender() ) {
    std::vector<bool> locked_elinks = BoardConfigModule().QueryELink(TARGETPORT, TargetIp, 8, true);
  }

  if ( ui->cmd_query_EVID == QObject::sender() ) {
    int EVID = BoardConfigModule().Query_EVID(TARGETPORT, TargetIp);
    
    if ( m_is_running ) {
      boost::this_thread::sleep(boost::posix_time::milliseconds(200));
      EVID = dataHandle().current_EVID();
    }

    m_sx.str("");
    if ( EVID < 0 ) {
      m_sx << "Error: Query EVID failed \n" ;
      PopUpError(m_sx.str().c_str());
    }
    else            m_sx << "Query EVID: Current EVID = " << EVID << " \n";
    msg()(m_sx);
    
    m_sx.str("");
    m_sx << "EVID = " << EVID ;
    ui->QueryOutput->setText( QString::fromStdString(m_sx.str()) );
  }

  if ( ui->QueryCurrentTTCRegVal == QObject::sender() ) {
    loadMiniDAQConfig_fromTTC();
    PopUpInfo("GUI display set to current TTC register values");
  }

  if ( ui->cmd_elink_fifo_disable == QObject::sender() ) {

    uint32_t elink_mask=0;
    
    if ( ui->elink_lked_1->isChecked() ) elink_mask += 0b1;
    if ( ui->elink_lked_2->isChecked() ) elink_mask += 0b10;
    if ( ui->elink_lked_3->isChecked() ) elink_mask += 0b100;
    if ( ui->elink_lked_4->isChecked() ) elink_mask += 0b1000;
    if ( ui->elink_lked_5->isChecked() ) elink_mask += 0b10000;
    if ( ui->elink_lked_6->isChecked() ) elink_mask += 0b100000;
    if ( ui->elink_lked_7->isChecked() ) elink_mask += 0b1000000;
    if ( ui->elink_lked_8->isChecked() ) elink_mask += 0b10000000;

    send_ok = BoardConfigModule().eFIFO_disable(TARGETPORT, TargetIp,
						elink_mask);

    if ( send_ok ) {
      QString Board_mask = QString("%1").arg( (elink_mask & 0b11111111),
					      8, 2, QChar('0'));
      
      
      m_sx.str("");
      m_sx << "FIFO successfully disabled for elinks " << Board_mask.toStdString() ;
      PopUpInfo( m_sx.str().c_str() );
    }
    else {
      m_sx.str("");
      m_sx << "FIFO disabled command failed to send";
      PopUpError( m_sx.str().c_str() );

    }
  }

  if ( ui->ManualSetELink == QObject::sender() ) {
    SetManualELinkPhase();
    m_sx.str("");
    m_sx << "Manual eLinks phases successfully set: Current Elink Phases and locked boards are displayed in GUI ";
    PopUpInfo( m_sx.str().c_str() );
  }

  //--------------------------------------------------------//
  //                    global commands 
  //--------------------------------------------------------//

  if ( ui->cmd_FPGA_reset == QObject::sender() )  {
    send_ok = BoardConfigModule().GlobalCtr_KC705Reset(TARGETPORT, TargetIp);
    if ( !send_ok ) {
      PopUpError("Error: KC705 Reset failed");
    }
    else {
      PopUpInfo("KC705 Reset Successful");
    }
  }

  //--------------------------------------------------------//

  if ( ui->cmd_Ext_Trigger== QObject::sender() )  {
    if      ( ui->Ext_Trigger_En->currentIndex() == 0 ) { // external trigger
      send_ok = BoardConfigModule().GlobalCtr_TrigMode(TARGETPORT, TargetIp, false);
    }
    else if ( ui->Ext_Trigger_En->currentIndex() == 1 ) { // internal trigger
      send_ok = BoardConfigModule().GlobalCtr_TrigMode(TARGETPORT, TargetIp, true);
    }
    if ( !send_ok ) {
      PopUpError("Error: error during configuring trigger mode");
    }
    else {
      if ( ui->Ext_Trigger_En->currentIndex() == 0 ) PopUpInfo("Using External Trigger");
      else                                           PopUpInfo("Using Internal Trigger");
    }

  }

  //--------------------------------------------------------//

  if ( ui->cmd_DAQ_Enable== QObject::sender() )  {
    if ( ui->Daq_En->currentIndex() == 0 ) { // disable
      send_ok = BoardConfigModule().GlobalCtr_DAQ_Enabled(TARGETPORT, TargetIp, false);
    }
    else if ( ui->Daq_En->currentIndex() == 1) { // enable
      send_ok = BoardConfigModule().GlobalCtr_DAQ_Enabled(TARGETPORT, TargetIp, true);
    }
    if ( !send_ok ) {
      PopUpError("Error: error during enable DAQ");
    }
    else {
      if ( ui->Daq_En->currentIndex() == 0 ) PopUpInfo("DAQ successfully disabled");
      else                                   PopUpInfo("DAQ successfully enabled");
    }
  }

  //--------------------------------------------------------//
  
  if ( ui->cmd_ReadOut_Enable== QObject::sender() )  {

    if ( ui->ReadOut_En->currentIndex() == 0 ) {  // disable
      send_ok = BoardConfigModule().GlobalCtr_Readout_Enabled(TARGETPORT, TargetIp, false);
    }
    else if ( ui->ReadOut_En->currentIndex() == 1 ) {  //enable
      send_ok = BoardConfigModule().GlobalCtr_Readout_Enabled(TARGETPORT, TargetIp, true);
    }
    if ( !send_ok ) {
      PopUpError("Error: error during enable readout ");
    }
    else {
      if ( ui->ReadOut_En->currentIndex() == 0 ) PopUpInfo("Readout successfully disabled");
      else                                       PopUpInfo("Readout successfully enabled");
    }
  }

  //--------------------------------------------------------//

  if ( ui->cmd_TTC_start == QObject::sender() )  {
    send_ok = BoardConfigModule().GlobalCtr_TTC_StartBit(TARGETPORT, TargetIp,
                                                         ui->TTC_start_bit->value());
    if ( !send_ok ) {
      PopUpError("Error: error during configuring TTC Start Bit ");
    }
    else {
      m_sx.str("");
      m_sx << "TTC start bit successfully set to " << ui->TTC_start_bit->value() ;
      PopUpInfo( m_sx.str().c_str() );
    }
  }

  //--------------------------------------------------------//
  //                     TTC commands
  //--------------------------------------------------------//

  if ( ui->cmd_soft_reset_low == QObject::sender() ) {

    for ( uint i = 0; i < elink_lked.size(); i++ ) {

      if ( elink_lked.at(i)->isChecked() ) {
	bool send_ok_i = BoardConfigModule().FEB_Soft_Reset_GPIO_Low(TARGETPORT, TargetIp, 
					     ui->cmd_boardType_2->currentIndex(), i);
	if ( !send_ok_i ) {
	  m_sx.str("");
	  m_sx << "Error: Board " << i << " FEB sync soft reset failed, Aborting \n ";
	  msg()(m_sx);
	  send_ok = false;
	  PopUpError(m_sx.str().c_str());
	  break;
	}
      }	  

    }

  }

  if ( ui->cmd_soft_reset_high == QObject::sender() ) {

    for ( uint i = 0; i < elink_lked.size(); i++ ) {

      if ( elink_lked.at(i)->isChecked() ) {
        bool send_ok_i = BoardConfigModule().FEB_Soft_Reset_GPIO_High(TARGETPORT, TargetIp,
								      ui->cmd_boardType_2->currentIndex(), i);
        if ( !send_ok_i ) {
          m_sx.str("");
          m_sx << "Error: Board " << i << " FEB GPIO reset post soft reset failed, Aborting \n ";
          msg()(m_sx);
          send_ok = false;
          PopUpError(m_sx.str().c_str());
          break;
        }
      }

    }

    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

  }

  if ( ui->cmd_Gate_nBC == QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_Gate_nBC(TARGETPORT, TargetIp,
                                                   ui->Gate_nBC->value());
    if ( !send_ok ) {
      PopUpError("Error: Configuring Gate nBCID failed");
    }
    else {
      PopUpInfo( "Gate nBCID successfully configured" );
    }
  }

  if ( ui->cmd_latency_setup== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_TrigLatency(TARGETPORT, TargetIp,
                                                      ui->Latency_nBC->value());
    if ( !send_ok ) {
      PopUpError("Error: Configuring Trigger Latency failed");
    }
    else {
      PopUpInfo("Trigger latency successfully set");
    }

  }

  if ( ui->cmd_SCA_Reset== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_SCA_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: SCA Reset failed");
    }
    else {
      PopUpInfo("SCA Reset successful");
    }

  }

  if ( ui->cmd_EVID_Reset== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_Evt_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: Event ID Reset failed");
    }
    else {
      PopUpInfo("EVID reset successful");
    }
  }

  if ( ui->cmd_BC_Reset== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_BC_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: BC Reset failed");
    }
    else {
      PopUpInfo("BCID reset successful");
    }

  }


  if ( ui->cmd_L0_Evt_Reset== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_L0_Evt_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: L0 EVID Reset failed");
    }
    else {
      PopUpInfo("L0 EVID reset successful");
    }

  }

  if ( ui->cmd_L0_Accept== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_L0_Accept_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: L0 Accept Reset failed");
    }
    else {
      PopUpInfo("L0 Accept reset successful");
    }

  }

  if ( ui->cmd_L1_Accept== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_L1_Accept_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: L1 Accept Reset failed");
    }
    else {
      PopUpInfo("L1 Accept reset successful");
    }

  }

  if ( ui->cmd_VMM_Soft_Reset== QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_VMM_Soft_Reset(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: VMM Soft Reset failed");
    }
    else {
      PopUpInfo("VMM Soft Reset successful");
    }

  }

  if ( ui->cmd_VMM_TP == QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_VMM_TP(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: VMM TP failed");
    }
    else {
      PopUpInfo("VMM TP successful");
    }

  }

  if ( ui->cmd_TTC_pulse_lv == QObject::sender() )  {
    send_ok = BoardConfigModule().TTC_Ctr_TTC_pulse_lv(TARGETPORT, TargetIp, ui->cmd_TTC_pulse_lv_val->value());

    if ( !send_ok ) {
      PopUpError("Error: TTC pulse lv set failed");
    }
    else {
      PopUpInfo("TTC pulse lv set successful");
    }

  }

  if ( ui->cmd_set_SCA_GPIO == QObject::sender() ) {

    uint32_t dir_mask  = 0;
    uint32_t dout_mask = 0;
    for ( int i=0; i<32; i++ ) {
      dir_mask  += ( GPIO_dir_chan_enable_val.at(i) << i );
      dout_mask += ( GPIO_dout_chan_enable_val.at(i) << i );
    }

    send_ok = BoardConfigModule().Set_SCA_GPIO( TARGETPORT, TargetIp,
						ui->FE_Board_Type->currentIndex(), ui->FE_Board_ID->value(),
						dir_mask, dout_mask );

    if ( !send_ok ) {
      PopUpError("Error: set SCA GPIO failed");
    }
    else {
      PopUpInfo("set SCA GPIO successful");
    }

  }

  //--------------------------------------------------------//
  //                VMM Test Pulse Commands
  //--------------------------------------------------------//

  if ( ui->cmd_CKTP_NMax == QObject::sender() ) {
    send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp,
							  ui->CKTP_NMax->value());
    if ( !send_ok ) {
      PopUpError("Error: Configuring CKTP nMax failed");
    }
    else {
      PopUpInfo("CKTP nMax config successful");
    }

  }

  if ( ui->cmd_CKBC_NMax == QObject::sender() ) {
    send_ok = BoardConfigModule().VMM_TestPulse_CKBC_nMax(TARGETPORT, TargetIp,
                                                          ui->CKBC_NMax->value());

    if ( !send_ok ) {
      PopUpError("Error: Configuring CKBC nMax failed");
    }
    else {
      PopUpInfo("CKBC nMax config successful");
    }

  }

  if ( ui->cmd_CKTP_Spacing == QObject::sender() ) {
    send_ok = BoardConfigModule().VMM_TestPulse_CKTP_Spacing(TARGETPORT, TargetIp,
							     ui->CKTP_Spacing->value());

    if ( !send_ok ) {
      PopUpError("Error: Configuring CKTP spacing failed");
    }
    else {
      PopUpInfo("CKTP Spacing config successful");
    }

  }

  //--------------------------------------------------------//

  if ( ui->cmd_TTC_F1 == QObject::sender() ) {
    send_ok = BoardConfigModule().TTC_F1(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: Sending TTC F1 failed");
    }
    else {
      PopUpInfo("Sending TTC F1 successful");
    }

  }

  if ( ui->cmd_TTC_F2 == QObject::sender() ) {
    send_ok = BoardConfigModule().TTC_F2(TARGETPORT, TargetIp);

    if ( !send_ok ) {
      PopUpError("Error: Sending TTC F2 failed");
    }
    else {
      PopUpInfo("Sending TTC F2 successful");
    }

  }


  if ( ui->cmd_SCA_GO == QObject::sender() )  {

    vector<uint32_t> adc_val = BoardConfigModule().Query_SCA_ADC(TARGETPORT, TargetIp, ui->sca_boardType->currentIndex(),
								 ui->cmd_SCA_ADC_board_ID->value(), ui->cmd_SCA_ADC_chan_mux->value(), 
								 ui->cmd_SCA_ADC_chan_cur->value());
    
    if ( find_avg(adc_val) == 0xffff ) {
      send_ok = false;
      PopUpError("Error: Query SCA ADC failed");
    }
    else {
      
      send_ok = true;

      m_sx.str("");
      m_sx << "Board " << ui->cmd_SCA_ADC_board_ID->value() << " ADC ch " << ui->cmd_SCA_ADC_chan_mux->value() << " ADC val " << find_avg(adc_val) ;
      
      QString sstr = QString::fromStdString( m_sx.str() );

      ui->cmd_SCA_ADC_value->setText( sstr );
      
      PopUpInfo("Query SCA ADC successful");
    }
    
  }

  m_reply_error = send_ok;

}
void MainWindow::Sync_Soft_Reset() {

  //----------------------------------------------------//
  //     set all SCA -> VMM Ena GPIOs to low
  //----------------------------------------------------//
  
  prepareAndSendConfigDir(false, false, true, false, false, false);

  //------------------------------------------------------------------------//
  //             Apply Synchronous Reset Signal via TTC module              //
  //------------------------------------------------------------------------//

  BoardConfigModule().TTC_Ctr_VMM_Soft_Reset(TARGETPORT, TargetIp);

  //----------------------------------------------------//
  //     set all SCA -> VMM Ena GPIOs to low              
  //----------------------------------------------------//
  prepareAndSendConfigDir(false, false, false, true, false, false);

}
// ------------------------------------------------------------------------- //
int MainWindow::QueryAllELinks( bool isSetup )
{

  stringstream m_sx;
  
  miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask = 0; 

  int nLinks = 8;
  
  std::vector<std::vector<bool>> LinkOffsets;
  std::vector<int> ELink_BestLockLoc;
  LinkOffsets.resize(0);
  ELink_BestLockLoc.resize(0);

  std::vector<int> AllBad = {-1,-1,-1,-1,-1,-1,-1,-1};

  int nLinksLocked=0;

  bool send_ok = true;

  //------------------------------------------------------------//
  //             Scan ELink phase from 0 - 63 for all links
  //------------------------------------------------------------//
  for ( int i=0; i<64; i++ ) {

    std::vector<int> temp_i = {i,i,i,i};

    //  Send elink delay for all links
    send_ok = BoardConfigModule().setELinkDelay0to3(TARGETPORT, TargetIp, temp_i);
    if ( !send_ok ) { 
      msg()("Set Elink Delay Error"); 
      DisplayLinkLockedStatus(AllBad);
      return 0; 
    }
    if ( isSetup ) {
      icommand++;
      if ( icommand > 200 ) icommand = 200;
      ui->SetupProgressBar->setValue( icommand );
    }

    send_ok = BoardConfigModule().setELinkDelay4to7(TARGETPORT, TargetIp, temp_i);
    if ( !send_ok ) { 
      msg()("Set Elink Delay Error"); 
      DisplayLinkLockedStatus(AllBad);
      return 0; 
    }
    if ( isSetup ) {
      icommand++;
      if ( icommand > 200 ) icommand = 200;
      ui->SetupProgressBar->setValue( icommand );
    }

    //  Query Link to find good links
    std::vector<bool> temp = BoardConfigModule().QueryELink(TARGETPORT, TargetIp, nLinks);
    if ( temp.size() == 0 ) {
      msg()("Query Elink failed");
      DisplayLinkLockedStatus(AllBad);
      return 0;
    }
    if ( isSetup ) {
      icommand++;
      if ( icommand > 200 ) icommand = 200;
      ui->SetupProgressBar->setValue( icommand );
    }

    LinkOffsets.push_back(temp);
  }

  miniDAQConfigHandle().miniDAQ_Settings().elink_matrix = LinkOffsets;

  //-----------------------------------------------------------//
  //   Set Link delay to best found positions for each link
  //-----------------------------------------------------------//

  ELink_BestLockLoc = CheckLinkLocked(LinkOffsets, nLinksLocked);

  int elink_mask = 0;
  for ( uint i=0; i<ELink_BestLockLoc.size(); i++ ) {
    if ( ELink_BestLockLoc.at(i) >= 0 ) { elink_mask += ( 0b1 << i ); }
  }

  miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask = elink_mask;

  //----------------------------------------------------------//

  send_ok = BoardConfigModule().SetFinalELinkDelay(TARGETPORT, TargetIp, ELink_BestLockLoc);
  if ( !send_ok ) {
    msg()("Set Elink Delay Error");
    DisplayLinkLockedStatus(AllBad);
    return 0;
  }
  if ( isSetup ) {
    icommand++;
    if ( icommand > 200 ) icommand = 200;
    ui->SetupProgressBar->setValue( icommand );
  }

  //----------------------------------------------------//
  //   Check the correct elinks are locked
  //----------------------------------------------------//

  for ( int j=0; j<3; j++ ) {
    std::vector<bool> Final_QueryELink = BoardConfigModule().QueryELink(TARGETPORT, TargetIp, nLinks);
    
    if ( ELink_BestLockLoc.size() != Final_QueryELink.size() ) {
      msg()("Final Elink Query Error: Retry Query");
      DisplayLinkLockedStatus(AllBad);
      continue;
    }
    if ( isSetup ) {
      icommand++;
      if ( icommand > 200 ) icommand = 200;
      ui->SetupProgressBar->setValue( icommand );
    }

    //int nLinkLocked = 0;
    
    for ( uint i=0; i<Final_QueryELink.size(); i++ ) {
      if ( Final_QueryELink.at(i) && ELink_BestLockLoc.at(i) == -1 ) {
	m_sx.str("");
	if ( i < 2 ) m_sx << "eLink " << i << " no longer locked at previous best-lock location: retrying query";
	if ( i == 2 ) m_sx << "eLink " << i << " no longer locked at previous best-lock location: please re-scan";
	msg()(m_sx);
	DisplayLinkLockedStatus(AllBad);
	if ( i < 2 ) break;
	if ( i == 2 ) return 0;
      }
      else if ( ELink_BestLockLoc.at(i) != -1 && !Final_QueryELink.at(i) ) {
	m_sx.str("");
        if ( i < 2 ) m_sx << "eLink " << i << " no longer locked at previous best-lock location: retrying query";
	if ( i == 2 ) m_sx << "eLink " << i << " no longer locked at previous best-lock location: please re-scan";
        msg()(m_sx);
        DisplayLinkLockedStatus(AllBad);
        if ( i < 2 ) break;
	if ( i == 2 ) return 0;
      }
    }
    
    // if the same links are locked as found in the scan, we are good
    break;
  }


  DisplayLinkLockedStatus(ELink_BestLockLoc);

  return nLinksLocked;
}
//-----------------------------------------------------------------------//
void MainWindow::SetManualELinkPhase()
{

  bool send_ok = true;

  std::vector<int> ELink_BestLockLoc;
  std::vector<int> AllBad;
  ELink_BestLockLoc.resize(0);
  AllBad.resize(0);

  for ( uint i=0; i<ELink_Manual_Phase.size(); i++ ) {
    ELink_BestLockLoc.push_back( ELink_Manual_Phase.at(i)->value() );
    AllBad.push_back(-1);
  }    

  send_ok = BoardConfigModule().SetFinalELinkDelay(TARGETPORT, TargetIp, ELink_BestLockLoc);
  if ( !send_ok ) {
    msg()("Set Elink Delay Error");
    DisplayLinkLockedStatus(AllBad);
    return;
  }

  std::vector<bool> locked_elinks = BoardConfigModule().QueryELink(TARGETPORT, TargetIp, 8, true);
  if ( locked_elinks.size() == 0 ) {
    msg()("Query Elink Delay Error");
    DisplayLinkLockedStatus(AllBad);
    return;
  }

  for ( uint i=0; i< locked_elinks.size(); i++ ) {
    if ( !locked_elinks.at(i) ) ELink_BestLockLoc.at(i) = -1;
  }

  DisplayLinkLockedStatus( ELink_BestLockLoc );

  return;

}

//-----------------------------------------------------------------------//
//   Make sure all Channels are Locked
//   Keep Searchig until all links have been locked and then
//   either unlocked.  This way we know all avaible settings for a lock
//-----------------------------------------------------------------------//
std::vector<int> MainWindow::CheckLinkLocked( std::vector< std::vector<bool> > LinkOffsets, int & nlinks ) 
{

  stringstream m_sx;

  std::vector<bool> LinkLocked;
  LinkLocked.resize(0);

  std::vector<std::vector<uint>> LinkLockedLocation;

  for ( uint j=0; j< LinkOffsets.at(0).size(); j++ ) {
    LinkLocked.push_back(false);
  }

  //-----------------------------------------------------------//
  //                  for testing purposes                     //
  //-----------------------------------------------------------//

  /*
  LinkOffsets.resize(0);

  for ( int i=0; i<24; i++ ) {

    std::vector<bool> test1 = { 1,1,1,1,1,1,1,1 };
    std::vector<bool> test0 = { 0,0,0,0,0,0,0,0 };
    
    if ( i == 2 || i == 3 || i == 4 || i == 15 || i == 20 || i == 21 ) {
      LinkOffsets.push_back(test0);
    }
    else          LinkOffsets.push_back(test1);    
  }
  */

  //-----------------------------------------------------------//
  // "j" is the number of links
  // "i" is the number of offset checked
  //-----------------------------------------------------------//

  m_sx.str("");
  m_sx << "// ----------------------------------------------------------------------------------------------//";
  msg() ( m_sx );

  m_sx.str("");
  m_sx << "ELink Phase Matrix  ";
  msg() ( m_sx );

  //-------------------------------------//
  //         loop over j links
  //-------------------------------------//
  for ( uint j=0; j<LinkOffsets.at(0).size(); j++ ) {

    m_sx.str("");
    m_sx << "board " << j << " phases locked: ";

    int n_blocks = 0;
    bool previous_phase_locked = false;

    std::vector<uint> currentLinkLocked;
    std::vector<uint> longestLinkLocked;
    currentLinkLocked.resize(0);
    longestLinkLocked.resize(0);

    //------------------------------------------//
    //         loop over first 32 phases
    //------------------------------------------//
    for ( uint i=0; i < 32; i++ ) {

      if ( LinkOffsets.size() <= i ) continue;

      if ( LinkOffsets.at(i).at(j) ) {
	m_sx << 1 << " " ;

	currentLinkLocked.push_back(i);

	// if its last phase and it is locked then consider this a block
	if ( i == 31 ) {
	  if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
	    longestLinkLocked = currentLinkLocked;
	    currentLinkLocked.resize(0);
	  }
	  else {
	    currentLinkLocked.resize(0);
	  }
	}

	// if the previous phase is not locked then this is the start of a new block of locked phases
	if ( !previous_phase_locked ) {
	  n_blocks++;
	}

	previous_phase_locked = true;

      }
      else {
	m_sx << 0 << " ";

	//---------------------------------------//
	//  end of new patch of locked phases
	//---------------------------------------//

	if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
	  longestLinkLocked = currentLinkLocked;
	  currentLinkLocked.resize(0);
	}
	else {
	  currentLinkLocked.resize(0);
	}
	
	previous_phase_locked = false;
      }

    } // end of loop over 32 phases

    //-----------------------------------//
    //     loop over last 32 phases
    //-----------------------------------//

    for ( uint i=32; i < LinkOffsets.size(); i++ ) {

      if ( LinkOffsets.at(i).at(j) ) {
        m_sx << 1 << " " ;

        currentLinkLocked.push_back(i);

        // if its last phase and it is locked then consider this a block                                                                       
        if ( i == LinkOffsets.size() - 1 ) {
          if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
            longestLinkLocked = currentLinkLocked;
            currentLinkLocked.resize(0);
          }
          else {
            currentLinkLocked.resize(0);
          }
        }

        // if the previous phase is not locked then this is the start of a new block of locked phases                                          
        if ( !previous_phase_locked ) {
          n_blocks++;
        }

        previous_phase_locked = true;

      }
      else {
        m_sx << 0 << " ";

        //---------------------------------------//
        //  end of new patch of locked phases      
        //---------------------------------------//
        if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
          longestLinkLocked = currentLinkLocked;
          currentLinkLocked.resize(0);
        }
        else {
          currentLinkLocked.resize(0);
        }

        previous_phase_locked = false;
      }

    } // end of loop over last 32 phases 

    //------------------------------------------------------//

    // push back longest link locked segment
    LinkLockedLocation.push_back( longestLinkLocked );
    // output line in phase matrix
    msg()(m_sx);

  } // end of loop over j links

  m_sx.str("");
  m_sx << "// ---------------------------------------------------------------------------------------------- //";
  msg() ( m_sx );

  //------------------------------------------------------------------------------//
  //     Pick out the best phase from the longest continous locked phase block
  //------------------------------------------------------------------------------//

  std::vector<int> Elink_bestLockLoc;
  Elink_bestLockLoc.resize(0);
  nlinks = 0;

  m_sx.str("");
  m_sx << " Best Elink phases: ";

  for ( uint j=0; j<LinkLockedLocation.size(); j++ ) {
    std::vector<uint> linkLoc_j = LinkLockedLocation.at(j);

    // eLink j is unlocked
    if ( linkLoc_j.size() == 0 ) {
      Elink_bestLockLoc.push_back(-1);
      m_sx << "NA ";
    }
    else {
      Elink_bestLockLoc.push_back( linkLoc_j.at( linkLoc_j.size()/2 ) ); // take the middle index
      nlinks++;
      m_sx << linkLoc_j.at( linkLoc_j.size()/2 ) << " ";
    }
  }

  m_sx << "\n";
  msg()(m_sx);

  return Elink_bestLockLoc;
}

//-----------------------------------------------------------------------//

void MainWindow::DisplayLinkLockedStatus(std::vector<int> best_delay){

  stringstream sx;
  std::vector<QString> Link_status_strs;
  Link_status_strs.resize(0);

  for ( uint i=0; i < best_delay.size(); i++ ) {
    sx.str("");
    if ( best_delay.at(i) >= 0 ) {
      sx << "Link locked. Delay: " << best_delay.at(i);
      ELink_Manual_Phase.at(i)->setValue(best_delay.at(i));
    }
    else {
      sx << "Link not locked";
      ELink_Manual_Phase.at(i)->setValue(0);
    }
    Link_status_strs.push_back( QString::fromStdString(sx.str()) );
  }

  for ( int i=Link_status_strs.size(); i<8; i++ ){
    Link_status_strs.push_back(QString::fromStdString("Link not locked"));
    ELink_Manual_Phase.at(i)->setValue(0);
  }
  
  for ( uint i=0; i<Link_status_strs.size(); i++ ) {
    ELink_stat.at(i)->setText( Link_status_strs.at(i) );
  }

  //------------------------------------------------//

  uint32_t elink_mask = 0;

  for ( uint i=0; i<best_delay.size(); i++ ) {

    if ( best_delay.at(i) >= 0 ) {
      BoardID_Linked.at(i)->setChecked(true);
      BoardID_Linked.at(i)->setEnabled(true);
      elink_lked.at(i)->setChecked(true);
      elink_lked.at(i)->setEnabled(true);
      elink_mask += ( 0b1 << i );
    }
    else {
      BoardID_Linked.at(i)->setChecked(false);
      //      BoardID_Linked.at(i)->setEnabled(false);
      elink_lked.at(i)->setChecked(false);
      //      elink_lked.at(i)->setEnabled(false);
    }

  }

  miniDAQConfigHandle().miniDAQ_Settings().elink_locked_mask = elink_mask;


  //------------------------------------------------//

}

// ------------------------------------------------------------------------- //
void MainWindow::selectDirConfig()
{
  stringstream sx;
  //  QFileDialog dialog;
  // dialog.setFileMode(QFileDialog::Directory);

  QString filename = QFileDialog::getExistingDirectory(this,
						       tr("Load board configuration XML"), "../readout_configuration/" );

  if(filename.isNull()) return;

  QDir ftest(filename);
  //ftest.setFileName(filename);                                                                                                                           
  if(!ftest.exists()) {
    //if(!ftest.open(QIODevice::ReadWrite)) {                                                                                                              
    sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
    msg()(sx);
    PopUpError(sx.str().c_str()); sx.str("");

    //ui->configXmlDirectoryField->setText("");

    ui->configXmlDirectoryField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
    //delay();                                                                                                                                         
    //delay();                                                                                                                                          
    //ui->configXmlDirectoryField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->configXmlDirectoryField->setText(filename);
    sx << "Setting configuration file to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->configXmlDirectoryField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
}
// ------------------------------------------------------------------------- //
void MainWindow::selectOutputDir()
{

  stringstream sx;

  QString filename = QFileDialog::getExistingDirectory(this,
                                                       tr("Load output directory"), "../output/" );

  if(filename.isNull()) return;

  QDir ftest(filename);

  if(!ftest.exists()) {

    sx << "output directory (" << filename.toStdString() << ") unable to be opened";
    msg()(sx);
    PopUpError(sx.str().c_str()); sx.str("");
    //ui->outputDirField->setText("");

    ui->outputDirField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
    //delay();
    //delay();
    //ui->outputDirField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->outputDirField->setText(filename);
    sx << "Setting output directory to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->outputDirField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");

  }

}
// ------------------------------------------------------------------------- //
void MainWindow::selectVMMConfigXMLFile()
{
  stringstream sx;
  QString filename = QFileDialog::getOpenFileName(this,
						  tr("Load board configuration XML"), "../readout_configuration/",
						  tr("XML Files (*.xml)"));
  if(filename.isNull()) return;

  QFile ftest(filename);
  //ftest.setFileName(filename);
  if(!ftest.exists()) {
    //if(!ftest.open(QIODevice::ReadWrite)) {
    sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
    msg()(sx); 
    PopUpError(sx.str().c_str()); sx.str("");
    //ui->configVMMXmlFilenameField->setText("");

    ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }"); 
    //delay();
    //delay();
    //ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->configVMMXmlFilenameField->setText(filename);
    sx << "Setting configuration file to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->load_config_vmm->setEnabled(true);
    ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");

  }
}
//----------------------------------------------------//

void MainWindow::selectRocASICConfigXMLFile()
{
  stringstream sx;
  QString filename = QFileDialog::getOpenFileName(this,
                                                  tr("Load roc asic configuration XML"), "../readout_configuration/",
                                                  tr("XML Files (*.xml)"));
  if(filename.isNull()) return;

  QFile ftest(filename);
  if(!ftest.exists()) {
    sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
    msg()(sx);
    PopUpError(sx.str().c_str()); sx.str("");

    ui->configRocASICXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->configRocASICXmlFilenameField->setText(filename);
    sx << "Setting configuration file to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->load_config_roc_asic->setEnabled(true);
    ui->configRocASICXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
}

//----------------------------------------------------//                                                                                                   
void MainWindow::selectBoardConfigXMLFile()
{
  stringstream sx;
  QString filename = QFileDialog::getOpenFileName(this,
                                                  tr("Load board configuration XML"), "../readout_configuration/",
                                                  tr("XML Files (*.xml)"));
  if(filename.isNull()) return;

  QFile ftest(filename);
  if(!ftest.exists()) {
    sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
    msg()(sx); 
    PopUpError(sx.str().c_str()); sx.str("");
    //ui->configBoardXmlFilenameField->setText("");

    ui->configBoardXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
    //delay();
    //delay();
    //ui->configBoardXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->configBoardXmlFilenameField->setText(filename);
    sx << "Setting configuration file to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->load_config_board->setEnabled(true);
    ui->configBoardXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");

  }
}

//----------------------------------------------------//
void MainWindow::selectTDSConfigXMLFile()
{
  stringstream sx;
  QString filename = QFileDialog::getOpenFileName(this,
                                                  tr("Load board configuration XML"), "../readout_configuration/",
                                                  tr("XML Files (*.xml)"));
  if(filename.isNull()) return;

  QFile ftest(filename);
  //ftest.setFileName(filename);                                                                                                                                     
  if(!ftest.exists()) {
    //if(!ftest.open(QIODevice::ReadWrite)) {                                                                                                                        
    sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
    msg()(sx);
    PopUpError(sx.str().c_str()); sx.str("");
    //ui->configTDSXmlFilenameField->setText("");

    ui->configTDSXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
    //delay();
    //delay();
    //ui->configTDSXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->configTDSXmlFilenameField->setText(filename);
    sx << "Setting configuration file to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->load_config_tds->setEnabled(true);
    ui->configTDSXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");

  }
}
//----------------------------------------------------// 
void MainWindow::selectRocFPGAConfigXMLFile()
{
  stringstream sx;
  QString filename = QFileDialog::getOpenFileName(this,
                                                  tr("Load ROC FPGA configuration XML"), "../readout_configuration/",
                                                  tr("XML Files (*.xml)"));
  if(filename.isNull()) return;

  QFile ftest(filename);
  //ftest.setFileName(filename);             
  if(!ftest.exists()) {
    //if(!ftest.open(QIODevice::ReadWrite)) {
    sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
    msg()(sx);
    PopUpError(sx.str().c_str()); sx.str("");
    //ui->configRocFPGAXmlFilenameField->setText("");

    ui->configRocFPGAXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); selection-background-color: rgb(233, 99, 0); }");
    //delay();
    //delay();
    //ui->configRocFPGAXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");
  }
  else {
    ui->configRocFPGAXmlFilenameField->setText(filename);
    sx << "Setting configuration file to:\n    " << filename.toStdString();
    msg()(sx); sx.str("");
    ui->load_config_ROC_FPGA->setEnabled(true);
    ui->configRocFPGAXmlFilenameField->setStyleSheet("QLineEdit { background: white; selection-background-color: rgb(233, 99, 0); }");

  }
}


// ------------------------------------------------------------------------- //
void MainWindow::loadCurrentConfigBoard()
{
  stringstream m_sx;

  bool ok = false;
  if(ui->current_Board_Config->count() != 0) {

    QString Board_str = ui->current_Board_Config->currentText();

    QString boardID = Board_str.mid(6,1);

    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << boardID.toStdString() ;
    m_sx << "_current_tmp.xml";

    ok = BoardConfigHandle().LoadBoardConfig_from_File(QString::fromStdString(m_sx.str()));

    if(ok) {
      msg()("Configuration loaded successfully");
      LoadBoardConfig_from_Handler();
    }
    else {
      msg()("ERROR: Configuration unable to be loaded from requested file");
      PopUpError("ERROR: Configuration unable to be loaded");
    }
  }
  else {
    msg()("ERROR: No Boards Currently Configured");
    PopUpError("ERROR: No Boards currently configured");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadCurrentConfigVMM()
{

  stringstream m_sx;

  bool ok = false;
  if(ui->current_VMM_Config->count() != 0) {

    QString VMM_str = ui->current_VMM_Config->currentText();

    QString boardID = VMM_str.mid(6,1);
    QString vmmMask  = VMM_str.mid(12);

    //decode VMM_str to board ID and vmmMask

    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << boardID.toStdString() ;
    m_sx << "_vmm_" << vmmMask.toStdString() << "_current_tmp.xml";

    ok = VMMConfigHandle().LoadVMMConfig_from_File(QString::fromStdString(m_sx.str()));
    
    if(ok) {
      msg()("Configuration loaded successfully");
      LoadVMMConfig_from_Handler();
    }
    else {
      msg()("ERROR Configuration unable to be loaded from requested file");
      PopUpError("ERROR: Configuration unable to be loaded");
    }
  }
  else {
    msg()("ERROR: No VMM currently configured");
    PopUpError("ERROR: No VMM currently configured");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadCurrentConfigTDS()
{

  stringstream m_sx;
  bool ok = false;
  if(ui->current_TDS_Config->count() != 0) {

    QString TDS_str = ui->current_TDS_Config->currentText();

    QString boardID = TDS_str.mid(6,1);
    QString tdsMask = TDS_str.mid(12);

    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << boardID.toStdString() ;
    m_sx << "_tds_" << tdsMask.toStdString() << "_current_tmp.xml";

    ok = TDSConfigHandle().LoadTDSConfig_from_File(QString::fromStdString(m_sx.str()));

    if(ok) {
      msg()("Configuration loaded successfully");
      LoadTDSConfig_from_Handler();
    }
    else {
      msg()("ERROR: Configuration unable to be loaded from requested file");
      PopUpError("ERROR: Configuration unable to be loaded");
    }
  }
  else {
    msg()("ERROR: No TDS currently configured");
    PopUpError("ERROR: No TDS currently configured");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadCurrentConfigRocFPGA()
{

  stringstream m_sx;
  bool ok = false;
  if(ui->current_ROC_FPGA_Config->count() != 0) {

    QString ROC_str = ui->current_ROC_FPGA_Config->currentText();

    QString boardID = ROC_str.mid(6,1);

    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << boardID.toStdString() ;
    m_sx << "_roc_current_tmp.xml";

    ok = RocFPGAConfigHandle().Load_ROC_FPGAConfig_from_File(QString::fromStdString(m_sx.str()));

    if(ok) {
      msg()("Configuration loaded successfully");
      Load_ROC_FPGA_Config_from_Handler();
    }
    else {
      msg()("ERROR: Configuration unable to be loaded from requested file");
      PopUpError("ERROR: Configuration unable to be loaded");
    }
  }
  else {
    msg()("ERROR: No ROC FPGA currently configured");
    PopUpError("ERROR: No ROC FPGA currently configured");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadCurrentConfigRocASIC()
{

  stringstream m_sx;
  bool ok = false;
  if(ui->current_ROC_ASIC_Config->count() != 0) {

    QString ROC_str = ui->current_ROC_ASIC_Config->currentText();

    QString boardID = ROC_str.mid(6,1);

    m_sx.str("");
    m_sx << "../data_temp/temp/board_" << boardID.toStdString() ;
    m_sx << "_roc_asic_current_tmp.xml";

    ok = RocASICConfigHandle().Load_ROC_ASICConfig_from_File(QString::fromStdString(m_sx.str()));

    if(ok) {
      msg()("Configuration loaded successfully");
      Load_ROC_ASIC_Config_from_Handler();
    }
    else {
      msg()("ERROR: Configuration unable to be loaded from requested file");
      PopUpError("ERROR: Configuration unable to be loaded");
    }
  }
  else {
    msg()("ERROR: No ROC ASIC currently configured");
    PopUpError("ERROR: No ROC ASIC currently configured");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadVMMConfigurationFromFile()
{
  bool ok = false;
  stringstream m_sx;

  if(ui->configVMMXmlFilenameField->text().toStdString() != "") {

    QString filename = ui->configVMMXmlFilenameField->text();
    QFile ftest(filename);

    if(!ftest.exists()) {

      m_sx.str("");
      m_sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
      msg()(m_sx);
      PopUpError(m_sx.str().c_str());
      ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");

      return;
    }

    ok = VMMConfigHandle().LoadVMMConfig_from_File(ui->configVMMXmlFilenameField->text());
  }
  if(ok) {
    msg()("Configuration loaded successfully");
    ui->configVMMXmlFilenameField->setStyleSheet("background-color: green");
    LoadVMMConfig_from_Handler();
  }
  else {
    msg()("ERROR Configuration unable to be loaded from requested file");
    ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadBoardConfigurationFromFile()
{
  stringstream m_sx;

  bool ok = false;
  if(ui->configBoardXmlFilenameField->text().toStdString() != "") {

    QString filename = ui->configBoardXmlFilenameField->text();
    QFile ftest(filename);

    if(!ftest.exists()) {

      m_sx.str("");
      m_sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
      msg()(m_sx);
      PopUpError(m_sx.str().c_str());
      ui->configBoardXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");

      return;
    }

    ok = BoardConfigHandle().LoadBoardConfig_from_File(ui->configBoardXmlFilenameField->text());

  }
  if(ok) {
    msg()("Configuration loaded successfully");
    ui->configBoardXmlFilenameField->setStyleSheet("background-color: green");
    LoadBoardConfig_from_Handler();
  }
  else {
    msg()("ERROR Configuration unable to be loaded from requested file");
    ui->configBoardXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");
  }
  
}

// ------------------------------------------------------------------------- //
void MainWindow::loadTDSConfigurationFromFile()
{
  stringstream m_sx;

  bool ok = false;
  if(ui->configTDSXmlFilenameField->text().toStdString() != "") {

    QString filename = ui->configTDSXmlFilenameField->text();
    QFile ftest(filename);

    if(!ftest.exists()) {

      m_sx.str("");
      m_sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
      msg()(m_sx);
      PopUpError(m_sx.str().c_str());
      ui->configTDSXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");

      return;
    }

    ok = TDSConfigHandle().LoadTDSConfig_from_File(ui->configTDSXmlFilenameField->text());
  }

  if(ok) {
    msg()("Configuration loaded successfully");
    ui->configTDSXmlFilenameField->setStyleSheet("background-color: green");
    LoadTDSConfig_from_Handler();
  }
  else {
    msg()("ERROR Configuration unable to be loaded from requested file");
    ui->configTDSXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");
  }

}
// ------------------------------------------------------------------------- // 
void MainWindow::loadRocFPGAConfigurationFromFile()
{
  stringstream m_sx;

  bool ok = false;
  if(ui->configRocFPGAXmlFilenameField->text().toStdString() != "") {

    QString filename = ui->configRocFPGAXmlFilenameField->text();
    QFile ftest(filename);

    if(!ftest.exists()) {

      m_sx.str("");
      m_sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
      msg()(m_sx);
      PopUpError(m_sx.str().c_str());
      ui->configRocFPGAXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");

      return;
    }

    ok = RocFPGAConfigHandle().Load_ROC_FPGAConfig_from_File(ui->configRocFPGAXmlFilenameField->text());
  }

  if(ok) {
    msg()("ROC Configuration loaded successfully");
    ui->configRocFPGAXmlFilenameField->setStyleSheet("background-color: green");
    Load_ROC_FPGA_Config_from_Handler();
  }
  else {
    msg()("ERROR Configuration unable to be loaded from requested file");
    ui->configRocFPGAXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::loadRocASICConfigurationFromFile()
{
  stringstream m_sx;

  bool ok = false;
  if(ui->configRocASICXmlFilenameField->text().toStdString() != "") {

    QString filename = ui->configRocASICXmlFilenameField->text();
    QFile ftest(filename);

    if(!ftest.exists()) {

      m_sx.str("");
      m_sx << "Configuration file (" << filename.toStdString() << ") unable to be opened";
      msg()(m_sx);
      PopUpError(m_sx.str().c_str());
      ui->configRocASICXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");

      return;
    }

    ok = RocASICConfigHandle().Load_ROC_ASICConfig_from_File(ui->configRocASICXmlFilenameField->text());
  }

  if(ok) {
    msg()("ROC Configuration loaded successfully");
    ui->configRocASICXmlFilenameField->setStyleSheet("background-color: green");
    Load_ROC_ASIC_Config_from_Handler();
  }
  else {
    msg()("ERROR Configuration unable to be loaded from requested file");
    ui->configRocASICXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0);}");
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::LoadVMMConfig_from_Handler()
{
  // ------------------------------------------- //
  //  GlobalSetting
  // ------------------------------------------- //
  VMMGlobalSetting global = VMMConfigHandle().VMMGlobalSettings();

  ui->VMM_config_header->setText(QString::fromStdString(global.header));
  ui->VMM_board_Type->setCurrentIndex(global.board_Type);
  ui->VMM_boardID->setValue(global.boardID);
  ui->VMM_ASIC_Type->setCurrentIndex(global.ASIC_Type);
  //  ui->VMM_config_mask->setValue(global.vmmmask);
  Display_VMM_Mask((uint32_t) global.vmmmask);
  ui->VMM_config_command->setText(QString::fromStdString(global.command));

  ui->global_sp->setCurrentIndex(global.sp);
  ui->global_sdp->setCurrentIndex(global.sdp);
  ui->global_sbmx->setCurrentIndex(global.sbmx);
  ui->global_sbft->setCurrentIndex(global.sbft);
  ui->global_sbfp->setCurrentIndex(global.sbfp);
  ui->global_sbfm->setCurrentIndex(global.sbfm);
  ui->global_slg->setCurrentIndex(global.slg);
  ui->global_sm5->setCurrentIndex(global.sm5);
  ui->global_scmx->setCurrentIndex(global.scmx);
  ui->global_sfa->setCurrentIndex(global.sfa);
  ui->global_sfam->setCurrentIndex(global.sfam);
  ui->global_st->setCurrentIndex(global.st);
  ui->global_sfm->setCurrentIndex(global.sfm);
  ui->global_sg->setCurrentIndex(global.sg);
  ui->global_sng->setCurrentIndex(global.sng);
  ui->global_stot->setCurrentIndex(global.stot);
  ui->global_stpp->setCurrentIndex(global.stpp);
  ui->global_sttt->setCurrentIndex(global.sttt);
  ui->global_ssh->setCurrentIndex(global.ssh);
  ui->global_stc->setCurrentIndex(global.stc);
  ui->global_sdt_dac->setValue(global.sdt_dac);
  ui->global_sdp_dac->setValue(global.sdp_dac);
  ui->global_sc10b->setCurrentIndex(global.sc10b);
  ui->global_sc8b->setCurrentIndex(global.sc8b);
  ui->global_sc6b->setCurrentIndex(global.sc6b);
  ui->global_s8b->setCurrentIndex(global.s8b);
  ui->global_s6b->setCurrentIndex(global.s6b);
  ui->global_s10b->setCurrentIndex(global.s10b);
  ui->global_sdcks->setCurrentIndex(global.sdcks);
  ui->global_sdcka->setCurrentIndex(global.sdcka);
  ui->global_sdck6b->setCurrentIndex(global.sdck6b);
  ui->global_sdrv->setCurrentIndex(global.sdrv);
  ui->global_slvs->setCurrentIndex(global.slvs);
  ui->global_stcr->setCurrentIndex(global.stcr);
  ui->global_ssart->setCurrentIndex(global.ssart);
  ui->global_s32->setCurrentIndex(global.s32);
  ui->global_stlc->setCurrentIndex(global.stlc);
  ui->global_srec->setCurrentIndex(global.srec);
  ui->global_sbip->setCurrentIndex(global.sbip);
  ui->global_srat->setCurrentIndex(global.srat);
  ui->global_sfrst->setCurrentIndex(global.sfrst);
  ui->global_slvsbc->setCurrentIndex(global.slvsbc);
  ui->global_slvstp->setCurrentIndex(global.slvstp);
  ui->global_slvstk->setCurrentIndex(global.slvstk);
  ui->global_slvsdt->setCurrentIndex(global.slvsdt);
  ui->global_slvsart->setCurrentIndex(global.slvsart);
  ui->global_slvstki->setCurrentIndex(global.slvstki);
  ui->global_slvsena->setCurrentIndex(global.slvsena);
  ui->global_slvs6b->setCurrentIndex(global.slvs6b);
  ui->global_sL0enaV->setCurrentIndex(global.sL0enaV);
  ui->global_reset->setCurrentIndex(global.reset);
  ui->global_sL0ena->setCurrentIndex(global.sL0ena);
  ui->global_l0offset->setValue(global.l0offset);
  ui->global_offset->setValue(global.offset);
  ui->global_rollover->setValue(global.rollover);
  ui->global_window->setValue(global.window);
  ui->global_truncate->setValue(global.truncate);
  ui->global_nskip->setValue(global.nskip);
  ui->global_sL0cktest->setCurrentIndex(global.sL0cktest);
  ui->global_sL0ckinv->setCurrentIndex(global.sL0ckinv);
  ui->global_sL0dckinv->setCurrentIndex(global.sL0dckinv);
  ui->global_nskipm->setCurrentIndex(global.nskipm);

  // ------------------------------------------------- //
  //  individual channels
  // ------------------------------------------------- //
  for(int i = 0; i < 64; i++) {
    VMM_Channel chan = VMMConfigHandle().VMM_ChannelSettings(i);
    if(!((int)chan.number == i)) {
      stringstream ss;
      ss << "Channel config from ConfigHandler is out of sync!"
	 << " Attempting to access state of channel number " << i
	 << " but corresponding index in ConfigHandler channel map is "
	 << chan.number << "!";
      msg()(ss, "MainWindow::updateConfigState");
    }

    VMMSCBool[i]        = chan.sc;
    VMMSLBool[i]        = chan.sl;
    VMMSTHBool[i]       = chan.sth;
    VMMSTBool[i]        = chan.st;
    VMMSMBool[i]        = chan.sm;
    VMMSDValue[i]       = chan.sd;
    VMMSMXBool[i]       = chan.smx;
    VMMSZ010bValue[i]   = chan.sz10b;
    VMMSZ08bValue[i]    = chan.sz8b;
    VMMSZ06bValue[i]    = chan.sz6b;

    if(i<32) {
      // sc
      ui->sc_list_widget->item(i)->setSelected(VMMSCBool[i]);
      // sl
      ui->sl_list_widget->item(i)->setSelected(VMMSLBool[i]);
      // sth
      ui->sth_list_widget->item(i)->setSelected(VMMSTHBool[i]);
      // st
      ui->st_list_widget->item(i)->setSelected(VMMSTBool[i]);
      // sm
      ui->sm_list_widget->item(i)->setSelected(VMMSMBool[i]);
      // smx
      ui->smx_list_widget->item(i)->setSelected(VMMSMXBool[i]);
    }
    else {
      // sc
      ui->sc_list_widget_2->item(i-32)->setSelected(VMMSCBool[i]);
      // sl
      ui->sl_list_widget_2->item(i-32)->setSelected(VMMSLBool[i]);
      // sth
      ui->sth_list_widget_2->item(i-32)->setSelected(VMMSTHBool[i]);
      // st
      ui->st_list_widget_2->item(i-32)->setSelected(VMMSTBool[i]);
      // sm
      ui->sm_list_widget_2->item(i-32)->setSelected(VMMSMBool[i]);
      // smx
      ui->smx_list_widget_2->item(i-32)->setSelected(VMMSMXBool[i]);
    }

    VMMSDVoltage[i]->setCurrentIndex(VMMSDValue[i]);
    VMMSZ010bCBox[i]->setCurrentIndex(VMMSZ010bValue[i]);
    VMMSZ08bCBox[i]->setCurrentIndex(VMMSZ08bValue[i]);
    VMMSZ06bCBox[i]->setCurrentIndex(VMMSZ06bValue[i]);
  }
}

// ------------------------------------------------------------------------- //
void MainWindow::writeVMMConfigurationToFile()
{

  // open a dialog to select the output file name

  QString filename = QFileDialog::getSaveFileName(this,
						  tr("Save XML Configuration File"), "../readout_configuration/",
						  tr("All Files (*)"));

  if(filename.isNull()) return;

  LoadVMMConfig_to_Handler();
  // make the output XML
  VMMConfigHandle().WriteVMMConfig_to_File(filename);

}
// ------------------------------------------------------------------------- //
void MainWindow::writeTDSConfigurationToFile()
{

  // open a dialog to select the output file name                                                                                                                    

  QString filename = QFileDialog::getSaveFileName(this,
                                                  tr("Save XML Configuration File"), "../readout_configuration/",
                                                  tr("All Files (*)"));

  if(filename.isNull()) return;

  LoadTDSConfig_to_Handler();
  // make the output XML                                                                                                                                             
  TDSConfigHandle().WriteTDSConfig_to_File(filename);

}

// ------------------------------------------------------------------------- //
void MainWindow::writeRocFPGAConfigurationToFile()
{
  // open a dialog to select the output file name
  QString filename = QFileDialog::getSaveFileName(this,
                                                  tr("Save XML Configuration File"), "../readout_configuration/",
                                                  tr("All Files (*)"));

  if(filename.isNull()) return;

  Load_ROC_FPGA_Config_to_Handler();
  RocFPGAConfigHandle().Write_ROC_FPGAConfig_to_File(filename);

}
// ------------------------------------------------------------------------- //
void MainWindow::writeRocASICConfigurationToFile()
{
  // open a dialog to select the output file name  
  QString filename = QFileDialog::getSaveFileName(this,
                                                  tr("Save XML Configuration File"), "../readout_configuration/",
                                                  tr("All Files (*)"));

  if(filename.isNull()) return;

  Load_ROC_ASIC_Config_to_Handler();
  RocASICConfigHandle().Write_ROC_ASICConfig_to_File(filename);

}
// ------------------------------------------------------------------------- //
void MainWindow::writeBoardConfigurationToFile()
{

  QString filename = QFileDialog::getSaveFileName(this,
                                                  tr("Save XML Configuration File"), "../readout_configuration/",
                                                  tr("XML Files (*.xml)"));
  
  if(filename.isNull()) return;

  LoadBoardConfig_to_Handler();
  BoardConfigHandle().WriteBoardConfig_to_File(filename);

}
// ------------------------------------------------------------------------- //
void MainWindow::InitializeVMMChannelArrayValues()
{
  for(unsigned int i = 0; i < 64; i++) {
    VMMSCBool[i] = false;
    VMMSLBool[i] = false;
    VMMSTBool[i] = false;
    VMMSTHBool[i] = false;
    VMMSMBool[i] = false;
    VMMSMXBool[i] = false;
    VMMSPBool[i] = false;
    VMMSDValue[i] = 0;
    VMMSZ010bValue[i] = 0;
    VMMSZ08bValue[i] = 0;
    VMMSZ06bValue[i] = 0;
  }

  VMMSPBoolAll = false;
  VMMSCBoolAll = false;
  VMMSLBoolAll = false;
  VMMSTBoolAll = false;
  VMMSTHBoolAll = false;
  VMMSMBoolAll = false;
  VMMSMXBoolAll = false;
  VMMSZ010bBoolAll = false;
  VMMSZ08bBoolAll = false;
  VMMSZ06bBoolAll = false;

  VMMSPBoolAll2 = false;
  VMMSCBoolAll2 = false;
  VMMSLBoolAll2 = false;
  VMMSTBoolAll2 = false;
  VMMSTHBoolAll2 = false;
  VMMSMBoolAll2 = false;
  VMMSMXBoolAll2 = false;
  VMMSZ010bBoolAll2 = false;
  VMMSZ08bBoolAll2 = false;
  VMMSZ06bBoolAll2 = false;

}
// ------------------------------------------------------------------------- //
void MainWindow::CreateVMMChannelsFields()
{
  Font.setPointSize(10);
  int margin = 8;
  int channel_square_size = 16;



  //////////////////////////////////////////////////////
  // add the "global" labels
  //////////////////////////////////////////////////////
  //ui->channelLabelLayout->setContentsMargins(margin*0.75, margin*0.25, margin*2*10, margin);
  //ui->channelLabelLayout->setHorizontalSpacing(4);
  //ui->channelLabelLayout->setVerticalSpacing(1);

  //////////////////////////////////////////////////////
  // add the channel stuff
  //////////////////////////////////////////////////////

  channel_square_size = 15;
  //void  setContentsMargins(int left, int top, int right, int bottom)
  VMMchannelGridLayout->setContentsMargins(margin*0.75, margin*0.25, margin*2*10, margin);
  VMMchannelGridLayout->setHorizontalSpacing(4);
  VMMchannelGridLayout->setVerticalSpacing(1);

  QString initialValueRadio = "";
  QString counter;

  // set initial values for "global" channel bools
  VMMSPBoolAll=0;
  VMMSCBoolAll=0;
  VMMSLBoolAll=0;
  VMMSTBoolAll=0;
  VMMSTHBoolAll=0;
  VMMSMBoolAll=0;
  VMMSMXBoolAll=0;
  VMMSZ010bBoolAll=0;
  VMMSZ08bBoolAll=0;
  VMMSZ06bBoolAll=0;
    
  VMMSPBoolAll2=0;
  VMMSCBoolAll2=0;
  VMMSLBoolAll2=0;
  VMMSTBoolAll2=0;
  VMMSTHBoolAll2=0;
  VMMSMBoolAll2=0;
  VMMSMXBoolAll2=0;
  VMMSZ010bBoolAll2=0;
  VMMSZ08bBoolAll2=0;
  VMMSZ06bBoolAll2=0;

  // prevent scrolling (mouse wheel) from changing these widgets
  ////////////////////////////////////////////////////////////////////
  // add left-hand column
  ////////////////////////////////////////////////////////////////////
  for(int i = 0; i < 32; i++) {

    // this is the fixed channel label for each row on the LHS
    VMMChannel[i] = new QLineEdit(counter.setNum(i), ui->tab_3);
    VMMChannel[i]->setAlignment(Qt::AlignHCenter);

    VMMChannel[i]->setEnabled(false);
    VMMChannel[i]->setFixedSize(channel_square_size*1.22, channel_square_size);

    // these are the per-channel buttons
    VMMSC[i]    = new QPushButton(initialValueRadio,ui->tab_3); // cap
    VMMSL[i]    = new QPushButton(initialValueRadio,ui->tab_3); // leakage
    VMMST[i]    = new QPushButton(initialValueRadio,ui->tab_3); // test-cap
    VMMSTH[i]   = new QPushButton(initialValueRadio,ui->tab_3); // x10
    VMMSM[i]    = new QPushButton(initialValueRadio,ui->tab_3); // mask
    VMMSMX[i]   = new QPushButton(initialValueRadio,ui->tab_3); // mon trim
    VMMSDVoltage[i] = new QComboBox(ui->tab_3);

    // ADC zeros
    VMMSZ010bCBox[i] = new QComboBox(ui->tab_3);
    VMMSZ08bCBox[i] = new QComboBox(ui->tab_3);
    VMMSZ06bCBox[i] = new QComboBox(ui->tab_3);


    VMMSC[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMSL[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMST[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMSTH[i]   ->setFixedSize(channel_square_size, channel_square_size);
    VMMSM[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMSMX[i]   ->setFixedSize(31, channel_square_size);
    VMMSDVoltage[i]->setFixedSize(55, 12);
    VMMSDVoltage[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSDVoltage[i]->addItem(counter.setNum(j)+" mV");

    VMMSZ010bCBox[i]->setFixedSize(55, 12);
    VMMSZ08bCBox[i]->setFixedSize(55, 12);
    VMMSZ06bCBox[i]->setFixedSize(55, 12);
    VMMSZ010bCBox[i]->setFont(Font);
    VMMSZ08bCBox[i]->setFont(Font);
    VMMSZ06bCBox[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSZ010bCBox[i]->addItem(counter.setNum(j)+"");
    for(int j = 0; j < 16; j++) VMMSZ08bCBox[i]->addItem(counter.setNum(j)+"");
    for(int j = 0; j < 8; j++)  VMMSZ06bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ010bCBox[i]->setCurrentIndex(0);
    VMMSZ08bCBox[i]->setCurrentIndex(0);
    VMMSZ06bCBox[i]->setCurrentIndex(0);
        

    VMMSC[i]    ->setStyleSheet("background-color:  lightGray");
    VMMSL[i]    ->setStyleSheet("background-color:  lightGray");
    VMMST[i]    ->setStyleSheet("background-color:  lightGray");
    VMMSTH[i]   ->setStyleSheet("background-color:  lightGray");
    VMMSM[i]    ->setStyleSheet("background-color:  lightGray");
    VMMSMX[i]   ->setStyleSheet("background-color:  lightGray");

    // these are the boolean values associated with the buttons
    VMMSPBool[i]    = 0;
    VMMSCBool[i]    = 0;
    VMMSMBool[i]    = 0;
    VMMSTBool[i]    = 0;
    VMMSTHBool[i]   = 0;
    VMMSLBool[i]    = 0;
    VMMSMXBool[i]   = 0;

    VMMSZ010bValue[i] = 0;
    VMMSZ08bValue[i]  = 0;
    VMMSZ06bValue[i]  = 0;


    // now put these guys on the grid
    // multiselect
    VMMchannelGridLayout->addWidget(VMMChannel[i],             i+1+1, 1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSC[i],                  i+1+1, 3, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSL[i],                  i+1+1, 4, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSTH[i],                 i+1+1, 5, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMST[i],                  i+1+1, 6, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSM[i],                  i+1+1, 6+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSDVoltage[i],           i+1+1, 7+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSMX[i],                 i+1+1, 8+1, Qt::AlignCenter); 
    VMMchannelGridLayout->addWidget(VMMSZ010bCBox[i],          i+1+1, 9+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSZ08bCBox[i],           i+1+1,10+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSZ06bCBox[i],           i+1+1,11+1, Qt::AlignCenter);

    // prevent mouse scrolling from manipulating these widgets
    VMMSDVoltage[i]->installEventFilter(this);
    VMMSDVoltage[i]->setFocusPolicy(Qt::ClickFocus);
    VMMSZ010bCBox[i]->installEventFilter(this);
    VMMSZ08bCBox[i]->installEventFilter(this);
    VMMSZ06bCBox[i]->installEventFilter(this);

  } // i (LHS column)
    ////////////////////////////////////////////////////////////////////
    // add righ-hand column
    ////////////////////////////////////////////////////////////////////
  for(int i = 32; i < 64; i++) {

    // this is the fixed channel label for each row on the LHS
    VMMChannel[i] = new QLineEdit(counter.setNum(i), ui->tab_3);
    VMMChannel[i]->setAlignment(Qt::AlignHCenter);

    VMMChannel[i]->setEnabled(false);
    VMMChannel[i]->setFixedSize(channel_square_size*1.22, channel_square_size);

    // these are the per-channel buttons
    VMMSC[i]    = new QPushButton(initialValueRadio,ui->tab_3); // cap
    VMMSL[i]    = new QPushButton(initialValueRadio,ui->tab_3); // leakage
    VMMST[i]    = new QPushButton(initialValueRadio,ui->tab_3); // test-cap
    VMMSTH[i]   = new QPushButton(initialValueRadio,ui->tab_3); // x10
    VMMSM[i]    = new QPushButton(initialValueRadio,ui->tab_3); // mask
    VMMSMX[i]   = new QPushButton(initialValueRadio,ui->tab_3); // mon trim
    VMMSDVoltage[i] = new QComboBox(ui->tab_3);

    // ADC zeros
    VMMSZ010bCBox[i] = new QComboBox(ui->tab_3);
    VMMSZ08bCBox[i] = new QComboBox(ui->tab_3);
    VMMSZ06bCBox[i] = new QComboBox(ui->tab_3);


    VMMSC[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMSL[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMST[i]    ->setFixedSize(channel_square_size, channel_square_size);
    VMMSTH[i]   ->setFixedSize(channel_square_size, channel_square_size);
    VMMSM[i]    ->setFixedSize(channel_square_size, channel_square_size);

    VMMSMX[i]   ->setFixedSize(31, channel_square_size-2);
    VMMSDVoltage[i]->setFixedSize(55, channel_square_size-2);
    VMMSDVoltage[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSDVoltage[i]->addItem(counter.setNum(j)+" mV");

    VMMSZ010bCBox[i]->setFixedSize(55, channel_square_size-2);
    VMMSZ08bCBox[i]->setFixedSize(55, channel_square_size-2);
    VMMSZ06bCBox[i]->setFixedSize(55, channel_square_size-2);
    VMMSZ010bCBox[i]->setFont(Font);
    VMMSZ08bCBox[i]->setFont(Font);
    VMMSZ06bCBox[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSZ010bCBox[i]->addItem(counter.setNum(j)+"");
    for(int j = 0; j < 16; j++) VMMSZ08bCBox[i]->addItem(counter.setNum(j)+"");
    for(int j = 0; j < 8; j++)  VMMSZ06bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ010bCBox[i]->setCurrentIndex(0);
    VMMSZ08bCBox[i]->setCurrentIndex(0);
    VMMSZ06bCBox[i]->setCurrentIndex(0);
        

    VMMSC[i]    ->setStyleSheet("background-color:  lightGray");
    VMMSL[i]    ->setStyleSheet("background-color:  lightGray");
    VMMST[i]    ->setStyleSheet("background-color:  lightGray");
    VMMSTH[i]   ->setStyleSheet("background-color:  lightGray");
    VMMSM[i]    ->setStyleSheet("background-color:  lightGray");
    VMMSMX[i]   ->setStyleSheet("background-color:  lightGray");

    // these are the boolean values associated with the buttons
    VMMSPBool[i]    = 0;
    VMMSCBool[i]    = 0;
    VMMSMBool[i]    = 0;
    VMMSTBool[i]    = 0;
    VMMSTHBool[i]   = 0;
    VMMSLBool[i]    = 0;
    VMMSMXBool[i]   = 0;

    VMMSZ010bValue[i] = 0;
    VMMSZ08bValue[i]  = 0;
    VMMSZ06bValue[i]  = 0;

    QLabel *spacer = new QLabel("  ");


    // now put these guys on the grid
    // void     addLayout(QLayout * layout, int row, int column, Qt::Alignment alignment = 0)

    VMMchannelGridLayout->addWidget(spacer,                    i+1-32+1, 1+12, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMChannel[i],             i+1-32+1, 1+1+12, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSC[i],                  i+1-32+1, 3+1+12, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSL[i],                  i+1-32+1, 4+1+12, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSTH[i],                 i+1-32+1, 5+1+12, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMST[i],                  i+1-32+1, 6+1+12, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSM[i],                  i+1-32+1, 6+1+12+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSDVoltage[i],           i+1-32+1, 7+1+12+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSMX[i],                 i+1-32+1, 8+1+12+1, Qt::AlignCenter); 
    VMMchannelGridLayout->addWidget(VMMSZ010bCBox[i],          i+1-32+1, 9+1+12+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSZ08bCBox[i],           i+1-32+1,10+1+12+1, Qt::AlignCenter);
    VMMchannelGridLayout->addWidget(VMMSZ06bCBox[i],           i+1-32+1,11+1+12+1, Qt::AlignCenter);

    // prevent mouse scrolling from manipulating these widgets
    VMMSDVoltage[i]->installEventFilter(this);
    VMMSDVoltage[i]->setFocusPolicy(Qt::ClickFocus);
    VMMSZ010bCBox[i]->installEventFilter(this);
    VMMSZ08bCBox[i]->installEventFilter(this);
    VMMSZ06bCBox[i]->installEventFilter(this);

  } // i (RHS column)

    // set the grid layout geometry
    // QRect(int x, int y, int width, int height)
  VMMchannelGridLayout->setGeometry(QRect(620,12,100,2000));
  //VMMchannelGridLayout->setGeometry(QRect(620,12,380,2000));
  dummy->setLayout(VMMchannelGridLayout);


  // attach scroll areay to the widget that the channel grid is attached to
  // ui->scrollArea->setWidget(dummy);
  // ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);


  ///////////////////////////////////////////////////////////////
  // widget connections for "global" channel buttons
  ///////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////
  // widget connections for individual channel buttons
  ///////////////////////////////////////////////////////////////
  for(int i = 0; i < 64; i++) {

    // channel states
    connect(VMMSC[i],  SIGNAL(pressed()), this, SLOT(updateVMMChannelState()));
    connect(VMMSM[i],  SIGNAL(pressed()), this, SLOT(updateVMMChannelState()));
    connect(VMMST[i],  SIGNAL(pressed()), this, SLOT(updateVMMChannelState()));
    connect(VMMSTH[i], SIGNAL(pressed()), this, SLOT(updateVMMChannelState()));
    connect(VMMSL[i],  SIGNAL(pressed()), this, SLOT(updateVMMChannelState()));
    connect(VMMSMX[i], SIGNAL(pressed()), this, SLOT(updateVMMChannelState()));
    connect(VMMSDVoltage[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelVoltages(int)));
    connect(VMMSZ010bCBox[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelADCs(int)));
    connect(VMMSZ08bCBox[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelADCs(int)));
    connect(VMMSZ06bCBox[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelADCs(int)));
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::CreateVMMChannelsFieldsMulti()
{
  Font.setPointSize(10);
  int margin = 8;
  int channel_square_size = 16;

  //////////////////////////////////////////////////////
  // add the "global" labels
  //////////////////////////////////////////////////////
  //ui->channelLabelLayout->setContentsMargins(margin*0.75, margin*0.25, margin*2*10, margin);
  //ui->channelLabelLayout->setHorizontalSpacing(4);
  //ui->channelLabelLayout->setVerticalSpacing(1);

  //ui->VMMChanLabel->setFixedSize(channel_square_size*1.22, channel_square_size);
  //ui->VMMChanLabel->setEnabled(false);

  ui->SDLabel_2->setFixedSize(55, channel_square_size*1.15);
  ui->SDLabel2_2->setFixedSize(55, channel_square_size*1.15);

  ui->SZ010bLabel_2->setFixedSize(55, channel_square_size);
  ui->SZ08bLabel_2->setFixedSize(55, channel_square_size);
  ui->SZ06bLabel_2->setFixedSize(55, channel_square_size);
    
  //ui->VMMChanLabel2->setFixedSize(channel_square_size*1.22, channel_square_size);
  //ui->VMMChanLabel2->setEnabled(false);

  ui->SZ010bLabel2_2->setFixedSize(55, channel_square_size);
  ui->SZ08bLabel2_2->setFixedSize(55, channel_square_size);
  ui->SZ06bLabel2_2->setFixedSize(55, channel_square_size);
    

  //////////////////////////////////////////////////////
  // add the channel stuff
  //////////////////////////////////////////////////////

  channel_square_size = 15;
  //void  setContentsMargins(int left, int top, int right, int bottom)
  VMMchannelGridLayout->setContentsMargins(margin*0.75, margin*0.25, margin*2*10, margin);
  VMMchannelGridLayout->setHorizontalSpacing(4);
  VMMchannelGridLayout->setVerticalSpacing(1);

  QString initialValueRadio = "";
  QString counter;

  // add "global" channel trim labels
  for(int i = 0; i < 32; i++) {
    ui->SDLabel_2->addItem(counter.setNum(i)+" mV");
    ui->SDLabel2_2->addItem(counter.setNum(i)+" mV");
  }
  // add "global" 10-bit zero values
  for(int i = 0; i < 32; i++) {
    ui->SZ010bLabel_2->addItem(counter.setNum(i)+"");
    ui->SZ010bLabel2_2->addItem(counter.setNum(i)+"");
  }
  // add "global" 8-bit zero values
  for(int i = 0; i < 16; i++) {
    ui->SZ08bLabel_2->addItem(counter.setNum(i)+"");
    ui->SZ08bLabel2_2->addItem(counter.setNum(i)+"");
  }
  // add "global" 6-bit zero values
  for(int i = 0; i < 8; i++) {
    ui->SZ06bLabel_2->addItem(counter.setNum(i)+"");
    ui->SZ06bLabel2_2->addItem(counter.setNum(i)+"");
  }


  // set initial values for "global" channel bools
  VMMSPBoolAll=0;
  VMMSCBoolAll=0;
  VMMSLBoolAll=0;
  VMMSTBoolAll=0;
  VMMSTHBoolAll=0;
  VMMSMBoolAll=0;
  VMMSMXBoolAll=0;
  VMMSZ010bBoolAll=0;
  VMMSZ08bBoolAll=0;
  VMMSZ06bBoolAll=0;
    
  VMMSPBoolAll2=0;
  VMMSCBoolAll2=0;
  VMMSLBoolAll2=0;
  VMMSTBoolAll2=0;
  VMMSTHBoolAll2=0;
  VMMSMBoolAll2=0;
  VMMSMXBoolAll2=0;
  VMMSZ010bBoolAll2=0;
  VMMSZ08bBoolAll2=0;
  VMMSZ06bBoolAll2=0;

  // prevent scrolling (mouse wheel) from changing these widgets
  ui->SDLabel_2     ->installEventFilter(this);
  ui->SDLabel2_2    ->installEventFilter(this);
  ui->SZ010bLabel_2 ->installEventFilter(this);
  ui->SZ08bLabel_2  ->installEventFilter(this);
  ui->SZ06bLabel_2  ->installEventFilter(this);

  ui->SZ010bLabel2_2 ->installEventFilter(this);
  ui->SZ08bLabel2_2  ->installEventFilter(this);
  ui->SZ06bLabel2_2  ->installEventFilter(this);

  // left hand column
  std::stringstream num_string;
  for(int i = 0; i < 32; i++) {

    if(i==0) {
      //ui->sd_list_widget->addItem("");
      //ui->sd_list_widget->setItemWidget(ui->sd_list_widget->item(i), ui->SDLabel_2);
      //ui->chan_trim_layout->addWidget(ui->SDLabel_2, i, 0, Qt::AlignCenter);
    }

    // this is the fixed channel label for each row on the LHS
    num_string.str("");
    num_string << i;
    ui->chan_label_list_widget->addItem(QString::fromStdString(num_string.str()));
    ui->chan_label_list_widget->item(i)->setTextAlignment(Qt::AlignHCenter);

    //////////////////////////////////////////////////////////
    // VMMSD
    //////////////////////////////////////////////////////////
    VMMSDVoltage[i] = new QComboBox(ui->tab_8);
    VMMSDVoltage[i]->setFixedSize(55, 15);
    //VMMSDVoltage[i]->setFixedSize(55, channel_square_size*0.95);
    VMMSDVoltage[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSDVoltage[i]->addItem(counter.setNum(j)+" mV");
    ui->sd_list_widget->addItem("");
    ui->sd_list_widget->setItemWidget(ui->sd_list_widget->item(i), VMMSDVoltage[i]);

    ui->sd_list_widget->item(i)->setSizeHint(QSize(ui->sd_list_widget->item(i)->sizeHint().width(),16));

    //////////////////////////////////////////////////////////
    // ADC zeros
    //////////////////////////////////////////////////////////
    VMMSZ010bCBox[i] = new QComboBox(ui->tab_8);
    VMMSZ08bCBox[i]  = new QComboBox(ui->tab_8);
    VMMSZ06bCBox[i]  = new QComboBox(ui->tab_8);

    VMMSZ010bCBox[i]->setFixedSize(55, 15); 
    VMMSZ010bCBox[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSZ010bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ010bCBox[i]->setCurrentIndex(0);
    VMMSZ010bValue[i] = 0;
    ui->sc10_list_widget->addItem("");
    ui->sc10_list_widget->setItemWidget(ui->sc10_list_widget->item(i), VMMSZ010bCBox[i]);
    ui->sc10_list_widget->item(i)->setSizeHint(QSize(ui->sc10_list_widget->item(i)->sizeHint().width(),16));

    VMMSZ08bCBox[i]->setFixedSize(55, 15); 
    VMMSZ08bCBox[i]->setFont(Font);
    for(int j = 0; j < 16; j++) VMMSZ08bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ08bCBox[i]->setCurrentIndex(0);
    VMMSZ08bValue[i] = 0;
    ui->sc8_list_widget->addItem("");
    ui->sc8_list_widget->setItemWidget(ui->sc8_list_widget->item(i), VMMSZ08bCBox[i]);
    ui->sc8_list_widget->item(i)->setSizeHint(QSize(ui->sc8_list_widget->item(i)->sizeHint().width(),16));

    VMMSZ06bCBox[i]->setFixedSize(55, 15); 
    VMMSZ06bCBox[i]->setFont(Font);
    for(int j = 0; j < 8; j++) VMMSZ06bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ06bCBox[i]->setCurrentIndex(0);
    VMMSZ06bValue[i] = 0;
    ui->sc6_list_widget->addItem("");
    ui->sc6_list_widget->setItemWidget(ui->sc6_list_widget->item(i), VMMSZ06bCBox[i]);
    ui->sc6_list_widget->item(i)->setSizeHint(QSize(ui->sc6_list_widget->item(i)->sizeHint().width(),16));

  } // i (LHS column)

    // left hand column
  for(int i = 32; i < 64; i++) {

    // this is the fixed channel label for each row on the LHS
    num_string.str("");
    num_string << i;
    ui->chan_label_list_widget_2->addItem(QString::fromStdString(num_string.str()));
    ui->chan_label_list_widget_2->item(i-32)->setTextAlignment(Qt::AlignHCenter);

    //////////////////////////////////////////////////////////
    // VMMSD
    //////////////////////////////////////////////////////////
    VMMSDVoltage[i] = new QComboBox(ui->tab_8);
    VMMSDVoltage[i]->setFixedSize(55, 15);
    //VMMSDVoltage[i]->setFixedSize(55, channel_square_size*0.95);
    VMMSDVoltage[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSDVoltage[i]->addItem(counter.setNum(j)+" mV");
    ui->sd_list_widget_2->addItem("");
    ui->sd_list_widget_2->setItemWidget(ui->sd_list_widget_2->item(i-32), VMMSDVoltage[i]);
    ui->sd_list_widget_2->item(i-32)->setSizeHint(QSize(ui->sd_list_widget_2->item(i-32)->sizeHint().width(),16));

    //////////////////////////////////////////////////////////
    // ADC zeros
    //////////////////////////////////////////////////////////
    VMMSZ010bCBox[i] = new QComboBox(ui->tab_8);
    VMMSZ08bCBox[i]  = new QComboBox(ui->tab_8);
    VMMSZ06bCBox[i]  = new QComboBox(ui->tab_8);

    VMMSZ010bCBox[i]->setFixedSize(55, 15); 
    VMMSZ010bCBox[i]->setFont(Font);
    for(int j = 0; j < 32; j++) VMMSZ010bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ010bCBox[i]->setCurrentIndex(0);
    VMMSZ010bValue[i] = 0;
    ui->sc10_list_widget_2->addItem("");
    ui->sc10_list_widget_2->setItemWidget(ui->sc10_list_widget_2->item(i-32), VMMSZ010bCBox[i]);
    ui->sc10_list_widget_2->item(i-32)->setSizeHint(QSize(ui->sc10_list_widget_2->item(i-32)->sizeHint().width(),16));

    VMMSZ08bCBox[i]->setFixedSize(55, 15); 
    VMMSZ08bCBox[i]->setFont(Font);
    for(int j = 0; j < 16; j++) VMMSZ08bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ08bCBox[i]->setCurrentIndex(0);
    VMMSZ08bValue[i] = 0;
    ui->sc8_list_widget_2->addItem("");
    ui->sc8_list_widget_2->setItemWidget(ui->sc8_list_widget_2->item(i-32), VMMSZ08bCBox[i]);
    ui->sc8_list_widget_2->item(i-32)->setSizeHint(QSize(ui->sc8_list_widget_2->item(i-32)->sizeHint().width(),16));

    VMMSZ06bCBox[i]->setFixedSize(55, 15); 
    VMMSZ06bCBox[i]->setFont(Font);
    for(int j = 0; j < 8; j++) VMMSZ06bCBox[i]->addItem(counter.setNum(j)+"");
    VMMSZ06bCBox[i]->setCurrentIndex(0);
    VMMSZ06bValue[i] = 0;
    ui->sc6_list_widget_2->addItem("");
    ui->sc6_list_widget_2->setItemWidget(ui->sc6_list_widget_2->item(i-32), VMMSZ06bCBox[i]);
    ui->sc6_list_widget_2->item(i-32)->setSizeHint(QSize(ui->sc6_list_widget_2->item(i-32)->sizeHint().width(),16));

  } // i (LHS column)



    ///////////////////////////////////////////////
    // multi
    ///////////////////////////////////////////////
  vector<QListWidget*> channel_list_widgets;
  channel_list_widgets.push_back(ui->sc_list_widget);
  channel_list_widgets.push_back(ui->sl_list_widget);
  channel_list_widgets.push_back(ui->sth_list_widget);
  channel_list_widgets.push_back(ui->st_list_widget);
  channel_list_widgets.push_back(ui->sm_list_widget);

  channel_list_widgets.push_back(ui->sc_list_widget_2);
  channel_list_widgets.push_back(ui->sl_list_widget_2);
  channel_list_widgets.push_back(ui->sth_list_widget_2);
  channel_list_widgets.push_back(ui->st_list_widget_2);
  channel_list_widgets.push_back(ui->sm_list_widget_2);

  channel_list_widgets.push_back(ui->smx_list_widget);
  channel_list_widgets.push_back(ui->smx_list_widget_2);

  //for(auto list : channel_list_widgets) {
  for(size_t i = 0; i < channel_list_widgets.size(); i++) {
    QListWidget* list = channel_list_widgets.at(i);
    //list->setStyleSheet( "QListWidget::item { border-bottom: 1px solid black; }" );
    if(i < 10) {
      for(uint ichan = 0; ichan < 32; ichan++) {
	list->addItem("");
	//list->item(ichan)->setSizeHint(QSize(list->item(ichan)->sizeHint().width(), list->height()/32));
	list->item(ichan)->setSizeHint(QSize(list->item(ichan)->sizeHint().width(), 14));
      }
    }
    else {
      for(uint ichan = 0; ichan < 32; ichan++) {
	list->addItem("");
	list->item(ichan)->setSizeHint(QSize(list->item(ichan)->sizeHint().width(), 14));
      } // ichan
    }
  }

  ////////////////////////////////////////////////////////////////
  // widget connections for "all channel" buttons at the top
  ////////////////////////////////////////////////////////////////

  // SC
  connect(ui->SCLabel_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  connect(ui->SCLabel2_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  // SL
  connect(ui->SLLabel_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  connect(ui->SLLabel2_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  // STH
  connect(ui->STHLabel_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  connect(ui->STHLabel2_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  // ST
  connect(ui->STLabel_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  connect(ui->STLabel2_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  // SM
  connect(ui->SMLabel_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  connect(ui->SMLabel2_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  // SMX
  connect(ui->SMXLabel_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));
  connect(ui->SMXLabel2_2,  SIGNAL(pressed()),  this, SLOT(updateVMMChannelState()));

  //connect(ui->sc_list_widget, SIGNAL(itemChanged(QListWidgetItem*)),
  //        this, SLOT(updateIndividualChannelState(QListWidgetItem*)));
  // SC
  connect(ui->sc_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  connect(ui->sc_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  // SL
  connect(ui->sl_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  connect(ui->sl_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  // STH
  connect(ui->sth_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  connect(ui->sth_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  // ST 
  connect(ui->st_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  connect(ui->st_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  // SM 
  connect(ui->sm_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  connect(ui->sm_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  // SMX 
  connect(ui->smx_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));
  connect(ui->smx_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualVMMChannelState()));

  // ADC offsets
  connect(ui->SZ010bLabel_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelADCs(int)));
  connect(ui->SZ08bLabel_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelADCs(int)));
  connect(ui->SZ06bLabel_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelADCs(int)));
    
  connect(ui->SZ010bLabel2_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelADCs(int)));
  connect(ui->SZ08bLabel2_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelADCs(int)));
  connect(ui->SZ06bLabel2_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelADCs(int)));

  // Channel threshold trims
  connect(ui->SDLabel_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelVoltages(int)));
  connect(ui->SDLabel2_2, SIGNAL(currentIndexChanged(int)),
	  this, SLOT(updateVMMChannelVoltages(int)));

  for(int i = 0; i < 64; i++) {
    connect(VMMSDVoltage[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelVoltages(int)));
    connect(VMMSZ010bCBox[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelADCs(int)));
    connect(VMMSZ08bCBox[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelADCs(int)));
    connect(VMMSZ06bCBox[i], SIGNAL(currentIndexChanged(int)),
	    this, SLOT(updateVMMChannelADCs(int)));
  }



}
// ------------------------------------------------------------------------- //
void MainWindow::CreateGPIOChanFieldsMulti()
{
  Font.setPointSize(10);
  int margin = 8;
  //  int channel_square_size = 16;                                                                                                                                                                                           
  bool channelEnableInitValue = true;

  //////////////////////////////////////////////////////
  // add the channel stuff                              
  //////////////////////////////////////////////////////
  //  channel_square_size = 15;                         
  //void  setContentsMargins(int left, int top, int right, int bottom)
  GPIOchanGridLayout->setContentsMargins(margin*0.75, margin*0.25, margin*2*10, margin);
  GPIOchanGridLayout->setHorizontalSpacing(4);
  GPIOchanGridLayout->setVerticalSpacing(1);

  QString initialValueRadio = "";
  QString counter;

  GPIO_dir_chan_enable_val.resize(0);
  GPIO_dout_chan_enable_val.resize(0);

  for( int i=0; i<32; i++ ) {
    GPIO_dir_chan_enable_val.push_back(true);
    GPIO_dout_chan_enable_val.push_back(true);
  }

  std::stringstream num_string;
  for(int i = 0; i < 32; i++) {

    // this is the fixed channel label for each row on the LHS
    num_string.str("");
    num_string << i;
    if ( i >=0 && i < 32 ) {
      ui->GPIO_chan_label_list_widget->addItem(QString::fromStdString(num_string.str()));
      ui->GPIO_chan_label_list_widget->item(i)->setTextAlignment(Qt::AlignHCenter);
    }

    //////////////////////////////////////////////////////////
    // GPIO                                                    
    //////////////////////////////////////////////////////////

    // simple enable/disable
    if ( i >=0 && i < 32 ) {
      ui->GPIO_dir_chan_enable_list_widget->addItem("");
      ui->GPIO_dir_chan_enable_list_widget->item(i)
        ->setSizeHint(QSize(ui->GPIO_dir_chan_enable_list_widget->item(i)->sizeHint().width(),14));
      ui->GPIO_dir_chan_enable_list_widget->item(i)->setSelected(channelEnableInitValue);

      ui->GPIO_dout_chan_enable_list_widget->addItem("");
      ui->GPIO_dout_chan_enable_list_widget->item(i)
        ->setSizeHint(QSize(ui->GPIO_dout_chan_enable_list_widget->item(i)->sizeHint().width(),14));
      ui->GPIO_dout_chan_enable_list_widget->item(i)->setSelected(channelEnableInitValue);
    }

    GPIO_dir_chan_enable_val.at(i) = channelEnableInitValue;
    GPIO_dout_chan_enable_val.at(i)= channelEnableInitValue;
  }

  ////////////////////////////////////////////////////////////////
  // widget connections for "all channel" buttons at the top      
  ////////////////////////////////////////////////////////////////

  // delay
  connect(ui->GPIO_Dir_Ch_Enable,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateGPIOChannelEnable(int)));
  connect(ui->GPIO_Dout_Ch_Enable, SIGNAL(currentIndexChanged(int)),  this, SLOT(updateGPIOChannelEnable(int)));

  // enable
  connect(ui->GPIO_dir_chan_enable_list_widget,  SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualGPIOChannelState()));
  connect(ui->GPIO_dout_chan_enable_list_widget, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualGPIOChannelState()));


}


float MainWindow::find_avg( std::vector<uint32_t> vec ) {

  float tot=0;
  uint32_t icount=0;

  for ( int i=0; i<vec.size(); i++ ) {
    if (vec.at(i) != 0xffff ) {
      tot += vec.at(i);
      icount++;
      
      std::cout << "count " << icount << " adc val " << vec.at(i) << std::endl;
    }
  }

  if ( vec.size() != 0 ) {
    return tot/icount;
  }
  else {
    return 0xffff;
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::CreateTDSChannelsFieldsMulti()
{
  Font.setPointSize(10);
  int margin = 8;
  //  int channel_square_size = 16;
  bool channelEnableInitValue = true;

  //////////////////////////////////////////////////////
  // add the channel stuff
  //////////////////////////////////////////////////////

  //  channel_square_size = 15;
  //void  setContentsMargins(int left, int top, int right, int bottom)
  TDSchannelGridLayout->setContentsMargins(margin*0.75, margin*0.25, margin*2*10, margin);
  TDSchannelGridLayout->setHorizontalSpacing(4);
  TDSchannelGridLayout->setVerticalSpacing(1);

  QString initialValueRadio = "";
  QString counter;

  for ( int j=0; j<8; j++ ) {
    ui->TDSChanDelay_1->addItem(counter.setNum(3.125*j,'g',5) + " ns");
    ui->TDSChanDelay_2->addItem(counter.setNum(3.125*j,'g',5) + " ns");
    ui->TDSChanDelay_3->addItem(counter.setNum(3.125*j,'g',5) + " ns");
    ui->TDSChanDelay_4->addItem(counter.setNum(3.125*j,'g',5) + " ns");
  }

  ui->TDSChanDelay_1->installEventFilter(this);
  ui->TDSChanDelay_2->installEventFilter(this);
  ui->TDSChanDelay_3->installEventFilter(this);
  ui->TDSChanDelay_4->installEventFilter(this);

  // left hand column
  std::stringstream num_string;
  for(int i = 0; i < 128; i++) {

    if(i==0) {
      //ui->sd_list_widget->addItem("");
      //ui->sd_list_widget->setItemWidget(ui->sd_list_widget->item(i), ui->SDLabel_2);
      //ui->chan_trim_layout->addWidget(ui->SDLabel_2, i, 0, Qt::AlignCenter);
    }

    // this is the fixed channel label for each row on the LHS
    num_string.str("");
    num_string << i;
    if ( i >=0 && i < 32 ) {
      ui->TDS_chan_label_list_widget_1->addItem(QString::fromStdString(num_string.str()));
      ui->TDS_chan_label_list_widget_1->item(i)->setTextAlignment(Qt::AlignHCenter);
    }
    else if ( i >=32 && i < 64 ) {
      ui->TDS_chan_label_list_widget_2->addItem(QString::fromStdString(num_string.str()));
      ui->TDS_chan_label_list_widget_2->item(i-32)->setTextAlignment(Qt::AlignHCenter);
    }
    else if ( i >= 64 && i < 96 ) {
      ui->TDS_chan_label_list_widget_3->addItem(QString::fromStdString(num_string.str()));
      ui->TDS_chan_label_list_widget_3->item(i-64)->setTextAlignment(Qt::AlignHCenter);
    }
    else if ( i>=96 && i < 128 ) {
      ui->TDS_chan_label_list_widget_4->addItem(QString::fromStdString(num_string.str()));
      ui->TDS_chan_label_list_widget_4->item(i-96)->setTextAlignment(Qt::AlignHCenter);
    }


    //////////////////////////////////////////////////////////
    // TDS
    //////////////////////////////////////////////////////////
    if ( i < 104 ) TDSChanDelay[i] = new QComboBox(ui->tab_11);

    for ( int j=0; j<8; j++ ) {
      if ( i < 104 ) TDSChanDelay[i]->addItem(counter.setNum(3.125*j,'g',5) + " ns");
    }

    // simple enable/disable  
    if ( i >=0 && i < 32 ) {
      ui->TDS_chan_enable_list_widget_1->addItem("");
      ui->TDS_chan_enable_list_widget_1->item(i)
	->setSizeHint(QSize(ui->TDS_chan_enable_list_widget_1->item(i)->sizeHint().width(),14));
      ui->TDS_chan_enable_list_widget_1->item(i)->setSelected(channelEnableInitValue);      

      ui->TDS_chan_delay_list_widget_1->addItem("");
      ui->TDS_chan_delay_list_widget_1->setItemWidget(ui->TDS_chan_delay_list_widget_1->item(i), TDSChanDelay[i]);
      ui->TDS_chan_delay_list_widget_1->item(i)->setSizeHint(QSize(ui->TDS_chan_delay_list_widget_1->item(i)->sizeHint().width(),14));
    }
    else if ( i >=32 && i < 64 ) {
      ui->TDS_chan_enable_list_widget_2->addItem("");
      ui->TDS_chan_enable_list_widget_2->item(i-32)
        ->setSizeHint(QSize(ui->TDS_chan_enable_list_widget_2->item(i-32)->sizeHint().width(),14));
      ui->TDS_chan_enable_list_widget_2->item(i-32)->setSelected(channelEnableInitValue);

      ui->TDS_chan_delay_list_widget_2->addItem("");
      ui->TDS_chan_delay_list_widget_2->setItemWidget(ui->TDS_chan_delay_list_widget_2->item(i-32), TDSChanDelay[i]);
      ui->TDS_chan_delay_list_widget_2->item(i-32)->setSizeHint(QSize(ui->TDS_chan_delay_list_widget_2->item(i-32)->sizeHint().width(),14));
    }
    else if ( i >=64 && i < 96 ) {
      ui->TDS_chan_enable_list_widget_3->addItem("");
      ui->TDS_chan_enable_list_widget_3->item(i-64)
        ->setSizeHint(QSize(ui->TDS_chan_enable_list_widget_3->item(i-64)->sizeHint().width(),14));
      ui->TDS_chan_enable_list_widget_3->item(i-64)->setSelected(channelEnableInitValue);

      ui->TDS_chan_delay_list_widget_3->addItem("");
      ui->TDS_chan_delay_list_widget_3->setItemWidget(ui->TDS_chan_delay_list_widget_3->item(i-64), TDSChanDelay[i]);
      ui->TDS_chan_delay_list_widget_3->item(i-64)->setSizeHint(QSize(ui->TDS_chan_delay_list_widget_3->item(i-64)->sizeHint().width(),14));
    }
    else if ( i >=96 && i < 128 ) {
      ui->TDS_chan_enable_list_widget_4->addItem("");
      ui->TDS_chan_enable_list_widget_4->item(i-96)
        ->setSizeHint(QSize(ui->TDS_chan_enable_list_widget_4->item(i-96)->sizeHint().width(),14));
      ui->TDS_chan_enable_list_widget_4->item(i-96)->setSelected(channelEnableInitValue);

      if ( i < 104 ) {
	ui->TDS_chan_delay_list_widget_4->addItem("");
	ui->TDS_chan_delay_list_widget_4->setItemWidget(ui->TDS_chan_delay_list_widget_4->item(i-96), TDSChanDelay[i]);
	ui->TDS_chan_delay_list_widget_4->item(i-96)->setSizeHint(QSize(ui->TDS_chan_delay_list_widget_4->item(i-96)->sizeHint().width(),14));
      }
    }

    TDSChannelEnableValue[i]=channelEnableInitValue;
    TDSChannelDelayValue[i]=0;
  }

  ////////////////////////////////////////////////////////////////
  // widget connections for "all channel" buttons at the top
  ////////////////////////////////////////////////////////////////

  
  // enable
  connect(ui->TDSChanDelay_1,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelDelay(int)));
  connect(ui->TDSChanDelay_2,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelDelay(int)));
  connect(ui->TDSChanDelay_3,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelDelay(int)));
  connect(ui->TDSChanDelay_4,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelDelay(int)));

  // delay
  connect(ui->TDSChanEnable_1,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelEnable(int)));
  connect(ui->TDSChanEnable_2,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelEnable(int)));
  connect(ui->TDSChanEnable_3,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelEnable(int)));
  connect(ui->TDSChanEnable_4,  SIGNAL(currentIndexChanged(int)),  this, SLOT(updateTDSChannelEnable(int)));

  // enable
  connect(ui->TDS_chan_enable_list_widget_1, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualTDSChannelState()));
  connect(ui->TDS_chan_enable_list_widget_2, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualTDSChannelState()));
  connect(ui->TDS_chan_enable_list_widget_3, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualTDSChannelState()));
  connect(ui->TDS_chan_enable_list_widget_4, SIGNAL(itemSelectionChanged()), this, SLOT(updateIndividualTDSChannelState()));

  // delay
  for(int i = 0; i < 104; i++) {

    connect(TDSChanDelay[i], SIGNAL(currentIndexChanged(int)), this, SLOT(updateIndividualTDSChannelDelay()));

  }
}
// --------------------------------------------------------------------------- //
void MainWindow::updateGPIOChannelEnable(int index)
{
  ignore_gpio_channel_update = true;

  if(ui->GPIO_Dir_Ch_Enable == QObject::sender()){
    for(int j=0; j<32; j++){
      ui->GPIO_dir_chan_enable_list_widget->item(j)->setSelected(!index);
      GPIO_dir_chan_enable_val.at(j)=!index;
    }
  }

  if(ui->GPIO_Dout_Ch_Enable == QObject::sender()){
    for(int j=0; j<32; j++){
      ui->GPIO_dout_chan_enable_list_widget->item(j)->setSelected(!index);
      GPIO_dout_chan_enable_val.at(j)=!index;
    }
  }

  ignore_gpio_channel_update = false;

}
//-----------------------------------------------------------------------------//
void MainWindow::updateTDSChannelEnable(int index)
{
  ignore_tds_channel_update = true;

  if(ui->TDSChanEnable_1 == QObject::sender()){
    for(int j=0; j<32; j++){
      ui->TDS_chan_enable_list_widget_1->item(j)->setSelected(!index);
      TDSChannelEnableValue[j]=!index;
    }
  }
  else if(ui->TDSChanEnable_2 == QObject::sender()){
    for( int j=32; j<64; j++) {
      ui->TDS_chan_enable_list_widget_2->item(j-32)->setSelected(!index);
      TDSChannelEnableValue[j]=!index;
    }
  }
  else if(ui->TDSChanEnable_3 == QObject::sender()){
    for( int j=64; j<96; j++) {
      ui->TDS_chan_enable_list_widget_3->item(j-64)->setSelected(!index);
      TDSChannelEnableValue[j]=!index;
    }
  }
  else if(ui->TDSChanEnable_4 == QObject::sender()){
    for( int j=96; j<128; j++) {
      ui->TDS_chan_enable_list_widget_4->item(j-96)->setSelected(!index);
      TDSChannelEnableValue[j]=!index;
    }
  }

  ignore_tds_channel_update = false;

}
//---------------------------------------------------------------------------//
void MainWindow::updateTDSChannelDelay(int index)
{
  
  if (index > 0) index = index - 1;

  ignore_tds_channel_update = true;

  if(ui->TDSChanDelay_1 == QObject::sender()){
    for(int j=0; j<32; j++){
      TDSChanDelay[j]->setCurrentIndex(index);
      TDSChannelDelayValue[j]=index;
    }
  }
  else if(ui->TDSChanDelay_2 == QObject::sender()){
    for( int j=32; j<64; j++) {
      TDSChanDelay[j]->setCurrentIndex(index);
      TDSChannelDelayValue[j]=index;
    }
  }
  else if(ui->TDSChanDelay_3 == QObject::sender()){
    for( int j=64; j<96; j++) {
      TDSChanDelay[j]->setCurrentIndex(index);
      TDSChannelDelayValue[j]=index;
    }
  }
  else if(ui->TDSChanDelay_4 == QObject::sender()){
    for( int j=96; j<104; j++) {
      TDSChanDelay[j]->setCurrentIndex(index);
      TDSChannelDelayValue[j]=index;
    }
  }

  ignore_tds_channel_update = false;

}
// -------------------------------------------------------------------------- //
void MainWindow::updateIndividualGPIOChannelState()
{
  if(ignore_gpio_channel_update) return;

  // ---------------- GPIO enable ----------------- //
  if(ui->GPIO_dir_chan_enable_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      GPIO_dir_chan_enable_val[i] = ui->GPIO_dir_chan_enable_list_widget->item(i)->isSelected();
    }
  }

  if(ui->GPIO_dout_chan_enable_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      GPIO_dout_chan_enable_val[i] = ui->GPIO_dout_chan_enable_list_widget->item(i)->isSelected();
    }
  }

}
// ------------------------------------------------------------------------- //
void MainWindow::updateIndividualTDSChannelState()
{
  if(ignore_tds_channel_update) return;

  // ---------------- TDS enable ----------------- //
  if(ui->TDS_chan_enable_list_widget_1 == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      TDSChannelEnableValue[i] = ui->TDS_chan_enable_list_widget_1->item(i)->isSelected();
    }
  }
  if(ui->TDS_chan_enable_list_widget_2 == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      TDSChannelEnableValue[i+32] = ui->TDS_chan_enable_list_widget_2->item(i)->isSelected();
    }
  }
  if(ui->TDS_chan_enable_list_widget_3 == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      TDSChannelEnableValue[i+64] = ui->TDS_chan_enable_list_widget_3->item(i)->isSelected();
    }
  }
  if(ui->TDS_chan_enable_list_widget_4 == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      TDSChannelEnableValue[i+96] = ui->TDS_chan_enable_list_widget_4->item(i)->isSelected();
    }
  }
}
// ------------------------------------------------------------------------- //
void MainWindow::updateIndividualTDSChannelDelay()
{
  if(ignore_tds_channel_update) return;

  // ***********************  Delay  ******************************** //
  for(int j=0;j<32;j++){
    if(ui->TDS_chan_delay_list_widget_1 == QObject::sender()){
      TDSChannelDelayValue[j]=TDSChanDelay[j]->currentIndex();
    }
    else if(ui->TDS_chan_delay_list_widget_2 == QObject::sender()){
      TDSChannelDelayValue[j+32]=TDSChanDelay[j+32]->currentIndex();
    }
    else if(ui->TDS_chan_delay_list_widget_3 == QObject::sender()){
      TDSChannelDelayValue[j+64]=TDSChanDelay[j+64]->currentIndex();
    }
    else if(ui->TDS_chan_delay_list_widget_4 == QObject::sender()){
      if ( j< 8 ) TDSChannelDelayValue[j+96]=TDSChanDelay[j+96]->currentIndex();
    }
  }
}
// ------------------------------------------------------------------------- //
void MainWindow::checkRequestedFile()
{
  stringstream sx;

  if(QObject::sender() == ui->configVMMXmlFilenameField) {
    ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: white; }");
    QString current_text = ui->configVMMXmlFilenameField->text();
    if(current_text=="") ui->load_config_vmm->setEnabled(false);
    else {
      QFile ftest(current_text);
      if(!ftest.exists()) {
	sx << "Configuration file (" << current_text.toStdString() << ") bad";
	msg()(sx); sx.str("");
	ui->load_config_vmm->setEnabled(false);

	ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: rgb(220, 0, 0); }"); 
	//delay();
	//delay();
	ui->configVMMXmlFilenameField->setStyleSheet("QLineEdit { background: white; }");
      }
      else {
	ui->load_config_vmm->setEnabled(true);
	ui->load_config_vmm->click();
      }
    }
  }
}
void MainWindow::updateVMMChannelState()
{
  ignore_channel_update = true;

  // ***********************  SC ALL *************************** //
  if(ui->SCLabel_2 == QObject::sender()) {
    for(int j = 0; j < 32; j++) {
      ui->sc_list_widget->item(j)->setSelected(!VMMSCBoolAll);
      VMMSCBool[j] = !VMMSCBoolAll;
      //cout << "VMMSCBool[" << j << "] " << VMMSCBool[j] << endl;
    }
    VMMSCBoolAll=!VMMSCBoolAll;
  }
  else if(ui->SCLabel2_2 == QObject::sender()) {
    for(int j = 32; j < 64; j++) {
      ui->sc_list_widget_2->item(j-32)->setSelected(!VMMSCBoolAll2);
      VMMSCBool[j] = !VMMSCBoolAll2;
      //cout << "VMMSCBool[" << j << "] " << VMMSCBool[j] << endl;
    }
    VMMSCBoolAll2=!VMMSCBoolAll2;
  }

  // ***********************  SL ALL *************************** //
  if(ui->SLLabel_2 == QObject::sender()) {
    for(int j = 0; j < 32; j++) {
      ui->sl_list_widget->item(j)->setSelected(!VMMSLBoolAll);
      VMMSLBool[j] = !VMMSLBoolAll;
      //cout << "VMMSLBool[" << j << "] " << VMMSLBool[j] << endl;
            
    }
    VMMSLBoolAll=!VMMSLBoolAll;
  }
  else if(ui->SLLabel2_2 == QObject::sender()) {
    for(int j = 32; j < 64; j++) {
      ui->sl_list_widget_2->item(j-32)->setSelected(!VMMSLBoolAll2);
      VMMSLBool[j] = !VMMSLBoolAll2;
      //cout << "VMMSLBool[" << j << "] " << VMMSLBool[j] << endl;
    }
    VMMSLBoolAll2=!VMMSLBoolAll2;
  }
    
  // ***********************  STH ALL *************************** //
  if(ui->STHLabel_2 == QObject::sender()) {
    for(int j = 0; j < 32; j++) {
      ui->sth_list_widget->item(j)->setSelected(!VMMSTHBoolAll);
      VMMSTHBool[j]=!VMMSTHBoolAll;
      //cout << "VMMSTHBool[" << j << "] " << VMMSTHBool[j] << endl;
    }
    VMMSTHBoolAll=!VMMSTHBoolAll;
  }
  else if(ui->STHLabel2_2 == QObject::sender()) {
    for(int j = 32; j < 64; j++) {
      ui->sth_list_widget_2->item(j-32)->setSelected(!VMMSTHBoolAll2);
      VMMSTHBool[j]=!VMMSTHBoolAll2;

      //cout << "VMMSTHBool[" << j << "] " << VMMSTHBool[j] << endl;
    }
    VMMSTHBoolAll2=!VMMSTHBoolAll2;
  }

  // ***********************  ST ALL *************************** //
  if(ui->STLabel_2 == QObject::sender()) {
    for(int j = 0; j < 32; j++) {
      ui->st_list_widget->item(j)->setSelected(!VMMSTBoolAll);
      VMMSTBool[j]=!VMMSTBoolAll;
      //cout << "VMMSTBool[" << j << "] " << VMMSTBool[j] << endl;
    }
    VMMSTBoolAll=!VMMSTBoolAll;
  } 
  if(ui->STLabel2_2 == QObject::sender()) {
    for(int j = 32; j < 64; j++) {
      ui->st_list_widget_2->item(j-32)->setSelected(!VMMSTBoolAll2);
      VMMSTBool[j]=!VMMSTBoolAll2;
      //cout << "VMMSTBool[" << j << "] " << VMMSTBool[j] << endl;
    }
    VMMSTBoolAll2=!VMMSTBoolAll2;
  } 
  // ***********************  SM ALL *************************** //
  if(ui->SMLabel_2 == QObject::sender()) {
    for(int j = 0; j < 32; j++) {
      ui->sm_list_widget->item(j)->setSelected(!VMMSMBoolAll);
      VMMSMBool[j]=!VMMSMBoolAll;
      //cout << "VMMSMBool[" << j << "] " << VMMSMBool[j] << endl;
    }
    VMMSMBoolAll=!VMMSMBoolAll;
  }
  if(ui->SMLabel2_2 == QObject::sender()) {
    for(int j = 32; j < 64; j++) {
      ui->sm_list_widget_2->item(j-32)->setSelected(!VMMSMBoolAll2);
      VMMSMBool[j]=!VMMSMBoolAll2;
      //cout << "VMMSMBool[" << j << "] " << VMMSMBool[j] << endl;
    }
    VMMSMBoolAll2=!VMMSMBoolAll2;
  }

  // ***********************  SMX ALL *************************** //
  if(ui->SMXLabel_2 == QObject::sender()) {
    for(int j = 0; j < 32; j++) {
      ui->smx_list_widget->item(j)->setSelected(!VMMSMXBoolAll);
      VMMSMXBool[j]=!VMMSMXBoolAll;
      //cout << "VMMSMXBool[" << j << "] " << VMMSMXBool[j] << endl;
    }
    VMMSMXBoolAll=!VMMSMXBoolAll;
  }
  if(ui->SMXLabel2_2 == QObject::sender()) {
    for(int j = 32; j < 64; j++) {
      ui->smx_list_widget_2->item(j-32)->setSelected(!VMMSMXBoolAll2);
      VMMSMXBool[j]=!VMMSMXBoolAll2;
      //cout << "VMMSMXBool[" << j << "] " << VMMSMXBool[j] << endl;
    }
    VMMSMXBoolAll2=!VMMSMXBoolAll2;
  }
  ignore_channel_update = false;
}

// ------------------------------------------------------------------------- //
void MainWindow::updateIndividualVMMChannelState()
{
  if(ignore_channel_update) return;
  vector<QListWidget*> channel_list_widgets;
  channel_list_widgets.push_back(ui->sc_list_widget);
  channel_list_widgets.push_back(ui->sl_list_widget);
  channel_list_widgets.push_back(ui->sth_list_widget);
  channel_list_widgets.push_back(ui->st_list_widget);
  channel_list_widgets.push_back(ui->sm_list_widget);

  channel_list_widgets.push_back(ui->sc_list_widget_2);
  channel_list_widgets.push_back(ui->sl_list_widget_2);
  channel_list_widgets.push_back(ui->sth_list_widget_2);
  channel_list_widgets.push_back(ui->st_list_widget_2);
  channel_list_widgets.push_back(ui->sm_list_widget_2);

  channel_list_widgets.push_back(ui->smx_list_widget);
  channel_list_widgets.push_back(ui->smx_list_widget_2);

  //cout << "individual \n" << endl;
  // ---------------- SC ----------------- //
  if(ui->sc_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      VMMSCBool[i] = ui->sc_list_widget->item(i)->isSelected();
      //cout << "VMMSCBool[" << i << "] : " << VMMSCBool[i] << endl;
    }
  }
  else if(ui->sc_list_widget_2 == QObject::sender()) {
    for(int i = 32; i < 64; i++) {
      VMMSCBool[i] = ui->sc_list_widget_2->item(i-32)->isSelected();
      //cout << "VMMSCBool[" << i << "] : " << VMMSCBool[i] << endl;
    }
  }
  // ---------------- SL ----------------- //
  if(ui->sl_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      VMMSLBool[i] = ui->sl_list_widget->item(i)->isSelected();
      //cout << "VMMSLBool[" << i << "] : " << VMMSLBool[i] << endl;
    }
  }
  else if(ui->sl_list_widget_2 == QObject::sender()) {
    for(int i = 32; i < 64; i++) {
      VMMSLBool[i] = ui->sl_list_widget_2->item(i-32)->isSelected();
      //cout << "VMMSLBool[" << i << "] : " << VMMSLBool[i] << endl;
    }
  }
  // ---------------- STH ----------------- //
  if(ui->sth_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      VMMSTHBool[i] = ui->sth_list_widget->item(i)->isSelected();
      //cout << "VMMSTHBool[" << i << "] : " << VMMSTHBool[i] << endl;
    }
  }
  else if(ui->sth_list_widget_2 == QObject::sender()) {
    for(int i = 32; i < 64; i++) {
      VMMSTHBool[i] = ui->sth_list_widget_2->item(i-32)->isSelected();
      //cout << "VMMSTHBool[" << i << "] : " << VMMSTHBool[i] << endl;
    }
  }
  // ---------------- ST ----------------- //
  if(ui->st_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      VMMSTBool[i] = ui->st_list_widget->item(i)->isSelected();
      //cout << "VMMSTBool[" << i << "] : " << VMMSTBool[i] << endl;
    }
  }
  else if(ui->st_list_widget_2 == QObject::sender()) {
    for(int i = 32; i < 64; i++) {
      VMMSTBool[i] = ui->st_list_widget_2->item(i-32)->isSelected();
      //cout << "VMMSTBool[" << i << "] : " << VMMSTBool[i] << endl;
    }
  }
  // ---------------- SM ----------------- //
  if(ui->sm_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      VMMSMBool[i] = ui->sm_list_widget->item(i)->isSelected();
      //cout << "VMMSMBool[" << i << "] : " << VMMSMBool[i] << endl;
    }
  }
  else if(ui->sm_list_widget_2 == QObject::sender()) {
    for(int i = 32; i < 64; i++) {
      VMMSMBool[i] = ui->sm_list_widget_2->item(i-32)->isSelected();
      //cout << "VMMSMBool[" << i << "] : " << VMMSMBool[i] << endl;
    }
  }
  // ---------------- SMX ----------------- //
  if(ui->smx_list_widget == QObject::sender()) {
    for(int i = 0; i < 32; i++) {
      VMMSMXBool[i] = ui->smx_list_widget->item(i)->isSelected();
      //cout << "VMMSMXBool[" << i << "] : " << VMMSMXBool[i] << endl;
    }
  }
  else if(ui->smx_list_widget_2 == QObject::sender()) {
    for(int i = 32; i < 64; i++) {
      VMMSMXBool[i] = ui->smx_list_widget_2->item(i-32)->isSelected();
      //cout << "VMMSMXBool[" << i << "] : " << VMMSMXBool[i] << endl;
    }
  }


}
// ------------------------------------------------------------------------- //
void MainWindow::updateVMMChannelADCs(int index)
{
  // ***********************  SD  ******************************* //
  for(int j=0;j<32;j++){
    if(ui->SZ010bLabel_2 == QObject::sender()){
      VMMSZ010bCBox[j]->setCurrentIndex(index);
      VMMSZ010bValue[j]=index;
      //cout << "SZ10[" << j << "] : " << VMMSZ010bValue[j] << endl;
    }
    if(ui->SZ08bLabel_2 == QObject::sender()){
      VMMSZ08bCBox[j]->setCurrentIndex(index);
      VMMSZ08bValue[j]=index;
      //cout << "SZ8[" << j << "] : " << VMMSZ08bValue[j] << endl;
    }
    if(ui->SZ06bLabel_2 == QObject::sender()){
      VMMSZ06bCBox[j]->setCurrentIndex(index);
      VMMSZ06bValue[j]=index;
      //cout << "SZ6[" << j << "] : " << VMMSZ06bValue[j] << endl;
    }
  }
  for(int j=32;j<64;j++){
    if(ui->SZ010bLabel2_2 == QObject::sender()){
      VMMSZ010bCBox[j]->setCurrentIndex(index);
      VMMSZ010bValue[j]=index;
      //cout << "SZ10[" << j << "] : " << VMMSZ010bValue[j] << endl;
    }
    if(ui->SZ08bLabel2_2 == QObject::sender()){
      VMMSZ08bCBox[j]->setCurrentIndex(index);
      VMMSZ08bValue[j]=index;
      //cout << "SZ8[" << j << "] : " << VMMSZ08bValue[j] << endl;
    }
    if(ui->SZ06bLabel2_2 == QObject::sender()){
      VMMSZ06bCBox[j]->setCurrentIndex(index);
      VMMSZ06bValue[j]=index;
      //cout << "SZ6[" << j << "] : " << VMMSZ06bValue[j] << endl;
    }
  }

  for(int i=0;i<64;i++){
    if(VMMSZ010bCBox[i] == QObject::sender()){
      VMMSZ010bValue[i]=index;
      //cout << "combo SZ10[" << i << "] : " << VMMSZ010bValue[i] << endl;
    }
    if(VMMSZ08bCBox[i] == QObject::sender()){
      VMMSZ08bValue[i]=index;
      //cout << "combo SZ8[" << i << "] : " << VMMSZ08bValue[i] << endl;
    }
    if(VMMSZ06bCBox[i] == QObject::sender()){
      VMMSZ06bValue[i]=index;
      //cout << "combo SZ6[" << i << "] : " << VMMSZ06bValue[i] << endl;
    }
  }


}
// ------------------------------------------------------------------------- //
void MainWindow::updateVMMChannelVoltages(int index)
{
  // ***********************  SD  ******************************** //
  if(ui->SDLabel_2 == QObject::sender()){
    for(int j=0;j<32;j++){
      VMMSDVoltage[j]->setCurrentIndex(index);
      VMMSDValue[j]=index;
      //cout << "  VMMSDValue[" << j << "] : " << VMMSDValue[j] << endl;
    }
  }
  if(ui->SDLabel2_2 == QObject::sender()){
    for(int j=32;j<64;j++){
      VMMSDVoltage[j]->setCurrentIndex(index);
      VMMSDValue[j]=index;
      //cout << "  VMMSDValue[" << j << "] : " << VMMSDValue[j] << endl;
    }
  }
  for(int i=0;i<64;i++){
    if(VMMSDVoltage[i] == QObject::sender()){
      VMMSDValue[i]=index;
      //cout << "i VMMSDValue[" << i << "] : " << VMMSDValue[i] << endl;
    }
  }

}

void MainWindow::LoadVMMConfig_to_Handler(){

  ///////////////////////////////////////////////////////
  // grab the global register values from the ui
  ///////////////////////////////////////////////////////
  VMMGlobalSetting global;
  //bool ok;

  global.header          = ui->VMM_config_header->text().toStdString();
  global.board_Type      = ui->VMM_board_Type->currentIndex();
  global.boardID         = ui->VMM_boardID->value();
  global.ASIC_Type       = ui->VMM_ASIC_Type->currentIndex();
  global.vmmmask         = Convert_VMM_Mask_to_Value(); //ui->VMM_config_mask->value();
  global.command         = ui->VMM_config_command->text().toStdString();

  global.sp               = ui->global_sp->currentIndex();
  global.sdp              = ui->global_sdp->currentIndex();
  global.sbmx             = ui->global_sbmx->currentIndex();
  global.sbft             = ui->global_sbft->currentIndex();
  global.sbfp             = ui->global_sbfp->currentIndex();
  global.sbfm             = ui->global_sbfm->currentIndex();
  global.slg              = ui->global_slg->currentIndex();
  global.sm5              = ui->global_sm5->currentIndex();
  global.scmx             = ui->global_scmx->currentIndex();
  global.sfa              = ui->global_sfa->currentIndex();
  global.sfam             = ui->global_sfam->currentIndex();
  global.st               = ui->global_st->currentIndex();
  global.sfm              = ui->global_sfm->currentIndex();
  global.sg               = ui->global_sg->currentIndex();
  global.sng              = ui->global_sng->currentIndex();
  global.stot             = ui->global_stot->currentIndex();
  global.stpp             = ui->global_stpp->currentIndex();
  global.sttt             = ui->global_sttt->currentIndex();
  global.ssh              = ui->global_ssh->currentIndex();
  global.stc              = ui->global_stc->currentIndex();
  global.sdt_dac          = ui->global_sdt_dac->value();
  global.sdp_dac          = ui->global_sdp_dac->value();
  global.sc10b            = ui->global_sc10b->currentIndex();
  global.sc8b             = ui->global_sc8b->currentIndex();
  global.sc6b             = ui->global_sc6b->currentIndex();
  global.s8b              = ui->global_s8b->currentIndex();
  global.s6b              = ui->global_s6b->currentIndex();
  global.s10b             = ui->global_s10b->currentIndex();
  global.sdcks            = ui->global_sdcks->currentIndex();
  global.sdcka            = ui->global_sdcka->currentIndex();
  global.sdck6b           = ui->global_sdck6b->currentIndex();
  global.sdrv             = ui->global_sdrv->currentIndex();
  global.slvs             = ui->global_slvs->currentIndex();
  global.stcr             = ui->global_stcr->currentIndex();
  global.ssart            = ui->global_ssart->currentIndex();
  global.s32              = ui->global_s32->currentIndex();
  global.stlc             = ui->global_stlc->currentIndex();
  global.srec             = ui->global_srec->currentIndex();
  global.sbip             = ui->global_sbip->currentIndex();
  global.srat             = ui->global_srat->currentIndex();
  global.sfrst            = ui->global_sfrst->currentIndex();
  global.slvsbc           = ui->global_slvsbc->currentIndex();
  global.slvstp           = ui->global_slvstp->currentIndex();
  global.slvstk           = ui->global_slvstk->currentIndex();
  global.slvsdt           = ui->global_slvsdt->currentIndex();
  global.slvsart          = ui->global_slvsart->currentIndex();
  global.slvstki          = ui->global_slvstki->currentIndex();
  global.slvsena          = ui->global_slvsena->currentIndex();
  global.slvs6b           = ui->global_slvs6b->currentIndex();
  global.sL0enaV          = ui->global_sL0enaV->currentIndex();
  global.reset            = ui->global_reset->currentIndex();
  global.sL0ena           = ui->global_sL0ena->currentIndex();
  global.l0offset         = ui->global_l0offset->value();
  global.offset           = ui->global_offset->value();
  global.rollover         = ui->global_rollover->value();
  global.window           = ui->global_window->value();
  global.truncate         = ui->global_truncate->value();
  global.nskip            = ui->global_nskip->value();
  global.sL0cktest        = ui->global_sL0cktest->currentIndex();
  global.sL0ckinv         = ui->global_sL0ckinv->currentIndex();
  global.sL0dckinv        = ui->global_sL0dckinv->currentIndex();
  global.nskipm           = ui->global_nskipm->currentIndex();

  ///////////////////////////////////////////////////////////
  // grab the channel registers
  ///////////////////////////////////////////////////////////
  vector<VMM_Channel> channels;
  for(int i = 0; i < 64; i++) {
    VMM_Channel ch;
    ch.number       = i;
    ch.sc           = VMMSCBool[i];
    ch.sl           = VMMSLBool[i];
    ch.st           = VMMSTBool[i];
    ch.sth          = VMMSTHBool[i];
    ch.sm           = VMMSMBool[i];
    ch.sd           = VMMSDValue[i];
    ch.smx          = VMMSMXBool[i];
    ch.sz10b        = VMMSZ010bValue[i];
    ch.sz8b         = VMMSZ08bValue[i];
    ch.sz6b         = VMMSZ06bValue[i];
    channels.push_back(ch);
  } // i


    // load them into the config_handler
  VMMConfigHandle().LoadVMMChipConfiguration(global, channels);
  // VMMConfigHandle().LoadFPGAClocksConfiguration(clocks_current);
  //  VMMConfigHandle().LoadTDAQConfiguration(daq);

}
void MainWindow::LoadBoardConfig_to_Handler(){

  ///////////////////////////////////////////////////////                                                                                                 
  // grab the global register values from the ui                                                                                                          
  ///////////////////////////////////////////////////////
                                                                                                 
  BoardSetting board;
  //bool ok;

  board.boardType = ui->FE_Board_Type->currentIndex();
  board.boardID = ui->FE_Board_ID->value();
  board.boardUN = ui->FE_Board_UniqueS->text();
  board.ROC_UniqueS = ui->FE_Board_ROC_UniqueS->text();

  //std::cout << board.ROC_UniqueS.toStdString() << " " << ui->FE_Board_ROC_UniqueS->text().toStdString() << std::endl;

  int nVMM = 0;
  int nTDS = 0;

  std::vector<int> VMMChip_ID;
  std::vector<int> TDSChip_ID;
  QStringList VMM_UniqueS_Numbers;
  QStringList TDS_UniqueS_Numbers;

  VMMChip_ID.clear();
  TDSChip_ID.clear();
  VMM_UniqueS_Numbers.clear();
  TDS_UniqueS_Numbers.clear();

  if ( ui->vmm_select_1->isChecked() ) {
    VMMChip_ID.push_back(1);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_1->text());
    nVMM++;
  }
  if ( ui->vmm_select_2->isChecked() ) {
    VMMChip_ID.push_back(2);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_2->text());
    nVMM++;
  }
  if ( ui->vmm_select_3->isChecked() ) {
    VMMChip_ID.push_back(3);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_3->text());
    nVMM++;
  }
  if ( ui->vmm_select_4->isChecked() ) {
    VMMChip_ID.push_back(4);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_4->text());
    nVMM++;
  }
  if ( ui->vmm_select_5->isChecked() ) {
    VMMChip_ID.push_back(5);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_5->text());
    nVMM++;
  }
  if ( ui->vmm_select_6->isChecked() ) {
    VMMChip_ID.push_back(6);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_6->text());
    nVMM++;
  }
  if ( ui->vmm_select_7->isChecked() ) {
    VMMChip_ID.push_back(7);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_7->text());
    nVMM++;
  }
  if ( ui->vmm_select_8->isChecked() ) {
    VMMChip_ID.push_back(8);
    VMM_UniqueS_Numbers.push_back(ui->VMM_UniqueS_8->text());
    nVMM++;
  }

  if ( ui->tds_select_1->isChecked() ) {
    TDSChip_ID.push_back(1);
    TDS_UniqueS_Numbers.push_back(ui->TDS_UniqueS_1->text());
    nTDS++;
  }
  if ( ui->tds_select_2->isChecked() ) {
    TDSChip_ID.push_back(2);
    TDS_UniqueS_Numbers.push_back(ui->TDS_UniqueS_2->text());
    nTDS++;
  }
  if ( ui->tds_select_3->isChecked() ) {
    TDSChip_ID.push_back(3);
    TDS_UniqueS_Numbers.push_back(ui->TDS_UniqueS_3->text());
    nTDS++;
  }
  if ( ui->tds_select_4->isChecked() ) {
    TDSChip_ID.push_back(4);
    TDS_UniqueS_Numbers.push_back(ui->TDS_UniqueS_4->text());
    nTDS++;
  } 

  board.nVMM = nVMM;
  board.nTDS = nTDS;

  board.VMMChip_ID = VMMChip_ID;
  board.TDSChip_ID = TDSChip_ID;
  board.VMM_UniqueS_Numbers = VMM_UniqueS_Numbers;
  board.TDS_UniqueS_Numbers = TDS_UniqueS_Numbers;

  board.GPIO_dir_enable  = GPIO_dir_chan_enable_val;
  board.GPIO_dout_enable = GPIO_dout_chan_enable_val;

  BoardConfigHandle().LoadBoardConfiguration(board);
  //std::cout << BoardConfigHandle().BoardSettings().ROC_UniqueS.toStdString() << std::endl;

}
void MainWindow::InitRocGUI() {

  SROC0_VMM_Ena.resize(0);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_0);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_1);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_2);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_3);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_4);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_5);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_6);
  SROC0_VMM_Ena.push_back(ui->SROC0_vmm_7);

  for ( uint i=0; i<SROC0_VMM_Ena.size(); i++ ) {
    SROC0_VMM_Ena.at(i)->setAutoExclusive(false);
  }

  SROC1_VMM_Ena.resize(0);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_0);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_1);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_2);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_3);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_4);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_5);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_6);
  SROC1_VMM_Ena.push_back(ui->SROC1_vmm_7);

  for (uint i=0; i<SROC1_VMM_Ena.size(); i++ ) {
    SROC1_VMM_Ena.at(i)->setAutoExclusive(false);
  }

  SROC2_VMM_Ena.resize(0);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_0);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_1);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_2);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_3);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_4);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_5);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_6);
  SROC2_VMM_Ena.push_back(ui->SROC2_vmm_7);

  for (uint i=0; i<SROC2_VMM_Ena.size(); i++ ) {
    SROC2_VMM_Ena.at(i)->setAutoExclusive(false);
  }

  SROC3_VMM_Ena.resize(0);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_0);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_1);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_2);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_3);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_4);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_5);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_6);
  SROC3_VMM_Ena.push_back(ui->SROC3_vmm_7);

  for (uint i=0; i<SROC3_VMM_Ena.size(); i++ ) {
    SROC3_VMM_Ena.at(i)->setAutoExclusive(false);
  }

  ROC_VMM_Ena.resize(0);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_0);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_1);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_2);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_3);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_4);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_5);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_6);
  ROC_VMM_Ena.push_back(ui->ROC_vmm_ena_7);

  for (uint i=0; i<ROC_VMM_Ena.size(); i++ ) {
    ROC_VMM_Ena.at(i)->setAutoExclusive(false);
  }

  SROC_EOP_Ena.resize(0);
  SROC_EOP_Ena.push_back(ui->SROC0_EOP_Ena);
  SROC_EOP_Ena.push_back(ui->SROC1_EOP_Ena);
  SROC_EOP_Ena.push_back(ui->SROC2_EOP_Ena);
  SROC_EOP_Ena.push_back(ui->SROC3_EOP_Ena);

  for (uint i=0; i<SROC_EOP_Ena.size(); i++ ) {
    SROC_EOP_Ena.at(i)->setAutoExclusive(false);
  }

  SROC_NullEvt_Ena.resize(0);
  SROC_NullEvt_Ena.push_back(ui->SROC0_NullEvt_Ena);
  SROC_NullEvt_Ena.push_back(ui->SROC1_NullEvt_Ena);
  SROC_NullEvt_Ena.push_back(ui->SROC2_NullEvt_Ena);
  SROC_NullEvt_Ena.push_back(ui->SROC3_NullEvt_Ena);

  for (uint i=0; i<SROC_NullEvt_Ena.size(); i++ ) {
    SROC_NullEvt_Ena.at(i)->setAutoExclusive(false);
  }

  SROC_Ena.resize(0);
  SROC_Ena.push_back(ui->SROC0_Ena);
  SROC_Ena.push_back(ui->SROC1_Ena);
  SROC_Ena.push_back(ui->SROC2_Ena);
  SROC_Ena.push_back(ui->SROC3_Ena);

  for (uint i=0; i<SROC_Ena.size(); i++ ) {
    SROC_Ena.at(i)->setAutoExclusive(false);
  }

  SROC_TDC_Ena.resize(0);
  SROC_TDC_Ena.push_back(ui->SROC0_TDC_Ena);
  SROC_TDC_Ena.push_back(ui->SROC1_TDC_Ena);
  SROC_TDC_Ena.push_back(ui->SROC2_TDC_Ena);
  SROC_TDC_Ena.push_back(ui->SROC3_TDC_Ena);

  for (uint i=0; i<SROC_TDC_Ena.size(); i++ ) {
    SROC_TDC_Ena.at(i)->setAutoExclusive(false);
  }

  SROC_Busy_Ena.resize(0);
  SROC_Busy_Ena.push_back(ui->SROC0_Busy_Ena);
  SROC_Busy_Ena.push_back(ui->SROC1_Busy_Ena);
  SROC_Busy_Ena.push_back(ui->SROC2_Busy_Ena);
  SROC_Busy_Ena.push_back(ui->SROC3_Busy_Ena);

  for (uint i=0; i<SROC_Busy_Ena.size(); i++ ) {
    SROC_Busy_Ena.at(i)->setAutoExclusive(false);
  }

  ROC_fake_vmm_failure.resize(0);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_0);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_1);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_2);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_3);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_4);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_5);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_6);
  ROC_fake_vmm_failure.push_back(ui->ROC_fake_vmm_failure_7);

  for (uint i=0; i<ROC_fake_vmm_failure.size(); i++ ) {
    ROC_fake_vmm_failure.at(i)->setAutoExclusive(false);
  }

  //-------------------------------------------------//

  VMM0_epll_phase_ena.resize(0);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_0);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_1);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_2);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_3);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_4);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_5);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_6);
  VMM0_epll_phase_ena.push_back(ui->VMM0_epll_phase_ena_7);

  VMM0_ctrl_delay_ena.resize(0);
  VMM0_ctrl_delay_ena.push_back(ui->VMM0_ctrl_delay_ena_0);
  VMM0_ctrl_delay_ena.push_back(ui->VMM0_ctrl_delay_ena_1);
  VMM0_ctrl_delay_ena.push_back(ui->VMM0_ctrl_delay_ena_2);
  VMM0_ctrl_delay_ena.push_back(ui->VMM0_ctrl_delay_ena_3);

  VMM0_ctrl_bypass_ena.resize(0);
  VMM0_ctrl_bypass_ena.push_back(ui->VMM0_ctrl_bypass_ena_0);
  VMM0_ctrl_bypass_ena.push_back(ui->VMM0_ctrl_bypass_ena_1);
  VMM0_ctrl_bypass_ena.push_back(ui->VMM0_ctrl_bypass_ena_2);
  VMM0_ctrl_bypass_ena.push_back(ui->VMM0_ctrl_bypass_ena_3);

  VMM0_TP_bypass_ena.resize(0);
  VMM0_TP_bypass_ena.push_back(ui->VMM0_TP_bypass_ena_0);
  VMM0_TP_bypass_ena.push_back(ui->VMM0_TP_bypass_ena_1);
  VMM0_TP_bypass_ena.push_back(ui->VMM0_TP_bypass_ena_2);
  VMM0_TP_bypass_ena.push_back(ui->VMM0_TP_bypass_ena_3);

  VMM0_TX_ena.resize(0);
  VMM0_TX_ena.push_back(ui->VMM0_TX_ena_0);
  VMM0_TX_ena.push_back(ui->VMM0_TX_ena_1);
  VMM0_TX_ena.push_back(ui->VMM0_TX_ena_2);
  VMM0_TX_ena.push_back(ui->VMM0_TX_ena_3);

  VMM0_TX_cSEL.resize(0);
  VMM0_TX_cSEL.push_back(ui->VMM0_TX_cSEL_0);
  VMM0_TX_cSEL.push_back(ui->VMM0_TX_cSEL_1);
  VMM0_TX_cSEL.push_back(ui->VMM0_TX_cSEL_2);
  VMM0_TX_cSEL.push_back(ui->VMM0_TX_cSEL_3);

  for (uint i=0; i<VMM0_epll_phase_ena.size(); i++ ) {
    VMM0_epll_phase_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM0_ctrl_delay_ena.size(); i++ ) {
    VMM0_ctrl_delay_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM0_ctrl_bypass_ena.size(); i++ ) {
    VMM0_ctrl_bypass_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM0_TP_bypass_ena.size(); i++ ) {
    VMM0_TP_bypass_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM0_TX_ena.size(); i++ ) {
    VMM0_TX_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM0_TX_cSEL.size(); i++ ) {
    VMM0_TX_cSEL.at(i)->setAutoExclusive(false);
  }

  //------------------------------------------------//

  VMM1_epll_phase_ena.resize(0);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_0);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_1);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_2);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_3);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_4);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_5);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_6);
  VMM1_epll_phase_ena.push_back(ui->VMM1_epll_phase_ena_7);

  VMM1_ctrl_delay_ena.resize(0);
  VMM1_ctrl_delay_ena.push_back(ui->VMM1_ctrl_delay_ena_0);
  VMM1_ctrl_delay_ena.push_back(ui->VMM1_ctrl_delay_ena_1);
  VMM1_ctrl_delay_ena.push_back(ui->VMM1_ctrl_delay_ena_2);
  VMM1_ctrl_delay_ena.push_back(ui->VMM1_ctrl_delay_ena_3);

  VMM1_ctrl_bypass_ena.resize(0);
  VMM1_ctrl_bypass_ena.push_back(ui->VMM1_ctrl_bypass_ena_0);
  VMM1_ctrl_bypass_ena.push_back(ui->VMM1_ctrl_bypass_ena_1);
  VMM1_ctrl_bypass_ena.push_back(ui->VMM1_ctrl_bypass_ena_2);
  VMM1_ctrl_bypass_ena.push_back(ui->VMM1_ctrl_bypass_ena_3);

  VMM1_TP_bypass_ena.resize(0);
  VMM1_TP_bypass_ena.push_back(ui->VMM1_TP_bypass_ena_0);
  VMM1_TP_bypass_ena.push_back(ui->VMM1_TP_bypass_ena_1);
  VMM1_TP_bypass_ena.push_back(ui->VMM1_TP_bypass_ena_2);
  VMM1_TP_bypass_ena.push_back(ui->VMM1_TP_bypass_ena_3);

  VMM1_TX_ena.resize(0);
  VMM1_TX_ena.push_back(ui->VMM1_TX_ena_0);
  VMM1_TX_ena.push_back(ui->VMM1_TX_ena_1);
  VMM1_TX_ena.push_back(ui->VMM1_TX_ena_2);
  VMM1_TX_ena.push_back(ui->VMM1_TX_ena_3);

  VMM1_TX_cSEL.resize(0);
  VMM1_TX_cSEL.push_back(ui->VMM1_TX_cSEL_0);
  VMM1_TX_cSEL.push_back(ui->VMM1_TX_cSEL_1);
  VMM1_TX_cSEL.push_back(ui->VMM1_TX_cSEL_2);
  VMM1_TX_cSEL.push_back(ui->VMM1_TX_cSEL_3);

  for (uint i=0; i<VMM1_epll_phase_ena.size(); i++ ) {
    VMM1_epll_phase_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM1_ctrl_delay_ena.size(); i++ ) {
    VMM1_ctrl_delay_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM1_ctrl_bypass_ena.size(); i++ ) {
    VMM1_ctrl_bypass_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM1_TP_bypass_ena.size(); i++ ) {
    VMM1_TP_bypass_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM1_TX_ena.size(); i++ ) {
    VMM1_TX_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<VMM1_TX_cSEL.size(); i++ ) {
    VMM1_TX_cSEL.at(i)->setAutoExclusive(false);
  }

  //-----------------------------------------------//

  TDS_epll_phase_ena.resize(0);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_0);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_1);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_2);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_3);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_4);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_5);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_6);
  TDS_epll_phase_ena.push_back(ui->TDS_epll_phase_ena_7);

  TDS_ctrl_delay_ena.resize(0);
  TDS_ctrl_delay_ena.push_back(ui->TDS_ctrl_delay_ena_0);
  TDS_ctrl_delay_ena.push_back(ui->TDS_ctrl_delay_ena_1);
  TDS_ctrl_delay_ena.push_back(ui->TDS_ctrl_delay_ena_2);
  TDS_ctrl_delay_ena.push_back(ui->TDS_ctrl_delay_ena_3);

  TDS_ctrl_bypass_ena.resize(0);
  TDS_ctrl_bypass_ena.push_back(ui->TDS_ctrl_bypass_ena_0);
  TDS_ctrl_bypass_ena.push_back(ui->TDS_ctrl_bypass_ena_1);
  TDS_ctrl_bypass_ena.push_back(ui->TDS_ctrl_bypass_ena_2);
  TDS_ctrl_bypass_ena.push_back(ui->TDS_ctrl_bypass_ena_3);

  TDS_TP_bypass_ena.resize(0);
  TDS_TP_bypass_ena.push_back(ui->TDS_TP_bypass_ena_0);
  TDS_TP_bypass_ena.push_back(ui->TDS_TP_bypass_ena_1);
  TDS_TP_bypass_ena.push_back(ui->TDS_TP_bypass_ena_2);
  TDS_TP_bypass_ena.push_back(ui->TDS_TP_bypass_ena_3);

  TDS_TX_ena.resize(0);
  TDS_TX_ena.push_back(ui->TDS_TX_ena_0);
  TDS_TX_ena.push_back(ui->TDS_TX_ena_1);
  TDS_TX_ena.push_back(ui->TDS_TX_ena_2);
  TDS_TX_ena.push_back(ui->TDS_TX_ena_3);

  TDS_TX_cSEL.resize(0);
  TDS_TX_cSEL.push_back(ui->TDS_TX_cSEL_0);
  TDS_TX_cSEL.push_back(ui->TDS_TX_cSEL_1);
  TDS_TX_cSEL.push_back(ui->TDS_TX_cSEL_2);
  TDS_TX_cSEL.push_back(ui->TDS_TX_cSEL_3);

  for (uint i=0; i<TDS_epll_phase_ena.size(); i++ ) {
    TDS_epll_phase_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<TDS_ctrl_delay_ena.size(); i++ ) {
    TDS_ctrl_delay_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<TDS_ctrl_bypass_ena.size(); i++ ) {
    TDS_ctrl_bypass_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<TDS_TP_bypass_ena.size(); i++ ) {
    TDS_TP_bypass_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<TDS_TX_ena.size(); i++ ) {
    TDS_TX_ena.at(i)->setAutoExclusive(false);
  }

  for (uint i=0; i<TDS_TX_cSEL.size(); i++ ) {
    TDS_TX_cSEL.at(i)->setAutoExclusive(false);
  }

  //-----------------------------------------------//

  ROC_int_epll_phase_ena.resize(0);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_0);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_1);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_2);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_3);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_4);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_5);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_6);
  ROC_int_epll_phase_ena.push_back(ui->ROC_int_epll_phase_ena_7);

  for (uint i=0; i<ROC_int_epll_phase_ena.size(); i++ ) {
    ROC_int_epll_phase_ena.at(i)->setAutoExclusive(false);
  }

  ui->SROC0_Eport0_Ena->setAutoExclusive(false);
  ui->SROC0_Eport1_Ena->setAutoExclusive(false);
  ui->SROC1_Eport0_Ena->setAutoExclusive(false);
  ui->SROC1_Eport1_Ena->setAutoExclusive(false);
  ui->SROC2_Eport0_Ena->setAutoExclusive(false);
  ui->SROC2_Eport1_Ena->setAutoExclusive(false);
  ui->SROC3_Eport0_Ena->setAutoExclusive(false);
  ui->SROC3_Eport1_Ena->setAutoExclusive(false);

}
void MainWindow::Display_Roc_Analog_VMM0_epll_Ena_Mask( int iMask, uint32_t mask ){

  if ( iMask == 0 ) {
    for ( uint i=0; i<VMM0_epll_phase_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM0_epll_phase_ena.at(i)->setChecked(true);
      }
      else {
	VMM0_epll_phase_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 1 ) {
    for ( uint i=0; i < VMM0_ctrl_delay_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM0_ctrl_delay_ena.at(i)->setChecked(true);
      }
      else {
        VMM0_ctrl_delay_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 2 ) {
    for ( uint i=0; i < VMM0_ctrl_bypass_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
	VMM0_ctrl_bypass_ena.at(i)->setChecked(true);
      }
      else {
	VMM0_ctrl_bypass_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 3 ) {
    for ( uint i=0; i < VMM0_TP_bypass_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
	VMM0_TP_bypass_ena.at(i)->setChecked(true);
      }
      else {
	VMM0_TP_bypass_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 4 ) {
    for ( uint i=0; i < VMM0_TX_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
	VMM0_TX_ena.at(i)->setChecked(true);
      }
      else {
	VMM0_TX_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 5 ) {
    for( int i=0; i < VMM0_TX_cSEL.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
	VMM0_TX_cSEL.at(i)->setChecked(true);
      }
      else {
	VMM0_TX_cSEL.at(i)->setChecked(false);
      }
    }
  }

}

void MainWindow::Display_Roc_Analog_VMM1_epll_Ena_Mask( int iMask, uint32_t mask ){

  if ( iMask == 0 ) {
    for ( uint i=0; i<VMM1_epll_phase_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM1_epll_phase_ena.at(i)->setChecked(true);
      }
      else {
        VMM1_epll_phase_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 1 ) {
    for ( uint i=0; i < VMM1_ctrl_delay_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM1_ctrl_delay_ena.at(i)->setChecked(true);
      }
      else {
        VMM1_ctrl_delay_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 2 ) {
    for ( uint i=0; i < VMM1_ctrl_bypass_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM1_ctrl_bypass_ena.at(i)->setChecked(true);
      }
      else {
        VMM1_ctrl_bypass_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 3 ) {
    for ( uint i=0; i < VMM1_TP_bypass_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM1_TP_bypass_ena.at(i)->setChecked(true);
      }
      else {
        VMM1_TP_bypass_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 4 ) {
    for ( uint i=0; i < VMM1_TX_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM1_TX_ena.at(i)->setChecked(true);
      }
      else {
        VMM1_TX_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 5 ) {
    for( int i=0; i < VMM1_TX_cSEL.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        VMM1_TX_cSEL.at(i)->setChecked(true);
      }
      else {
        VMM1_TX_cSEL.at(i)->setChecked(false);
      }
    }
  }

}

void MainWindow::Display_Roc_Analog_TDS_epll_Ena_Mask( int iMask, uint32_t mask ){

  if ( iMask == 0 ) {
    for ( uint i=0; i<TDS_epll_phase_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        TDS_epll_phase_ena.at(i)->setChecked(true);
      }
      else {
        TDS_epll_phase_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 1 ) {
    for ( uint i=0; i < TDS_ctrl_delay_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        TDS_ctrl_delay_ena.at(i)->setChecked(true);
      }
      else {
        TDS_ctrl_delay_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 2 ) {
    for ( uint i=0; i < TDS_ctrl_bypass_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        TDS_ctrl_bypass_ena.at(i)->setChecked(true);
      }
      else {
        TDS_ctrl_bypass_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 3 ) {
    for ( uint i=0; i < TDS_TP_bypass_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        TDS_TP_bypass_ena.at(i)->setChecked(true);
      }
      else {
        TDS_TP_bypass_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 4 ) {
    for ( uint i=0; i < TDS_TX_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        TDS_TX_ena.at(i)->setChecked(true);
      }
      else {
        TDS_TX_ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 5 ) {
    for( int i=0; i < TDS_TX_cSEL.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        TDS_TX_cSEL.at(i)->setChecked(true);
      }
      else {
        TDS_TX_cSEL.at(i)->setChecked(false);
      }
    }
  }

}

void MainWindow::Display_Roc_Analog_ROC_int_epll_Ena_Mask( int iMask, uint32_t mask ){

  if ( iMask == 0 ) {
    for ( uint i=0; i<ROC_int_epll_phase_ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = ith_mask << i;
      if ( mask & ith_mask ) {
        ROC_int_epll_phase_ena.at(i)->setChecked(true);
      }
      else {
        ROC_int_epll_phase_ena.at(i)->setChecked(false);
      }
    }
  }

}

uint32_t MainWindow::Roc_Analog_VMM0_epll_Ena_Mask( int iMask ){
  uint32_t mask = 0;

  if ( iMask == 0 ) {
    for ( uint i=0; i<VMM0_epll_phase_ena.size(); i++ ) {
      mask += ( VMM0_epll_phase_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 1 ) {
    for ( uint i=0; i < VMM0_ctrl_delay_ena.size(); i++ ) {
      mask += ( VMM0_ctrl_delay_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 2 ) {
    for ( uint i=0; i < VMM0_ctrl_bypass_ena.size(); i++ ) {
      mask += ( VMM0_ctrl_bypass_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 3 ) {
    for ( uint i=0; i < VMM0_TP_bypass_ena.size(); i++ ) {
      mask += ( VMM0_TP_bypass_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 4 ) {
    for ( uint i=0; i < VMM0_TX_ena.size(); i++ ) {
      mask += ( VMM0_TX_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 5 ) {
    for( uint i=0; i < VMM0_TX_cSEL.size(); i++ ) {
      mask += ( VMM0_TX_cSEL.at(i)->isChecked() << i );
    }
  }

  return mask;

}

uint32_t MainWindow::Roc_Analog_VMM1_epll_Ena_Mask( int iMask ){
  uint32_t mask = 0;

  if ( iMask == 0 ) {
    for ( uint i=0; i<VMM1_epll_phase_ena.size(); i++ ) {
      mask += ( VMM1_epll_phase_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==1 ) {
    for ( uint i=0; i < VMM1_ctrl_delay_ena.size(); i++ ) {
      mask += ( VMM1_ctrl_delay_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==2 ) {
    for( uint i=0; i < VMM1_ctrl_bypass_ena.size(); i++ ) {
      mask += ( VMM1_ctrl_bypass_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==3 ) {
    for( uint i=0; i < VMM1_TP_bypass_ena.size(); i++ ) {
      mask += ( VMM1_TP_bypass_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==4 ) {
    for( uint i=0; i < VMM1_TX_ena.size(); i++ ) {
      mask += ( VMM1_TX_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==5 ) {
    for( uint i=0; i < VMM1_TX_cSEL.size(); i++ ) {
      mask += ( VMM1_TX_cSEL.at(i)->isChecked() << i );
    }
  }

  return mask;

}

uint32_t MainWindow::Roc_Analog_TDS_epll_Ena_Mask( int iMask ){
  uint32_t mask = 0;

  if ( iMask == 0 ) {
    for ( uint i=0; i<TDS_epll_phase_ena.size(); i++ ) {
      mask += ( TDS_epll_phase_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==1 ) {
    for ( uint i=0; i < TDS_ctrl_delay_ena.size(); i++ ) {
      mask += ( TDS_ctrl_delay_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==2 ) {
    for( uint i=0; i < TDS_ctrl_bypass_ena.size(); i++ ) {
      mask += ( TDS_ctrl_bypass_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==3 ) {
    for( uint i=0; i < TDS_TP_bypass_ena.size(); i++ ) {
      mask += ( TDS_TP_bypass_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==4 ) {
    for( uint i=0; i < TDS_TX_ena.size(); i++ ) {
      mask += ( TDS_TX_ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask ==5 ) {
    for( uint i=0; i < TDS_TX_cSEL.size(); i++ ) {
      mask += ( TDS_TX_cSEL.at(i)->isChecked() << i );
    }
  }

  return mask;

}

uint32_t MainWindow::Roc_Analog_ROC_int_epll_Ena_Mask( int iMask ){
  uint32_t mask = 0;

  if ( iMask == 0 ) {
    for ( uint i=0; i<ROC_int_epll_phase_ena.size(); i++ ) {
      mask += ( ROC_int_epll_phase_ena.at(i)->isChecked() << i );
    }
  }

  return mask;

}

uint32_t MainWindow::Roc_Digital_Ena_Mask( int iMask ){
  uint32_t mask = 0;

  if ( iMask == 0 ) {
    for ( uint i=0; i<SROC0_VMM_Ena.size(); i++ ) {
      if ( SROC0_VMM_Ena.at(i)->isChecked() ) {
	mask += ( 0b1 << i );
      }
    }
  }
  if ( iMask == 1 ) {
    for( uint i=0; i<SROC1_VMM_Ena.size(); i++ ) {
      mask += ( SROC1_VMM_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 2 ) {
    for( uint i=0; i<SROC2_VMM_Ena.size(); i++ ) {
      mask += ( SROC2_VMM_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 3 ) {
    for( uint i=0; i<SROC3_VMM_Ena.size(); i++ ) {
      mask += ( SROC3_VMM_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 4 ) {
    for( uint i=0; i<ROC_VMM_Ena.size(); i++ ) {
      mask += ( ROC_VMM_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 5 ) {
    for( uint i=0; i<SROC_EOP_Ena.size(); i++ ) {
      if ( SROC_EOP_Ena.at(i)->isChecked() ) {
	mask += ( 0b1 << i );
      }
    }
  }
  if ( iMask == 6 ) {
    for( uint i=0; i<SROC_NullEvt_Ena.size(); i++ ) {
      mask += ( SROC_NullEvt_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 7 ) {
    for( uint i=0; i<SROC_Ena.size(); i++ ) {
      mask += ( SROC_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 8 ) {
    for( uint i=0; i<SROC_TDC_Ena.size(); i++ ) {
      mask += ( SROC_TDC_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 9 ) {
    for( uint i=0; i<SROC_Busy_Ena.size(); i++ ) {
      mask += ( SROC_Busy_Ena.at(i)->isChecked() << i );
    }
  }
  if ( iMask == 10 ) {
    for ( uint i=0; i<ROC_fake_vmm_failure.size(); i++ ) {
      mask += ( ROC_fake_vmm_failure.at(i)->isChecked() << i );
    }
  }

  //  std::cout << iMask << " " << mask << std::endl;

  return mask;

}

void MainWindow::Display_Roc_Digital_Ena_Mask( int iMask, uint32_t mask ){

  if ( iMask == 0 ) {
    for ( uint i=0; i<SROC0_VMM_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
	SROC0_VMM_Ena.at(i)->setChecked(true);
      }
      else {
        SROC0_VMM_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 1 ) {
    for( uint i=0; i<SROC1_VMM_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC1_VMM_Ena.at(i)->setChecked(true);
      }
      else {
        SROC1_VMM_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 2 ) {
    for( uint i=0; i<SROC2_VMM_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC2_VMM_Ena.at(i)->setChecked(true);
      }
      else {
        SROC2_VMM_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 3 ) {
    for( uint i=0; i<SROC3_VMM_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC3_VMM_Ena.at(i)->setChecked(true);
      }
      else {
        SROC3_VMM_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 4 ) {
    for( uint i=0; i<ROC_VMM_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        ROC_VMM_Ena.at(i)->setChecked(true);
      }
      else {
        ROC_VMM_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 5 ) {
    for( uint i=0; i<SROC_EOP_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC_EOP_Ena.at(i)->setChecked(true);
      }
      else {
        SROC_EOP_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 6 ) {
    for( uint i=0; i<SROC_NullEvt_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC_NullEvt_Ena.at(i)->setChecked(true);
      }
      else {
        SROC_NullEvt_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 7 ) {
    for( uint i=0; i<SROC_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC_Ena.at(i)->setChecked(true);
      }
      else {
        SROC_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 8 ) {
    for( uint i=0; i<SROC_TDC_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC_TDC_Ena.at(i)->setChecked(true);
      }
      else {
        SROC_TDC_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 9 ) {
    for( uint i=0; i<SROC_Busy_Ena.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        SROC_Busy_Ena.at(i)->setChecked(true);
      }
      else {
        SROC_Busy_Ena.at(i)->setChecked(false);
      }
    }
  }
  if ( iMask == 10 ) {
    for ( uint i=0; i<ROC_fake_vmm_failure.size(); i++ ) {
      uint32_t ith_mask = 0b1;
      ith_mask = 0b1 << i;
      if ( mask & ith_mask ) {
        ROC_fake_vmm_failure.at(i)->setChecked(true);
      }
      else {
        ROC_fake_vmm_failure.at(i)->setChecked(false);
      }
    }
  }

}

void MainWindow::Load_ROC_ASIC_Config_to_Handler(){

  ///////////////////////////////////////////////////////
  // grab the global register values from the ui         
  ///////////////////////////////////////////////////////

  ROC_ASIC_Setting global;
  //bool ok;

  global.header          = ui->ROC_ASIC_config_header->text().toStdString();
  global.board_Type      = ui->ROC_ASIC_board_Type->currentIndex();
  global.boardID         = ui->ROC_ASIC_boardID->value();
  global.ASIC_Type       = ui->ROC_ASIC_Type_2->currentIndex();
  global.chipID          = 0;
  global.command         = ui->ROC_ASIC_config_command->text().toStdString();
  global.UniqueS         = ui->ROC_ASIC_UniqueS->text().toStdString();

  //---------------------------------------------------//

  global.L1_first      = ui->ROC_digital_L1_first->value();
  global.even_parity   = ui->ROC_digital_parity->currentIndex();
  global.ROC_ID        = ui->ROC_digital_ROC_ID->value();

  global.elink_speed_sROC0 = ui->ROC_digital_elink_speed_0->currentIndex();
  global.elink_speed_sROC1 = ui->ROC_digital_elink_speed_1->currentIndex();
  global.elink_speed_sROC2 = ui->ROC_digital_elink_speed_2->currentIndex();
  global.elink_speed_sROC3 = ui->ROC_digital_elink_speed_3->currentIndex();

  global.vmm_mask_sROC0    = Roc_Digital_Ena_Mask( 0 );
  global.vmm_mask_sROC1    = Roc_Digital_Ena_Mask( 1 );
  global.vmm_mask_sROC2    = Roc_Digital_Ena_Mask( 2 );
  global.vmm_mask_sROC3    = Roc_Digital_Ena_Mask( 3 );

  global.EOP_enable_sROC_mask     = Roc_Digital_Ena_Mask( 5 );
  global.NullEvt_enable_sROC_mask = Roc_Digital_Ena_Mask( 6 );

  global.bypass           = ui->ROC_digital_bypass->currentIndex();
  global.timeout_ena      = ui->ROC_digital_timeout_ena->currentIndex();

  global.TTC_start_bits   = ui->ROC_digital_TTC_start_bit->value();

  global.enable_sROC_mask = Roc_Digital_Ena_Mask( 7 );

  global.vmm_enable_mask  = Roc_Digital_Ena_Mask( 4 );
  global.timeout          = ui->ROC_digital_timeout->value();

  global.TX_current       = ui->ROC_digital_TX_cSel->currentIndex();

  global.BC_offset        = ui->ROC_digital_BC_offset->value();
  global.BC_rollover      = ui->ROC_digital_BC_rollover->value();

  int mask=0;
  mask += ui->SROC0_Eport0_Ena->isChecked();
  mask += ui->SROC0_Eport1_Ena->isChecked() << 1;
  global.eport_sROC0 = mask;

  mask=0;
  mask += ui->SROC1_Eport0_Ena->isChecked();
  mask += ui->SROC1_Eport1_Ena->isChecked() << 1;
  global.eport_sROC1 = mask;

  mask=0;
  mask += ui->SROC2_Eport0_Ena->isChecked();
  mask += ui->SROC2_Eport1_Ena->isChecked() << 1;
  global.eport_sROC2 = mask;

  mask=0;
  mask += ui->SROC3_Eport0_Ena->isChecked();
  mask += ui->SROC3_Eport1_Ena->isChecked() << 1;
  global.eport_sROC3 = mask;

  global.fake_vmm_failure_mask = Roc_Digital_Ena_Mask( 10 );
  global.TDS_enable_sROC_mask  = Roc_Digital_Ena_Mask( 8 );
  global.BUSY_enable_sROC_mask = Roc_Digital_Ena_Mask( 9 );

  global.BUSY_On_Limit         = ui->ROC_digital_BUSY_On_Limit->value();
  global.BUSY_Off_Limit        = ui->ROC_digital_BUSY_Off_Limit->value();

  global.Max_L1_Events_no_comma = ui->ROC_digital_max_L1_evts->value();
  
  global.tp_bypass_global = ui->ROC_int_TP_bypass_global->currentIndex();
  global.tp_phase_global  = ui->ROC_int_TP_phase_global->value();
  global.LockOutInv  = ui->ROC_int_LockOutInv->currentIndex();
  global.testOutEn   = ui->ROC_int_TestOutEna->currentIndex();
  global.testOutMux  = ui->ROC_int_TestOutMux->currentIndex();
  global.VMM_BCR_INV = ui->ROC_int_VMM_BCR_INV->value();
  global.VMM_ENA_INV = ui->ROC_int_VMM_ENA_INV->value();
  global.VMM_L0_INV  = ui->ROC_int_VMM_L0_INV->value();
  global.VMM_TP_INV  = ui->ROC_int_VMM_TP_INV->value();
  global.TDS_BCR_INV = ui->ROC_int_TDS_BCR_INV->value();

  //----------------------------------------------------//

  ROC_ASIC_analog_epll epll_VMM0;

  epll_VMM0.ePllPhase40MHz_0  = ui->VMM0_ePllPhase40MHz_0->value();
  epll_VMM0.ePllPhase40MHz_1  = ui->VMM0_ePllPhase40MHz_1->value();
  epll_VMM0.ePllPhase40MHz_2  = ui->VMM0_ePllPhase40MHz_2->value();
  epll_VMM0.ePllPhase40MHz_3  = ui->VMM0_ePllPhase40MHz_3->value();

  epll_VMM0.ePllPhase160MHz_0 = ui->VMM0_ePllPhase160MHz_0->value();
  epll_VMM0.ePllPhase160MHz_1 = ui->VMM0_ePllPhase160MHz_1->value();
  epll_VMM0.ePllPhase160MHz_2 = ui->VMM0_ePllPhase160MHz_2->value();
  epll_VMM0.ePllPhase160MHz_3 = ui->VMM0_ePllPhase160MHz_3->value();

  epll_VMM0.ePllInstantLock       = ui->VMM0_ePll_Instant_Lock->currentIndex();
  epll_VMM0.ePllReset             = ui->VMM0_ePll_Reset->currentIndex();
  epll_VMM0.bypassPll             = ui->VMM0_bypass_Pll->currentIndex();
  epll_VMM0.ePllLockEn            = ui->VMM0_ePll_Lock_En->currentIndex();

  epll_VMM0.ePll_Ref_Freq     = ui->VMM0_ePll_Reference_Freq->value();
  epll_VMM0.ePllEnablePhase   = Roc_Analog_VMM0_epll_Ena_Mask(0);
  epll_VMM0.ePll_lcp          = ui->VMM0_ePll_lcp->value();
  epll_VMM0.ePll_cap          = ui->VMM0_ePll_cap->value();
  epll_VMM0.ePll_res          = ui->VMM0_ePll_res->value();

  epll_VMM0.tp_phase_0        = ui->VMM0_TP_phase_0->value();
  epll_VMM0.tp_phase_1        = ui->VMM0_TP_phase_1->value();
  epll_VMM0.tp_phase_2        = ui->VMM0_TP_phase_2->value();
  epll_VMM0.tp_phase_3        = ui->VMM0_TP_phase_3->value();
  epll_VMM0.tp_bypass_mask    = Roc_Analog_VMM0_epll_Ena_Mask(3);

  epll_VMM0.ctrl_phase_0      = ui->VMM0_CTRL_Phase_0->value();
  epll_VMM0.ctrl_phase_1      = ui->VMM0_CTRL_Phase_1->value();
  epll_VMM0.ctrl_phase_2      = ui->VMM0_CTRL_Phase_2->value();
  epll_VMM0.ctrl_phase_3      = ui->VMM0_CTRL_Phase_3->value();
  epll_VMM0.ctrl_bypass_mask  = Roc_Analog_VMM0_epll_Ena_Mask(2);

  epll_VMM0.ctrl_delay_0      = ui->VMM0_CTRL_Delay_0->value();
  epll_VMM0.ctrl_delay_1      = ui->VMM0_CTRL_Delay_1->value();
  epll_VMM0.ctrl_delay_2      = ui->VMM0_CTRL_Delay_2->value();
  epll_VMM0.ctrl_delay_3      = ui->VMM0_CTRL_Delay_3->value();
  epll_VMM0.ctrl_delay_mask   = Roc_Analog_VMM0_epll_Ena_Mask(1);

  epll_VMM0.tx_enable         = Roc_Analog_VMM0_epll_Ena_Mask(4);
  epll_VMM0.tx_csel_enable    = Roc_Analog_VMM0_epll_Ena_Mask(5);

  //----------------------------------------------------//

  ROC_ASIC_analog_epll epll_VMM1;

  epll_VMM1.ePllPhase40MHz_0  = ui->VMM1_ePllPhase40MHz_0->value();
  epll_VMM1.ePllPhase40MHz_1  = ui->VMM1_ePllPhase40MHz_1->value();
  epll_VMM1.ePllPhase40MHz_2  = ui->VMM1_ePllPhase40MHz_2->value();
  epll_VMM1.ePllPhase40MHz_3  = ui->VMM1_ePllPhase40MHz_3->value();

  epll_VMM1.ePllPhase160MHz_0 = ui->VMM1_ePllPhase160MHz_0->value();
  epll_VMM1.ePllPhase160MHz_1 = ui->VMM1_ePllPhase160MHz_1->value();
  epll_VMM1.ePllPhase160MHz_2 = ui->VMM1_ePllPhase160MHz_2->value();
  epll_VMM1.ePllPhase160MHz_3 = ui->VMM1_ePllPhase160MHz_3->value();

  epll_VMM1.ePllInstantLock       = ui->VMM1_ePll_Instant_Lock->currentIndex();
  epll_VMM1.ePllReset             = ui->VMM1_ePll_Reset->currentIndex();
  epll_VMM1.bypassPll             = ui->VMM1_bypass_Pll->currentIndex();
  epll_VMM1.ePllLockEn            = ui->VMM1_ePll_Lock_En->currentIndex();

  epll_VMM1.ePll_Ref_Freq     = ui->VMM1_ePll_Reference_Freq->value();
  epll_VMM1.ePllEnablePhase   = Roc_Analog_VMM1_epll_Ena_Mask(0);
  epll_VMM1.ePll_lcp          = ui->VMM1_ePll_lcp->value();
  epll_VMM1.ePll_cap          = ui->VMM1_ePll_cap->value();
  epll_VMM1.ePll_res          = ui->VMM1_ePll_res->value();

  epll_VMM1.tp_phase_0        = ui->VMM1_TP_phase_0->value();
  epll_VMM1.tp_phase_1        = ui->VMM1_TP_phase_1->value();
  epll_VMM1.tp_phase_2        = ui->VMM1_TP_phase_2->value();
  epll_VMM1.tp_phase_3        = ui->VMM1_TP_phase_3->value();
  epll_VMM1.tp_bypass_mask    = Roc_Analog_VMM1_epll_Ena_Mask(3);

  epll_VMM1.ctrl_phase_0      = ui->VMM1_CTRL_Phase_0->value();
  epll_VMM1.ctrl_phase_1      = ui->VMM1_CTRL_Phase_1->value();
  epll_VMM1.ctrl_phase_2      = ui->VMM1_CTRL_Phase_2->value();
  epll_VMM1.ctrl_phase_3      = ui->VMM1_CTRL_Phase_3->value();
  epll_VMM1.ctrl_bypass_mask  = Roc_Analog_VMM1_epll_Ena_Mask(2);

  epll_VMM1.ctrl_delay_0      = ui->VMM1_CTRL_Delay_0->value();
  epll_VMM1.ctrl_delay_1      = ui->VMM1_CTRL_Delay_1->value();
  epll_VMM1.ctrl_delay_2      = ui->VMM1_CTRL_Delay_2->value();
  epll_VMM1.ctrl_delay_3      = ui->VMM1_CTRL_Delay_3->value();
  epll_VMM1.ctrl_delay_mask   = Roc_Analog_VMM1_epll_Ena_Mask(1);

  epll_VMM1.tx_enable         = Roc_Analog_VMM1_epll_Ena_Mask(4);
  epll_VMM1.tx_csel_enable    = Roc_Analog_VMM1_epll_Ena_Mask(5);

  //----------------------------------------------------//

  ROC_ASIC_analog_epll epll_TDS;

  epll_TDS.ePllPhase40MHz_0  = ui->TDS_ePllPhase40MHz_0->value();
  epll_TDS.ePllPhase40MHz_1  = ui->TDS_ePllPhase40MHz_1->value();
  epll_TDS.ePllPhase40MHz_2  = ui->TDS_ePllPhase40MHz_2->value();
  epll_TDS.ePllPhase40MHz_3  = ui->TDS_ePllPhase40MHz_3->value();

  epll_TDS.ePllPhase160MHz_0 = ui->TDS_ePllPhase160MHz_0->value();
  epll_TDS.ePllPhase160MHz_1 = ui->TDS_ePllPhase160MHz_1->value();
  epll_TDS.ePllPhase160MHz_2 = ui->TDS_ePllPhase160MHz_2->value();
  epll_TDS.ePllPhase160MHz_3 = ui->TDS_ePllPhase160MHz_3->value();

  epll_TDS.ePllInstantLock       = ui->TDS_ePll_Instant_Lock->currentIndex();
  epll_TDS.ePllReset             = ui->TDS_ePll_Reset->currentIndex();
  epll_TDS.bypassPll             = ui->TDS_bypass_Pll->currentIndex();
  epll_TDS.ePllLockEn            = ui->TDS_ePll_Lock_En->currentIndex();

  epll_TDS.ePll_Ref_Freq     = ui->TDS_ePll_Reference_Freq->value();
  epll_TDS.ePllEnablePhase   = Roc_Analog_TDS_epll_Ena_Mask(0);
  epll_TDS.ePll_lcp          = ui->TDS_ePll_lcp->value();
  epll_TDS.ePll_cap          = ui->TDS_ePll_cap->value();
  epll_TDS.ePll_res          = ui->TDS_ePll_res->value();

  epll_TDS.tp_phase_0        = ui->TDS_TP_phase_0->value();
  epll_TDS.tp_phase_1        = ui->TDS_TP_phase_1->value();
  epll_TDS.tp_phase_2        = ui->TDS_TP_phase_2->value();
  epll_TDS.tp_phase_3        = ui->TDS_TP_phase_3->value();
  epll_TDS.tp_bypass_mask    = Roc_Analog_TDS_epll_Ena_Mask(3);

  epll_TDS.ctrl_phase_0      = ui->TDS_CTRL_Phase_0->value();
  epll_TDS.ctrl_phase_1      = ui->TDS_CTRL_Phase_1->value();
  epll_TDS.ctrl_phase_2      = ui->TDS_CTRL_Phase_2->value();
  epll_TDS.ctrl_phase_3      = ui->TDS_CTRL_Phase_3->value();
  epll_TDS.ctrl_bypass_mask  = Roc_Analog_TDS_epll_Ena_Mask(2);

  epll_TDS.ctrl_delay_0      = ui->TDS_CTRL_Delay_0->value();
  epll_TDS.ctrl_delay_1      = ui->TDS_CTRL_Delay_1->value();
  epll_TDS.ctrl_delay_2      = ui->TDS_CTRL_Delay_2->value();
  epll_TDS.ctrl_delay_3      = ui->TDS_CTRL_Delay_3->value();
  epll_TDS.ctrl_delay_mask   = Roc_Analog_TDS_epll_Ena_Mask(1);

  epll_TDS.tx_enable         = Roc_Analog_TDS_epll_Ena_Mask(4);
  epll_TDS.tx_csel_enable    = Roc_Analog_TDS_epll_Ena_Mask(5);

  //----------------------------------------------------//

  ROC_ASIC_analog_epll epll_ROC_int;

  epll_ROC_int.ePllPhase40MHz_0  = ui->ROC_int_ePllPhase40MHz_0->value();
  epll_ROC_int.ePllPhase40MHz_1  = ui->ROC_int_ePllPhase40MHz_1->value();
  epll_ROC_int.ePllPhase40MHz_2  = ui->ROC_int_ePllPhase40MHz_2->value();
  epll_ROC_int.ePllPhase40MHz_3  = ui->ROC_int_ePllPhase40MHz_3->value();

  epll_ROC_int.ePllPhase160MHz_0 = ui->ROC_int_ePllPhase160MHz_0->value();
  epll_ROC_int.ePllPhase160MHz_1 = ui->ROC_int_ePllPhase160MHz_1->value();
  epll_ROC_int.ePllPhase160MHz_2 = ui->ROC_int_ePllPhase160MHz_2->value();
  epll_ROC_int.ePllPhase160MHz_3 = ui->ROC_int_ePllPhase160MHz_3->value();

  epll_ROC_int.ePllInstantLock       = ui->ROC_int_ePll_Instant_Lock->currentIndex();
  epll_ROC_int.ePllReset             = ui->ROC_int_ePll_Reset->currentIndex();
  epll_ROC_int.bypassPll             = ui->ROC_int_bypass_Pll->currentIndex();
  epll_ROC_int.ePllLockEn            = ui->ROC_int_ePll_Lock_En->currentIndex();

  epll_ROC_int.ePll_Ref_Freq     = ui->ROC_int_ePll_Reference_Freq->value();
  epll_ROC_int.ePllEnablePhase   = Roc_Analog_ROC_int_epll_Ena_Mask(0);
  epll_ROC_int.ePll_lcp          = ui->ROC_int_ePll_lcp->value();
  epll_ROC_int.ePll_cap          = ui->ROC_int_ePll_cap->value();
  epll_ROC_int.ePll_res          = ui->ROC_int_ePll_res->value();

  //----------------------------------------------------//

  RocASICConfigHandle().Load_ROC_ASIC_Configuration(global, epll_VMM0, epll_VMM1,
						    epll_TDS, epll_ROC_int);


}
void MainWindow::LoadTDSConfig_to_Handler(){

  ///////////////////////////////////////////////////////
  // grab the global register values from the ui         
  ///////////////////////////////////////////////////////
  TDSGlobalSetting global;
  //bool ok;

  global.header          = ui->TDS_config_header->text().toStdString();
  global.board_Type      = ui->TDS_board_Type->currentIndex();
  global.boardID         = ui->TDS_boardID->value();
  global.ASIC_Type       = ui->TDS_ASIC_Type->currentIndex();
  global.chipID          = Convert_TDS_Mask_to_Value();
  global.command         = ui->TDS_config_command->text().toStdString();

  global.bcid_offset              = ui->TDS_bcid_offset->value();
  global.bcid_rollover            = ui->TDS_bcid_rollover->value();
  global.bcid_clockphase          = ui->TDS_bcid_clockphase->currentIndex();
  global.match_window             = ui->TDS_match_window->currentIndex();
  global.vmm0_clockphase          = ui->TDS_vmm0_clock_phase->value();
  global.vmm1_clockphase          = ui->TDS_vmm1_clock_phase->value();
  global.SER_PLL_current          = ui->TDS_SER_PLL_current->value();
  global.SER_PLL_resistor         = ui->TDS_SER_PLL_resistor->value();
  global.soft_resets              = ui->TDS_soft_reset->currentIndex();
  global.reject_window            = ui->TDS_reject_window->value();
  global.bypass_prompt            = ui->TDS_bypass_prompt->currentIndex();
  global.prompt_circuit           = ui->TDS_prompt_circuit->value();
  global.bypass_trigger           = ui->TDS_bypass_trigger->currentIndex();
  global.bypass_scrambler         = ui->TDS_bypass_scrambler->currentIndex();
  global.test_frame2Router_enable = ui->TDS_test_frame2Router_enable->currentIndex();
  global.stripTDS_globaltest      = ui->TDS_stripTDS_globaltest->currentIndex();
  global.PRBS_en                  = ui->TDS_PRBS_en->currentIndex();


  //----------------------------------------------------//
  vector<TDS_Channel> channels;

  for ( int i=0; i<128; i++ ) {
    TDS_Channel ch;
    
    ch.number  = i;
    ch.enabled = TDSChannelEnableValue[i];
    if ( i < 104 ) ch.delay = TDSChanDelay[i]->currentIndex();//TDSChannelDelayValue[i];
    else           ch.delay = 0;

    channels.push_back(ch);
 }

  //----------------------------------------------------//

  vector<TDS_LUT>     LUTs;
  TDS_LUT LUT;
  LUT.number = 0;
  LUT.BandID    = (quint16) ui->LUT0_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT0_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 1;
  LUT.BandID    = (quint16) ui->LUT1_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT1_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 2;
  LUT.BandID    = (quint16) ui->LUT2_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT2_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 3;
  LUT.BandID    = (quint16) ui->LUT3_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT3_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 4;
  LUT.BandID    = (quint16) ui->LUT4_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT4_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 5;
  LUT.BandID    = (quint16) ui->LUT5_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT5_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 6;
  LUT.BandID    = (quint16) ui->LUT6_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT6_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 7;
  LUT.BandID    = (quint16) ui->LUT7_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT7_LeadStrip->value();
  LUTs.push_back(LUT);

  LUT.number = 8;
  LUT.BandID    = (quint16) ui->LUT8_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT8_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 9;
  LUT.BandID    = (quint16) ui->LUT9_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT9_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 10;
  LUT.BandID    = (quint16) ui->LUT10_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT10_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 11;
  LUT.BandID    = (quint16) ui->LUT11_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT11_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 12;
  LUT.BandID    = (quint16) ui->LUT12_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT12_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 13;
  LUT.BandID    = (quint16) ui->LUT13_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT13_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 14;
  LUT.BandID    = (quint16) ui->LUT14_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT14_LeadStrip->value();
  LUTs.push_back(LUT);
  LUT.number = 15;
  LUT.BandID    = (quint16) ui->LUT15_BandID->value();
  LUT.LeadStrip = (quint16) ui->LUT15_LeadStrip->value();
  LUTs.push_back(LUT);

  TDSConfigHandle().LoadTDSChipConfiguration(global, channels, LUTs);

}
//-----------------------------------------------------------------//
void MainWindow::Load_ROC_FPGA_Config_to_Handler(){

  ///////////////////////////////////////////////////////
  // grab the global register values from the ui         
  ///////////////////////////////////////////////////////
  ROC_FPGASetting global;
  //bool ok;

  global.header          = ui->ROC_config_header->text().toStdString();
  global.board_Type      = ui->ROC_board_Type->currentIndex();
  global.boardID         = ui->ROC_boardID->value();
  global.ASIC_Type       = ui->ROC_ASIC_Type->currentIndex();
  global.chipID          = 0; // always only one ROC, set to 0
  global.command         = ui->ROC_config_command->text().toStdString();
  global.UniqueS         = ui->ROC_UniqueS->text().toStdString();

  uint32_t gpio_mask = 0;

  gpio_mask += ui->gpio_select_0->isChecked();
  gpio_mask += ui->gpio_select_1->isChecked() << 1;
  gpio_mask += ui->gpio_select_2->isChecked() << 2;
  gpio_mask += ui->gpio_select_3->isChecked() << 3;
  gpio_mask += ui->gpio_select_4->isChecked() << 4;
  gpio_mask += ui->gpio_select_5->isChecked() << 5;
  gpio_mask += ui->gpio_select_6->isChecked() << 6;
  gpio_mask += ui->gpio_select_7->isChecked() << 7;

  global.vmm_enable_mask = gpio_mask;

  global.cktp_vs_ckbc_skew    = ui->ROC_cktp_vs_ckbc_skew->value();
  global.cktp_width           = ui->ROC_cktp_width->value();
  global.trigger_window_width = ui->ROC_trigger_width->value();

  RocFPGAConfigHandle().Load_ROC_FPGAConfiguration(global);

}
// ------------------------------------------------------------------------- //
void MainWindow::LoadBoardConfig_from_Handler()
{
  BoardSetting board = BoardConfigHandle().BoardSettings();

  ui->FE_Board_Type->setCurrentIndex(board.boardType);
  ui->FE_Board_ID->setValue(board.boardID);
  ui->FE_Board_UniqueS->setText(board.boardUN);
  ui->FE_Board_ROC_UniqueS->setText(board.ROC_UniqueS);

  ui->vmm_select_1->setChecked(false);
  ui->vmm_select_2->setChecked(false);
  ui->vmm_select_3->setChecked(false);
  ui->vmm_select_4->setChecked(false);
  ui->vmm_select_5->setChecked(false);
  ui->vmm_select_6->setChecked(false);
  ui->vmm_select_7->setChecked(false);
  ui->vmm_select_8->setChecked(false);

  ui->VMM_UniqueS_1->setText("");
  ui->VMM_UniqueS_2->setText("");
  ui->VMM_UniqueS_3->setText("");
  ui->VMM_UniqueS_4->setText("");
  ui->VMM_UniqueS_5->setText("");
  ui->VMM_UniqueS_6->setText("");
  ui->VMM_UniqueS_7->setText("");
  ui->VMM_UniqueS_8->setText("");

  ui->VMM_UniqueS_1->setEnabled(false);
  ui->VMM_UniqueS_2->setEnabled(false);
  ui->VMM_UniqueS_3->setEnabled(false);
  ui->VMM_UniqueS_4->setEnabled(false);
  ui->VMM_UniqueS_5->setEnabled(false);
  ui->VMM_UniqueS_6->setEnabled(false);
  ui->VMM_UniqueS_7->setEnabled(false);
  ui->VMM_UniqueS_8->setEnabled(false);

  ui->tds_select_1->setChecked(false);
  ui->tds_select_2->setChecked(false);
  ui->tds_select_3->setChecked(false);
  ui->tds_select_4->setChecked(false);

  ui->TDS_UniqueS_1->setText("");
  ui->TDS_UniqueS_2->setText("");
  ui->TDS_UniqueS_3->setText("");
  ui->TDS_UniqueS_4->setText("");

  ui->TDS_UniqueS_1->setEnabled(false);
  ui->TDS_UniqueS_2->setEnabled(false);
  ui->TDS_UniqueS_3->setEnabled(false);
  ui->TDS_UniqueS_4->setEnabled(false);

  ui->VMMChipSelect->clear();
  ui->TDSChipSelect->clear();

  for ( int i=0; i<board.nVMM; i++ ) {
    if ( board.VMMChip_ID.at(i) == 1 ) {
      ui->VMM_UniqueS_1->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_1->setEnabled(true);
      ui->vmm_select_1->setChecked(true);
      ui->VMMChipSelect->addItem("chip 0");
    }
    if ( board.VMMChip_ID.at(i) == 2 ) {
      ui->VMM_UniqueS_2->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_2->setEnabled(true);
      ui->vmm_select_2->setChecked(true);
      ui->VMMChipSelect->addItem("chip 1");
    }
    if ( board.VMMChip_ID.at(i) == 3 ) {
      ui->VMM_UniqueS_3->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_3->setEnabled(true);
      ui->vmm_select_3->setChecked(true);
      ui->VMMChipSelect->addItem("chip 2");
    }
    if ( board.VMMChip_ID.at(i) == 4 ) {
      ui->VMM_UniqueS_4->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_4->setEnabled(true);
      ui->vmm_select_4->setChecked(true);
      ui->VMMChipSelect->addItem("chip 3");
    }
    if ( board.VMMChip_ID.at(i) == 5 ) {
      ui->VMM_UniqueS_5->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_5->setEnabled(true);
      ui->vmm_select_5->setChecked(true);
      ui->VMMChipSelect->addItem("chip 4");
    }
    if ( board.VMMChip_ID.at(i) == 6 ) {
      ui->VMM_UniqueS_6->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_6->setEnabled(true);
      ui->vmm_select_6->setChecked(true);
      ui->VMMChipSelect->addItem("chip 5");
    }
    if ( board.VMMChip_ID.at(i) == 7 ) {
      ui->VMM_UniqueS_7->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_7->setEnabled(true);
      ui->vmm_select_7->setChecked(true);
      ui->VMMChipSelect->addItem("chip 6");
    }
    if ( board.VMMChip_ID.at(i) == 8 ) {
      ui->VMM_UniqueS_8->setText(board.VMM_UniqueS_Numbers.at(i));
      ui->VMM_UniqueS_8->setEnabled(true);
      ui->vmm_select_8->setChecked(true);
      ui->VMMChipSelect->addItem("chip 7");
    }
  }

  for ( int i=0; i<board.nTDS; i++ ) {
    if ( board.TDSChip_ID.at(i) == 1 ) {
      ui->TDS_UniqueS_1->setText(board.TDS_UniqueS_Numbers.at(i));
      ui->TDS_UniqueS_1->setEnabled(true);
      ui->tds_select_1->setChecked(true);
      ui->TDSChipSelect->addItem("chip 0");
    }
    if ( board.TDSChip_ID.at(i) == 2 ) {
      ui->TDS_UniqueS_2->setText(board.TDS_UniqueS_Numbers.at(i));
      ui->TDS_UniqueS_2->setEnabled(true);
      ui->tds_select_2->setChecked(true);
      ui->TDSChipSelect->addItem("chip 1");
    }
    if ( board.TDSChip_ID.at(i) == 3 ) {
      ui->TDS_UniqueS_3->setText(board.TDS_UniqueS_Numbers.at(i));
      ui->TDS_UniqueS_3->setEnabled(true);
      ui->tds_select_3->setChecked(true);
      ui->TDSChipSelect->addItem("chip 2");
    }
    if ( board.TDSChip_ID.at(i) == 4 ) {
      ui->TDS_UniqueS_4->setText(board.TDS_UniqueS_Numbers.at(i));
      ui->TDS_UniqueS_4->setEnabled(true);
      ui->tds_select_4->setChecked(true);
      ui->TDSChipSelect->addItem("chip 3");
    }
  }

  //-----------------------------------------------------------//

  //  std::cout << board.GPIO_dir_enable.size() << std::endl;

  GPIO_dir_chan_enable_val  = board.GPIO_dir_enable;
  GPIO_dout_chan_enable_val = board.GPIO_dout_enable;

  //  std::cout << GPIO_dir_chan_enable_val.size() << std::endl;

  ignore_gpio_channel_update = true;

  for ( int i=0; i < 32; i++ ) {
    ui->GPIO_dir_chan_enable_list_widget->item(i)->setSelected(  GPIO_dir_chan_enable_val.at(i) );
    ui->GPIO_dout_chan_enable_list_widget->item(i)->setSelected( GPIO_dout_chan_enable_val.at(i) );    
  }

  ignore_gpio_channel_update = false;

}
// ------------------------------------------------------------------------- //
void MainWindow::Load_ROC_FPGA_Config_from_Handler()
{
  // ------------------------------------------- //
  //  GlobalSetting                                
  // ------------------------------------------- //
  ROC_FPGASetting global = RocFPGAConfigHandle().ROC_FPGASettings();

  ui->ROC_config_header->setText(QString::fromStdString(global.header));
  ui->ROC_board_Type->setCurrentIndex(global.board_Type);
  ui->ROC_boardID->setValue(global.boardID);
  ui->ROC_ASIC_Type->setCurrentIndex(global.ASIC_Type);
  
  ui->ROC_config_command->setText(QString::fromStdString(global.command));
  ui->ROC_UniqueS->setText(QString::fromStdString(global.UniqueS));

  uint32_t gpio_mask = global.vmm_enable_mask;

  //std::cout << gpio_mask << std::endl;
  if ( gpio_mask &       0b1 )  ui->gpio_select_0->setChecked(true);
  else                          ui->gpio_select_0->setChecked(false);
  if ( gpio_mask &      0b10 )  ui->gpio_select_1->setChecked(true);
  else                          ui->gpio_select_1->setChecked(false);
  if ( gpio_mask &     0b100 )  ui->gpio_select_2->setChecked(true);
  else                          ui->gpio_select_2->setChecked(false);
  if ( gpio_mask &    0b1000 )  ui->gpio_select_3->setChecked(true);
  else                          ui->gpio_select_3->setChecked(false);
  if ( gpio_mask &   0b10000 )  ui->gpio_select_4->setChecked(true);
  else                          ui->gpio_select_4->setChecked(false);
  if ( gpio_mask &  0b100000 )  ui->gpio_select_5->setChecked(true);
  else                          ui->gpio_select_5->setChecked(false);
  if ( gpio_mask & 0b1000000 )  ui->gpio_select_6->setChecked(true);
  else                          ui->gpio_select_6->setChecked(false);
  if ( gpio_mask & 0b10000000 ) ui->gpio_select_7->setChecked(true);
  else                          ui->gpio_select_7->setChecked(false);

  ui->ROC_cktp_vs_ckbc_skew->setValue( global.cktp_vs_ckbc_skew );
  ui->ROC_cktp_width->setValue(        global.cktp_width );
  ui->ROC_trigger_width->setValue(     global.trigger_window_width );

}

// ------------------------------------------------------------------------- //
void MainWindow::Load_ROC_ASIC_Config_from_Handler()
{

  ROC_ASIC_Setting global = RocASICConfigHandle().ROC_ASIC_Settings();

  ui->ROC_ASIC_config_header->setText(QString::fromStdString(global.header));
  ui->ROC_ASIC_board_Type->setCurrentIndex(global.board_Type);
  ui->ROC_ASIC_boardID->setValue(global.boardID);
  ui->ROC_ASIC_Type->setCurrentIndex(global.ASIC_Type);
  ui->ROC_ASIC_config_command->setText(QString::fromStdString(global.command));

  ui->ROC_digital_L1_first->setValue( global.L1_first );
  ui->ROC_digital_parity->setCurrentIndex( global.even_parity );
  ui->ROC_digital_ROC_ID->setValue( global.ROC_ID );

  ui->ROC_digital_elink_speed_0->setCurrentIndex(global.elink_speed_sROC0);
  ui->ROC_digital_elink_speed_1->setCurrentIndex(global.elink_speed_sROC1);
  ui->ROC_digital_elink_speed_2->setCurrentIndex(global.elink_speed_sROC2);
  ui->ROC_digital_elink_speed_3->setCurrentIndex(global.elink_speed_sROC3);

  //---------------------------------------------------------//

  Display_Roc_Digital_Ena_Mask( 0, global.vmm_mask_sROC0 );
  Display_Roc_Digital_Ena_Mask( 1, global.vmm_mask_sROC1 );
  Display_Roc_Digital_Ena_Mask( 2, global.vmm_mask_sROC2 );
  Display_Roc_Digital_Ena_Mask( 3, global.vmm_mask_sROC3 );

  Display_Roc_Digital_Ena_Mask( 5, global.EOP_enable_sROC_mask );
  Display_Roc_Digital_Ena_Mask( 6, global.NullEvt_enable_sROC_mask );

  ui->ROC_digital_bypass->setCurrentIndex( global.bypass );
  ui->ROC_digital_timeout_ena->setCurrentIndex( global.timeout_ena );
  ui->ROC_digital_TTC_start_bit->setValue( global.TTC_start_bits );

  Display_Roc_Digital_Ena_Mask( 7, global.enable_sROC_mask );
  Display_Roc_Digital_Ena_Mask( 4, global.vmm_enable_mask );

  ui->ROC_digital_timeout->setValue( global.timeout );
  ui->ROC_digital_TX_cSel->setCurrentIndex( global.TX_current );
  ui->ROC_digital_BC_offset->setValue( global.BC_offset );
  ui->ROC_digital_BC_rollover->setValue( global.BC_rollover );

  //----------------------------------------------------------//

  ui->SROC0_Eport0_Ena->setChecked(  (global.eport_sROC0 & 0b1)  );
  ui->SROC0_Eport1_Ena->setChecked( ((global.eport_sROC0 & 0b10) >> 1) );

  ui->SROC1_Eport0_Ena->setChecked(  (global.eport_sROC1 & 0b1)  );
  ui->SROC1_Eport1_Ena->setChecked( ((global.eport_sROC1 & 0b10) >> 1) );

  ui->SROC2_Eport0_Ena->setChecked(  (global.eport_sROC2 & 0b1)  );
  ui->SROC2_Eport1_Ena->setChecked( ((global.eport_sROC2 & 0b10) >> 1) );

  ui->SROC3_Eport0_Ena->setChecked(  (global.eport_sROC3 & 0b1)  );
  ui->SROC3_Eport1_Ena->setChecked( ((global.eport_sROC3 & 0b10) >> 1) );

  //----------------------------------------------------------//

  Display_Roc_Digital_Ena_Mask( 10, global.fake_vmm_failure_mask );
  Display_Roc_Digital_Ena_Mask( 8, global.TDS_enable_sROC_mask );
  Display_Roc_Digital_Ena_Mask( 9, global.BUSY_enable_sROC_mask );

  ui->ROC_digital_BUSY_On_Limit->setValue( global.BUSY_On_Limit );
  ui->ROC_digital_BUSY_Off_Limit->setValue( global.BUSY_Off_Limit );

  ui->ROC_digital_max_L1_evts->setValue( global.Max_L1_Events_no_comma );

  ui->ROC_int_TP_bypass_global->setCurrentIndex( global.tp_bypass_global );
  ui->ROC_int_TP_phase_global->setValue( global.tp_phase_global );
  ui->ROC_int_LockOutInv->setCurrentIndex( global.LockOutInv );
  ui->ROC_int_TestOutEna->setCurrentIndex( global.testOutEn );
  ui->ROC_int_TestOutMux->setCurrentIndex( global.testOutMux );
  ui->ROC_int_VMM_BCR_INV->setValue( global.VMM_BCR_INV );
  ui->ROC_int_VMM_ENA_INV->setValue( global.VMM_ENA_INV );
  ui->ROC_int_VMM_L0_INV->setValue( global.VMM_L0_INV );
  ui->ROC_int_VMM_TP_INV->setValue( global.VMM_TP_INV );
  ui->ROC_int_TDS_BCR_INV->setValue( global.TDS_BCR_INV );

  //----------------------------------------------------------//

  ROC_ASIC_analog_epll epll_VMM0 = RocASICConfigHandle().ROC_ASIC_Ana_VMM0_Settings();

  ui->VMM0_ePllPhase40MHz_0->setValue( epll_VMM0.ePllPhase40MHz_0 );
  ui->VMM0_ePllPhase40MHz_1->setValue( epll_VMM0.ePllPhase40MHz_1 );
  ui->VMM0_ePllPhase40MHz_2->setValue( epll_VMM0.ePllPhase40MHz_2 );
  ui->VMM0_ePllPhase40MHz_3->setValue( epll_VMM0.ePllPhase40MHz_3 );

  ui->VMM0_ePllPhase160MHz_0->setValue( epll_VMM0.ePllPhase160MHz_0 );
  ui->VMM0_ePllPhase160MHz_1->setValue( epll_VMM0.ePllPhase160MHz_1 );
  ui->VMM0_ePllPhase160MHz_2->setValue( epll_VMM0.ePllPhase160MHz_2 );
  ui->VMM0_ePllPhase160MHz_3->setValue( epll_VMM0.ePllPhase160MHz_3 );

  ui->VMM0_ePll_Instant_Lock->setCurrentIndex( epll_VMM0.ePllInstantLock );
  ui->VMM0_ePll_Reset->setCurrentIndex( epll_VMM0.ePllReset );
  ui->VMM0_bypass_Pll->setCurrentIndex( epll_VMM0.bypassPll );
  ui->VMM0_ePll_Lock_En->setCurrentIndex( epll_VMM0.ePllLockEn );

  ui->VMM0_ePll_Reference_Freq->setValue( epll_VMM0.ePll_Ref_Freq );
  Display_Roc_Analog_VMM0_epll_Ena_Mask(0, epll_VMM0.ePllEnablePhase);

  ui->VMM0_ePll_lcp->setValue( epll_VMM0.ePll_lcp );
  ui->VMM0_ePll_cap->setValue( epll_VMM0.ePll_cap );
  ui->VMM0_ePll_res->setValue( epll_VMM0.ePll_res );

  ui->VMM0_TP_phase_0->setValue( epll_VMM0.tp_phase_0 );
  ui->VMM0_TP_phase_1->setValue( epll_VMM0.tp_phase_1 );
  ui->VMM0_TP_phase_2->setValue( epll_VMM0.tp_phase_2 );
  ui->VMM0_TP_phase_3->setValue( epll_VMM0.tp_phase_3 );
  Display_Roc_Analog_VMM0_epll_Ena_Mask(3, epll_VMM0.tp_bypass_mask);

  ui->VMM0_CTRL_Phase_0->setValue( epll_VMM0.ctrl_phase_0 );
  ui->VMM0_CTRL_Phase_1->setValue( epll_VMM0.ctrl_phase_1 );
  ui->VMM0_CTRL_Phase_2->setValue( epll_VMM0.ctrl_phase_2 );
  ui->VMM0_CTRL_Phase_3->setValue( epll_VMM0.ctrl_phase_3 );
  Display_Roc_Analog_VMM0_epll_Ena_Mask(2, epll_VMM0.ctrl_bypass_mask);

  ui->VMM0_CTRL_Delay_0->setValue( epll_VMM0.ctrl_delay_0 );
  ui->VMM0_CTRL_Delay_1->setValue( epll_VMM0.ctrl_delay_1 );
  ui->VMM0_CTRL_Delay_2->setValue( epll_VMM0.ctrl_delay_2 );
  ui->VMM0_CTRL_Delay_3->setValue( epll_VMM0.ctrl_delay_3 );
  Display_Roc_Analog_VMM0_epll_Ena_Mask(1, epll_VMM0.ctrl_delay_mask);

  Display_Roc_Analog_VMM0_epll_Ena_Mask(4, epll_VMM0.tx_enable);
  Display_Roc_Analog_VMM0_epll_Ena_Mask(5, epll_VMM0.tx_csel_enable);

  //--------------------------------------------------//

  ROC_ASIC_analog_epll epll_VMM1 = RocASICConfigHandle().ROC_ASIC_Ana_VMM1_Settings();

  ui->VMM1_ePllPhase40MHz_0->setValue( epll_VMM1.ePllPhase40MHz_0 );
  ui->VMM1_ePllPhase40MHz_1->setValue( epll_VMM1.ePllPhase40MHz_1 );
  ui->VMM1_ePllPhase40MHz_2->setValue( epll_VMM1.ePllPhase40MHz_2 );
  ui->VMM1_ePllPhase40MHz_3->setValue( epll_VMM1.ePllPhase40MHz_3 );

  ui->VMM1_ePllPhase160MHz_0->setValue( epll_VMM1.ePllPhase160MHz_0 );
  ui->VMM1_ePllPhase160MHz_1->setValue( epll_VMM1.ePllPhase160MHz_1 );
  ui->VMM1_ePllPhase160MHz_2->setValue( epll_VMM1.ePllPhase160MHz_2 );
  ui->VMM1_ePllPhase160MHz_3->setValue( epll_VMM1.ePllPhase160MHz_3 );

  ui->VMM1_ePll_Instant_Lock->setCurrentIndex( epll_VMM1.ePllInstantLock );
  ui->VMM1_ePll_Reset->setCurrentIndex( epll_VMM1.ePllReset );
  ui->VMM1_bypass_Pll->setCurrentIndex( epll_VMM1.bypassPll );
  ui->VMM1_ePll_Lock_En->setCurrentIndex( epll_VMM1.ePllLockEn );

  ui->VMM1_ePll_Reference_Freq->setValue( epll_VMM1.ePll_Ref_Freq );
  Display_Roc_Analog_VMM1_epll_Ena_Mask(0, epll_VMM1.ePllEnablePhase);

  ui->VMM1_ePll_lcp->setValue( epll_VMM1.ePll_lcp );
  ui->VMM1_ePll_cap->setValue( epll_VMM1.ePll_cap );
  ui->VMM1_ePll_res->setValue( epll_VMM1.ePll_res );

  ui->VMM1_TP_phase_0->setValue( epll_VMM1.tp_phase_0 );
  ui->VMM1_TP_phase_1->setValue( epll_VMM1.tp_phase_1 );
  ui->VMM1_TP_phase_2->setValue( epll_VMM1.tp_phase_2 );
  ui->VMM1_TP_phase_3->setValue( epll_VMM1.tp_phase_3 );
  Display_Roc_Analog_VMM1_epll_Ena_Mask(3, epll_VMM1.tp_bypass_mask);

  ui->VMM1_CTRL_Phase_0->setValue( epll_VMM1.ctrl_phase_0 );
  ui->VMM1_CTRL_Phase_1->setValue( epll_VMM1.ctrl_phase_1 );
  ui->VMM1_CTRL_Phase_2->setValue( epll_VMM1.ctrl_phase_2 );
  ui->VMM1_CTRL_Phase_3->setValue( epll_VMM1.ctrl_phase_3 );
  Display_Roc_Analog_VMM1_epll_Ena_Mask(2, epll_VMM1.ctrl_bypass_mask);

  ui->VMM1_CTRL_Delay_0->setValue( epll_VMM1.ctrl_delay_0 );
  ui->VMM1_CTRL_Delay_1->setValue( epll_VMM1.ctrl_delay_1 );
  ui->VMM1_CTRL_Delay_2->setValue( epll_VMM1.ctrl_delay_2 );
  ui->VMM1_CTRL_Delay_3->setValue( epll_VMM1.ctrl_delay_3 );
  Display_Roc_Analog_VMM1_epll_Ena_Mask(1, epll_VMM1.ctrl_delay_mask);

  Display_Roc_Analog_VMM1_epll_Ena_Mask(4, epll_VMM1.tx_enable);
  Display_Roc_Analog_VMM1_epll_Ena_Mask(5, epll_VMM1.tx_csel_enable);

  //--------------------------------------------------------//

  ROC_ASIC_analog_epll epll_TDS = RocASICConfigHandle().ROC_ASIC_Ana_TDS_Settings();

  ui->TDS_ePllPhase40MHz_0->setValue( epll_TDS.ePllPhase40MHz_0 );
  ui->TDS_ePllPhase40MHz_1->setValue( epll_TDS.ePllPhase40MHz_1 );
  ui->TDS_ePllPhase40MHz_2->setValue( epll_TDS.ePllPhase40MHz_2 );
  ui->TDS_ePllPhase40MHz_3->setValue( epll_TDS.ePllPhase40MHz_3 );

  ui->TDS_ePllPhase160MHz_0->setValue( epll_TDS.ePllPhase160MHz_0 );
  ui->TDS_ePllPhase160MHz_1->setValue( epll_TDS.ePllPhase160MHz_1 );
  ui->TDS_ePllPhase160MHz_2->setValue( epll_TDS.ePllPhase160MHz_2 );
  ui->TDS_ePllPhase160MHz_3->setValue( epll_TDS.ePllPhase160MHz_3 );

  ui->TDS_ePll_Instant_Lock->setCurrentIndex( epll_TDS.ePllInstantLock );
  ui->TDS_ePll_Reset->setCurrentIndex( epll_TDS.ePllReset );
  ui->TDS_bypass_Pll->setCurrentIndex( epll_TDS.bypassPll );
  ui->TDS_ePll_Lock_En->setCurrentIndex( epll_TDS.ePllLockEn );

  ui->TDS_ePll_Reference_Freq->setValue( epll_TDS.ePll_Ref_Freq );
  Display_Roc_Analog_TDS_epll_Ena_Mask(0, epll_TDS.ePllEnablePhase);

  ui->TDS_ePll_lcp->setValue( epll_TDS.ePll_lcp );
  ui->TDS_ePll_cap->setValue( epll_TDS.ePll_cap );
  ui->TDS_ePll_res->setValue( epll_TDS.ePll_res );

  ui->TDS_TP_phase_0->setValue( epll_TDS.tp_phase_0 );
  ui->TDS_TP_phase_1->setValue( epll_TDS.tp_phase_1 );
  ui->TDS_TP_phase_2->setValue( epll_TDS.tp_phase_2 );
  ui->TDS_TP_phase_3->setValue( epll_TDS.tp_phase_3 );
  Display_Roc_Analog_TDS_epll_Ena_Mask(3, epll_TDS.tp_bypass_mask);

  ui->TDS_CTRL_Phase_0->setValue( epll_TDS.ctrl_phase_0 );
  ui->TDS_CTRL_Phase_1->setValue( epll_TDS.ctrl_phase_1 );
  ui->TDS_CTRL_Phase_2->setValue( epll_TDS.ctrl_phase_2 );
  ui->TDS_CTRL_Phase_3->setValue( epll_TDS.ctrl_phase_3 );
  Display_Roc_Analog_TDS_epll_Ena_Mask(2, epll_TDS.ctrl_bypass_mask);

  ui->TDS_CTRL_Delay_0->setValue( epll_TDS.ctrl_delay_0 );
  ui->TDS_CTRL_Delay_1->setValue( epll_TDS.ctrl_delay_1 );
  ui->TDS_CTRL_Delay_2->setValue( epll_TDS.ctrl_delay_2 );
  ui->TDS_CTRL_Delay_3->setValue( epll_TDS.ctrl_delay_3 );
  Display_Roc_Analog_TDS_epll_Ena_Mask(1, epll_TDS.ctrl_delay_mask);

  Display_Roc_Analog_TDS_epll_Ena_Mask(4, epll_TDS.tx_enable);
  Display_Roc_Analog_TDS_epll_Ena_Mask(5, epll_TDS.tx_csel_enable);

  //-----------------------------------------------------//

  ROC_ASIC_analog_epll epll_ROC_int = RocASICConfigHandle().ROC_ASIC_Ana_Int_Settings();

  ui->ROC_int_ePllPhase40MHz_0->setValue( epll_ROC_int.ePllPhase40MHz_0 );
  ui->ROC_int_ePllPhase40MHz_1->setValue( epll_ROC_int.ePllPhase40MHz_1 );
  ui->ROC_int_ePllPhase40MHz_2->setValue( epll_ROC_int.ePllPhase40MHz_2 );
  ui->ROC_int_ePllPhase40MHz_3->setValue( epll_ROC_int.ePllPhase40MHz_3 );

  ui->ROC_int_ePllPhase160MHz_0->setValue( epll_ROC_int.ePllPhase160MHz_0 );
  ui->ROC_int_ePllPhase160MHz_1->setValue( epll_ROC_int.ePllPhase160MHz_1 );
  ui->ROC_int_ePllPhase160MHz_2->setValue( epll_ROC_int.ePllPhase160MHz_2 );
  ui->ROC_int_ePllPhase160MHz_3->setValue( epll_ROC_int.ePllPhase160MHz_3 );

  ui->ROC_int_ePll_Instant_Lock->setCurrentIndex( epll_ROC_int.ePllInstantLock );
  ui->ROC_int_ePll_Reset->setCurrentIndex( epll_ROC_int.ePllReset );
  ui->ROC_int_bypass_Pll->setCurrentIndex( epll_ROC_int.bypassPll );
  ui->ROC_int_ePll_Lock_En->setCurrentIndex( epll_ROC_int.ePllLockEn );

  ui->ROC_int_ePll_Reference_Freq->setValue( epll_ROC_int.ePll_Ref_Freq );
  Display_Roc_Analog_ROC_int_epll_Ena_Mask(0, epll_ROC_int.ePllEnablePhase);

  ui->ROC_int_ePll_lcp->setValue( epll_ROC_int.ePll_lcp );
  ui->ROC_int_ePll_cap->setValue( epll_ROC_int.ePll_cap );
  ui->ROC_int_ePll_res->setValue( epll_ROC_int.ePll_res );

  //---------------------------------------------------//

}

// ------------------------------------------------------------------------- //
void MainWindow::LoadTDSConfig_from_Handler()
{
  // ------------------------------------------- //
  //  GlobalSetting                                
  // ------------------------------------------- //
  TDSGlobalSetting global = TDSConfigHandle().TDSGlobalSettings();

  ui->TDS_config_header->setText(QString::fromStdString(global.header));
  ui->TDS_board_Type->setCurrentIndex(global.board_Type);
  ui->TDS_boardID->setValue(global.boardID);
  ui->TDS_ASIC_Type->setCurrentIndex(global.ASIC_Type);
  //ui->TDS_config_mask->setValue(global.chipID);
  Display_TDS_Mask((uint32_t) global.chipID);
  ui->TDS_config_command->setText(QString::fromStdString(global.command));

  ui->TDS_bcid_offset->setValue(global.bcid_offset);
  ui->TDS_bcid_rollover->setValue(global.bcid_rollover);
  ui->TDS_bcid_clockphase->setCurrentIndex(global.bcid_clockphase);
  ui->TDS_match_window->setCurrentIndex(global.match_window);
  ui->TDS_vmm0_clock_phase->setValue(global.vmm0_clockphase);
  ui->TDS_vmm1_clock_phase->setValue(global.vmm1_clockphase);
  ui->TDS_SER_PLL_current->setValue(global.SER_PLL_current);
  ui->TDS_SER_PLL_resistor->setValue(global.SER_PLL_resistor);
  ui->TDS_soft_reset->setCurrentIndex(global.soft_resets);
  ui->TDS_reject_window->setValue(global.reject_window);
  ui->TDS_bypass_prompt->setCurrentIndex(global.bypass_prompt);
  ui->TDS_prompt_circuit->setValue(global.prompt_circuit);
  ui->TDS_bypass_trigger->setCurrentIndex(global.bypass_trigger);
  ui->TDS_bypass_scrambler->setCurrentIndex(global.bypass_scrambler);
  ui->TDS_test_frame2Router_enable->setCurrentIndex(global.test_frame2Router_enable);
  ui->TDS_stripTDS_globaltest->setCurrentIndex(global.stripTDS_globaltest);
  ui->TDS_PRBS_en->setCurrentIndex(global.PRBS_en);

  vector<TDS_Channel> channels;

  for ( int i=0; i<128; i++ ) {
    TDS_Channel chan = TDSConfigHandle().TDS_ChannelSettings(i);

    TDSChannelEnableValue[chan.number]=chan.enabled;
    TDSChannelDelayValue[chan.number]=chan.delay;

    if ( i>=0 && i < 32 ) { 
      ui->TDS_chan_enable_list_widget_1->item(i)->setSelected(TDSChannelEnableValue[chan.number]);
    }
    if ( i>=32 && i < 64 ) {
      ui->TDS_chan_enable_list_widget_2->item(i-32)->setSelected(TDSChannelEnableValue[chan.number]);
    }
    if ( i>=64 && i < 96 ) {
      ui->TDS_chan_enable_list_widget_3->item(i-64)->setSelected(TDSChannelEnableValue[chan.number]);
    }
    if ( i>=96 && i < 128 ) {
      ui->TDS_chan_enable_list_widget_4->item(i-96)->setSelected(TDSChannelEnableValue[chan.number]);
    }

    if ( i < 104 ) TDSChanDelay[i]->setCurrentIndex(TDSChannelDelayValue[chan.number]);
  }

  ui->LUT0_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(0).BandID);
  ui->LUT0_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(0).LeadStrip);
  ui->LUT1_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(1).BandID);
  ui->LUT1_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(1).LeadStrip);
  ui->LUT2_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(2).BandID);
  ui->LUT2_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(2).LeadStrip);
  ui->LUT3_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(3).BandID);
  ui->LUT3_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(3).LeadStrip);

  ui->LUT4_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(4).BandID);
  ui->LUT4_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(4).LeadStrip);
  ui->LUT5_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(5).BandID);
  ui->LUT5_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(5).LeadStrip);
  ui->LUT6_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(6).BandID);
  ui->LUT6_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(6).LeadStrip);
  ui->LUT7_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(7).BandID);
  ui->LUT7_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(7).LeadStrip);

  ui->LUT8_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(8).BandID);
  ui->LUT8_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(8).LeadStrip);
  ui->LUT9_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(9).BandID);
  ui->LUT9_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(9).LeadStrip);
  ui->LUT10_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(10).BandID);
  ui->LUT10_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(10).LeadStrip);
  ui->LUT11_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(11).BandID);
  ui->LUT11_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(11).LeadStrip);

  ui->LUT12_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(12).BandID);
  ui->LUT12_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(12).LeadStrip);
  ui->LUT13_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(13).BandID);
  ui->LUT13_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(13).LeadStrip);
  ui->LUT14_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(14).BandID);
  ui->LUT14_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(14).LeadStrip);
  ui->LUT15_BandID->setValue(TDSConfigHandle().TDS_LUTSettings(15).BandID);
  ui->LUT15_LeadStrip->setValue(TDSConfigHandle().TDS_LUTSettings(15).LeadStrip);

}

std::string MainWindow::getRunDir() {

  stringstream m_sx;

  m_sx.str("");
  if ( ui->outputDirField->text() != "" ) {
    m_sx << ui->outputDirField->text().toStdString()
	 << "/run_" << ui->runNumber->text().toStdString() ;
  }
  else {
    m_sx << "../output"
	 << "/run_" << ui->runNumber->text().toStdString() ;
  }

  return m_sx.str();

}

void MainWindow::startRun()
{

  icommand = 300;
  ui->SetupProgressBar->setValue( icommand );

  // reset some run parameters
  m_is_running = true;
  m_run_EVID_time.resize(0);
  m_run_start_elinks = BoardConfigModule().QueryELink(TARGETPORT, TargetIp, 8, true);

  ui->run_start_time->setText("");
  ui->run_end_time->setText("");

  ui->Run_Evt_Cnt->setText("0");

  //  dataHandle().set_run_maxevents( ui->run_max_evt->value() );

  ui->run_max_evt->setEnabled(false);

  /*
  //-------------------------------------------------------------------//
  //        Disable Qt daq socket to allow for new daq socket
  //              new socket will constantly read data
  //-------------------------------------------------------------------//

  //socketHandle().closeAndDisconnect("stgc");
  //  if ( socketHandle().daqSocket().isBound() ) socketHandle().closeAndDisconnect("daq");
  if ( daq_bound ) {
    socketHandle().closeAndDisconnect("daq"); 
    daq_bound = false;
  }

  boost::this_thread::sleep(boost::posix_time::milliseconds(100)); 

  //-------------------------------------------------------------------//
  //        Setting config modules to running = true disables readout
  //              using the now closed Qt sockets
  //-------------------------------------------------------------------//

  BaseConfigModule().set_is_running( m_is_running );
  BoardConfigModule().set_is_running( m_is_running );
  VMMConfigModule().set_is_running( m_is_running );
  TDSConfigModule().set_is_running( m_is_running );
  */
  //-------------------------------------------------------------------//

  stringstream m_sx;

  // Cannot change configuration during the middle of a run.
  //  DisableConfiguration();

  //   rest run event counter to zero

  //------------------------------------------------------------------//

  //  boost::this_thread::sleep(boost::posix_time::milliseconds(100));

  m_sx.str("");
  m_sx << "Starting New Run \n ";
  msg()(m_sx); m_sx.str("");

  m_sx.str("");
  m_sx << "mv " << getRunDir() << " ../output/previous_run";
  try {
    system( m_sx.str().c_str() );
  }
  catch(std::exception &e) {}

  m_sx.str("");
  m_sx << "mkdir " << getRunDir();
  try {
    system( m_sx.str().c_str() );
  }
  catch(std::exception &e) {}

  dataHandle().setOutDir( getRunDir() );
  dataHandle().setRunNumber( ui->runNumber->text().toStdString() );

  ui->runNumber->setEnabled(false);

  //-----------------------------------------------------------------//
  //            Start run first then enable DAQ/readout
  //-----------------------------------------------------------------//

  if ( ui->writeData->isChecked() ) { dataHandle().setOutputRaw(true);    }
  else                              { dataHandle().setOutputRaw(false);   }
  if ( ui->writeDecoded->isChecked()  ) { dataHandle().setOutputDecoded(true); }
  else                                  { dataHandle().setOutputDecoded(false); }

  /*
  if ( !data_bound ) {
    try {
      dataHandle().gather_data();
      data_bound = true;
    }
    catch (const std::exception& e) { 
      data_bound = false;
      daq_bound = true;

      m_sx.str("");
      m_sx << "Error: previous run still running. stopping run first \n";
      msg()(m_sx);

      socketHandle().closeAndDisconnect("daq");
      stopRun();

      PopUpError("Error: Previous Run still running/DAQ socket occupied please try start run again");

      return;
    }
  }
  ui->connectionLabel->setText( "Preparing Run: Socket Listening for Data" );
  */

  //---------------------------------------------------------------------//
  //              After starting to listen turn on readout
  //---------------------------------------------------------------------//

  bool send_ok = true;

  // ------------------------------------------------------------------- //
  //                 Reset all VMMs twice just to be safe
  // ------------------------------------------------------------------- //

  if ( ui->configXmlDirectoryField->text() != "" && ui->start_run_reset_vmms->isChecked() ) {

    /*
    //  2x VMM auto and hard reset
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
    prepareAndSendConfigDir(false,  true, false, false, true, true);

    // CKTP spacing = 0xffff
    bool send_ok = BoardConfigModule().VMM_TestPulse_CKTP_Spacing(TARGETPORT, TargetIp, 0xffff );
    if ( !send_ok)  {
      PopUpError("Error: Failed to set training pulse spacing to 0xffff, Aborting Prepare Run");
      stopRun();
      return;
    }
    send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp, 2 );
    if ( !send_ok)  {
      PopUpError("Error: Failed to set training pulse frequency to 0, Aborting Prepare Run");
      stopRun();
      return;
    }
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

    prepareAndSendConfigDir(false, false, false, false, true, true);
    prepareAndSendConfigDir(true, false,  false, false, true, true);
    prepareAndSendConfigDir(false, false, false, false, true, true);
    
    prepareAndSendConfigDir(false, true,  false, false, true, true);
    // CKTP spacing = 0xffff
    send_ok = BoardConfigModule().VMM_TestPulse_CKTP_Spacing(TARGETPORT, TargetIp, 0xffff );
    if ( !send_ok)  {
      PopUpError("Error: Failed to set training pulse spacing to 0xffff, Aborting Prepare Run");
      stopRun();
      return;
    }
    send_ok = BoardConfigModule().VMM_TestPulse_CKTP_nMax(TARGETPORT, TargetIp, 2 );
    if ( !send_ok)  {
      PopUpError("Error: Failed to set training pulse frequency to 0, Aborting Prepare Run");
      stopRun();
      return;
    }
    boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

    prepareAndSendConfigDir(false, false, false, false, true, true);
    prepareAndSendConfigDir(true,  false, false, false, true, true);
    prepareAndSendConfigDir(false, false, false, false, true, true);

    */

    //  Synchronous Board Soft Reset

    /*
    send_ok = BoardConfigModule().TTC_Ctr_BC_Reset(TARGETPORT, TargetIp);
    if ( !send_ok) {
      m_sx.str("");
      m_sx << "Error: Sending BCID reset failed. Aborting run \n ";
      msg()(m_sx);
      ui->connectionLabel->setText("Preparing Run: BCID reset failed" );

      PopUpError("Error: Failed reset BCID, Aborting Start Run");

      stopRun();
      return;
    }
    */

    ui->connectionLabel->setText("Preparing Run: BCID reset" );
    Sync_Soft_Reset();

    //-----------------------------------------------------------------------------------//
    
    // ------------------------------------------------ //
    //         reset BCID and EVID on boards              
    // ------------------------------------------------ //


    send_ok = BoardConfigModule().TTC_Ctr_Evt_Reset(TARGETPORT, TargetIp);
    if ( !send_ok) {
      m_sx.str("");
      m_sx << "Error: Sending EVID reset failed. Aborting run \n ";
      msg()(m_sx);
      ui->connectionLabel->setText("Preparing Run: EVID reset failed" );

      PopUpError("Error: Failed reset EVID, Aborting Start Run");

      stopRun();
      return;
    }
    ui->connectionLabel->setText("Preparing Run: EVID reset" );

    //---------------------------------------------------------------------------------//

    ui->connectionLabel->setText("Preparing Run: Ready to Start Run and Begin Taking Data" );

    DisableConfiguration();
  }
  else if ( ui->start_run_reset_vmms->isChecked() ) {

    // ------------------------------------------------ //     
    //         reset BCID and EVID on boards                   
    // ------------------------------------------------ //     
    /*
    send_ok = BoardConfigModule().TTC_Ctr_BC_Reset(TARGETPORT, TargetIp);
    if ( !send_ok) {
      m_sx.str("");
      m_sx << "Error: Sending BCID reset failed. Aborting run \n ";
      msg()(m_sx);
      ui->connectionLabel->setText("Preparing Run: BCID reset failed" );

      PopUpError("Error: Failed reset BCID, Aborting Start Run");

      stopRun();
      return;
    }
    ui->connectionLabel->setText("Preparing Run: BCID reset" );
    */

    send_ok = BoardConfigModule().TTC_Ctr_Evt_Reset(TARGETPORT, TargetIp);
    if ( !send_ok) {
      m_sx.str("");
      m_sx << "Error: Sending EVID reset failed. Aborting run \n ";
      msg()(m_sx);
      ui->connectionLabel->setText("Preparing Run: EVID reset failed" );

      PopUpError("Error: Failed reset EVID, Aborting Start Run");

      stopRun();
      return;
    }
    ui->connectionLabel->setText("Preparing Run: EVID reset" );

    //------------------------------------------------------------------------------//
    DisableConfiguration();

    m_sx.str("");
    m_sx << "Warning: VMMs not reset but ready for start run if this is determined to be not needed (for example when VMMs are already configured from previous run and in good state) \n";
    msg()( m_sx );
    ui->connectionLabel->setText("Preparing Run: Waiting for Manual Reset of VMMs" );
    PopUpWarning( m_sx.str().c_str() );

  }
  else {

    // ------------------------------------------------ //
    //         reset BCID and EVID on boards              
    // ------------------------------------------------ //
    /*
    send_ok = BoardConfigModule().TTC_Ctr_BC_Reset(TARGETPORT, TargetIp);
    if ( !send_ok) {
      m_sx.str("");
      m_sx << "Error: Sending BCID reset failed. Aborting run \n ";
      msg()(m_sx);
      ui->connectionLabel->setText("Preparing Run: BCID reset failed" );

      PopUpError("Error: Failed reset BCID, Aborting Start Run");

      stopRun();
      return;
    }
    ui->connectionLabel->setText("Preparing Run: BCID reset" );
    */

    send_ok = BoardConfigModule().TTC_Ctr_Evt_Reset(TARGETPORT, TargetIp);
    if ( !send_ok) {
      m_sx.str("");
      m_sx << "Error: Sending EVID reset failed. Aborting run \n ";
      msg()(m_sx);
      ui->connectionLabel->setText("Preparing Run: EVID reset failed" );

      PopUpError("Error: Failed reset EVID, Aborting Start Run");

      stopRun();
      return;
    }
    ui->connectionLabel->setText("Preparing Run: EVID reset" );

    //------------------------------------------------------------------------------//
    ui->connectionLabel->setText("Preparing Run Successful: Ready to Start Run and Begin Taking Data" );

    DisableConfiguration();
  }

  if ( !send_ok ) {
    m_sx.str("");
    m_sx << "Error: Sending VMM reset failed. Aborting run \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("Preparing Run: VMM reset failed" );

    PopUpError("Error: Failed reset VMM, Aborting Start Run");

    stopRun();
    return;
  }

  dataHandle().setEvtCount(0);

  //---------------------------------------------------------------------//

  loadMiniDAQConfig_fromTTC();

  //-------------------------------------------------------------------//
  //        Disable Qt daq socket to allow for new daq socket            
  //              new socket will constantly read data                   
  //-------------------------------------------------------------------//

  //socketHandle().closeAndDisconnect("stgc");
  //  if ( socketHandle().daqSocket().isBound() ) socketHandle().closeAndDisconnect("daq"); 

  if ( daq_bound ) {
    socketHandle().closeAndDisconnect("daq");
    daq_bound = false;
  }

  boost::this_thread::sleep(boost::posix_time::milliseconds(100));

  //-------------------------------------------------------------------//
  //        Setting config modules to running = true disables readout    
  //              using the now closed Qt sockets                        
  //-------------------------------------------------------------------//
  BaseConfigModule().set_is_running( m_is_running );
  BoardConfigModule().set_is_running( m_is_running );
  VMMConfigModule().set_is_running( m_is_running );
  TDSConfigModule().set_is_running( m_is_running );
  RocFPGAConfigModule().set_is_running( m_is_running );

  //-------------------------------------------------------------------//    

  if ( !data_bound ) {
    try {
      dataHandle().gather_data();
      data_bound = true;
    }
    catch (const std::exception& e) {
      data_bound = false;
      daq_bound = true;

      m_sx.str("");
      m_sx << "Error: previous run still running. stopping run first \n";
      msg()(m_sx);

      socketHandle().closeAndDisconnect("daq");
      stopRun();

      PopUpError("Error: Previous Run still running/DAQ socket occupied please try start run again");

      return;
    }
  }
  //  ui->connectionLabel->setText( "Preparing Run: Socket Listening for Data" );


  icommand = 450;
  ui->SetupProgressBar->setValue(icommand );

  //  std::cout << "n command sent: " << icommand << std::endl;

}

void MainWindow::enableDAQ() 
{

  icommand = 450;
  ui->SetupProgressBar->setValue( icommand );

  bool send_ok;
  std::stringstream m_sx;

  // make copies of configuration at the time of start run
  if ( ui->configXmlDirectoryField->text() != "" ) { 
    m_sx.str("");
    m_sx << "cp -r " << ui->configXmlDirectoryField->text().toStdString() 
	 << " " << getRunDir() << "/." ;
	
    system(m_sx.str().c_str());
  }
  else {
    m_sx.str("");
    m_sx << getRunDir() << "/vmm.xml";
    ui->configVMMXmlFilenameField->setText(QString::fromStdString(m_sx.str()));
    //    ui->write_config_vmm->click();
    LoadVMMConfig_to_Handler();
    VMMConfigHandle().WriteVMMConfig_to_File(QString::fromStdString(m_sx.str()));

    m_sx.str("");
    m_sx << getRunDir() << "/tds.xml";
    ui->configTDSXmlFilenameField->setText(QString::fromStdString(m_sx.str()));
    //    ui->write_config_tds->click();
    LoadTDSConfig_to_Handler();
    TDSConfigHandle().WriteTDSConfig_to_File(QString::fromStdString(m_sx.str()));

    m_sx.str("");
    m_sx << getRunDir() << "/board.xml";
    ui->configBoardXmlFilenameField->setText(QString::fromStdString(m_sx.str()));
    //    ui->write_config_board->click();
    LoadBoardConfig_to_Handler();
    BoardConfigHandle().WriteBoardConfig_to_File(QString::fromStdString(m_sx.str()));
  }

  //  enable readout
  send_ok = BoardConfigModule().GlobalCtr_Readout_Enabled(TARGETPORT, TargetIp, true);
  icommand = 460;
  ui->SetupProgressBar->setValue(icommand );

  if ( !send_ok) {
    m_sx.str("");
    m_sx << "Error: Sending start DAQ failed. Aborting run \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("Preparing Run: DAQ Enable failed" );

    PopUpError("Error: Failed enable readout, Aborting enable DAQ");

    stopRun();
    return;
  }
  ui->connectionLabel->setText("Preparing Run: DAQ Enabled" );

  send_ok = BoardConfigModule().GlobalCtr_DAQ_Enabled(TARGETPORT, TargetIp, true);
  icommand = 470;
  ui->SetupProgressBar->setValue(icommand );

  if ( !send_ok ) {
    m_sx.str("");
    m_sx << "Error: Sending start DAQ failed. Aborting run \n ";
    msg()(m_sx);
    ui->connectionLabel->setText("Preparing Run: Readout enable failed" );

    PopUpError("Error: Failed enable DAQ, Aborting enable DAQ");

    stopRun();
    return;
  }
  ui->connectionLabel->setText("Run started successfully" );

  QueryRunStatus();

  ui->run_start_time->setText(GetCurrentTimeStr());
  ui->run_end_time->setText("");

  dataHandle().setRunStartTime( GetCurrentTimeStr().toStdString() );
  
  loadMiniDAQConfig_fromTTC();

  icommand = 500;
  ui->SetupProgressBar->setValue(icommand );

  //  std::cout << "n commands total: " << icommand << std::endl;

  ui->enableDAQ->setEnabled(false);

}

void MainWindow::stopRun()
{

  stringstream m_sx;
  m_is_running = false;

  BoardConfigModule().GlobalCtr_Readout_Enabled(TARGETPORT, TargetIp, false);
  BoardConfigModule().GlobalCtr_DAQ_Enabled(TARGETPORT, TargetIp, false);

  ui->run_end_time->setText(GetCurrentTimeStr());
  dataHandle().setRunEndTime( GetCurrentTimeStr().toStdString() );

  if ( data_bound ) {
    dataHandle().endRun();
    data_bound = false;

    boost::this_thread::sleep(boost::posix_time::milliseconds(100));
  }

  m_sx.str("");
  m_sx << "Run ended, open for configurations again \n ";
  msg()(m_sx); m_sx.str("");
  ui->connectionLabel->setText("Run Ended" );
  
  buildHostIP();
  buildTargetIP();
  //  if ( !socketHandle().daqSocket().isBound() ) socketHandle().addSocket(HostIp, "daq",  TARGETPORT, QUdpSocket::ShareAddress);
  if ( !daq_bound ) {
    socketHandle().addSocket(HostIp, "daq",  TARGETPORT, QUdpSocket::ShareAddress);
    daq_bound = true;
  }

  //--------------------------------------------------//
  BaseConfigModule().set_is_running( m_is_running );
  BoardConfigModule().set_is_running( m_is_running );
  VMMConfigModule().set_is_running( m_is_running );
  TDSConfigModule().set_is_running( m_is_running );
  RocFPGAConfigModule().set_is_running( m_is_running );
  //-------------------------------------------------//

  EnableConfiguration();

}

void MainWindow::DisableConfiguration()
{

  //----------------------------------------------------//
  //    cannot perform certain actions during run
  //----------------------------------------------------//

  ui->setup_mini_daq->setEnabled(false);
  ui->setupSCA->setEnabled(false);
  ui->ConfiguringFEBs->setEnabled(false);
  ui->Calib_whole_dir->setEnabled(false);
  
  ui->config_board_baselineVMMs->setEnabled(false);
  ui->config_board_calibrateVMMs->setEnabled(false);
  ui->vmm_calib_single->setEnabled(false);
  ui->vmm_scan_baseline->setEnabled(false);

  //  ui->Send_Config->setEnabled(false);
  //  ui->ReconfigureBoard->setEnabled(false);
  //  ui->ReconfigureVMM->setEnabled(false);
  //  ui->ReconfigureTDS->setEnabled(false);
  //  ui->SendConfiguration->setEnabled(false);
  /*
  ui->cmd_DAQ_Enable->setEnabled(false);
  ui->cmd_EVID_Reset->setEnabled(false);
  ui->cmd_Ext_Trigger->setEnabled(false);
  ui->cmd_FPGA_reset->setEnabled(false);
  ui->cmd_Gate_nBC->setEnabled(false);
  ui->cmd_ReadOut_Enable->setEnabled(false);
  ui->cmd_SCA_Reset->setEnabled(false);
  ui->cmd_TTC_start->setEnabled(false);
  ui->cmd_CKBC_Freq->setEnabled(false);
  ui->cmd_latency_setup->setEnabled(false);
  ui->cmd_CKTP_NMax->setEnabled(false);
  ui->cmd_CKTP_Period->setEnabled(false);
  ui->cmd_CKTP_Skew->setEnabled(false);
  ui->cmd_CKTP_Width->setEnabled(false);
  */

  ui->startRunning->setEnabled(false);
  ui->enableDAQ->setEnabled(true);
  ui->stopRunning->setEnabled(true);

  ui->run_max_evt->setEnabled(false);
  ui->runNumber->setEnabled(false);

}


void MainWindow::EnableConfiguration()
{

  ui->setup_mini_daq->setEnabled(true);
  ui->setupSCA->setEnabled(true);
  ui->ConfiguringFEBs->setEnabled(true);
  ui->Calib_whole_dir->setEnabled(true);
  //  ui->Send_Config->setEnabled(true);

  ui->config_board_baselineVMMs->setEnabled(true);
  ui->config_board_calibrateVMMs->setEnabled(true);
  ui->vmm_calib_single->setEnabled(true);
  ui->vmm_scan_baseline->setEnabled(true);

  ui->ReconfigureBoard->setEnabled(true);
  ui->ReconfigureVMM->setEnabled(true);
  ui->ReconfigureTDS->setEnabled(true);
  ui->SendConfiguration->setEnabled(true);
  ui->cmd_DAQ_Enable->setEnabled(true);
  ui->cmd_EVID_Reset->setEnabled(true);
  ui->cmd_Ext_Trigger->setEnabled(true);
  ui->cmd_FPGA_reset->setEnabled(true);
  ui->cmd_Gate_nBC->setEnabled(true);
  ui->cmd_ReadOut_Enable->setEnabled(true);
  ui->cmd_SCA_Reset->setEnabled(true);
  ui->cmd_TTC_start->setEnabled(true);
  //ui->cmd_CKBC_Freq->setEnabled(true);
  ui->cmd_latency_setup->setEnabled(true);
  ui->cmd_CKTP_NMax->setEnabled(true);
  //ui->cmd_CKTP_Period->setEnabled(true);
  //ui->cmd_CKTP_Skew->setEnabled(true);
  //ui->cmd_CKTP_Width->setEnabled(true);

  ui->startRunning->setEnabled(true);
  ui->enableDAQ->setEnabled(false);
  ui->stopRunning->setEnabled(false);

  ui->run_max_evt->setEnabled(true);
  ui->runNumber->setEnabled(true);

}

void MainWindow::QueryRunStatus() {

  stringstream m_sx;

  if ( !m_is_running ) return;

  m_sx.str("");
  m_sx << "Info: Query Run Status at " << GetCurrentTimeStr().toStdString();
  //  msg()(m_sx);

  // do nothing if not running

  //--------------------------------------------------------//
  // otherwise query EVID, ELink status every minute
  //--------------------------------------------------------//

  // send query commands
  BoardConfigModule().Query_EVID(TARGETPORT, TargetIp);
  BoardConfigModule().QueryELink(TARGETPORT, TargetIp, 8, true);

  boost::this_thread::sleep(boost::posix_time::milliseconds(200));

  long current_EVID = dataHandle().current_EVID();

  if ( m_run_EVID_time.size() >= 1 ) {

    // keep track of how many multiple of 0xffff has passed
    long i_times = ( m_run_EVID_time.back() / 0xffff );

    // rolls over once everything current_EVID < remainder of previous entry
    if ( current_EVID < (m_run_EVID_time.back() % 0xffff) ) {
      i_times++;
    }

    m_run_EVID_time.push_back( current_EVID + i_times * 0xffff );

  }
  else {
    m_run_EVID_time.push_back( current_EVID );
  }

  std::vector<bool> m_run_current_ELinks = dataHandle().current_locked_elinks() ;

  //----------------------------------------//

  // trigger rate in HZ;
  float trigger_rate = 0;
  if ( m_run_EVID_time.size() >= 2 )  {
    trigger_rate = (m_run_EVID_time.back() - m_run_EVID_time.at(m_run_EVID_time.size()-2))/1.;
  }

  //-----------------------------------------//

  m_sx.str("");
  m_sx << m_run_EVID_time.back()-m_run_EVID_time.at(0);
  
  ui->Run_Evt_Cnt->setText(  QString::fromStdString( m_sx.str() ) );

  //-----------------------------------------//

  char buffer [50];
  
  sprintf(buffer, "%+.3e Hz", trigger_rate);
  ui->Run_Trig_Rate->setText(  QString::fromUtf8( buffer ) );

  //------------------------------------------//

  //  bool lost_link = false;

  for ( uint i=0; i<m_run_current_ELinks.size(); i++ ) {
    if ( m_run_start_elinks.at(i) && !m_run_current_ELinks.at(i) ) {
      m_sx.str("");
      m_sx << "FATAL: Lost ELink " << i << "! \n";
      msg()(m_sx);
      //lost_link = true;
    }
  }

  // disable for now during development of software
  //  if ( lost_link ) stopRun();

  if ( m_run_EVID_time.back()-m_run_EVID_time.at(0) > ui->run_max_evt->value() && 
       ui->run_max_evt->value() != 0 ) {
    stopRun();
    PopUpInfo("Max Triggered Events Reached: Stopping Run");
  }

}

void MainWindow::DisplayDateTime() {
  ui->Date_Time_Out->setText(GetCurrentTimeStr());
}

QString MainWindow::GetCurrentTimeStr() {

  QTime time = QTime::currentTime();

  int day, month, year;
  QDate date = QDate::currentDate();
  date.getDate(&year,&month,&day);

  QString timestr = time.toString("hh:mm");

  stringstream m_sx;
  m_sx.str("");
  m_sx << day << "/" << month << "/" << year << " ";

  QString datestr = QString::fromStdString(m_sx.str());
  
  return datestr+timestr;

}

//-----------------------------------------------------------------------------------------------//

bool MainWindow::Set_CalibrateVMM_Channel( int ichan, int ivmm, int iboard, int boardType, 
					   bool trimming, int trimmingbit ) {
  
  if ( boardType == board_type_pFEB || 
       boardType == board_type_pFEB_v22 ||
       boardType == board_type_sFEB_v22 ) {
    VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << ivmm );
  }
  else {
    VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
  }

  VMMConfigHandle().VMMGlobalSettings().boardID = iboard;
  VMMConfigHandle().VMMGlobalSettings().board_Type = boardType;

  VMMConfigHandle().Set_CalibrateVMM_Channel( ichan, trimming, trimmingbit );

  buildTargetIP();

  bool send_ok = true;
  send_ok = VMMConfigModule().SendVMMConfig(TARGETPORT, TargetIp);

  //  boost::this_thread::sleep(boost::posix_time::milliseconds(500));

  return send_ok;

}

std::vector<uint32_t> MainWindow::CalibrateVMM_Channel( int ivmm, int iboard, int boardType) {

  buildTargetIP();

  std::vector<uint32_t> m_ADC = BoardConfigModule().Query_SCA_ADC_VMM(TARGETPORT, TargetIp, boardType, iboard, ivmm, 0);

  int itry = 0;
  while ( m_ADC.size() == 0 ) {
    if ( itry == 3 ) break;
    m_ADC = BoardConfigModule().Query_SCA_ADC_VMM(TARGETPORT, TargetIp, boardType, iboard, ivmm, 0);
    itry++;
  }

  return m_ADC;
}

//------------------------------------------------------------------------------------------------//

//------------------------------------------------------------------------------------------------//

std::vector<uint32_t> MainWindow::Scan_Baseline( int ivmm, int iboard, int boardType )
{

  std::vector<uint32_t>chan_baselines;
  chan_baselines.resize(0);

  stringstream m_sx;

  for ( int ichan = 0; ichan < 64; ichan++ ) {
    Set_CalibrateVMM_Channel( ichan, ivmm, iboard, boardType, false, 0 );
    std::vector<uint32_t> baseline_ADC = CalibrateVMM_Channel( ivmm, iboard, boardType );
    if ( baseline_ADC.size() > 0 ) {
      chan_baselines.push_back( find_avg(baseline_ADC) );
    }
    else {
      m_sx.str("");
      m_sx << " baseline calibration for channel " << ichan << " on VMM " << ivmm << ", board "<< iboard << " failed ";
      msg()(m_sx);
      PopUpError(m_sx.str().c_str());
      chan_baselines.resize(0);
      return chan_baselines;
    }
  }

  VMMConfigHandle().VMMCalibSettings().chan_baselines = chan_baselines;

  m_sx.str("");
  m_sx << " baseline calibration for VMM " << ivmm << ", board " << iboard << " succeeded ";
  msg()(m_sx);
  PopUpInfo(m_sx.str().c_str());

  return chan_baselines;

}
//------------------------------------------------------------------------------------------------//
std::vector<std::vector<uint32_t>> MainWindow::Scan_Baseline_Multi( int ivmm, int iboard, int boardType )
{

  std::vector<std::vector<uint32_t>> chan_baselines_multi;
  chan_baselines_multi.resize(0);

  stringstream m_sx;

  int ntrial = 1;

  for ( int ichan = 0; ichan < 64; ichan++ ) {
    Set_CalibrateVMM_Channel( ichan, ivmm, iboard, boardType, false, 0 );
    std::vector<uint32_t> chan_baseline_ichan;
    chan_baseline_ichan.resize(0);

    for ( int itrial = 0; itrial < ntrial; itrial++ ) {
      std::vector<uint32_t> baseline_ADC_i = CalibrateVMM_Channel( ivmm, iboard, boardType );

      if ( baseline_ADC_i.size() > 0 ) {

	for ( int i=0; i<baseline_ADC_i.size(); i++ ) {
	  chan_baseline_ichan.push_back( baseline_ADC_i.at(i) );
	}

      }
      else {
	m_sx.str("");
	m_sx << " baseline calibration for channel " << ichan << " on VMM " << ivmm << ", board "<< iboard << " failed ";
	msg()(m_sx);
	PopUpError(m_sx.str().c_str());
	//chan_baselines_multi.resize(0);
	return chan_baselines_multi;
      }
    }
    chan_baselines_multi.push_back( chan_baseline_ichan );

  }

  m_sx.str("");
  m_sx << " baseline scan for VMM " << ivmm << ", board " << iboard << " succeeded ";
  msg()(m_sx);
  PopUpInfo(m_sx.str().c_str());

  return chan_baselines_multi;

}
//------------------------------------------------------------------------------------------------//

void MainWindow::Display_Baseline_Scan() {

  stringstream m_sx;
  m_sx.str("");
  
  std::vector<uint32_t>chan_baselines;
  chan_baselines.resize(0);

  chan_baselines = VMMConfigHandle().VMMCalibSettings().chan_baselines ;

  m_sx << "baseline SCA ADC measurements \n";
  m_sx << " chan #       baseline ADC          chan #       baseline ADC \n";

  for ( int ichan = 0; ichan < chan_baselines.size()/2; ichan++ ) {
    m_sx << "  " << ichan << "                 " << chan_baselines.at(ichan);
    if ( ichan +32 < chan_baselines.size() ) {
      m_sx << "               " << ichan+32 << "             " << chan_baselines.at(ichan+32) << " \n";
    }
    else {
      m_sx << " \n";
    }
  }

  PopUpInfo( m_sx.str().c_str() );

}

//------------------------------------------------------------------------------------------------//

void MainWindow::Display_Trimming_Matrix() {

  stringstream m_sx;
  m_sx.str("");

  std::vector<std::vector<uint32_t>> chan_baseline_vs_trim = VMMConfigHandle().VMMCalibSettings().chan_baseline_vs_trim;

  m_sx << "per channel trimming ADC matrix \n";

  for ( uint ichan = 0; ichan < chan_baseline_vs_trim.size(); ichan++ ) {
    std::vector<uint32_t> ichan_baseline_vs_trim = chan_baseline_vs_trim.at(ichan);

    m_sx << "channel " << ichan << "    ";

    for ( uint itrim = 0; itrim < ichan_baseline_vs_trim.size(); itrim++ ) {
      m_sx << ichan_baseline_vs_trim.at(itrim) << " ";
    }

    m_sx << "\n";

  }

  PopUpInfo( m_sx.str().c_str() );

}

//------------------------------------------------------------------------------------------------//
std::vector<std::vector<uint32_t>> MainWindow::Scan_TrimmingThreshold( int ivmm, int iboard, int boardType )
{

  // [ichan][itrim] matrix
  std::vector<std::vector<uint32_t>> chan_baseline_vs_trim;
  chan_baseline_vs_trim.resize(0);

  stringstream m_sx;
  int nchan = 64;
  int ntrim = 32;

  for ( int ichan = 0; ichan < nchan; ichan++ ) {

    std::vector<uint32_t> ichan_baseline_vs_trim;
    ichan_baseline_vs_trim.resize(0);

    for ( int itrim = 0; itrim < ntrim; itrim++ ) {
      
      Set_CalibrateVMM_Channel( ichan, ivmm, iboard, boardType, true, itrim);

      std::vector<uint32_t> i_trim_ADC = CalibrateVMM_Channel( ivmm, iboard, boardType);

      if ( i_trim_ADC.size() > 0 ) {
	ichan_baseline_vs_trim.push_back( find_avg(i_trim_ADC) );
      }
      else {
	m_sx.str("");
	m_sx << " calibration for channel " << ichan << " on VMM " << ivmm 
	     << ", board "<< iboard << " failed: continuing to try other channels ";
	msg()(m_sx);
	PopUpError(m_sx.str().c_str());
	ichan_baseline_vs_trim.push_back( 0xffff );
	//chan_baseline_vs_trim.resize(0);
	//return chan_baseline_vs_trim;
      }


    } // loop over itrim

    chan_baseline_vs_trim.push_back( ichan_baseline_vs_trim );
    
  } // loop over ichan

  VMMConfigHandle().VMMCalibSettings().chan_baseline_vs_trim = chan_baseline_vs_trim;

  m_sx.str("");
  m_sx << "calibrating trimming bit for VMM " << ivmm << ", board "<< iboard << " succeeded ";
  msg()(m_sx);
  PopUpInfo(m_sx.str().c_str());

  return chan_baseline_vs_trim;

}

//------------------------------------------------------------------------------------------//

void MainWindow::Find_Optimal_Trimming(int ivmm, int iboard, int boardType) {

  std::cout<< "load calib to gui/handler " << std::endl;

  //---------------------------------------------------------------//
  //     get the full nchannel by ntrimming ADC matrix
  //---------------------------------------------------------------//

  std::vector<std::vector<uint32_t>> m_chan_baseline_vs_trim = Scan_TrimmingThreshold( ivmm, iboard, boardType );

  if ( m_chan_baseline_vs_trim.size() == 0 ) return;

  //---------------------------------------------------------------//
  //     Find the best trimming bit values for the VMM
  //---------------------------------------------------------------//

  VMMCalibModule().m_chan_baseline_vs_trim = m_chan_baseline_vs_trim;
  VMMCalibModule().find_Opt_Trim_Bit();
  std::vector<uint32_t> m_opt_trim_bits = VMMCalibModule().get_opt_trim_bits();

  if ( m_opt_trim_bits.size() == 0 ) {
    std::cout << "no trimming bits found " << std::endl;
    return;
  }

  //--------------------------------------------------------------//
  //             Display optimal trimming bit
  //--------------------------------------------------------------//

  for ( uint i=0; i<m_opt_trim_bits.size(); i++ ) {
    VMMSDVoltage[i]->setCurrentIndex( m_opt_trim_bits.at(i) );
    std::cout << "channel " << i << " optimal trimming bit " << m_opt_trim_bits.at(i) ;
  }

  LoadVMMConfig_to_Handler();

  VMMConfigHandle().SetVMM_OptTrimBits( m_opt_trim_bits );

  //------------------------------------------------------------//
  Display_Trimming_Matrix();

}

//------------------------------------------------------------------------------------------------//

void MainWindow::CalibrateVMM() {

  LoadVMMConfig_to_Handler(); 

  int vmmID     = -1;
  
  for ( int i=0; i<8; i++ ) {
    if ( ( VMMConfigHandle().VMMGlobalSettings().vmmmask & ( 0b1 << i ) ) ) vmmID = i;
  }

  int boardID   = VMMConfigHandle().VMMGlobalSettings().boardID;
  int boardType = VMMConfigHandle().VMMGlobalSettings().board_Type;

  if ( vmmID >= 0 ) Find_Optimal_Trimming( vmmID, boardID, boardType );
  else {
    PopUpError ( "vmm not selected " );
    return;
  }
}

void MainWindow::FindBaseline() {

  LoadVMMConfig_to_Handler();

  int vmmID     = -1;

  for (int i=0; i<8; i++ ) {
    if ( ( VMMConfigHandle().VMMGlobalSettings().vmmmask & ( 0b1 << i ) ) ) vmmID = i;
  }

  int boardID   = VMMConfigHandle().VMMGlobalSettings().boardID;
  int boardType = VMMConfigHandle().VMMGlobalSettings().board_Type;

  if ( vmmID >= 0 ) {
    Scan_Baseline( vmmID, boardID, boardType );
    Display_Baseline_Scan();
  }
  else {
    PopUpError ( "vmm not selected" );
    return;
  }

}

//---------------------------------------------------------------//

void MainWindow::CalibrateBoard_ConfigVMMs( int ichan, int type, int trimmingbit, int ithreshold ) {

  stringstream m_sx;

  m_sx.str("");
  m_sx << "Sending vmm configuration for board "
       << ui->FE_Board_ID->value() << " with Unique ID "
       << ui->FE_Board_UniqueS->text().toStdString() << std::endl;
  msg()(m_sx);

  //-------------------------------------------//                                                              

  LoadBoardConfig_to_Handler();
  buildTargetIP();

  int boardType = BoardConfigHandle().BoardSettings().boardType;
  int boardID   = BoardConfigHandle().BoardSettings().boardID;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());

  //-------------------------------------------//

  bool all_ok = true;

  for ( int i=nVMM-1; i>=0; i-- ) {

    bool ok;

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(i)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {

      for ( int j=0; j<1; j++ ) {
        ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);

        VMMConfigHandle().SetBoardID(   boardID );
        VMMConfigHandle().SetBoardType( boardType );

	// change VMM calibration to monitor correct channel with trimming bit

	switch (type) {
        case type_trimming: 
	  VMMConfigHandle().Set_CalibrateVMM_Channel( ichan, true, trimmingbit );
	  break;
        case type_baseline: 
	  VMMConfigHandle().Set_CalibrateVMM_Channel( ichan, false, 0 );   
	  break;
        case type_ThDAC:    
	  VMMConfigHandle().Set_CalibrateVMM_ThDAC( ithreshold );
	  break;
	}

        // sFEB configuration always 8 VMM in daisy chain.                                                     
        // each VMM configuration only have VMM_ID = ith VMM.                                                  
        // pFEB configuration can send multiple VMMs at the same time                                          

        if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
	  std::cout << "daisy chain config,setting VMMmask to 0b11111111" << std::endl;
          VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
          // always hard reset VMM 0,1,2,3 on sFEB                                                             
          //if ( i == 0 || i == 1 || i == 2 || i == 3 ) VMMConfigHandle().setResetHardBit( true ) ;            
        }
        else {
	  std::cout << "point to point FEB config, setting VMMmask to " << i << std::endl;
          VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << i );
        }

        if(ok) {
          msg()(("Configuration loaded successfully for " + file_name).toStdString());
        }
        else {
          msg()("problem loading configuration from file, retrying");
          continue;
        }

        bool send_ok;
        send_ok = VMMConfigModule().SendVMMConfig(TARGETPORT, TargetIp);

        //for testing                                                                                          
        //      send_ok = true;                                                                                

        if ( send_ok ) {
	  m_sx.str("");
	  m_sx << "Board ID " << VMMConfigHandle().VMMGlobalSettings().boardID;

	  QString vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
					      8, 2, QChar('0'));
	  m_sx << " VMM mask "  << vmmMask.toStdString() << " calib config sent correctly";
	  msg()( m_sx );

	  //boost::this_thread::sleep(boost::posix_time::milliseconds(700));
          break;
        }
        else {
          if ( j != 0 ) {
            msg()("error when configuring VMM, retrying");
            continue;
          }
          else {
            m_sx.str("");
            m_sx << "Board ID " << VMMConfigHandle().VMMGlobalSettings().boardID;

            QString vmmMask = QString("%1").arg(VMMConfigHandle().VMMGlobalSettings().vmmmask,
                                                8, 2, QChar('0'));
            m_sx << " VMM mask "  << vmmMask.toStdString();

            //PopUpError("Error: VMM failed to configure after 1 tries", m_sx.str().c_str());
            all_ok = false;
          }
        }

      }
    }
    else {
      msg()(("Configuration file " + file_name + " does not exist").toStdString());
      PopUpError( ("Configuration file " + file_name + " does not exist").toStdString().c_str() );
      return;
    }

  }


}

//-------------------------------------------------------//

void MainWindow::FindBoardBaselineDetailed() {

  // vector[ichan][ivmm][itrial]
  std::vector<std::vector<std::vector<uint32_t>>> m_chan_baseline_per_vmm_multi = ScanBaselineWholeBoardMulti();

  stringstream m_sx;

  //--------------------------------------------------------------//

  if ( m_chan_baseline_per_vmm_multi.size() > 0 ) {
    std::cout << "nvmm  " << m_chan_baseline_per_vmm_multi.at(0).size() << std::endl;
    std::cout << "nchan " << m_chan_baseline_per_vmm_multi.size() << std::endl;
  }
  else {
    std::cout << "nvmm  0" << std::endl;
  }

  //-------------------------------------------------------------//

  VMMCalibModule().m_chan_baseline_trials = m_chan_baseline_per_vmm_multi;

  VMMCalibModule().Calc_Baseline_Avg();
  VMMCalibModule().Calc_Baseline_RMS();

  //-------------------------------------------------------------//

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());
  LoadBoardConfig_to_Handler();

  //-------------------------------------------------------------//
  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  //-------------------------------------------------------------//

  for ( int ivmm=nVMM-1; ivmm >= 0; ivmm-- ) {

    bool ok;

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(ivmm)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {

      ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);

      VMMConfigHandle().SetBoardID(   boardID );
      VMMConfigHandle().SetBoardType( boardType );

      std::vector< float >    baseline_float = VMMCalibModule().m_chan_baseline_mean.at(ivmm);
      std::vector< uint32_t > baseline_uint;
      baseline_uint.resize(0);

      for ( uint j=0; j<baseline_float.size(); j++ ) {
	baseline_uint.push_back( baseline_float.at(j) );
      }
      
      VMMConfigHandle().VMMCalibSettings().chan_baselines = baseline_uint;

      if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
	std::cout << "daisy chain config,setting VMMmask to 0b11111111" << std::endl;
        VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
      }
      else {
	std::cout << "point to point FEB config, setting VMMmask to " << ivmm << std::endl;
        VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << ivmm );
      }

      VMMConfigHandle().WriteVMMConfig_to_File(file_name);

    } // if file exists

  } // loop over ivmm

  m_sx.str("");
  m_sx << boardConfigFile.absoluteDir().absolutePath().toStdString() 
       << "/Board_" << boardID << "_Baseline_Histos.root";

  QString file_name_root = QString::fromStdString( m_sx.str() );

  VMMCalibModule().write_Calib_VMM_Histos( file_name_root, boardID );

}

//-------------------------------------------------------//
void MainWindow::FindBoardBaseline() {

  std::vector<std::vector<uint32_t>> m_chan_baseline_per_vmm = ScanBaselineWholeBoard();

  std::cout << "nvmm  " << m_chan_baseline_per_vmm.size() << std::endl;
  if ( m_chan_baseline_per_vmm.size() > 0 ) {
    std::cout << "nchan " << m_chan_baseline_per_vmm.at(0).size() << std::endl;
  }

  //-------------------------------------------------------------//

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());
  LoadBoardConfig_to_Handler();

  //-------------------------------------------------------------//
  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  //-------------------------------------------------------------//

  for ( int ivmm=nVMM-1; ivmm >= 0; ivmm-- ) {

    bool ok;

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(ivmm)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {

      ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);

      VMMConfigHandle().SetBoardID(   boardID );
      VMMConfigHandle().SetBoardType( boardType );

      VMMConfigHandle().VMMCalibSettings().chan_baselines = m_chan_baseline_per_vmm.at(ivmm);

      if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
	std::cout << "daisy chain config,setting VMMmask to 0b11111111" << std::endl;
        VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
      }
      else {
	std::cout << "point to point FEB config, setting VMMmask to " << ivmm << std::endl;
        VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << ivmm );
      }

      VMMConfigHandle().WriteVMMConfig_to_File(file_name);

    }

  }

}

//-------------------------------------------------------//

void MainWindow::FindBoardCalibration() {

  std::vector<std::vector<std::vector<uint32_t>>> m_chan_baseline_vs_trim_per_vmm = CalibrateWholeBoard();

  std::cout << "nvmm  " << m_chan_baseline_vs_trim_per_vmm.size() << std::endl;
  if ( m_chan_baseline_vs_trim_per_vmm.size() > 0 ) {
    std::cout << "nchan " << m_chan_baseline_vs_trim_per_vmm.at(0).size() << std::endl;
    if ( m_chan_baseline_vs_trim_per_vmm.at(0).size() > 0 ) {
      std::cout << "ntrim " << m_chan_baseline_vs_trim_per_vmm.at(0).at(0).size() << std::endl;
    }
  }

  //-------------------------------------------------------------//

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());
  LoadBoardConfig_to_Handler();

  //-------------------------------------------------------------//

  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  //-------------------------------------------------------------//

  // work for both point to point and daisy chain
  for ( int ivmm=nVMM-1; ivmm >= 0; ivmm-- ) {
    
    VMMCalibModule().m_chan_baseline_vs_trim = m_chan_baseline_vs_trim_per_vmm.at(ivmm);  
    VMMCalibModule().find_Opt_Trim_Bit();                                                 
    std::vector<uint32_t> m_opt_trim_bits = VMMCalibModule().get_opt_trim_bits();         

    bool ok;

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(ivmm)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {

      ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);

      VMMConfigHandle().SetBoardID(   boardID );
      VMMConfigHandle().SetBoardType( boardType );

      VMMConfigHandle().SetVMM_OptTrimBits( m_opt_trim_bits );      

      if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
	std::cout << "daisy chain config,setting VMMmask to 0b11111111" << std::endl;
	VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
      }
      else {
	std::cout << "point to point FEB config, setting VMMmask to " << ivmm << std::endl;
	VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << ivmm );
      }

      VMMConfigHandle().VMMCalibSettings().chan_baseline_vs_trim = m_chan_baseline_vs_trim_per_vmm.at(ivmm);  

      VMMConfigHandle().WriteVMMConfig_to_File(file_name);

    }

  }

}

//---------------------------------------------------------------------------------------------//
void MainWindow::FindBoardThDacCalibration() {

  //---------------------------------------------------------------------//
  // m_ThDAC_output_matrix is a two length vector<vector<vector<vector<uint32_t>>>>
  // m_ThDAC_output_matrix.at(0) contains the m_threshold_dac_matrix
  // m_ThDAC_output_matrix.at(1) contains the matrix of [ivmm][iThDAC][itrial] of the thDAC scan
  // threshold_dac_matrix is two vector<vector<vector<uint32_t>>>
  // m_threshold_dac_matrix.at(0) is the vector of scanned ADC value for each threshold dac value
  // m_threshold_dac_matrix.at(1) is the vector of threshold dac values
  //---------------------------------------------------------------------//
  std::vector<std::vector<std::vector<std::vector<uint32_t>>>> m_ThDAC_output_matrix = ScanThresholdDacWholeBoard();

  if ( m_ThDAC_output_matrix.size() == 0 ) {
    msg()("Error: Threshold DAC scan failed");
    return;
  }

  std::vector<std::vector<std::vector<uint32_t>>> m_threshold_dac_matrix = m_ThDAC_output_matrix.at(0);

  if ( m_threshold_dac_matrix.size() == 0 ) {
    msg()("Error: Threshold DAC scan failed");
    return;
  }

  stringstream m_sx;

  std::vector<std::vector<uint32_t>> m_threshold_dac_adc_per_vmm = m_threshold_dac_matrix.at(0);
  std::vector<std::vector<uint32_t>> m_threshold_dac_val_per_vmm = m_threshold_dac_matrix.at(1);

  std::cout << "nvmm " <<  m_threshold_dac_adc_per_vmm.size() << " scanned " << std::endl;

  //-------------------------------------------------------------//

  VMMCalibModule().m_threshold_dac_adc_per_vmm        = m_threshold_dac_adc_per_vmm;
  VMMCalibModule().m_threshold_dac_val_per_vmm        = m_threshold_dac_val_per_vmm;
  VMMCalibModule().m_threshold_dac_adc_per_vmm_trials = m_ThDAC_output_matrix.at(1);

  //-------------------------------------------------------------//

  QFileInfo boardConfigFile(ui->configBoardXmlFilenameField->text());
  LoadBoardConfig_to_Handler();

  //-------------------------------------------------------------//

  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  //-------------------------------------------------------------//

  /*
  // work for both point to point and daisy chain
  for ( int ivmm=nVMM-1; ivmm >= 0; ivmm-- ) {

    VMMCalibModule().m_chan_baseline_vs_trim = m_chan_baseline_vs_trim_per_vmm.at(ivmm);
    VMMCalibModule().find_Opt_Trim_Bit();
    std::vector<uint32_t> m_opt_trim_bits = VMMCalibModule().get_opt_trim_bits();

    //---------------------------------------------------------------------------//

    bool ok;

    QString file_name = boardConfigFile.absoluteDir().absolutePath() +
      "/VMM_"+BoardConfigHandle().BoardSettings().VMM_UniqueS_Numbers.at(ivmm)+".xml";

    QFile test(file_name);

    if ( test.exists() ) {


      ok = VMMConfigHandle().LoadVMMConfig_from_File(file_name);

      VMMConfigHandle().SetBoardID(   boardID );
      VMMConfigHandle().SetBoardType( boardType );

      VMMConfigHandle().SetVMM_OptTrimBits( m_opt_trim_bits );

      if ( VMMConfigHandle().VMMGlobalSettings().board_Type == board_type_sFEB ) {
	std::cout << "daisy chain config,setting VMMmask to 0b11111111" << std::endl;
        VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b11111111 );
      }
      else {
	std::cout << "point to point FEB config, setting VMMmask to " << ivmm << std::endl;
        VMMConfigHandle().VMMGlobalSettings().vmmmask = ( 0b1 << ivmm );
      }

      VMMConfigHandle().VMMCalibSettings().chan_baseline_vs_trim = m_chan_baseline_vs_trim_per_vmm.at(ivmm);


      VMMConfigHandle().WriteVMMConfig_to_File(file_name);

    }

  }

  */

  m_sx.str("");
  m_sx << boardConfigFile.absoluteDir().absolutePath().toStdString()
       << "/Board_" << boardID << "_ThDAC_Histos.root";

  QString file_name_root = QString::fromStdString( m_sx.str() );

  VMMCalibModule().write_ThDAC_Calib_VMM_Histos( file_name_root, boardID );

}

std::vector<std::vector<uint32_t>> MainWindow::ScanBaselineWholeBoard() {

  stringstream m_sx;

  //------------------------------------------------------//

  LoadBoardConfig_to_Handler();

  //------------------------------------------------------//
  std::vector<std::vector<uint32_t>> m_chan_baseline_per_vmm;
  m_chan_baseline_per_vmm.resize(0);

  //------------------------------------------------------//
  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  for ( int ivmm = 0; ivmm < nVMM; ivmm++ ) {

    std::vector<uint32_t> ivmm_chan_baseline;
    ivmm_chan_baseline.resize(0);

    m_chan_baseline_per_vmm.push_back( ivmm_chan_baseline );

  }

  for ( int ichan=0; ichan < 64; ichan++ ) {

    //---------------------------------------------------------//
    //           Configure all VMMs to be ready                //
    //---------------------------------------------------------//
    CalibrateBoard_ConfigVMMs( ichan, type_baseline, 0, 0 );

    //---------------------------------------------------------//
    for ( int ivmm = 0; ivmm < nVMM; ivmm++ ) {
      
      std::vector<uint32_t> i_baseline_ADC = CalibrateVMM_Channel( ivmm, boardID, boardType);

      if ( i_baseline_ADC.size() > 0 ) {
	m_chan_baseline_per_vmm.at(ivmm).push_back( find_avg(i_baseline_ADC) );
      }
      else {
	m_sx.str("");
	m_sx << " calibration for channel " << ichan << " on VMM " << ivmm << ", board "<< boardID << " failed ";
	msg()(m_sx);
	m_chan_baseline_per_vmm.at(ivmm).push_back( 0xffff );
      }
      
    }

  }

  return m_chan_baseline_per_vmm;

}

//---------------------------------------------------------------------//

std::vector<std::vector<std::vector<uint32_t>>> MainWindow::ScanBaselineWholeBoardMulti() {

  stringstream m_sx;

  //------------------------------------------------------//

  LoadBoardConfig_to_Handler();

  //------------------------------------------------------//
  std::vector<std::vector<std::vector<uint32_t>>> m_chan_baseline_multi_per_vmm;
  m_chan_baseline_multi_per_vmm.resize(0);

  //------------------------------------------------------//
  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  int nchan = 64;
  int ntrial = 5;

  for ( int ichan=0; ichan < nchan; ichan++ ) {

    //---------------------------------------------------------//
    //           Configure all VMMs to be ready                //
    //---------------------------------------------------------//
    CalibrateBoard_ConfigVMMs( ichan, type_baseline, 0, 0 );

    std::vector<std::vector<uint32_t>> ichan_baseline_multi;
    ichan_baseline_multi.resize(0);

    //---------------------------------------------------------//
    for ( int ivmm = 0; ivmm < nVMM; ivmm++ ) {

      std::vector<uint32_t> ivmm_chan_baseline_multi;
      ivmm_chan_baseline_multi.resize(0);

      for ( int itrial = 0; itrial < ntrial; itrial++ ) {
	std::vector<uint32_t> i_baseline_ADC = CalibrateVMM_Channel( ivmm, boardID, boardType);
	
	if ( i_baseline_ADC.size() > 0 ) {
	  std::cout << "scanned vmm " << ivmm << " channel " << ichan << " trial " << itrial << " nsample/trial " << i_baseline_ADC.size() << " adc " << find_avg(i_baseline_ADC) << std::endl;
	  for ( int i=0; i< i_baseline_ADC.size(); i++ ) {
	    ivmm_chan_baseline_multi.push_back( i_baseline_ADC.at(i) );
	  }
	}
	else {
	  m_sx.str("");
	  m_sx << " calibration for channel " << ichan << " on VMM " << ivmm << ", board "<< boardID << " failed ";
	  msg()(m_sx);
	}
      } // itrial loop

      ichan_baseline_multi.push_back( ivmm_chan_baseline_multi );

    } // ivmm loop

    m_chan_baseline_multi_per_vmm.push_back( ichan_baseline_multi );
  } // ichan loop

  /*
  for ( int i=0; i< m_chan_baseline_multi_per_vmm.size(); i++ ) {

    std::cout << "chan " << i << " adc " ;

    for ( int j=0; j< m_chan_baseline_multi_per_vmm.at(i).size(); j++ ) {

      std::cout << m_chan_baseline_multi_per_vmm.at(i).at(j).at(0) << " " ;

      //      for ( int k=0; k<m_chan_baseline_multi_per_vmm.at(i).at(j).size(); k++ ) {
      //	std::cout << m_chan_baseline_multi_per_vmm.at(i).at(j).at(k) << " " ;
      //      }
    }

    std::cout << std::endl;

  }
  */

  return m_chan_baseline_multi_per_vmm;

}

//----------------------------------------------------------------------------------------------------//
std::vector<std::vector<std::vector<std::vector<uint32_t>>>> MainWindow::ScanThresholdDacWholeBoard() {

  stringstream m_sx;

  //------------------------------------------------------//

  LoadBoardConfig_to_Handler();

  //------------------------------------------------------//
  std::vector<std::vector<uint32_t>> m_threshold_dac_adc_per_vmm;
  m_threshold_dac_adc_per_vmm.resize(0);

  std::vector<std::vector<uint32_t>> m_threshold_dac_val_per_vmm;
  m_threshold_dac_val_per_vmm.resize(0);

  std::vector<std::vector<std::vector<uint32_t>>> m_threshold_dac_adc_per_vmm_trials;

  std::vector<std::vector<std::vector<uint32_t>>> m_threshold_dac_matrix;
  m_threshold_dac_matrix.resize(0);

  std::vector<std::vector<std::vector<std::vector<uint32_t>>>> m_output_matrix;
  m_output_matrix.resize(0);

  //------------------------------------------------------//

  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  int nstep = 20;
  int ntrial = 5;

  //-----------------------------------------------------//

  for ( int ivmm = 0; ivmm<nVMM; ivmm++ ) {
    
    std::vector<uint32_t> ivmm_threshold_dac_adc;
    ivmm_threshold_dac_adc.resize(0);
    
    std::vector<std::vector<uint32_t>> ivmm_threshold_dac_adc_trials;
    ivmm_threshold_dac_adc_trials.resize(0);

    std::vector<uint32_t> ivmm_threshold_dac_val;
    ivmm_threshold_dac_val.resize(0);

    m_threshold_dac_adc_per_vmm.push_back( ivmm_threshold_dac_adc );
    m_threshold_dac_val_per_vmm.push_back( ivmm_threshold_dac_val );

    m_threshold_dac_adc_per_vmm_trials.push_back( ivmm_threshold_dac_adc_trials );

  }

  for ( int istep=0; istep < nstep; istep++ ) {

    //---------------------------------------------------------//
    //           Configure all VMMs to be for ithreshold       //
    //---------------------------------------------------------//

    int stepSize = 400 / nstep;
    int ithreshold = 100 + istep * stepSize;

    CalibrateBoard_ConfigVMMs( 0, type_ThDAC, 0, ithreshold );

    //---------------------------------------------------------//
    for ( int ivmm = 0; ivmm < nVMM; ivmm++ ) {

      std::vector<uint32_t> i_VMM_ADC;
      i_VMM_ADC.resize(0);

      for ( int itrial = 0; itrial < ntrial; itrial++ ) {
	std::vector<uint32_t> i_ADC = CalibrateVMM_Channel( ivmm, boardID, boardType);
	
	std::cout << "scanned vmm " << ivmm << " istep " << istep << " itrial " << itrial << " threshold DAC " << ithreshold << " adc " << find_avg(i_ADC) << std::endl;
	
	for ( int i=0; i<i_ADC.size(); i++ ) {
	  i_VMM_ADC.push_back( i_ADC.at(i) );
	}
      }

      m_threshold_dac_adc_per_vmm.at(ivmm).push_back( find_avg(i_VMM_ADC) );
      m_threshold_dac_adc_per_vmm_trials.at(ivmm).push_back( i_VMM_ADC );
      m_threshold_dac_val_per_vmm.at(ivmm).push_back( ithreshold );

    } // ivmm loop

  } // ichan loop

  m_threshold_dac_matrix.push_back( m_threshold_dac_adc_per_vmm );
  m_threshold_dac_matrix.push_back( m_threshold_dac_val_per_vmm );

  m_output_matrix.push_back( m_threshold_dac_matrix );
  m_output_matrix.push_back( m_threshold_dac_adc_per_vmm_trials );

  return m_output_matrix;

}

//-------------------------------------------------------------------------------------//

std::vector<std::vector<std::vector<uint32_t>>> MainWindow::CalibrateWholeBoard() {

  stringstream m_sx;

  //------------------------------------------------------//

  LoadBoardConfig_to_Handler();

  //------------------------------------------------------//

  std::vector<std::vector<std::vector<uint32_t>>> m_chan_baseline_vs_trim_per_vmm;
  m_chan_baseline_vs_trim_per_vmm.resize(0);

  //------------------------------------------------------//
  
  int boardID   = BoardConfigHandle().BoardSettings().boardID;
  int boardType = BoardConfigHandle().BoardSettings().boardType;

  int nVMM = BoardConfigHandle().BoardSettings().nVMM;

  int ntrim = 32;
  int nchan = 64;

  for ( int ivmm = 0; ivmm < nVMM; ivmm++ ) {
    
    std::vector<std::vector<uint32_t>> ivmm_chan_baseline_vs_trim;
    ivmm_chan_baseline_vs_trim.resize(0);

    for ( int ichan = 0; ichan < nchan; ichan++ ) {
      
      std::vector<uint32_t> ichan_baseline_vs_trim;
      ichan_baseline_vs_trim.resize(0);

      ivmm_chan_baseline_vs_trim.push_back( ichan_baseline_vs_trim );

    }

    m_chan_baseline_vs_trim_per_vmm.push_back( ivmm_chan_baseline_vs_trim );

  }

  for ( int ichan=0; ichan < nchan; ichan++ ) {

    for ( int itrim=0; itrim < ntrim; itrim++ ) {

      //---------------------------------------------------------//
      //           Configure all VMMs to be ready                //
      //---------------------------------------------------------//
      
      CalibrateBoard_ConfigVMMs( ichan, type_trimming, itrim, 0 );
      
      //---------------------------------------------------------//

      for ( int ivmm = 0; ivmm < nVMM; ivmm++ ) {
	
	std::vector<uint32_t> i_trim_ADC = CalibrateVMM_Channel( ivmm, boardID, boardType);
	
	if ( i_trim_ADC.size() > 0 ) {
	  std::cout << "scanned vmm "   << ivmm 
		    << " channel "      << ichan 
		    << " trimming bit " << itrim 
		    << " adc "          << find_avg(i_trim_ADC) << std::endl;
	  
	  m_chan_baseline_vs_trim_per_vmm.at(ivmm).at(ichan).push_back( find_avg(i_trim_ADC) );
	}
	else {
	  m_sx.str("");
	  m_sx << " calibration for channel " << ichan << " on VMM " << ivmm << ", board "<< boardID << " failed ";
	  msg()(m_sx);
	  m_chan_baseline_vs_trim_per_vmm.at(ivmm).at(ichan).push_back( 0xffff );
	  //PopUpError(m_sx.str().c_str());
	  //m_chan_baseline_vs_trim_per_vmm.resize(0);
	  //return m_chan_baseline_vs_trim_per_vmm;
	}
	
      }

    }

  }

  return m_chan_baseline_vs_trim_per_vmm;

}
